import React, { Component } from "react";
import PropTypes from "prop-types";

class Form extends Component {
  static propTypes = {
    children: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.fields = [];
    this.arrayFields = [];
    this.state = {
      errors: {},
      isSubmitting: false,
      value: {}
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value && state.value && props.value.id !== state.value.id) {
      return { value: { ...props.value } };
    }

    return { value: { ...props.value, ...state.value } };
  }

  clean() {
    this.setState({ value: {} });
  }

  submit() {
    if (this.validateFields()) {
      this.action();
    }
  }

  onSubmit = event => {
    event.preventDefault();
    // this.focusNextField();
    this.validateFields();
  };

  validateFields() {
    const { validate } = this.props;
    if (!validate) {
      return this.action();
    }
    const field = this.fields.find(
      field =>
        !validate(field.props.name, this.getValue(field.props.name), this)
    );
    field ? this.focusField(field) : this.action();
  }

  focusField(fieldName) {
    const index = this.fields.indexOf(fieldName);
    const field = this.fields[index];
    field ? field.focus() : this.action();
  }

  focusNextField() {
    const activeIndex = this.fields.indexOf(document.activeElement);
    const field = this.fields[activeIndex + 1];
    field ? field.focus() : this.action();
  }

  action() {
    const { action } = this.props;

    action && action(this.state.value, this);
  }

  getValues = () => this.state.value;

  getValue = (name, defaultValue = "") => {
    return this.state.value[name] || defaultValue;
  };

  getBooleanValue = name => {
    return Boolean(this.getValue(name));
  };

  getArrayValue = (name, item) => {
    const value = this.getValue(name);
    return Boolean(value) && value.includes(item);
  };

  getDateTimeValue = name => {
    return this.state.value[name] || null;
  };

  field = element => {
    this.fields.push(element);
  };

  arrayField = element => {
    this.arrayFields.push(element);
  };

  error = name => {
    return this.state.errors[name];
  };

  setErrors(errors) {
    this.setState({ errors });
  }

  submitting() {
    this.setState({ isSubmitting: true });
  }

  submitted(errors) {
    this.setState({ isSubmitting: false });

    if (errors) {
      return this.setState({ errors });
    }

    const { onSubmitted } = this.props;
    if (onSubmitted) {
      onSubmitted();
    }
  }

  handleInputChange = event => {
    const field = event.target;
    const value = field.type === "checkbox" ? field.checked : field.value;
    this.handleChange(field.name, value);
  };

  handleArrayChange = event => {
    const field = event.target;
    let value = this.state.value[field.name] || [];
    if (field.checked) {
      if (!value.includes(field.value)) {
        value = [...value, field.value];
      }
    } else {
      value = value.filter(item => item !== field.value);
    }
    this.handleChange(field.name, value);
  };

  handleChange = (field, value) => {
    this.setState({ value: { ...this.state.value, [field]: value } });
  };

  render() {
    const { children } = this.props;
    const { isSubmitting } = this.state;

    return (
      <form onSubmit={this.onSubmit}>
        {children({
          getValue: this.getValue,
          getBooleanValue: this.getBooleanValue,
          getArrayValue: this.getArrayValue,
          getDateTimeValue: this.getDateTimeValue,
          handleChange: this.handleChange,
          handleInputChange: this.handleInputChange,
          handleArrayChange: this.handleArrayChange,
          field: this.field,
          arrayField: this.arrayField,
          error: this.error,
          isSubmitting
        })}
      </form>
    );
  }
}

export default Form;
