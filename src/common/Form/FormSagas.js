import { all, call, takeEvery } from "redux-saga/effects";

import { startFetching, stopFetching } from "../../services/Api/ApiActions";

function* startFetchingSaga(action) {
  yield call(startFetchingForm, action);
}

function* stopFetchingSaga(action) {
  yield call(stopFetchingForm, action);
}

function startFetchingForm(action) {
  const { form } = action.meta || {};
  form && form.submitting();
}

function stopFetchingForm(action) {
  const { form } = action.meta || {};
  form && form.submitted(action.error && action.payload.message);
}

export default function* formSagas() {
  yield all([
    takeEvery(startFetching, startFetchingSaga),
    takeEvery(stopFetching, stopFetchingSaga)
  ]);
}
