import React from "react";
import { connect } from "react-redux";

import {
  fetchSuggestions,
  cleanSuggestions
} from "../Suggestions/SuggestionsActions";
import { getSuggestions } from "../Suggestions/SuggestionsSelectors";
import DialogField from "./DialogField";

const DialogFieldContainer = props => <DialogField {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { name } = ownProps;
  return {
    suggestions: getSuggestions(state, name)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name } = ownProps;
  return {
    fetchSuggestions: query => dispatch(fetchSuggestions(name, query)),
    cleanSuggestions: () => dispatch(cleanSuggestions(name))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DialogFieldContainer);
