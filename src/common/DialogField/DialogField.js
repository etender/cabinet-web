import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import SimpleBar from "simplebar-react";
import {
  FormLabel,
  Typography,
  TextField,
  DialogContent,
  Button,
  withStyles
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../utils/CoreUtils";
import DialogFieldValue from "./DialogFieldValue";
import Dialog from "../Dialog/Dialog";
import Suggestions from "../Suggestions/Suggestions";
import HelpButton from "../HelpButton/HelpButton";

const messages = defineMessages({
  search: {
    id: "dialog-field.search",
    defaultMessage: "Поиск"
  }
});

const styles = theme => ({
  root: {
    // marginTop: theme.spacing(2)
  },
  openButton: {
    marginLeft: theme.spacing(2)
  },
  dialogPaper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(90)
    }
  },
  dialogContent: {
    paddingTop: 0,
    [theme.breakpoints.down("sm")]: {
      padding: `0 ${theme.spacing()}px`
    }
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

class DialogField extends Component {
  constructor(props) {
    super(props);

    const value = this.deserialize(this.props.value);
    this.selected = this.deserializeSelected(value);
    this.state = {
      open: false,
      value: value,
      search: null
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      const newValue = this.deserialize(this.props.value);
      this.setState({ value: newValue });
      this.selected = this.deserializeSelected(newValue);
    }
  }

  serialize(value) {
    const { serialize } = this.props;
    return serialize ? serialize(value) : value;
  }

  deserialize(value) {
    const { deserialize } = this.props;
    return deserialize(value);
  }

  deserializeSelected(value) {
    const { deserializeSelected } = this.props;
    return deserializeSelected ? deserializeSelected(value) : value;
  }

  handleOpen = event => {
    event.preventDefault();

    this.setState({ open: true });

    if (this.cleared) {
      const { value } = this.props;
      this.selected = this.deserializeSelected(value);
      this.cleared = false;
    }

    const { onOpen } = this.props;
    if (onOpen) {
      onOpen();
    }
  };

  handleClose = event => {
    this.setState({ open: false, search: null });
    const { cleanSuggestions } = this.props;
    cleanSuggestions && cleanSuggestions();
  };

  handleClear = () => {
    this.selected = this.deserializeSelected();
    this.cleared = true;
    this.forceUpdate();
  };

  handleApply = () => {
    this.handleClose();

    const { onChange } = this.props;
    onChange(this.serialize(this.selected));
  };

  handleToggleSelected = (item, selected) => {
    this.selected[item.id] = selected ? item : null;
  };

  handleSearch = event => {
    const { fetchSuggestions, cleanSuggestions } = this.props;
    const search = event.target.value;

    if (this.timeout) {
      window.clearTimeout(this.timeout);
    }

    this.timeout = window.setTimeout(() => {
      this.setState({ search });
      isEmpty(search)
        ? cleanSuggestions && cleanSuggestions()
        : fetchSuggestions && fetchSuggestions(search);
    }, 500);
  };

  handleDelete = item => {
    this.selected[item.id] = null;

    const { onChange } = this.props;
    onChange(this.serialize(this.selected));
  };

  render() {
    const {
      classes,
      children,
      intl,
      title,
      suggestions,
      helpButtonBody,
      buttonVariant
    } = this.props;
    let { suggestionsComponent } = this.props;
    const { formatMessage } = intl;
    const { open, value, search } = this.state;
    const selected = this.selected;
    const buttonVariantContained = buttonVariant === "contained";

    return (
      <div className={classes.root}>
        <FormLabel>{title}</FormLabel>

        <Button
          variant={buttonVariantContained ? "contained" : "text"}
          color={buttonVariantContained ? "primary" : "default"}
          component="button"
          onClick={this.handleOpen}
          className={classes.openButton}
        >
          <Typography color={buttonVariantContained ? "initial" : "primary"}>
            <FormattedMessage
              id="dialog-field.select"
              defaultMessage="Выбрать"
            />
          </Typography>
        </Button>
        {helpButtonBody && <HelpButton body={helpButtonBody} />}

        {value && (
          <div>
            {value.map(item => (
              <DialogFieldValue
                key={item.id}
                item={item}
                onDelete={this.handleDelete}
              />
            ))}
          </div>
        )}

        <Dialog
          open={open}
          toggleDialog={this.handleClose}
          classes={{ paper: classes.dialogPaper }}
          title={
            <Fragment>
              {title}
              <TextField
                fullWidth
                placeholder={formatMessage(messages.search)}
                onChange={this.handleSearch}
              />
            </Fragment>
          }
          formDisabled
          actionsDisabled
          buttons={
            <Fragment>
              <Button onClick={this.handleApply} color="primary">
                <FormattedMessage
                  id="dialog-field.apply"
                  defaultMessage="Применить"
                />
              </Button>
              <Button onClick={this.handleClear} color="primary">
                <FormattedMessage
                  id="dialog-field.clear"
                  defaultMessage="Очистить"
                />
              </Button>
            </Fragment>
          }
        >
          <DialogContent className={classes.dialogContent}>
            <SimpleBar style={{ height: "100%" }}>
              {open &&
                !search &&
                children({
                  selected,
                  handleToggleSelected: this.handleToggleSelected
                })}
              {open &&
                search &&
                ((suggestionsComponent &&
                  suggestionsComponent({
                    search,
                    selected,
                    handleToggleSelected: this.handleToggleSelected
                  })) || (
                  <Suggestions
                    items={suggestions}
                    onToggleSelected={this.handleToggleSelected}
                    selected={selected}
                  />
                ))}
            </SimpleBar>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

DialogField.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};

export default injectIntl(withStyles(styles)(DialogField));
