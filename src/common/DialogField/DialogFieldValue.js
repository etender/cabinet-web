import React, { Component } from "react";
import PropTypes from "prop-types";
import { Chip, withStyles } from "@material-ui/core";

const styles = theme => ({
  chip: {
    margin: theme.spacing(0.25)
  }
});

class DialogFieldValue extends Component {
  handleDelete = () => {
    const { item, onDelete } = this.props;
    onDelete(item);
  };

  render() {
    const { item, classes } = this.props;

    return (
      <Chip
        label={item.name}
        onDelete={this.handleDelete}
        className={classes.chip}
      />
    );
  }
}

DialogFieldValue.propTypes = {
  item: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default withStyles(styles)(DialogFieldValue);
