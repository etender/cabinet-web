import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import DialogFieldContainer from "../DialogField/DialogFieldContainer";
import TreeItemContainer from "../TreeItem/TreeItemContainer";

const styles = theme => ({
  root: {}
});

class TreeViewDialogField extends Component {
  static propTypes = {
    value: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  };

  serialize = value => {
    return Object.values(value).filter(item => item);
  };

  deserialize(value) {
    return value;
  }

  deserializeSelected(value) {
    return value
      ? value.reduce((result, item) => ({ ...result, [item.id]: item }), {})
      : {};
  }

  handleOpen = () => {
    const { fetch, node } = this.props;
    if (node) {
      return;
    }

    fetch({ id: "root" });
  };

  render() {
    const { name, title, value, onChange, helpButtonBody } = this.props;

    return (
      <DialogFieldContainer
        name={name}
        title={title}
        serialize={this.serialize}
        deserialize={this.deserialize}
        deserializeSelected={this.deserializeSelected}
        onChange={onChange}
        onOpen={this.handleOpen}
        value={value}
        helpButtonBody={helpButtonBody}
      >
        {({ selected, handleToggleSelected }) => (
          <TreeItemContainer
            name={name}
            nodeId="root"
            onToggleSelected={handleToggleSelected}
            selected={selected}
          />
        )}
      </DialogFieldContainer>
    );
  }
}

export default withStyles(styles)(TreeViewDialogField);
