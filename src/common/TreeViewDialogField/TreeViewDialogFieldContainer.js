import React from "react";
import { connect } from "react-redux";

import { fetchTreeEntities } from "../../services/Entities/EntitiesActions";
import { getTreeEntities } from "../../services/Entities/EntitiesSelectors";
import TreeViewDialogField from "./TreeViewDialogField";

const TreeViewDialogFieldContainer = props => (
  <TreeViewDialogField {...props} />
);

const mapStateToProps = (state, ownProps) => {
  const { name } = ownProps;
  return {
    node: getTreeEntities(state, name, "root")
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name } = ownProps;
  return {
    fetch: query => dispatch(fetchTreeEntities(name, query))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TreeViewDialogFieldContainer);
