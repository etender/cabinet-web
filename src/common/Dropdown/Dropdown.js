import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Link,
  MenuList,
  MenuItem,
  Popover,
  withStyles
} from "@material-ui/core";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

const styles = theme => ({
  popover: {},
  menuItem: {
    paddingTop: theme.spacing(),
    paddingBottom: theme.spacing()
  },
  link: {
    lineHeight: "31px",
    marginRight: "24px",
    color: theme.palette.grey["600"],
    display: "inline-flex",
    alignItems: "center",
    fontSize: "1.2em"
  }
});

export class Dropdown extends Component {
  static propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    onClose: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      menuNames: this.createMenuNames()
    };
  }

  createMenuNames() {
    const { menu } = this.props;

    return (menu || []).reduce((items, item) => {
      items[item.id] = item.name;
      return items;
    }, {});
  }
  componentDidUpdate(prevProps) {
    const { menu } = this.props;

    if (menu !== prevProps.menu) {
      this.setState({ menuNames: this.createMenuNames() });
    }
  }

  handleButtonClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handlePopoverClick = event => {
    if (event.target !== event.currentTarget) {
      this.handlePopoverClose();
    }
  };

  handlePopoverClose = event => {
    this.setState({ anchorEl: null });

    const { onClose } = this.props;
    onClose && onClose();
  };

  handleMenuClick = item => {
    this.handlePopoverClose();

    const { onChange } = this.props;
    onChange && onChange(item);
  };

  render() {
    const { classes, title, children, value, menu } = this.props;
    const { anchorEl, menuNames } = this.state;

    const open = Boolean(anchorEl);

    return (
      <Fragment>
        <Link
          component="button"
          onClick={this.handleButtonClick}
          className={classes.link}
        >
          {value === "none" || !menu ? title : menuNames[value]}
          <ArrowDropDownIcon className={classes.linkIcon} />
        </Link>
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={this.handlePopoverClose}
          className={classes.popover}
          classes={{
            paper: classes.popover
          }}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
        >
          {menu && (
            <MenuList>
              {menu.map(item => (
                <MenuItem
                  key={item.id}
                  value={item.id}
                  selected={item.id === value}
                  className={classes.menuItem}
                  onClick={() => this.handleMenuClick(item)}
                >
                  {item.name}
                </MenuItem>
              ))}
            </MenuList>
          )}
          {!menu && typeof children === "function"
            ? children({ popoverClose: this.handlePopoverClose })
            : children}
        </Popover>
      </Fragment>
    );
  }
}

export default withStyles(styles)(Dropdown);
