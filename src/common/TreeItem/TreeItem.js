import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Collapse,
  ListItem,
  ListItemIcon,
  ListItemText,
  withStyles
} from "@material-ui/core";
import {
  ExpandMore as ExpandMoreIcon,
  ExpandLess as ExpandLessIcon
} from "@material-ui/icons";
import grey from "@material-ui/core/colors/grey";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import TreeItemContainer from "./TreeItemContainer";

const styles = theme => ({
  container: {
    paddingLeft: theme.spacing(4)
  },
  icon: {
    color: "inherit",
    minWidth: theme.spacing(4)
  },
  selected: {
    backgroundColor: grey["300"],

    "&:hover": {
      backgroundColor: grey["400"]
    }
  },
  childSelected: {
    "& span": {
      fontWeight: "600"
    }
  },
  listItem: {
    marginBottom: theme.spacing(0.5),
    borderRadius: theme.spacing(2)
  },
  insert: {
    paddingLeft: theme.spacing(0)
  }
});

class TreeItem extends Component {
  static propTypes = {
    fetch: PropTypes.func.isRequired,
    node: PropTypes.object,
    nodeId: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    const { selected, nodeId } = this.props;
    this.state = {
      expanded: false,
      selected: selected[nodeId],
      childSelected: Object.keys(selected).some(
        key => key.startsWith(nodeId) && key !== nodeId
      )
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { selected, nodeId } = this.props;

    if (
      prevProps.selected !== selected &&
      this.state.selected !== undefined &&
      this.state.selected !== selected[nodeId]
    ) {
      this.setState({ selected: selected[nodeId] });
      this.changeChildSelected();
    }

    const { childSelected } = this.state;
    if (childSelected && prevProps.selected !== selected) {
      this.changeChildSelected();
    }
  }

  handleToggleSelected = () => {
    let { selected } = this.state;
    selected = !selected;

    this.changeSelected(selected);

    if (selected) {
      this.clearChildrenSelected();
      this.setState({ childSelected: false });
    }
  };

  clearChildrenSelected() {
    const { selected, node } = this.props;

    Object.keys(selected)
      .filter(key => key.startsWith(node.id) && key !== node.id)
      .forEach(key => this.toggleSelected(selected[key], false));
  }

  handleChildToggleSelected = (node, selected) => {
    const { onToggleSelected } = this.props;
    if (onToggleSelected) {
      onToggleSelected(node, selected);
    }

    this.changeChildSelected();

    if (selected) {
      this.changeSelected(false);
    }
  };

  changeChildSelected() {
    const { selected, nodeId } = this.props;
    this.setState({
      childSelected: Object.keys(selected).some(
        key => key.startsWith(nodeId) && key !== nodeId
      )
    });
  }

  changeSelected(selected) {
    this.setState({ selected });

    const { node } = this.props;
    this.toggleSelected(node, selected);
  }

  toggleSelected(node, selected) {
    if (!node) {
      return;
    }

    const { onToggleSelected } = this.props;
    if (onToggleSelected) {
      onToggleSelected(node, selected);
    }
  }

  handleToggleExpanded = event => {
    event.stopPropagation();

    this.setState(state => ({ expanded: !state.expanded }));

    const { fetch, node } = this.props;
    if (node.loaded) {
      return;
    }

    fetch({ parentId: node.id });
  };

  renderChildren = childId => {
    const { name, selected } = this.props;
    return (
      <TreeItemContainer
        key={childId}
        name={name}
        nodeId={childId}
        onToggleSelected={this.handleChildToggleSelected}
        selected={(this.state.selected && {}) || selected}
      />
    );
  };

  render() {
    const { node, classes } = this.props;
    const { expanded, selected, childSelected } = this.state;

    if (!node || !node.id) {
      return "";
    }

    if (node.id === "root" && node.childIds) {
      return node.childIds.map(this.renderChildren);
    }

    return (
      <div>
        <ListItem
          button
          onClick={this.handleToggleSelected}
          className={classNames(
            selected && classes.selected,
            childSelected && classes.childSelected,
            classes.listItem
          )}
        >
          {node.hasChild && (
            <ListItemIcon className={classes.icon}>
              {expanded ? (
                <ExpandLessIcon onClick={this.handleToggleExpanded} />
              ) : (
                <ExpandMoreIcon onClick={this.handleToggleExpanded} />
              )}
            </ListItemIcon>
          )}
          <ListItemText
            inset={!node.hasChild}
            primary={node.name}
            classes={{ inset: classes.insert }}
          />
        </ListItem>

        {node.hasChild && (
          <Collapse
            className={classes.container}
            unmountOnExit
            in={expanded}
            component="div"
          >
            {node.loading && (
              <div>
                <FormattedMessage
                  id="tree-item.loading"
                  defaultMessage="Загрузка..."
                />
              </div>
            )}
            {node.loaded && node.childIds.map(this.renderChildren)}
          </Collapse>
        )}
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(TreeItem));
