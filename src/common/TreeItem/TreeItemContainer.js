import React from "react";
import { connect } from "react-redux";

import { fetchTreeEntities } from "../../services/Entities/EntitiesActions";
import { getTreeEntities } from "../../services/Entities/EntitiesSelectors";
import TreeItem from "./TreeItem";

const TreeItemContainer = props => <TreeItem {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { name, nodeId } = ownProps;
  return {
    node: getTreeEntities(state, name, nodeId)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name } = ownProps;
  return {
    fetch: query => dispatch(fetchTreeEntities(name, query))
  };
};

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true
})(TreeItemContainer);
