import React, { Component } from "react";
import PropTypes from "prop-types";
import { Typography, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dialog from "../Dialog/Dialog";
import logo from "../../images/logo-color.svg";

const messages = defineMessages({
  title404: {
    id: "devise.error.title404",
    defaultMessage: "Неопознанный клиент."
  },
  description404: {
    id: "devise.error.description404",
    defaultMessage: "Не удалось определить пользователя системы."
  },
  title402: {
    id: "devise.error.title402",
    defaultMessage: "Срок действия вашей подписки истек."
  },
  description402: {
    id: "devise.error.description402",
    defaultMessage:
      "Для продления отправьте заявку менеджеру и он с вами свяжется."
  },
  title403: {
    id: "devise.error.title403",
    defaultMessage: "Доступ к услуге невозможен."
  },
  description403: {
    id: "devise.error.description403",
    defaultMessage: "Доступ к системе невозможен."
  },
  title406: {
    id: "devise.error.title406",
    defaultMessage: "Пустой результат."
  },
  description406: {
    id: "devise.error.description406",
    defaultMessage: "Ошибка входа в систему."
  },
  title500: {
    id: "devise.error.title500",
    defaultMessage: "Пустой результат."
  },
  description500: {
    id: "devise.error.description500",
    defaultMessage: "Ошибка сервера."
  },
  support: {
    id: "devise.error.support",
    defaultMessage:
      "Центр поддержки клиентов: 8-800-333-8888, <a href='mailto:ee@garant.ru'>ee@garant.ru</a>."
  }
});

const styles = theme => ({
  logo: {
    height: theme.spacing(6)
    // marginBottom: theme.spacing(5)
  },
  content: {
    padding: theme.spacing(4)
  }
});

class Error extends Component {
  static propTypes = {
    error: PropTypes.object.isRequired
  };

  render() {
    const { classes, error, children } = this.props;
    const { formatMessage } = this.props.intl;

    if (error.status === 401) {
      return "";
    }

    return (
      <Dialog
        open
        hideClose
        hideBackdrop
        title={<img src={logo} alt="logo" className={classes.logo} />}
        formDisabled
        contentDisabled
      >
        <div className={classes.content}>
          {/* <img src={logo} alt="logo" className={classes.logo} /> */}
          <Typography variant="h4">
            {formatMessage(messages[`title${error.status}`])}
          </Typography>
          <Typography paragraph>
            {formatMessage(messages[`description${error.status}`])}
          </Typography>
          {children}
          <Typography
            paragraph
            dangerouslySetInnerHTML={{
              __html: formatMessage(messages["support"])
            }}
          ></Typography>
        </div>
      </Dialog>
    );
  }
}

export default injectIntl(withStyles(styles)(Error));
