import { Component } from "react";

class SplashScreen extends Component {
  componentWillUnmount() {
    const splashscreen = document.getElementById("splashscreen");
    if (splashscreen.style["display"] !== "none") {
      splashscreen.style["display"] = "none";
    }
  }

  render() {
    return "";
  }
}

export default SplashScreen;
