import React, { Component } from "react";
import { Snackbar as MuiSnackbar } from "@material-ui/core";
import { injectIntl } from "react-intl";

class Snackbar extends Component {
  render() {
    const { open, message, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <MuiSnackbar
        open={open}
        autoHideDuration={5000}
        message={typeof message === "string" ? message : formatMessage(message)}
      ></MuiSnackbar>
    );
  }
}

export default injectIntl(Snackbar);
