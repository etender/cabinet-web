import { put, takeLatest, delay } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";

import { SNACKBAR_SHOW } from "./SnackbarConstants";
import { hideSnackbar } from "./SnackbarActions";

function* showSaga() {
  yield delay(3000);
  yield put(hideSnackbar());
}

function* snackbarSagas() {
  yield takeLatest(SNACKBAR_SHOW, showSaga);
}

sagaMiddleware.run(snackbarSagas);
