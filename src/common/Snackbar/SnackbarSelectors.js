export const getSnackbarOpen = state => state.snackbar.open;
export const getSnackbarMessage = state => state.snackbar.message;
