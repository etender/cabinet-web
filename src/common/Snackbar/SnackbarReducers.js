import { handleActions } from "redux-actions";

import { SNACKBAR_SHOW, SNACKBAR_HIDE } from "./SnackbarConstants";

const initialState = {
  open: false,
  message: ""
};

export default handleActions(
  {
    [SNACKBAR_SHOW]: (state, action) => ({
      ...state,
      open: true,
      message: action.payload
    }),

    [SNACKBAR_HIDE]: state => ({
      ...state,
      open: false,
      message: ""
    })
  },
  initialState
);
