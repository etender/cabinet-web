import { createAction } from "redux-actions";
import { SNACKBAR_HIDE, SNACKBAR_SHOW } from "./SnackbarConstants";

export const showSnackbar = createAction(SNACKBAR_SHOW);
export const hideSnackbar = createAction(SNACKBAR_HIDE);
