import React from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";

import "./SnackbarSagas";
import { getSnackbarOpen, getSnackbarMessage } from "./SnackbarSelectors";
import Snackbar from "./Snackbar";

const SnackbarContainer = props => <Snackbar {...props} />;

const mapStateToProps = state => {
  return {
    open: getSnackbarOpen(state),
    message: getSnackbarMessage(state)
  };
};

export default connect(mapStateToProps)(injectIntl(SnackbarContainer));
