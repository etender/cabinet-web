import { toggleDialog } from "../Dialog/DialogActions";
import { CONFIRMATION_DIALOG } from "./ConfirmationDialogConstants";

export const confirm = (title, message, action) =>
  toggleDialog(CONFIRMATION_DIALOG, { title, message, action });
