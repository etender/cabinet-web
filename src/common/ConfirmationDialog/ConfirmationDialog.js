import React, { Component } from "react";
import PropTypes from "prop-types";
import { Typography, withStyles, Button } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import DialogContainer from "../Dialog/DialogContainer";

const styles = theme => ({
  paper: {
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(2)
    }
  }
});

export class ConfirmationDialog extends Component {
  static propTypes = {
    name: PropTypes.string,
    title: PropTypes.string,
    message: PropTypes.string,
    action: PropTypes.func
  };

  handleAction = () => {
    const { action, toggleDialog } = this.props;
    if (action) {
      action();
    }

    toggleDialog();
  };

  render() {
    const { classes, name, title, message, action } = this.props;

    return (
      <DialogContainer
        name={name}
        classes={{ paper: classes.paper }}
        formDisabled
        actionsDisabled={!action}
        title={title}
        buttons={
          <Button color="primary" autoFocus onClick={this.handleAction}>
            <FormattedMessage id="confirm-dialog.ok" defaultMessage="ОК" />
          </Button>
        }
      >
        <Typography>{message}</Typography>
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(ConfirmationDialog));
