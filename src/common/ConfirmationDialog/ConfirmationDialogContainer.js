import React from "react";
import { connect } from "react-redux";

import { toggleDialog } from "../Dialog/DialogActions";
import { getOptions } from "../Dialog/DialogSelectors";
import { CONFIRMATION_DIALOG } from "./ConfirmationDialogConstants";
import ConfirmationDialog from "./ConfirmationDialog";

const ConfirmationDialogContainer = props => <ConfirmationDialog {...props} />;

const mapStateToProps = state => {
  return {
    ...getOptions(state, CONFIRMATION_DIALOG),
    name: CONFIRMATION_DIALOG
  };
};

export default connect(mapStateToProps, {
  toggleDialog: () => toggleDialog(CONFIRMATION_DIALOG)
})(ConfirmationDialogContainer);
