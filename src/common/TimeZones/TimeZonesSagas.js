import { takeLatest } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";

import { fetchEntitiesSaga } from "../../services/Entities/EntitiesSagas";
import { TIME_ZONES_FETCH } from "./TimeZonesConstants";

function* timeZonesSagas() {
  yield takeLatest(TIME_ZONES_FETCH, fetchEntitiesSaga);
}

sagaMiddleware.run(timeZonesSagas);
