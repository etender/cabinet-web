import { createAction } from "redux-actions";

import { TIME_ZONES_FETCH } from "./TimeZonesConstants";

export const fetchTimeZones = createAction(
  TIME_ZONES_FETCH,
  () => {},
  () => ({ entity: "time_zones" })
);
