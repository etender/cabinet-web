export const getTimeZones = state => state.timeZones;
export const shouldFetchTimeZones = state => state.timeZones.length === 0;
