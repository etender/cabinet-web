import { handleActions } from "redux-actions";

import { successApiType } from "../../services/Api/ApiHelpers";
import { TIME_ZONES_FETCH } from "./TimeZonesConstants";

const initialState = [];

export default handleActions(
  {
    [successApiType(TIME_ZONES_FETCH)]: (state, action) => action.payload.items
  },
  initialState
);
