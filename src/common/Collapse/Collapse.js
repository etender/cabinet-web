import React, { Component, Fragment } from "react";
import { Collapse as MuiCollapse, Button } from "@material-ui/core";
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon
} from "@material-ui/icons";

import { FormattedMessage } from "react-intl.macro";

class Collapse extends Component {
  constructor(props) {
    super(props);

    this.ref = React.createRef();

    this.state = {
      in: false,
      showMore: false
    };
  }

  componentDidMount() {
    const { collapsedHeight } = this.props;

    if (this.ref.current) {
      this.setState({
        showMore: this.ref.current.children[0].clientHeight > collapsedHeight
      });
    }
  }

  toggle = () => {
    const { in: inProp } = this.state;
    this.setState({ in: !inProp });
  };

  render() {
    const { children, collapsedHeight, ...other } = this.props;
    const { in: inProp, showMore } = this.state;
    const height =
      showMore || !this.ref.current
        ? collapsedHeight
        : this.ref.current.children[0].clientHeight;

    return (
      <Fragment>
        <MuiCollapse
          innerRef={this.ref}
          in={inProp}
          collapsedHeight={`${height}px`}
          {...other}
        >
          {children}
        </MuiCollapse>

        {showMore && (
          <Button onClick={this.toggle} size="small">
            {inProp ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            {inProp ? (
              <FormattedMessage id="collapse.hide" defaultMessage="Скрыть" />
            ) : (
              <FormattedMessage id="collapse.expand" defaultMessage="Ещё" />
            )}
          </Button>
        )}
      </Fragment>
    );
  }
}

export default Collapse;
