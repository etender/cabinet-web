import React, { Component } from "react";
import { Button, withStyles, Typography, withWidth } from "@material-ui/core";
import classNames from "classnames";
const styles = theme => ({
  root: {
    textTransform: "none",
    [theme.breakpoints.down("sm")]: {
      minWidth: theme.spacing(4)
    },
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  linkButton: {}
});

class LinkButton extends Component {
  render() {
    const { classes, title, onClick, children, width } = this.props;
    const withTitle = width !== "xs" && width !== "sm";
    return (
      <Button
        size="small"
        className={classNames(classes.root, classes.linkButton)}
        onClick={onClick}
        disableRipple
      >
        {children}
        <Typography variant="body1" component="span" color="primary">
          {withTitle && title}
        </Typography>
      </Button>
    );
  }
}

export default withWidth()(withStyles(styles)(LinkButton));
