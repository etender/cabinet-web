import { getEntityLoading } from "../../services/Entities/Entity/EntitySelectors";

export const getLoading = (state, entity) => getEntityLoading(state, entity);
