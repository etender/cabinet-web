import React from "react";
import { connect } from "react-redux";

import { fetch } from "./ExportButtonActions";
import { getLoading } from "./ExportButtonSelectors";
import ExportButton from "./ExportButton";

const ExportButtonContainer = props => <ExportButton {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { name } = ownProps;
  return {
    loading: getLoading(state, name)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name } = ownProps;
  return {
    fetch: query => dispatch(fetch(name, query))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExportButtonContainer);
