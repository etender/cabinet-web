import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, CircularProgress, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";
import GetAppIcon from "@material-ui/icons/GetApp";
import { isEmpty } from "../../utils/CoreUtils";

const messages = defineMessages({
  exportToExcel: {
    id: "tenders.tenders.export-to-excel",
    defaultMessage: "Экспорт в Excel"
  }
});

const styles = theme => ({
  wrapper: {
    position: "relative"
  },
  exportProgress: {
    position: "absolute",
    top: 2,
    left: 0,
    zIndex: 1
  }
});

export class ExportButton extends Component {
  static propTypes = {
    fetch: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired
  };

  handleExportToExcel = () => {
    const { fetch, query } = this.props;
    fetch(query);
  };

  render() {
    const { classes, intl, loading, query } = this.props;
    const { formatMessage } = intl;
    const disabled = loading || isEmpty(query);

    return (
      <div className={classes.wrapper}>
        <Button
          color="primary"
          startIcon={<GetAppIcon />}
          onClick={this.handleExportToExcel}
          disabled={disabled}
        >
          {formatMessage(messages.exportToExcel)}
        </Button>
        {loading && (
          <CircularProgress size={28} className={classes.exportProgress} />
        )}
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(ExportButton));
