import { handleActions } from "redux-actions";
import { toggleDialog } from "./DialogActions";

const initialState = {};

export default handleActions(
  {
    [toggleDialog]: (state, action) => {
      const dilogState = state[action.meta.name] || {};
      const open = dilogState.open;
      return {
        ...state,
        [action.meta.name]: {
          open: !open,
          options: open ? dilogState.options : action.payload
        }
      };
    }
  },
  initialState
);
