import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Button,
  Dialog as MuiDialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  IconButton,
  withWidth,
  withStyles
} from "@material-ui/core";
import { Close as CloseIcon } from "@material-ui/icons";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Form from "../Form/Form";

const styles = theme => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
    [theme.breakpoints.down("xs")]: {
      top: "0px",
      right: "0px"
    }
  },
  cardActions: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  title: {},
  titleHidden: {
    padding: 0
  },
  content: {},
  paper: {}
});

export class Dialog extends Component {
  static propTypes = {
    value: PropTypes.object,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    buttons: PropTypes.node,
    formDisabled: PropTypes.bool,
    contentDisabled: PropTypes.bool,
    actionsDisabled: PropTypes.bool,
    open: PropTypes.bool.isRequired,
    loading: PropTypes.bool,
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.func
    ]).isRequired,
    toggleDialog: PropTypes.func,
    onAction: PropTypes.func,
    onEnter: PropTypes.func,
    onSubmitted: PropTypes.func
  };

  handleFormAction = (value, form) => {
    const { onAction } = this.props;
    onAction(value, form);
  };

  handleClose = () => {
    const { toggleDialog, name, open, onClose } = this.props;
    if (open && toggleDialog) {
      toggleDialog(name);
    }

    if (onClose) {
      onClose();
    }
  };

  handleSubmitted = () => {
    const { onSubmitted } = this.props;
    if (onSubmitted) {
      onSubmitted();
    }

    this.handleClose();
  };

  renderContent(children) {
    const { classes, buttons, contentDisabled, actionsDisabled } = this.props;
    if (contentDisabled) {
      return children;
    }

    return (
      <Fragment>
        <DialogContent className={classes.content}>{children}</DialogContent>
        <DialogActions className={classes.cardActions}>
          {!actionsDisabled && (
            <Button onClick={this.handleClose} color="primary">
              <FormattedMessage id="dialog.cancel" defaultMessage="Отмена" />
            </Button>
          )}
          {buttons}
        </DialogActions>
      </Fragment>
    );
  }

  renderForm() {
    const { children, formDisabled, value } = this.props;
    if (formDisabled) {
      return this.renderContent(children);
    }

    return (
      <Form
        action={this.handleFormAction}
        value={value}
        onSubmitted={this.handleSubmitted}
      >
        {props => this.renderContent(children(props))}
      </Form>
    );
  }

  render() {
    const {
      classes,
      open,
      width,
      title,
      onEnter,
      fullScreen,
      hideBackdrop,
      hideClose
    } = this.props;

    return (
      <MuiDialog
        fullScreen={fullScreen && width === "xs"}
        hideBackdrop={hideBackdrop}
        open={open}
        onEnter={onEnter}
        onClose={this.handleClose}
        classes={{ paper: classes.paper }}
      >
        <DialogTitle
          className={classNames(classes.title, !title && classes.titleHidden)}
        >
          {title}
          {!hideClose && (
            <IconButton
              aria-label="Закрыть"
              className={classes.closeButton}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>
          )}
        </DialogTitle>
        {this.renderForm()}
      </MuiDialog>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(Dialog)));
