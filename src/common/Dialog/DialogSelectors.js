export const getOpen = (state, name) =>
  (state.dialogs[name] || {}).open || false;

export const getOptions = (state, name) =>
  (state.dialogs[name] || {}).options || {};
