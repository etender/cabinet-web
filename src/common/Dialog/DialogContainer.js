import React from "react";
import { connect } from "react-redux";

import { toggleDialog } from "./DialogActions";
import { getOpen } from "./DialogSelectors";
import Dialog from "./Dialog";

const DialogContainer = props => <Dialog {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { name } = ownProps;

  return {
    open: getOpen(state, name)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name } = ownProps;
  return {
    toggleDialog: () => dispatch(toggleDialog(name))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogContainer);
