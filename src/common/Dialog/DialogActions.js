import { createAction } from "redux-actions";

export const toggleDialog = createAction(
  "DIALOG_TOGGLE",
  (name, options) => options,
  name => ({ name })
);
