import React, { Component } from "react";
import PropTypes from "prop-types";
import { ListItem, ListItemText, withStyles } from "@material-ui/core";
import classNames from "classnames";
import grey from "@material-ui/core/colors/grey";

const styles = theme => ({
  selected: {
    backgroundColor: grey["300"],

    "&:hover": {
      backgroundColor: grey["400"]
    }
  },
  listItem: {
    marginBottom: theme.spacing(0.5),
    borderRadius: theme.spacing(2)
  }
});

class SuggestionItem extends Component {
  static propTypes = {
    onToggleSelected: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    const { selected } = this.props;
    this.state = {
      selected: selected
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { selected } = this.props;

    if (prevProps.selected !== selected) {
      this.setState({ selected: selected });
    }
  }

  handleClick = () => {
    let { selected } = this.state;
    selected = !selected;

    this.setState({ selected });

    const { onToggleSelected, item } = this.props;
    onToggleSelected(item, selected);
  };

  render() {
    const { item, classes } = this.props;
    const { selected } = this.state;

    return (
      <ListItem
        key={item.id}
        button
        onClick={this.handleClick}
        className={classNames(selected && classes.selected, classes.listItem)}
      >
        <ListItemText primary={item.name} />
      </ListItem>
    );
  }
}

export default withStyles(styles)(SuggestionItem);
