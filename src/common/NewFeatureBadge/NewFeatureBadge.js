import React, { Component } from "react";
import { Badge, withStyles } from "@material-ui/core";

const styles = theme => ({
  badge: {
    borderRadius: "2px",
    right: -theme.spacing(3),
    textTransform: "uppercase"
  }
});

export class NewFeatureBadge extends Component {
  render() {
    const { classes, children } = this.props;

    return (
      <Badge badgeContent="new" color="secondary" classes={classes}>
        {children}
      </Badge>
    );
  }
}

export default withStyles(styles)(NewFeatureBadge);
