const defaultSuggestions = [];
export const getSuggestions = (state, entity) =>
  state.suggestions[entity] || defaultSuggestions;
