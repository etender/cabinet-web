import { createAction } from "redux-actions";

import { SUGGESTIONS_FETCH, SUGGESTIONS_CLEAN } from "./SuggestionsConstants";

export const fetchSuggestions = createAction(
  SUGGESTIONS_FETCH,
  (entity, search) => ({ search }),
  entity => ({ entity })
);
export const cleanSuggestions = createAction(
  SUGGESTIONS_CLEAN,
  () => ({}),
  entity => ({ entity })
);
