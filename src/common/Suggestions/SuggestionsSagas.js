import { all, takeLatest } from "redux-saga/effects";

import { SUGGESTIONS_FETCH } from "./SuggestionsConstants";
import { fetchEntitiesSaga } from "../../services/Entities/EntitiesSagas";

export default function* suggestionsSagas() {
  yield all([takeLatest(SUGGESTIONS_FETCH, fetchEntitiesSaga)]);
}
