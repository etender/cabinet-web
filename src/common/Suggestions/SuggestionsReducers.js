import { handleActions } from "redux-actions";

import { successApiType } from "../../services/Api/ApiHelpers";
import { isEmpty } from "../../utils/CoreUtils";
import { SUGGESTIONS_FETCH, SUGGESTIONS_CLEAN } from "./SuggestionsConstants";

const initialState = {};

export default handleActions(
  {
    [successApiType(SUGGESTIONS_FETCH)]: (state, action) => {
      if (isEmpty(state[action.meta.entity]) && isEmpty(action.payload)) {
        return state;
      }

      return { ...state, [action.meta.entity]: action.payload };
    },
    [SUGGESTIONS_CLEAN]: (state, action) => {
      if (isEmpty(state[action.meta.entity])) {
        return state;
      }
      return { ...state, [action.meta.entity]: [] };
    }
  },
  initialState
);
