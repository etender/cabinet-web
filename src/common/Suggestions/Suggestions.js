import React, { Component } from "react";
import PropTypes from "prop-types";
import { List } from "@material-ui/core";
import { isEmpty } from "../../utils/CoreUtils";
import SuggestionItem from "../SuggestionItem/SuggestionItem";

class Suggestions extends Component {
  render() {
    const { items, selected, onToggleSelected } = this.props;

    if (isEmpty(items)) {
      return "";
    }

    return (
      <List>
        {items.map(item => (
          <SuggestionItem
            key={item.id}
            item={item}
            onToggleSelected={onToggleSelected}
            selected={selected[item.id]}
          />
        ))}
      </List>
    );
  }
}

Suggestions.propTypes = {
  onToggleSelected: PropTypes.func.isRequired
};

export default Suggestions;
