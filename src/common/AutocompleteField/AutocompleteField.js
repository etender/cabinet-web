import React, { Component } from "react";
import Downshift from "downshift";

import {
  Chip,
  Paper,
  TextField,
  InputAdornment,
  List,
  ListItem,
  ListItemText,
  withStyles
} from "@material-ui/core";
import SimpleBar from "simplebar-react";

import { isPresent, isEmpty, isString } from "../../utils/CoreUtils";
import HelpButton from "../HelpButton/HelpButton";

const styles = theme => ({
  container: {
    flexGrow: 1,
    position: "relative",
    marginBottom: theme.spacing(2)
  },
  paper: {
    position: "absolute",
    zIndex: 1200,
    marginTop: theme.spacing(),
    marginBottom: theme.spacing(),
    left: 0,
    right: 0,
    maxHeight: 300,
    overflow: "hidden",
    display: "flex"
  },
  chip: {
    margin: `${theme.spacing() / 2}px ${theme.spacing() / 4}px`,
    textOverflow: "ellipsis",
    maxWidth: "100%"
  },
  inputRoot: {
    flexWrap: "wrap",
    minHeight: "40px"
  },
  inputInput: {
    width: "auto",
    flexGrow: 1
  },
  listItem: {
    whiteSpace: "normal",
    cursor: "pointer"
  }
});

class AutocompleteField extends Component {
  constructor(props) {
    super(props);

    this.inputRef = React.createRef();

    this.state = {
      inputValue: "",
      selectedItem: this.defaultValue(),
      inFocus: false
    };
  }

  defaultValue() {
    const { value } = this.props;
    return value;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { fetch, clean, value } = this.props;
    const { inputValue } = this.state;

    if (prevProps.value !== value) {
      this.setState({ selectedItem: value });
    }

    if (!fetch || !clean) {
      return;
    }

    if (prevState.inputValue === inputValue) {
      return;
    }

    if (isEmpty(inputValue)) {
      return clean();
    }

    if (inputValue.length > 2) {
      if (this.timeout) {
        window.clearTimeout(this.timeout);
      }

      this.timeout = window.setTimeout(() => {
        fetch(inputValue);
      }, 500);
    }
  }

  handleChange = value => {
    const { objectValue, multiselected } = this.props;
    let { selectedItem } = this.state;

    if (value) {
      if (!objectValue && value.title) {
        value = value.title;
      }

      let hasItem;
      hasItem = objectValue
        ? selectedItem.find(item => item.id === value.id)
        : selectedItem.indexOf(value) !== -1;

      if (!hasItem) {
        selectedItem =
          multiselected === false ? [value] : [...selectedItem, value];
      }
    } else {
      selectedItem = this.defaultValue();
    }

    this.setState({
      inputValue: "",
      selectedItem
    });

    this.onChange(selectedItem);
  };

  onChange(value) {
    const { onChange } = this.props;
    if (onChange) {
      onChange(value);
    }
  }

  handleDelete = item => () => {
    let { selectedItem } = this.state;

    selectedItem = [...selectedItem];
    selectedItem.splice(selectedItem.indexOf(item), 1);

    this.setState({ selectedItem });

    this.onChange(selectedItem);
  };

  handleInputChange = event => {
    const value = event.target.value;
    this.setState({ inputValue: value });

    if (value.match(/,/)) {
      this.applySelected(event);
    }
  };

  handleInputKeyPress = event => {
    if (event.key === "Enter") {
      this.handleBlur(event);

      event.stopPropagation();
      event.preventDefault();
    }
  };

  handleInputKeyDown = event => {
    if (event.key !== "Backspace") {
      return;
    }

    const { inputValue } = this.state;
    let { selectedItem } = this.state;
    if (isPresent(inputValue)) {
      return;
    }

    if (selectedItem.length > 0) {
      selectedItem = [...selectedItem];
      selectedItem.splice(selectedItem.length - 1, 1);
    }

    this.setState({ selectedItem });
    this.onChange(selectedItem);
  };

  handleBlur = event => {
    const { selectOnly } = this.props;

    this.setState({ inFocus: false });

    if (selectOnly) {
      return;
    }

    const selectedItem = this.applySelected(event);

    this.onChange(selectedItem);
  };

  handleFocus = () => {
    this.setState({ inFocus: true });
  };

  handleShrinkToggle = () => {
    let { selectedItem, inFocus } = this.state;

    const selected = selectedItem.length > 0;
    return selected || inFocus ? true : false;
  };

  applySelected = event => {
    let { clean } = this.props;
    if (clean) {
      clean();
    }

    let { selectedItem } = this.state;

    selectedItem = event.target.value.split(",").reduce(
      (items, item) => {
        item = item.trim();

        if (!isEmpty(item) && items.indexOf(item) === -1) {
          items.push(item);
        }

        return items;
      },
      [...selectedItem]
    );

    this.setState({
      inputValue: "",
      selectedItem
    });

    return selectedItem;
  };

  renderSelectedItem(selectedItem) {
    const { multiselected, classes } = this.props;

    if (isEmpty(selectedItem)) {
      return;
    }

    if (multiselected === false) {
      selectedItem = [selectedItem];
    }

    return selectedItem.map((item, index) => (
      <Chip
        key={item.id || isString(item) ? item : index}
        tabIndex={-1}
        label={item.title || item}
        className={classes.chip}
        onDelete={this.handleDelete(item)}
      />
    ));
  }

  focus() {
    this.inputRef.current.focus();
  }

  render() {
    const {
      suggestions,
      classes,
      label,
      multiselected,
      helpButtonBody,
      error,
      helperText
    } = this.props;
    const { inputValue, selectedItem } = this.state;

    const disabled = multiselected === false && isPresent(selectedItem);

    return (
      <Downshift
        inputValue={inputValue}
        onChange={this.handleChange}
        itemToString={item => (item ? item.title : "")}
        selectedItem={selectedItem}
      >
        {({
          getInputProps,
          getItemProps,
          highlightedIndex,
          isOpen,
          selectedItem
        }) => (
          <div className={classes.container}>
            <TextField
              fullWidth
              label={label}
              disabled={disabled}
              ref={this.inputRef}
              error={error}
              helperText={helperText}
              InputLabelProps={{
                shrink: this.handleShrinkToggle()
              }}
              InputProps={getInputProps({
                classes: {
                  root: classes.inputRoot,
                  input: classes.inputInput
                },
                onChange: this.handleInputChange,
                onKeyPress: this.handleInputKeyPress,
                onKeyDown: this.handleInputKeyDown,
                onBlur: this.handleBlur,
                onFocus: this.handleFocus,
                startAdornment: this.renderSelectedItem(selectedItem),
                endAdornment: helpButtonBody && (
                  <InputAdornment position="end">
                    <HelpButton body={helpButtonBody} />
                  </InputAdornment>
                )
              })}
            />

            {suggestions && suggestions.length > 0 && isOpen && (
              <Paper className={classes.paper} square>
                <SimpleBar style={{ width: "100%" }}>
                  <List component="nav" aria-label="suggestions">
                    {suggestions.map((suggestion, index) => {
                      const isHiglightedIndex = highlightedIndex === index;
                      const itemProps = getItemProps({ item: suggestion });
                      return (
                        <ListItem
                          {...itemProps}
                          key={suggestion.id}
                          selected={highlightedIndex === index}
                          className={classes.listItem}
                          style={{
                            fontWeight: isHiglightedIndex ? 500 : 400
                          }}
                        >
                          <ListItemText
                            primary={suggestion.title}
                            secondary={
                              suggestion.subTitle ? suggestion.subTitle : null
                            }
                          />
                        </ListItem>
                      );
                    })}
                  </List>
                </SimpleBar>
              </Paper>
            )}
          </div>
        )}
      </Downshift>
    );
  }
}

export default withStyles(styles)(AutocompleteField);
