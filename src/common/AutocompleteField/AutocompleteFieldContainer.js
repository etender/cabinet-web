import React from "react";
import { connect } from "react-redux";

import {
  fetchSuggestions,
  cleanSuggestions
} from "../Suggestions/SuggestionsActions";

import AutocompleteField from "./AutocompleteField";
import { getSuggestions } from "../Suggestions/SuggestionsSelectors";

const AutocompleteFieldContainer = props => (
  <AutocompleteField {...props} ref={props.fieldRef} />
);

const mapStateToProps = (state, ownProps) => {
  const { name } = ownProps;
  return {
    suggestions: getSuggestions(state, name)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name } = ownProps;
  return {
    fetch: query => dispatch(fetchSuggestions(name, query)),
    clean: () => dispatch(cleanSuggestions(name))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AutocompleteFieldContainer);
