import React, { Component } from "react";
import PropTypes from "prop-types";
import { IconButton, Typography, Popover, withStyles } from "@material-ui/core";

import { injectIntl } from "react-intl";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";

const styles = theme => ({
  root: {
    padding: "5px"
  },
  popoverElement: {
    maxWidth: theme.spacing(50),
    padding: theme.spacing(2)
  }
});

export class HelpButton extends Component {
  static propTypes = {
    body: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
  }

  render() {
    const { classes, body } = this.props;
    let { anchorEl } = this.state;

    const handleClick = event => {
      this.setState({ anchorEl: event.currentTarget });
    };

    const handleClose = () => {
      this.setState({ anchorEl: null });
    };

    const open = Boolean(anchorEl);

    return (
      <>
        <IconButton
          aria-label="help"
          edge="end"
          className={classes.root}
          onClick={handleClick}
        >
          <HelpOutlineIcon />
        </IconButton>
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          classes={{ paper: classes.popoverElement }}
        >
          <Typography dangerouslySetInnerHTML={{ __html: body }}></Typography>
        </Popover>
      </>
    );
  }
}

export default injectIntl(withStyles(styles)(HelpButton));
