import React, { Component } from "react";
import PropTypes from "prop-types";
import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";
import InfiniteScroll from "react-infinite-scroller";
import { withStyles, withWidth } from "@material-ui/core";
import { injectIntl } from "react-intl";

const styles = theme => ({
  container: {
    width: "100%"
  }
});

class InfiniteList extends Component {
  static propTypes = {
    loadMore: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);

    this.ref = React.createRef();
  }

  componentDidMount() {
    this.updateScroller();
  }

  componentDidUpdate() {
    this.updateScroller();
  }

  updateScroller() {
    if (this.ref.current && !this.scroller) {
      this.scroller = this.ref.current.parentElement.parentElement;
      this.forceUpdate();
    }
  }

  render() {
    const { classes, children, loadMore, hasMore, variant, width } = this.props;

    const style = {
      height: "100%",
      maxHeight: "100%"
    };

    if (variant === "dialog") {
      style.maxHeight =
        width === "xs" || width === "sm" ? "100%" : "calc(100vh - 246px)";
    }

    return (
      <SimpleBar style={style}>
        <div className={classes.container} ref={this.ref}>
          <InfiniteScroll
            pageStart={1}
            loadMore={loadMore}
            useWindow={false}
            initialLoad={false}
            getScrollParent={() => this.scroller}
            hasMore={hasMore}
          >
            {children}
          </InfiniteScroll>
        </div>
      </SimpleBar>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(InfiniteList)));
