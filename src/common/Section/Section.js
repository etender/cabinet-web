import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  withStyles
} from "@material-ui/core";
import { injectIntl } from "react-intl";
import HelpButton from "../HelpButton/HelpButton";

const styles = theme => ({
  header: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  content: {
    padding: theme.spacing(3)
  },
  actions: {
    display: "flex",
    padding: theme.spacing(3),
    paddingTop: "0",
    justifyContent: "flex-end"
  },
  title: {
    display: "flex",
    justifyContent: "space-between"
  },
  titleAction: {
    alignSelf: "center"
  },
  card: {
    overflow: "unset"
  }
});

class Section extends Component {
  static propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    variant: PropTypes.string
  };

  render() {
    const {
      classes,
      children,
      title,
      titleAction,
      helpButtonBody,
      header,
      actions
    } = this.props;
    const variant = this.props.variant || "standart";

    return (
      <Fragment>
        {title && (
          <div className={classes.title}>
            <Typography className={classes.header} variant={"h6"}>
              {title}
              {helpButtonBody && <HelpButton body={helpButtonBody} />}
            </Typography>
            {titleAction && (
              <div className={classes.titleAction}>{titleAction}</div>
            )}
          </div>
        )}

        {variant === "standart" && children}
        {variant === "filled" && (
          <Card className={classes.card}>
            {header}
            <CardContent className={classes.content}>{children}</CardContent>
            {actions && (
              <CardActions className={classes.actions} disableSpacing>
                {actions}
              </CardActions>
            )}
          </Card>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(Section));
