import React, { Component } from "react";
import PropTypes from "prop-types";
import { ConnectedRouter } from "connected-react-router";

import { IntlProvider } from "react-intl";
import translations, { formats } from "../translations/locales";

import { CssBaseline, MuiThemeProvider } from "@material-ui/core";
import { theme } from "../themes/default";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import moment from "moment";
import "moment/locale/ru";

import SnackbarContainer from "../common/Snackbar/SnackbarContainer";
import Router from "../services/Router/Router";

// Intl.PluralRules polyfill
if (!Intl.PluralRules) {
  require("@formatjs/intl-pluralrules/polyfill");
  require("@formatjs/intl-pluralrules/dist/locale-data/ru");
}
// Intl RelativeTimeFormat polyfill
if (!Intl.RelativeTimeFormat) {
  require("@formatjs/intl-relativetimeformat/polyfill");
  require("@formatjs/intl-relativetimeformat/dist/locale-data/ru");
}

class App extends Component {
  static defaultProps = {
    locale: "ru"
  };

  static propTypes = {
    history: PropTypes.shape({}).isRequired,
    routes: PropTypes.shape({}).isRequired
  };

  componentDidMount() {
    const { locale } = this.props;
    moment.locale(locale);
  }

  render() {
    const { routes, locale, history, timeZone } = this.props;

    return (
      <IntlProvider
        locale={locale}
        messages={translations[locale]}
        formats={formats}
        timeZone={timeZone}
      >
        <MuiThemeProvider theme={theme}>
          <MuiPickersUtilsProvider
            utils={MomentUtils}
            locale={locale}
            moment={moment}
          >
            <CssBaseline />
            <ConnectedRouter history={history}>
              <Router routes={routes} />
            </ConnectedRouter>
            <SnackbarContainer />
          </MuiPickersUtilsProvider>
        </MuiThemeProvider>
      </IntlProvider>
    );
  }
}

export default App;
