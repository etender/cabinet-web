import React, { lazy } from "react";
import { connect } from "react-redux";

import history from "../utils/HistoryUtils";

import { PRIVATE_PATH } from "../modules/Private/PrivateConstants";
import { SHARED_PATH } from "../modules/Shared/SharedConstants";
import { getTimeZone } from "../services/Devise/DeviseSelectors";

import App from "./App";

const PrivateContainer = lazy(() =>
  import("../modules/Private/PrivateContainer")
);
const SharedContainer = lazy(() => import("../modules/Shared/SharedContainer"));

const routes = {
  [SHARED_PATH]: { container: SharedContainer },
  [PRIVATE_PATH]: { container: PrivateContainer }
};

const AppContainer = props => <App {...props} />;

const mapStateToProps = state => {
  return {
    history,
    routes,
    timeZone: getTimeZone(state)
  };
};

export default connect(mapStateToProps)(AppContainer);
