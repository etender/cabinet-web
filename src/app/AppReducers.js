import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import api from "../services/Api/ApiReducers";
import devise from "../services/Devise/DeviseReducers";
import dashboard from "../modules/Dashboard/DashboardReducers";
import tenders from "../modules/Tenders/TendersReducers";
import tender from "../modules/Tender/TenderReducers";
import favorites from "../modules/Favorites/FavoritesReducers";
import analytics from "../modules/Analytics/AnalyticsReducers";
import registrationExpired from "../modules/RegistrationExpired/RegistrationExpiredReducers";

import entities from "../services/Entities/EntitiesReducers";
import snackbar from "../common/Snackbar/SnackbarReducers";
import dialogs from "../common/Dialog/DialogReducers";
import timeZones from "../common/TimeZones/TimeZonesReducers";
import suggestions from "../common/Suggestions/SuggestionsReducers";

const rootReducers = history =>
  combineReducers({
    router: connectRouter(history),
    api,
    devise,
    dashboard,
    tenders,
    tender,
    favorites,
    registrationExpired,
    analytics,
    entities,
    snackbar,
    dialogs,
    timeZones,
    suggestions
  });

export default rootReducers;
