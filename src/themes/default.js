import { createMuiTheme } from "@material-ui/core/styles";
import { ruRU } from "@material-ui/core/locale";

export const theme = createMuiTheme(
  {
    typography: {
      fontSize: 12,
      overline: {
        fontSize: "0.8rem",
        lineHeight: "1.5em"
      }
    },
    palette: {
      primary: {
        main: "#0071B8",
        light: "#00ADEC"
      },
      secondary: {
        main: "#E44143"
      }
      // error: will use the default color
    },
    overrides: {
      MuiTableCell: {
        root: {
          fontSize: "0.83rem",
          lineHeight: "19px",
          paddingLeft: 0,
          verticalAlign: "top",
          letterSpacing: "0",
          "&&:last-child": {
            paddingRight: 0,
            textAlign: "right"
          }
        },
        head: {
          color: "#888888",
          lineHeight: "21px",
          fontWeight: "normal"
        }
      }
    },
    breakpoints: {
      keys: ["xs", "sm", "md", "lp", "lg", "lpl", "lpxl", "xl"],
      values: {
        xs: 0,
        sm: 600,
        md: 960,
        lp: 1024,
        lg: 1280,
        lpl: 1440,
        lpxl: 1600,
        xl: 1920
      }
    }
  },
  ruRU
);
