import { callApi } from "../../services/Api/ApiHelpers";

describe("ApiHelpers", () => {
  describe("callApi", () => {
    const path = {
      method: "GET",
      url: "http://test"
    };

    beforeEach(() => {
      fetch.resetMocks();
    });

    it("send request", () => {
      fetch.mockResponseOnce(JSON.stringify({ payload: "payload" }));

      callApi(path);

      expect(fetch.mock.calls.length).toEqual(1);
      expect(fetch.mock.calls[0][0]).toEqual(path.url);
    });

    describe("when success", () => {
      const payload = {
        data: "test"
      };

      beforeEach(() => {
        fetch.mockResponseOnce(JSON.stringify(payload));
      });

      it("return result", () => {
        callApi(path).then(result => {
          expect(result.json).toEqual(payload);
        });
      });
    });

    describe("when 401 failure", () => {
      const payload = {
        error: "Invalid email or password"
      };
      const result = {
        message: payload.error,
        status: 401
      };

      beforeEach(() => {
        fetch.mockResponseOnce(JSON.stringify(payload), { status: 401 });
      });

      it("return error object description", () => {
        callApi(path).catch(error => {
          expect(error).toEqual(result);
        });
      });
    });

    describe("when server failure", () => {
      const payload = "Server Error";

      beforeEach(() => {
        fetch.mockResponseOnce(payload, { status: 500 });
      });

      it("return error object description", () => {
        callApi(path).catch(error => {
          expect(error).toEqual(payload);
        });
      });
    });
  });
});
