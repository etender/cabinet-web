import React from "react";
import createComponentWithIntl from "../support/helpers/TestHelpers";

import UserSignIn from "../../modules/User/components/SignIn";

describe("<UserSignIn>", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = createComponentWithIntl(
      <UserSignIn signIn={(email, password) => {}} />
    );
  });

  it("snapshot", () => {
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
});
