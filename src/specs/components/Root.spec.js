import React from "react";
import { shallow } from "enzyme";
import { MemoryRouter } from "react-router-dom";
import configureStore from "redux-mock-store";

import Root from "../../components/Root";

describe("<Root>", () => {
  const initialState = {};
  const auth = false;
  const mockStore = configureStore();
  let store, wrapper, routes;

  beforeEach(() => {
    routes = {};
    store = mockStore(initialState);
    wrapper = shallow(
      <MemoryRouter>
        <Root auth={auth} routes={routes} />
      </MemoryRouter>
    );
  });

  it("renders without crashing", () => {
    expect(wrapper.length).toEqual(1);
  });
});
