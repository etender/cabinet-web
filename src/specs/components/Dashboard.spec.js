import React from "react";
import { shallow } from "enzyme";

import Dashboard from "../../components/Dashboard";

describe("<Root>", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Dashboard />);
  });

  it("renders without crashing", () => {
    expect(wrapper.length).toEqual(1);
  });

  it("snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
