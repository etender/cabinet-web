import { monitor } from "../../sagas/ApiSagas";
import { put } from "redux-saga/effects";
import { signOut } from "../../modules/User/actions/AuthActions";

describe("ApiSagas", () => {
  describe("monitor", () => {
    describe("with invalid token", () => {
      let saga;
      let failure = {
        type: "ACTION_FAILURE",
        error: {
          message: "error",
          status: 401
        }
      };

      beforeEach(() => {
        saga = monitor({ type: "ACTION_REQUEST" });
        saga.next();
      });

      it("call signOut action", () => {
        const next = saga.next({ failure });
        expect(next.value).toEqual(put(signOut()));
      });
    });

    describe("with valid token", () => {
      let saga;
      let success = {
        type: "ACTION_SUCCESS"
      };

      beforeEach(() => {
        saga = monitor({ type: "ACTION_REQUEST" });
        saga.next();
      });

      it("do nothing", () => {
        const next = saga.next({ success });
        expect(next.done).toBeTruthy();
      });
    });
  });
});
