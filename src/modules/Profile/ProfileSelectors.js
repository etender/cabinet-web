import { createSelector } from "reselect";
import { getCurrentUser } from "../../services/Devise/DeviseSelectors";

export const canChangeProfile = createSelector(
  getCurrentUser,
  user => !user.readOnly
);
