import React, { Component, Fragment } from "react";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
  TextField,
  withStyles,
  withWidth
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Form from "../../../common/Form/Form";

const messages = defineMessages({
  nameField: {
    id: "dashboard.user-name.name-field",
    defaultMessage: "Имя"
  },
  surnameField: {
    id: "dashboard.user-name.surname-field",
    defaultMessage: "Фамилия"
  },
  patronymicField: {
    id: "dashboard.user-name.patronymic-field",
    defaultMessage: "Отчество"
  }
});

const styles = theme => ({
  paper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(70)
    }
  }
});

export class UserName extends Component {
  handleClose = () => {
    this.props.onClose();
  };

  handleFormAction = (value, form) => {
    const { change } = this.props;

    const user = {};

    if (value.surname !== this.props.value.surname) {
      user.surname = value.surname;
    }

    if (value.name !== this.props.value.name) {
      user.name = value.name;
    }

    if (value.patronymic !== this.props.value.patronymic) {
      user.patronymic = value.patronymic;
    }

    change(user, form);
  };

  render() {
    const { classes, open, value, intl, width } = this.props;
    const { formatMessage } = intl;

    return (
      <Dialog
        fullScreen={width === "xs"}
        open={open}
        onClose={this.handleClose}
        aria-labelledby="title"
        classes={{ paper: classes.paper }}
      >
        <DialogTitle id="title">
          <FormattedMessage
            id="dashboard.user-name.title"
            defaultMessage="Изменение имени"
          />
        </DialogTitle>
        <Form
          action={this.handleFormAction}
          value={value}
          onSubmitted={this.handleClose}
        >
          {({ field, getValue, handleInputChange }) => (
            <Fragment>
              <DialogContent>
                <TextField
                  name="surname"
                  inputRef={field}
                  value={getValue("surname")}
                  onChange={handleInputChange}
                  required
                  fullWidth
                  label={formatMessage(messages.surnameField)}
                  InputLabelProps={{ required: true }}
                />
                <TextField
                  name="name"
                  inputRef={field}
                  value={getValue("name")}
                  onChange={handleInputChange}
                  required
                  fullWidth
                  label={formatMessage(messages.nameField)}
                  InputLabelProps={{ required: true }}
                />
                <TextField
                  name="patronymic"
                  inputRef={field}
                  value={getValue("patronymic")}
                  onChange={handleInputChange}
                  fullWidth
                  label={formatMessage(messages.patronymicField)}
                  InputLabelProps={{ required: false }}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  <FormattedMessage
                    id="dashboard.user-name.cancel"
                    defaultMessage="Отмена"
                  />
                </Button>
                <Button type="submit" color="primary" autoFocus>
                  <FormattedMessage
                    id="dashboard.user-name.change"
                    defaultMessage="Изменить"
                  />
                </Button>
              </DialogActions>
            </Fragment>
          )}
        </Form>
      </Dialog>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(UserName)));
