import React, { Component, Fragment } from "react";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
  TextField,
  withStyles
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import PropTypes from "prop-types";
import MaskedInput from "react-text-mask";
import withWidth from "@material-ui/core/withWidth";

import Form from "../../../common/Form/Form";

const messages = defineMessages({
  phoneField: {
    id: "dashboard.user-name.phone-field",
    defaultMessage: "Телефон"
  }
});

const styles = theme => ({
  paper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(70)
    }
  }
});

const TextMask = props => {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={ref => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[
        "+",
        "7",
        "(",
        /[1-9]/,
        /\d/,
        /\d/,
        ")",
        " ",
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/
      ]}
      placeholderChar={"\u2000"}
    />
  );
};

TextMask.propTypes = {
  inputRef: PropTypes.func.isRequired
};

export class UserPhone extends Component {
  handleClose = () => {
    this.props.onClose();
  };

  handleFormAction = (value, form) => {
    const { change } = this.props;
    if (value.phone !== this.props.value.phone) {
      change({ phone: value.phone }, form);
    }
  };

  render() {
    const { classes, open, value, intl, width } = this.props;
    const { formatMessage } = intl;

    return (
      <Dialog
        fullScreen={width === "xs"}
        open={open}
        onClose={this.handleClose}
        aria-labelledby="title"
        classes={{ paper: classes.paper }}
      >
        <DialogTitle id="title">
          <FormattedMessage
            id="dashboard.user-phone.title"
            defaultMessage="Изменение номера телефона"
          />
        </DialogTitle>
        <Form
          action={this.handleFormAction}
          value={value}
          onSubmitted={this.handleClose}
        >
          {({ field, getValue, handleInputChange }) => (
            <Fragment>
              <DialogContent>
                <TextField
                  name="phone"
                  inputRef={field}
                  required
                  fullWidth
                  label={formatMessage(messages.phoneField)}
                  InputProps={{
                    inputComponent: TextMask,
                    value: getValue("phone"),
                    onChange: handleInputChange
                  }}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  <FormattedMessage
                    id="dashboard.user-phone.cancel"
                    defaultMessage="Отмена"
                  />
                </Button>
                <Button type="submit" color="primary" autoFocus>
                  <FormattedMessage
                    id="dashboard.user-phone.change"
                    defaultMessage="Изменить"
                  />
                </Button>
              </DialogActions>
            </Fragment>
          )}
        </Form>
      </Dialog>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(UserPhone)));
