import { createAction } from "redux-actions";

import { showLink } from "../../services/Api/ApiActions";
import { PROFILE_PATH } from "./ProfileConstants";

export const showProfile = createAction(
  showLink.toString(),
  () => PROFILE_PATH
);
