import React, { Component, Fragment } from "react";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
  TextField,
  withStyles
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Form from "../../../common/Form/Form";

import withWidth from "@material-ui/core/withWidth";

const messages = defineMessages({
  emailField: {
    id: "dashboard.user-email.email-field",
    defaultMessage: "Электронная почта"
  }
});

const styles = theme => ({
  paper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(70)
    }
  }
});

export class UserEmail extends Component {
  handleClose = () => {
    this.props.onClose();
  };

  handleFormAction = (value, form) => {
    const { change } = this.props;
    if (value.email !== this.props.value.email) {
      change({ email: value.email }, form);
    }
  };

  render() {
    const { classes, open, intl, width } = this.props;
    const { formatMessage } = intl;

    let { value } = this.props;
    value = { email: value.unconfirmedEmail || value.email };
    return (
      <Dialog
        fullScreen={width === "xs"}
        open={open}
        onClose={this.handleClose}
        classes={{ paper: classes.paper }}
      >
        <DialogTitle id="title">
          <FormattedMessage
            id="dashboard.user-email.title"
            defaultMessage="Изменение электронной почты"
          />
        </DialogTitle>
        <Form
          action={this.handleFormAction}
          value={value}
          onSubmitted={this.handleClose}
        >
          {({ field, getValue, handleInputChange, error }) => (
            <Fragment>
              <DialogContent>
                <TextField
                  name="email"
                  inputRef={field}
                  value={getValue("email")}
                  onChange={handleInputChange}
                  required
                  fullWidth
                  label={formatMessage(messages.emailField)}
                  error={!!error("email")}
                  helperText={error("email")}
                  InputLabelProps={{ required: false }}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  <FormattedMessage
                    id="dashboard.user-email.cancel"
                    defaultMessage="Отмена"
                  />
                </Button>
                <Button type="submit" color="primary" autoFocus>
                  <FormattedMessage
                    id="dashboard.user-email.change"
                    defaultMessage="Изменить"
                  />
                </Button>
              </DialogActions>
            </Fragment>
          )}
        </Form>
      </Dialog>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(UserEmail)));
