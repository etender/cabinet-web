import React from "react";
import { connect } from "react-redux";

import Profile from "./Profile";
import {
  updateUser,
  cancelUpdateUser
} from "../../services/Devise/DeviseActions";
import { fetchTimeZones } from "../../common/TimeZones/TimeZonesActions";
import {
  getTimeZones,
  shouldFetchTimeZones
} from "../../common/TimeZones/TimeZonesSelectors";
import { getCurrentUser } from "../../services/Devise/DeviseSelectors";
import { canChangeProfile } from "./ProfileSelectors";

const ProfileContainer = props => <Profile {...props} />;

const mapStateToProps = state => {
  return {
    user: getCurrentUser(state),
    timeZones: getTimeZones(state),
    shouldFetchTimeZones: shouldFetchTimeZones(state),
    canChangeProfile: canChangeProfile(state)
  };
};

export default connect(mapStateToProps, {
  changeProfile: updateUser,
  cancelChangeProfile: cancelUpdateUser,
  fetchTimeZones
})(ProfileContainer);
