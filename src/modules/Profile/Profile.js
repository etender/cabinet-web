import React, { Component, Fragment } from "react";
import camelize from "camelize";
import SimpleBar from "simplebar-react";
import {
  Tooltip,
  Typography,
  Card,
  CardHeader,
  CardContent,
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  ListItemSecondaryAction,
  Grid,
  Switch,
  withStyles,
  IconButton
} from "@material-ui/core";
import { Close as CloseIcon } from "@material-ui/icons";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { FormattedDate, injectIntl } from "react-intl";

import UserName from "./UserName/UserName";
import UserEmail from "./UserEmail/UserEmail";
import UserTimeZone from "./UserTimeZone/UserTimeZone";
import UserPassword from "./UserPassword/UserPassword";
import UserPhone from "./UserPhone/UserPhone";

const messages = defineMessages({
  title: {
    id: "profile.title",
    defaultMessage: "Профиль пользователя"
  },
  baseTariff: {
    id: "user.tariff.base",
    defaultMessage: "48 часов"
  },
  demoTariff: {
    id: "user.tariff.demo",
    defaultMessage: "30 дней"
  },
  trioTariff: {
    id: "user.tariff.trio",
    defaultMessage: "3 месяца"
  },
  tuserlimitTariff: {
    id: "user.tariff.tuserlimit",
    defaultMessage: "3 месяца(огр. пользователей)"
  },
  halfTariff: {
    id: "user.tariff.half",
    defaultMessage: "6 месяцев"
  },
  nineTariff: {
    id: "user.tariff.nine",
    defaultMessage: "9 месяцев"
  },
  fullTariff: {
    id: "user.tariff.full",
    defaultMessage: "12 месяцев"
  },
  tenderProTariff: {
    id: "user.tariff.tender-pro",
    defaultMessage: "Тендер Про 1 год"
  },
  endlessTariff: {
    id: "user.tariff.endless",
    defaultMessage: "5 лет"
  },
  lightFullTariff: {
    id: "user.tariff.light-full",
    defaultMessage: "Light 12 мес."
  },
  lightBaseTariff: {
    id: "user.tariff.light-base",
    defaultMessage: "Light 1 мес."
  },
  cancelButton: {
    id: "user.tariff.cancel-button",
    defaultMessage: "Отмена"
  }
});

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    overflowY: "auto"
  },
  profileCard: {
    width: "100%",
    marginBottom: theme.spacing(2),
    marginRight: "auto",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    }
  },
  label: {
    color: "#888888",
    fontSize: "0.815rem"
  },
  labelBold: {
    color: "#888888",
    fontWeight: 600,
    fontSize: "0.815rem"
  },
  icon: {
    margin: theme.spacing(),
    fontSize: 32,
    color: theme.palette.secondary
  },
  grid: {
    display: "flex",
    alignItems: "center"
  },
  cardContent: {
    padding: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2)
    }
  },
  cardHeader: {
    paddingTop: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingBottom: theme.spacing(0.5),

    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2),
      paddingBottom: theme.spacing(0)
    }
  },
  disabled: {
    opacity: "1 !important"
  },
  email: {
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden"
  }
});

class Root extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nameOpen: false,
      emailOpen: false,
      phoneOpen: false,
      timeZoneOpen: false,
      passwordOpen: false
    };
  }

  handleNameToggle = () => {
    this.setState(state => ({ nameOpen: !state.nameOpen }));
  };

  handleEmailToggle = () => {
    this.setState(state => ({ emailOpen: !state.emailOpen }));
  };

  handlePhoneToggle = () => {
    this.setState(state => ({ phoneOpen: !state.phoneOpen }));
  };

  handleTimeZoneToggle = () => {
    this.setState(state => ({ timeZoneOpen: !state.timeZoneOpen }));
  };

  handlePasswordToggle = () => {
    this.setState(state => ({ passwordOpen: !state.passwordOpen }));
  };

  handleTendersNewSubscribeToggle = () => {
    const { user, changeProfile } = this.props;
    changeProfile({ tendersNewSubscription: !user.tendersNewSubscription });
  };

  handleTendersChangeSubscribeToggle = () => {
    const { user, changeProfile } = this.props;
    changeProfile({
      tendersChangeSubscription: !user.tendersChangeSubscription
    });
  };

  handleCancelClick = () => {
    const { cancelChangeProfile } = this.props;
    cancelChangeProfile();
  };

  render() {
    const {
      user,
      classes,
      intl,
      changeProfile,
      timeZones,
      fetchTimeZones,
      shouldFetchTimeZones,
      canChangeProfile
    } = this.props;
    const { formatMessage } = intl;
    const {
      nameOpen,
      emailOpen,
      phoneOpen,
      timeZoneOpen,
      passwordOpen
    } = this.state;

    const tariff =
      user.company.tariff && `${camelize(user.company.tariff)}Tariff`;

    const disabled = !canChangeProfile;

    return (
      <SimpleBar style={{ height: "100%" }}>
        <div className={classes.root}>
          <Card className={classes.profileCard}>
            <CardHeader
              title={formatMessage(messages.title)}
              className={classes.cardHeader}
            />
            <CardContent className={classes.cardContent}>
              <UserName
                open={nameOpen}
                value={user}
                onClose={this.handleNameToggle}
                change={changeProfile}
              />
              <UserEmail
                open={emailOpen}
                value={user}
                onClose={this.handleEmailToggle}
                change={changeProfile}
              />
              <UserPhone
                open={phoneOpen}
                value={user}
                onClose={this.handlePhoneToggle}
                change={changeProfile}
              />
              <UserTimeZone
                open={timeZoneOpen}
                selected={user.timeZone}
                onClose={this.handleTimeZoneToggle}
                items={timeZones}
                fetch={fetchTimeZones}
                shouldFetch={shouldFetchTimeZones}
                change={changeProfile}
              />
              <UserPassword
                open={passwordOpen}
                value={user}
                onClose={this.handlePasswordToggle}
                change={changeProfile}
              />
              <List>
                <ListItem
                  alignItems="center"
                  button
                  disabled={disabled}
                  classes={{ disabled: classes.disabled }}
                  onClick={this.handleNameToggle}
                >
                  <ListItemText
                    primary={
                      <Fragment>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={3} className={classes.grid}>
                            <Typography className={classes.label}>
                              <FormattedMessage
                                id="profile.name"
                                defaultMessage="Имя"
                              />
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={8} className={classes.grid}>
                            {user.surname} {user.name} {user.patronymic}
                          </Grid>
                        </Grid>
                      </Fragment>
                    }
                  />
                </ListItem>
                <ListItem
                  alignItems="center"
                  button
                  disabled={disabled}
                  classes={{ disabled: classes.disabled }}
                  onClick={this.handleEmailToggle}
                >
                  <ListItemText
                    primary={
                      <Fragment>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={3} className={classes.grid}>
                            <Typography className={classes.label}>
                              <FormattedMessage
                                id="profile.email"
                                defaultMessage="Электронная почта"
                              />
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={8} className={classes.grid}>
                            <div className={classes.email}>
                              {user.unconfirmedEmail || user.email}
                              {user.unconfirmedEmail && (
                                <div>
                                  <FormattedMessage
                                    id="profile.unconfirmed-email"
                                    defaultMessage="Подтвердите адрес электронной почты"
                                  />
                                </div>
                              )}
                            </div>
                          </Grid>
                        </Grid>
                      </Fragment>
                    }
                  />
                  {user.unconfirmedEmail && (
                    <ListItemSecondaryAction>
                      <Tooltip title={formatMessage(messages.cancelButton)}>
                        <IconButton onClick={this.handleCancelClick}>
                          <CloseIcon />
                        </IconButton>
                      </Tooltip>
                    </ListItemSecondaryAction>
                  )}
                </ListItem>
                <ListItem
                  alignItems="center"
                  button
                  disabled={disabled}
                  classes={{ disabled: classes.disabled }}
                  onClick={this.handlePhoneToggle}
                >
                  <ListItemText
                    primary={
                      <Fragment>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={3} className={classes.grid}>
                            <Typography className={classes.label}>
                              <FormattedMessage
                                id="profile.phone"
                                defaultMessage="Телефон"
                              />
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={8} className={classes.grid}>
                            {user.phone}
                          </Grid>
                        </Grid>
                      </Fragment>
                    }
                  />
                </ListItem>
                <ListItem
                  alignItems="center"
                  button
                  disabled={disabled}
                  classes={{ disabled: classes.disabled }}
                  onClick={this.handleTimeZoneToggle}
                >
                  <ListItemText
                    primary={
                      <Fragment>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={3} className={classes.grid}>
                            <Typography className={classes.label}>
                              <FormattedMessage
                                id="profile.time-zone"
                                defaultMessage="Часовой пояс"
                              />
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={8} className={classes.grid}>
                            {user.timeZoneTitle}
                          </Grid>
                        </Grid>
                      </Fragment>
                    }
                  />
                </ListItem>
                {!user.foreign && (
                  <ListItem
                    alignItems="center"
                    button
                    disabled={disabled}
                    classes={{ disabled: classes.disabled }}
                    onClick={this.handlePasswordToggle}
                  >
                    <ListItemText
                      primary={
                        <Fragment>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={3} className={classes.grid}>
                              <Typography className={classes.label}>
                                <FormattedMessage
                                  id="profile.password"
                                  defaultMessage="Пароль"
                                />
                              </Typography>
                            </Grid>
                            <Grid item xs={12} sm={8} className={classes.grid}>
                              ••••••••
                            </Grid>
                          </Grid>
                        </Fragment>
                      }
                    />
                  </ListItem>
                )}
                {!user.foreign && (
                  <ListItem alignItems="center">
                    <ListItemText
                      primary={
                        <Fragment>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={3} className={classes.grid}>
                              <Typography className={classes.label}>
                                <FormattedMessage
                                  id="profile.role"
                                  defaultMessage="Роль"
                                />
                              </Typography>
                            </Grid>
                            <Grid item xs={12} sm={8} className={classes.grid}>
                              {user.admin && (
                                <FormattedMessage
                                  id="profile.admin"
                                  defaultMessage="Администратор"
                                />
                              )}
                              {user.coordinator && (
                                <FormattedMessage
                                  id="profile.coordinator"
                                  defaultMessage="Координатор"
                                />
                              )}
                            </Grid>
                          </Grid>
                        </Fragment>
                      }
                    />
                  </ListItem>
                )}
                {!user.foreign && (
                  <ListItem alignItems="center">
                    <ListItemText
                      primary={
                        <Fragment>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={3} className={classes.grid}>
                              <Typography className={classes.label}>
                                <FormattedMessage
                                  id="profile.tariff"
                                  defaultMessage="Тариф"
                                />
                              </Typography>
                            </Grid>
                            <Grid item xs={12} sm={8} className={classes.grid}>
                              {tariff && formatMessage(messages[tariff])}
                            </Grid>
                          </Grid>
                        </Fragment>
                      }
                    />
                  </ListItem>
                )}
                {!user.foreign && (
                  <ListItem alignItems="center">
                    <ListItemText
                      primary={
                        <Fragment>
                          <Grid container spacing={2}>
                            <Grid item xs={12} sm={3} className={classes.grid}>
                              <Typography className={classes.label}>
                                <FormattedMessage
                                  id="profile.tariff-end-at"
                                  defaultMessage="Подписка до"
                                />
                              </Typography>
                            </Grid>
                            <Grid item xs={12} sm={8} className={classes.grid}>
                              <FormattedDate
                                id="tariff.end-at"
                                year="numeric"
                                month="long"
                                day="numeric"
                                value={user.company.tariffEndAt}
                              />
                            </Grid>
                          </Grid>
                        </Fragment>
                      }
                    />
                  </ListItem>
                )}
                {user.canSubscribe && (
                  <Fragment>
                    <ListSubheader
                      component="div"
                      id="nested-list-subheader"
                      className={classes.labelBold}
                    >
                      <FormattedMessage
                        id="profile.subscription"
                        defaultMessage="Настройка уведомлений"
                      />
                    </ListSubheader>
                    <ListItem alignItems="center">
                      <ListItemText
                        primary={
                          <Typography>
                            <FormattedMessage
                              id="profile.subscription-tenders-new"
                              defaultMessage="Ежедневная рассылка о новых аукционах"
                            />
                          </Typography>
                        }
                      />
                      <ListItemSecondaryAction>
                        <Switch
                          edge="end"
                          color="primary"
                          disabled={disabled}
                          onChange={this.handleTendersNewSubscribeToggle}
                          checked={user.tendersNewSubscription}
                        />
                      </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem alignItems="center">
                      <ListItemText
                        primary={
                          <Typography>
                            <FormattedMessage
                              id="profile.subscription-tenders-change"
                              defaultMessage="Ежедневная рассылка об изменениях в тендерах"
                            />
                          </Typography>
                        }
                      />
                      <ListItemSecondaryAction>
                        <Switch
                          edge="end"
                          color="primary"
                          disabled={disabled}
                          onChange={this.handleTendersChangeSubscribeToggle}
                          checked={user.tendersChangeSubscription}
                        />
                      </ListItemSecondaryAction>
                    </ListItem>
                  </Fragment>
                )}
              </List>
            </CardContent>
          </Card>
        </div>
      </SimpleBar>
    );
  }
}

export default injectIntl(withStyles(styles)(Root));
