import React, { Component, Fragment } from "react";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
  TextField,
  withStyles
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import withWidth from "@material-ui/core/withWidth";

import Form from "../../../common/Form/Form";

const messages = defineMessages({
  passwordField: {
    id: "dashboard.user-password.password-field",
    defaultMessage: "Пароль"
  },
  passwordConfirmationField: {
    id: "dashboard.user-password.password-confirmation-field",
    defaultMessage: "Подтвердите пароль"
  },
  currentPasswordField: {
    id: "dashboard.user-password.current-password-field",
    defaultMessage: "Текущий пароль"
  }
});

const styles = theme => ({
  paper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(70)
    }
  },
  hidden: {
    display: "none"
  }
});

export class UserPassword extends Component {
  handleClose = () => {
    this.props.onClose();
  };

  handleFormAction = (value, form) => {
    const { change } = this.props;
    const { currentPassword, password, passwordConfirmation } = value;
    change({ currentPassword, password, passwordConfirmation }, form);
  };

  render() {
    const { classes, open, intl, value, width } = this.props;
    const { formatMessage } = intl;

    return (
      <Dialog
        fullScreen={width === "xs"}
        open={open}
        onClose={this.handleClose}
        aria-labelledby="title"
        classes={{ paper: classes.paper }}
      >
        <DialogTitle id="title">
          <FormattedMessage
            id="dashboard.user-password.title"
            defaultMessage="Изменение пароля"
          />
        </DialogTitle>
        <Form
          action={this.handleFormAction}
          value={value}
          onSubmitted={this.handleClose}
        >
          {({ field, getValue, handleInputChange, error }) => (
            <Fragment>
              {/* Fix Chrome issue: [DOM] Password forms should have (optionally hidden) username fields for accessibility: (More info: https://goo.gl/9p2vKq)*/}
              <input
                type="text"
                name="email"
                readOnly
                value={value.email}
                className={classes.hidden}
                autoComplete="email"
              />
              <DialogContent>
                <TextField
                  name="currentPassword"
                  type="password"
                  inputRef={field}
                  value={getValue("currentPassword")}
                  onChange={handleInputChange}
                  required
                  fullWidth
                  autoComplete="current-password"
                  label={formatMessage(messages.currentPasswordField)}
                  error={!!error("currentPassword")}
                  helperText={error("currentPassword")}
                  InputLabelProps={{ required: false }}
                />
                <TextField
                  name="password"
                  type="password"
                  inputRef={field}
                  value={getValue("password")}
                  onChange={handleInputChange}
                  required
                  fullWidth
                  autoComplete="new-password"
                  label={formatMessage(messages.passwordField)}
                  error={!!error("password")}
                  helperText={error("password")}
                  InputLabelProps={{ required: false }}
                />
                <TextField
                  name="passwordConfirmation"
                  type="password"
                  inputRef={field}
                  value={getValue("passwordConfirmation")}
                  onChange={handleInputChange}
                  required
                  fullWidth
                  autoComplete="new-password"
                  label={formatMessage(messages.passwordConfirmationField)}
                  error={!!error("passwordConfirmation")}
                  helperText={error("passwordConfirmation")}
                  InputLabelProps={{ required: false }}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  <FormattedMessage
                    id="dashboard.user-password.cancel"
                    defaultMessage="Отмена"
                  />
                </Button>
                <Button type="submit" color="primary" autoFocus>
                  <FormattedMessage
                    id="dashboard.user-password.change"
                    defaultMessage="Изменить"
                  />
                </Button>
              </DialogActions>
            </Fragment>
          )}
        </Form>
      </Dialog>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(UserPassword)));
