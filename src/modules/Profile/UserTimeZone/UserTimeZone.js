import React, { Component } from "react";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  List,
  ListItem,
  ListItemText,
  Button,
  withStyles
} from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";
import withWidth from "@material-ui/core/withWidth";
import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";
import "../../../common/TimeZones/TimeZonesSagas";

const styles = theme => ({
  paper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(70)
    }
  }
});

export class UserTimeZone extends Component {
  handleEnter = () => {
    const { fetch, shouldFetch } = this.props;
    if (shouldFetch) {
      fetch();
    }
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleAction = item => {
    const { change } = this.props;
    if (item.name !== this.props.selected) {
      change({ timeZone: item.name, timeZoneTitle: item.title });
    }
    this.props.onClose();
  };

  render() {
    const { classes, open, items, selected, width } = this.props;

    return (
      <Dialog
        open={open}
        onClose={this.handleClose}
        onEnter={this.handleEnter}
        aria-labelledby="title"
        classes={{ paper: classes.paper }}
        fullScreen={width === "xs"}
      >
        <DialogTitle id="title">
          <FormattedMessage
            id="dashboard.user-time-zone.title"
            defaultMessage="Изменение временной зоны"
          />
        </DialogTitle>
        <DialogContent>
          <SimpleBar style={{ height: "100%" }}>
            <List>
              {items.map(item => {
                return (
                  <ListItem
                    button
                    key={item.name}
                    selected={selected === item.name}
                    onClick={() => this.handleAction(item)}
                  >
                    <ListItemText primary={item.title} />
                  </ListItem>
                );
              })}
            </List>
          </SimpleBar>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            <FormattedMessage
              id="dashboard.user-time-zone.cancel"
              defaultMessage="Отмена"
            />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(UserTimeZone)));
