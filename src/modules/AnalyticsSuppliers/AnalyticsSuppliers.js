import React, { Component } from "react";
import SimpleBar from "simplebar-react";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";

import FilterForm from "./FilterForm/FilterForm";
import FilterDescriptionContainer from "../Analytics/FilterDescription/FilterDescriptionContainer";
import SupplierDescriptionContainer from "./SupplierDescription/SupplierDescriptionContainer";
import TendersSummaryContainer from "./TendersSummary/TendersSummaryContainer";
// import ControlResultsContainer from "./ControlResults/ControlResultsContainer";
import PurchasesSummaryContainer from "./PurchasesSummary/PurchasesSummaryContainer";
import FilterDescription from "./FilterDescription/FilterDescription";

const messages = defineMessages({
  helpButtonBody: {
    id: "analytics-suppliers.filter-description.help-button-body",
    defaultMessage: `Вы получаете аналитику тендерной деятельности поставщика <strong>за последний год</strong> в рамках закупок по 44 ФЗ и 223 ФЗ (ЕИС). Результаты поиска показывают статистику по завершенным закупкам поставщика, где есть победитель.`
  }
});

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    height: "100%",
    overflowY: "auto",

    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    }
  }
});

export class AnalyticsSuppliers extends Component {
  render() {
    const {
      filter,
      editing,
      entity,
      entityPath,
      defaultFilter,
      intl,
      classes
    } = this.props;
    const { formatMessage } = intl;

    return (
      <SimpleBar style={{ height: "100%" }}>
        <div className={classes.root}>
          {editing && (
            <FilterForm
              entity={entity}
              path={entityPath}
              defaultFilter={defaultFilter}
            />
          )}
          {!editing && (
            <>
              <FilterDescriptionContainer
                entity={entity}
                path={entityPath}
                defaultFilter={defaultFilter}
                component={FilterDescription}
                helpButtonBody={formatMessage(messages.helpButtonBody)}
              />
              <SupplierDescriptionContainer
                filter={filter}
                variant="filled"
                autofetch
              />
              <TendersSummaryContainer
                filter={filter}
                variant="filled"
                autofetch
              />
              <PurchasesSummaryContainer
                filter={filter}
                variant="filled"
                autofetch
              />
            </>
          )}
        </div>
      </SimpleBar>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsSuppliers));
