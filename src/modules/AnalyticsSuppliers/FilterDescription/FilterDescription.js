import React, { Component } from "react";
import { Typography, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { grey } from "@material-ui/core/colors";

import ArrayFieldDescription from "../../Dashboard/FilterDescription/ArrayFieldDescription";

const messages = defineMessages({
  suppliers: {
    id: "analytics-suppliers.filter-description.suppliers",
    defaultMessage: "по поставщику:"
  }
});

const styles = theme => ({
  root: {
    color: grey[600]
  }
});

class FilterDescriptionContent extends Component {
  render() {
    const { classes, value, intl } = this.props;
    const { formatMessage } = intl;

    const { suppliers } = value || {};

    return (
      <Typography className={classes.root}>
        <ArrayFieldDescription
          title={formatMessage(messages.suppliers)}
          value={suppliers}
        />
      </Typography>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterDescriptionContent));
