import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../../utils/CoreUtils";
import AutocompleteField from "../../../common/AutocompleteField/AutocompleteField";
import FilterFormContainer from "../../Analytics/FilterForm/FilterFormContainer";

const messages = defineMessages({
  suppliersField: {
    id: "analytics-suppliers.filter-form.suppliers-field",
    defaultMessage: "Укажите ИНН поставщика"
  },
  suppliersFieldHelpMessage: {
    id: "analytics-suppliers.filter-form.suppliers-field-help-message",
    defaultMessage: "Введите ИНН организации-поставщика"
  },
  helpButtonBody: {
    id: "analytics-suppliers.filter-form.help-button-body",
    defaultMessage: `Вы получаете аналитику тендерной деятельности поставщика <strong>за последний год</strong> в рамках закупок по 44 ФЗ и 223 ФЗ (ЕИС). Результаты поиска показывают статистику по завершенным закупкам поставщика, где есть победитель.`
  }
});

class FilterForm extends Component {
  validate(field, value, form) {
    if (field === "suppliers" && isEmpty(value)) {
      form.setErrors({ suppliers: "не может быть пустым" });
      return false;
    }
    return true;
  }

  render() {
    const { entity, path, defaultFilter, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <FilterFormContainer
        entity={entity}
        path={path}
        defaultFilter={defaultFilter}
        helpButtonBody={formatMessage(messages.helpButtonBody)}
        validate={this.validate}
      >
        {({ field, getValue, handleChange, error }) => (
          <AutocompleteField
            fullWidth
            multiselected={false}
            label={formatMessage(messages.suppliersField)}
            name="suppliers"
            ref={field}
            value={getValue("suppliers")}
            onChange={value => handleChange("suppliers", value)}
            helpButtonBody={formatMessage(messages.suppliersFieldHelpMessage)}
            error={!!error("suppliers")}
            helperText={error("suppliers")}
          />
        )}
      </FilterFormContainer>
    );
  }
}

export default injectIntl(FilterForm);
