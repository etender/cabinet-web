import React from "react";
import { connect } from "react-redux";

import {
  getFilter,
  getIsEditing
} from "../Analytics/FilterBuilder/FilterBuilderSelectors";
import {
  SUPPLIERS_NAME,
  SUPPLIERS_PATH,
  SUPPLIERS_DEFAULT_FILTER
} from "./AnalyticsSuppliersConstants";
import AnalyticsSupplier from "./AnalyticsSuppliers";

const AnalyticsSupplierContainer = props => <AnalyticsSupplier {...props} />;

const mapStateToProps = state => {
  const filter = getFilter(state, SUPPLIERS_NAME);
  return {
    filter,
    editing: getIsEditing(state, SUPPLIERS_NAME),
    entity: SUPPLIERS_NAME,
    entityPath: SUPPLIERS_PATH,
    defaultFilter: SUPPLIERS_DEFAULT_FILTER
  };
};

export default connect(mapStateToProps)(AnalyticsSupplierContainer);
