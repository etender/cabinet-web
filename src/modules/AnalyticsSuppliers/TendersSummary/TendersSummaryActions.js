import {
  fetchEntity,
  cleanEntity
} from "../../../services/Entities/EntitiesActions";
import { ENTITY } from "./TendersSummaryConstants";

export const fetchTendersSummary = query => fetchEntity(ENTITY, query);

export const cleanTendersSummary = () => cleanEntity(ENTITY);
