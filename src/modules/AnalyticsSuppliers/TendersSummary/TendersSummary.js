import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../../utils/CoreUtils";
import Section from "../../../common/Section/Section";
import { normalizeFilter } from "../../Tender/Filter/FilterHelpers";
import TendersSummaryCard from "../../AnalyticsCustomers/TendersSummary/TendersSummaryCard";
import TendersSummarySkeleton from "./TendersSummarySkeleton";

const messages = defineMessages({
  title: {
    id: "analytics-suppliers.tenders-summary.title",
    defaultMessage: "Статистика по поставщику"
  },
  helpButtonBody: {
    id: "analytics-suppliers.tenders-summary.help-button-body",
    defaultMessage: `Отчет наглядно покажет тендерную деятельность поставщика за последний год в рамках закупок по 44 ФЗ и 223 ФЗ (ЕИС): 
    <ul>
      <li>объем дохода,</li>
      <li>количество участий и побед,</li>
      <li>статистику по допускам и снижению цены,</li>
      <li>количество заказчиков, с которыми были заключены контракты.</li>
    </ul>`
  },
  contracts: {
    id: "analytics-suppliers.tenders-summary.contracts",
    defaultMessage: "контракта на<br/> {sum, number, rub}"
  },
  participationCount: {
    id: "analytics-suppliers.tenders-summary.participation-count",
    defaultMessage: "участий в торгах"
  },
  averagePriceDrop: {
    id: "analytics-suppliers.tenders-summary.average-price-drop",
    defaultMessage: "среднее снижение <br/>цены"
  },
  customersCount: {
    id: "analytics-suppliers.tenders-summary.customers-count",
    defaultMessage: "заказчика"
  },
  averageSuppliersCount: {
    id: "analytics-suppliers.tenders-summary.average-suppliers-count",
    defaultMessage: "среднее количество <br/>участников"
  },
  participationAdmission: {
    id: "analytics-suppliers.tenders-summary.participation-admission",
    defaultMessage: "процент допуска"
  }
});

const styles = theme => ({
  summary: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap"
  },
  summaryItem: {
    marginRight: theme.spacing(3),
    marginBottom: theme.spacing(3),
    minWidth: "175px"
  },
  cardContent: {
    paddingBottom: "0 !important"
  }
});

export class TendersSummary extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
  };

  componentDidMount() {
    const { autofetch, cleanTendersSummary } = this.props;
    if (autofetch) {
      cleanTendersSummary();
      this.fetch();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { filter, cleanTendersSummary } = this.props;

    if (isEmpty(filter)) {
      if (!this.cleaned) {
        this.cleaned = true;
        cleanTendersSummary();
      }

      return;
    }

    if (prevProps.filter !== filter) {
      this.fetch();
    }
  }

  fetch() {
    const { fetchTendersSummary, filter } = this.props;

    if (isEmpty(filter)) {
      return;
    }

    fetchTendersSummary(this.buildQuery());
  }

  buildQuery() {
    const { filter } = this.props;

    return {
      search: normalizeFilter(filter)
    };
  }

  render() {
    const { summary, classes, loaded, variant, intl } = this.props;
    const { formatMessage, formatNumber } = intl;

    return (
      <Section
        variant={variant}
        title={formatMessage(messages.title)}
        helpButtonBody={formatMessage(messages.helpButtonBody)}
        classes={{ content: classes.cardContent }}
      >
        {!loaded && <TendersSummarySkeleton />}
        {loaded && (
          <div className={classes.summary}>
            {summary &&
              Object.keys(summary).map(key => (
                <div key={key} className={classes.summaryItem}>
                  <TendersSummaryCard
                    value={formatNumber(summary[key]["value"], {
                      style: "decimal"
                    })}
                    title={formatMessage(messages[key], {
                      sum: summary[key]["sum"]
                    })}
                    unit={summary[key]["unit"]}
                    key={key}
                  />
                </div>
              ))}
          </div>
        )}
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(TendersSummary));
