import { getEntity } from "../../../services/Entities/EntitiesSelectors";
import { getEntityLoaded } from "../../../services/Entities/Entity/EntitySelectors";
import { ENTITY } from "./TendersSummaryConstants";

export const getTendersSummary = state => getEntity(state, ENTITY);
export const getTendersSummaryLoaded = state => getEntityLoaded(state, ENTITY);
