import React, { Component } from "react";
import PropTypes from "prop-types";
import { Typography, withStyles } from "@material-ui/core";

const styles = theme => ({
  root: {
    position: "relative"
  },
  value: {
    fontSize: "3.34em",
    color: "#0071B8",
    fontWeight: "500",
    lineHeight: "1.2"
  },
  title: {
    maxWidth: "159px"
  },
  square: {
    top: "0",
    left: "0",
    width: "40px",
    height: "40px",
    position: "absolute",
    backgroundColor: "#E0F0FB"
  },
  valueBlock: {
    paddingLeft: "8px",
    paddingTop: "8px"
  },
  titleBlock: {
    position: "relative",
    zIndex: 2,
    paddingLeft: "8px"
  }
});

export class TendersSummaryCard extends Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  };

  render() {
    const { value, unit, title, classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.square} style={{ zIndex: 1 }} />
        <div
          className={classes.valueBlock}
          style={{ position: "relative", zIndex: 2 }}
        >
          <Typography className={classes.value}>
            {value} {unit}
          </Typography>
        </div>
        <div className={classes.titleBlock}>
          <Typography className={classes.title}>
            <span dangerouslySetInnerHTML={{ __html: title }} />
          </Typography>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TendersSummaryCard);
