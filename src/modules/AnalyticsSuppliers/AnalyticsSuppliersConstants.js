import { createSharedPath } from "../Shared/SharedHelpers";

export const SUPPLIERS_PATH = createSharedPath("/analytics/suppliers");
export const SUPPLIERS_NAME = "suppliers";
export const SUPPLIERS_DEFAULT_FILTER = { federalLaw: ["fl223", "fl44"] };
