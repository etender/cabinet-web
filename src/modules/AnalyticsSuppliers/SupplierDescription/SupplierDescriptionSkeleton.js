import React, { Component } from "react";
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableRow
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { injectIntl } from "react-intl";
import { defineMessages } from "react-intl.macro";

const messages = defineMessages({
  inn: {
    id: "analytics-suppliers.supplier-description-skeleton.inn",
    defaultMessage: "ИНН"
  },
  kpp: {
    id: "analytics-suppliers.supplier-description-skeleton.kpp",
    defaultMessage: "КПП"
  },
  ogrn: {
    id: "analytics-suppliers.supplier-description-skeleton.ogrn",
    defaultMessage: "ОГРН"
  },
  actions: {
    id: "analytics-suppliers.supplier-description-skeleton.actions",
    defaultMessage: "Действия"
  }
});

class SupplierDescriptionSkeleton extends Component {
  render() {
    const { intl } = this.props;
    const { formatMessage } = intl;

    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{formatMessage(messages.inn)}</TableCell>
            <TableCell>{formatMessage(messages.kpp) || "-"}</TableCell>
            <TableCell>{formatMessage(messages.ogrn) || "-"}</TableCell>
            <TableCell>{formatMessage(messages.actions) || "-"}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>
              <Skeleton width="100%" height="30%" />
            </TableCell>
            <TableCell>
              <Skeleton width="100%" height="30%" />
            </TableCell>
            <TableCell>
              <Skeleton width="100%" height="30%" />
            </TableCell>
            <TableCell>
              <Skeleton width="100%" height="30%" />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    );
  }
}

export default injectIntl(SupplierDescriptionSkeleton);
