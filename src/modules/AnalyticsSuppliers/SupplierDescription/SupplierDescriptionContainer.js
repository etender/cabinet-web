import React from "react";
import { connect } from "react-redux";

import SupplierDescription from "./SupplierDescription";
import {
  getSupplierDescription,
  getSupplierDescriptionLoading
} from "./SupplierDescriptionSelector";
import {
  fetchSupplierDescription,
  cleanSupplierDescription
} from "./SupplierDescriptionActions";

const SupplierDescriptionContainer = props => (
  <SupplierDescription {...props} />
);

const mapStateToProps = state => {
  return {
    supplier: getSupplierDescription(state),
    loaded: getSupplierDescriptionLoading(state)
  };
};

export default connect(mapStateToProps, {
  fetchSupplierDescription,
  cleanSupplierDescription
})(SupplierDescriptionContainer);
