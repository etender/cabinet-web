import {
  fetchEntity,
  cleanEntity
} from "../../../services/Entities/EntitiesActions";
import { ENTITY } from "./SupplierDescriptionConstants";

export const fetchSupplierDescription = query => fetchEntity(ENTITY, query);

export const cleanSupplierDescription = () => cleanEntity(ENTITY);
