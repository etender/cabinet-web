import React, { Component } from "react";
import {
  withStyles,
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableRow
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../../utils/CoreUtils";
import Section from "../../../common/Section/Section";
import { normalizeFilter } from "../../Tender/Filter/FilterHelpers";
import SupplierDescriptionSkeleton from "./SupplierDescriptionSkeleton";
import ReportButtonContainer from "../ReportButton/ReportButtonContainer";

const messages = defineMessages({
  title: {
    id: "analytics-suppliers.supplier-description.title",
    defaultMessage: "Поставщик"
  },
  inn: {
    id: "analytics-suppliers.supplier-description.inn",
    defaultMessage: "ИНН"
  },
  kpp: {
    id: "analytics-suppliers.supplier-description.kpp",
    defaultMessage: "КПП"
  },
  ogrn: {
    id: "analytics-suppliers.supplier-description.ogrn",
    defaultMessage: "ОГРН"
  },
  actions: {
    id: "analytics-suppliers.supplier-description.actions",
    defaultMessage: "Действия"
  }
});

const styles = theme => ({
  details: {
    width: "120px"
  }
});

export class SupplierDescription extends Component {
  componentDidMount() {
    const { autofetch, cleanSupplierDescription } = this.props;
    if (autofetch) {
      cleanSupplierDescription();
      this.fetch();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { filter, cleanSupplierDescription } = this.props;

    if (isEmpty(filter)) {
      if (!this.cleaned) {
        this.cleaned = true;
        cleanSupplierDescription();
      }

      return;
    }

    if (prevProps.filter !== filter) {
      this.fetch();
    }
  }

  fetch() {
    const { fetchSupplierDescription, filter } = this.props;

    if (isEmpty(filter)) {
      return;
    }

    fetchSupplierDescription(this.buildQuery());
  }

  buildQuery() {
    const { filter } = this.props;

    return {
      search: normalizeFilter(filter)
    };
  }

  render() {
    const { supplier, loaded, variant, classes, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <>
        {loaded && supplier && (
          <Section
            variant={variant}
            title={supplier.name || formatMessage(messages.title)}
          >
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell classes={{ root: classes.details }}>
                    {formatMessage(messages.inn)}
                  </TableCell>
                  <TableCell classes={{ root: classes.details }}>
                    {formatMessage(messages.kpp)}
                  </TableCell>
                  <TableCell classes={{ root: classes.details }}>
                    {formatMessage(messages.ogrn)}
                  </TableCell>
                  <TableCell>{formatMessage(messages.actions)}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>{supplier.inn || "—"}</TableCell>
                  <TableCell>{supplier.kpp || "—"}</TableCell>
                  <TableCell>{supplier.ogrn || "—"}</TableCell>
                  <TableCell>
                    {supplier.inn && (
                      <ReportButtonContainer inn={supplier.inn} />
                    )}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Section>
        )}
        {!loaded && (
          <Section variant={variant} title={formatMessage(messages.title)}>
            <SupplierDescriptionSkeleton />
          </Section>
        )}
      </>
    );
  }
}

export default injectIntl(withStyles(styles)(SupplierDescription));
