import { getEntity } from "../../../services/Entities/EntitiesSelectors";
import { getEntityLoaded } from "../../../services/Entities/Entity/EntitySelectors";
import { ENTITY } from "./SupplierDescriptionConstants";

export const getSupplierDescription = state => getEntity(state, ENTITY);

export const getSupplierDescriptionLoading = state =>
  getEntityLoaded(state, ENTITY);
