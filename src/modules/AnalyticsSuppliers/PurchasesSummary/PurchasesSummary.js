import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Link,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TableFooter,
  TablePagination,
  withStyles
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { FormattedDate, injectIntl } from "react-intl";

import { isPresent, isEmpty } from "../../../utils/CoreUtils";
import Section from "../../../common/Section/Section";
import ExportButtonContainer from "../../../common/ExportButton/ExportButtonContainer";
import { normalizeFilter } from "../../Tender/Filter/FilterHelpers";
import PurchasesSummaryHeader from "./PurchasesSummaryHeader";
import PurchasesSummarySkeleton from "./PurchasesSummarySkeleton";
import { EXPORT_ENTITY } from "./PurchasesSummaryConstants";

const messages = defineMessages({
  title: {
    id: "analytics-suppliers.purchases-summary.title",
    defaultMessage: "Анализ тендеров поставщика"
  },
  helpButtonBody: {
    id: "analytics-suppliers.purchases-summary.help-button-body",
    defaultMessage: `Отчет покажет перечень закупок по 44 ФЗ и 223 ФЗ (ЕИС), в которых победил поставщик <strong>за последний год</strong>. Поможет спрогнозировать его поведение в предстоящих тендерах, показав на какое снижение цены готов идти поставщик ради победы. А также позволит выявить Заказчиков, в  тендерах которых организация регулярно участвует.`
  }
});

const styles = theme => ({
  nowrapCell: {
    whiteSpace: "nowrap"
  }
});

export class PurchasesSummary extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
  };

  constructor(props) {
    super(props);

    this.query = {};

    this.state = {
      page: 0,
      orderBy: "published_at",
      order: "desc"
    };
  }

  componentDidMount() {
    const { autofetch, cleanPurchasesSummary } = this.props;
    if (autofetch) {
      cleanPurchasesSummary();
      this.fetch();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { filter, cleanPurchasesSummary } = this.props;
    const { page, orderBy, order } = this.state;

    if (isEmpty(filter)) {
      if (!this.cleaned) {
        this.cleaned = true;
        cleanPurchasesSummary();
      }

      return;
    }

    if (prevProps.filter !== filter) {
      this.fetch();
      return;
    }

    if (
      prevState.page !== page ||
      prevState.orderBy !== orderBy ||
      prevState.order !== order
    ) {
      this.fetch();
    }
  }

  fetch() {
    const { fetchPurchasesSummary, filter } = this.props;

    if (isEmpty(filter)) {
      return;
    }

    this.query = this.buildQuery();
    fetchPurchasesSummary(this.query);
  }

  buildQuery() {
    const { filter } = this.props;
    const { page, orderBy, order } = this.state;

    return {
      search: filter && normalizeFilter(filter),
      page,
      sort: `${orderBy} ${order}`
    };
  }

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  handleSort = (event, orderBy, order) => {
    this.setState({ orderBy, order });
  };

  render() {
    const {
      purchases: { items, total },
      loading,
      classes,
      variant,
      intl
    } = this.props;
    const perPage = 5;
    const { page, orderBy, order } = this.state;
    const { formatNumber, formatMessage } = intl;

    const hasPages = isPresent(total) && total > 0;

    return (
      <Section
        variant={variant}
        title={formatMessage(messages.title)}
        titleAction={
          <ExportButtonContainer name={EXPORT_ENTITY} query={this.query} />
        }
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        <TableContainer>
          <Table>
            <PurchasesSummaryHeader
              onSort={this.handleSort}
              orderBy={orderBy}
              order={order}
            />
            <TableBody>
              {loading && <PurchasesSummarySkeleton />}
              {!loading &&
                items &&
                items.map((item, index) => (
                  <TableRow key={index}>
                    <TableCell>
                      <Link
                        href={`/tender/#{"id":"${item.id}"}`}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {item.tenderName}
                      </Link>
                    </TableCell>
                    <TableCell align="right">
                      <span className={classes.nowrapCell}>
                        <FormattedDate
                          id="analytics-suppliers.purchases-summary.publishedAt"
                          year="numeric"
                          month="numeric"
                          day="numeric"
                          hour="numeric"
                          minute="numeric"
                          value={item.publishedAt}
                        />
                      </span>
                    </TableCell>
                    <TableCell align="right">
                      {formatNumber(item.supplierPrice, { style: "decimal" })}
                    </TableCell>
                    <TableCell align="right">
                      {item.priceDrop === 100 ? "-" : `${item.priceDrop}`}
                    </TableCell>
                    <TableCell align="right">{item.customerName}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
            {hasPages && total > 5 && (
              <TableFooter>
                <TableRow>
                  <TablePagination
                    colSpan={7}
                    count={total}
                    rowsPerPage={perPage}
                    rowsPerPageOptions={[perPage]}
                    page={page}
                    onChangePage={this.handleChangePage}
                  />
                </TableRow>
              </TableFooter>
            )}
          </Table>
        </TableContainer>
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(PurchasesSummary));
