import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  withStyles
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

const messages = defineMessages({
  tenderName: {
    id: "analytics-suppliers.purchases-summary-header.tender-name",
    defaultMessage: "Наименование закупки"
  },
  publishedAt: {
    id: "analytics-suppliers.purchases-summary-header.published-at",
    defaultMessage: "Дата публикации"
  },
  lotPrice: {
    id: "analytics-suppliers.purchases-summary-header.price",
    defaultMessage: "Сум. контракта"
  },
  priceDrop: {
    id: "analytics-suppliers.purchases-summary-header.price-drop",
    defaultMessage: "% снижения"
  },
  customerName: {
    id: "analytics-suppliers.purchases-summary-header.customer-name",
    defaultMessage: "Заказчик"
  }
});

const styles = theme => ({
  publishedAt: {
    width: 110
  },
  lotPrice: {
    width: 100
  },
  priceDrop: {
    width: 75
  },
  suppliersCount: {
    width: 75
  },
  tableSortLabel: {
    flexDirection: "initial"
  },
  tableSortLabelIcon: {
    marginRight: "0px"
  }
});

const cells = [
  { id: "tender_name", name: "tenderName" },
  { id: "published_at", name: "publishedAt" },
  { id: "lot_price", name: "lotPrice" },
  { id: "price_drop", name: "priceDrop" },
  { id: "customer_name", name: "customerName" }
];

const defaultOrder = "desc";
export class PurchasesSummaryHeader extends Component {
  static propTypes = {
    orderBy: PropTypes.string.isRequired,
    order: PropTypes.string.isRequired
  };

  createSortHandler(newOrderBy) {
    return event => {
      const { order, onSort, orderBy } = this.props;
      const newOrder =
        orderBy === newOrderBy
          ? order === defaultOrder
            ? "asc"
            : defaultOrder
          : defaultOrder;

      onSort(event, newOrderBy, newOrder);
    };
  }

  render() {
    const { orderBy, order, classes, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <TableHead>
        <TableRow>
          {cells.map(cell => (
            <TableCell
              key={cell.id}
              align={cell.name === "tenderName" ? "left" : "right"}
              className={classes[cell.name]}
              sortDirection={orderBy === cell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === cell.id}
                direction={orderBy === cell.id ? order : defaultOrder}
                onClick={this.createSortHandler(cell.id)}
                classes={{
                  root: classes.tableSortLabel,
                  icon: classes.tableSortLabelIcon
                }}
              >
                <span className={classes[cell.name]}>
                  {formatMessage(messages[cell.name])}
                </span>
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }
}

export default injectIntl(withStyles(styles)(PurchasesSummaryHeader));
