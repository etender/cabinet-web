import {
  createEntity,
  fetchEntity
} from "../../../services/Entities/Entity/EntityActions";
import { ENTITY } from "./ReportButtonConstants";

export const createReport = supplierNumber =>
  createEntity(ENTITY, { supplierNumber }, { binnary: true });

export const fetchReport = id => fetchEntity(ENTITY, { id }, { binnary: true });
