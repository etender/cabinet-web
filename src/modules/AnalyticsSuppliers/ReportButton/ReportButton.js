import React, { Component } from "react";
import { Button, CircularProgress, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";
import GetAppIcon from "@material-ui/icons/GetApp";

const messages = defineMessages({
  title: {
    id: "analytics-suppliers.report-button.title",
    defaultMessage: "Скачать подробную справку в PDF"
  },
  error: {
    id: "analytics-suppliers.report-button.error",
    defaultMessage: "Произошла ошибка во время генерации отчета"
  },
  authorizationError: {
    id: "analytics-suppliers.report-button.authorization-error",
    defaultMessage: "Доступ к ресурсу ограничен правами"
  },
  authenticationError: {
    id: "analytics-suppliers.report-button.authentication-error",
    defaultMessage: "Запрос не авторизован"
  },
  notFoundError: {
    id: "analytics-suppliers.report-button.authentication-error",
    defaultMessage: "Организация не найдена"
  }
});

const styles = theme => ({
  wrapper: {
    position: "relative",
    display: "inline-block"
  },
  progress: {
    position: "absolute",
    top: 2,
    left: 0,
    zIndex: 1
  }
});

export class ReportButton extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { task, loading } = this.props;

    // Server error response
    if (prevProps.loading !== loading && loading === false && !task) {
      this.noticeError();
    }

    if (prevProps.task !== task && task) {
      const {
        fetchReport,
        task: { id, retryAfter, error, status }
      } = this.props;

      if (error) {
        this.noticeError(status);
        return;
      }

      setTimeout(() => {
        fetchReport(id);
      }, retryAfter * 1000);
    }
  }

  noticeError(status = 0) {
    const { showSnackbar, intl } = this.props;
    const { formatMessage } = intl;

    switch (status) {
      case 403:
        showSnackbar(formatMessage(messages.authorizationError));
        break;
      case 401:
        showSnackbar(formatMessage(messages.authenticationError));
        break;
      case 404:
      case 422:
        showSnackbar(formatMessage(messages.notFoundError));
        break;
      default:
        showSnackbar(formatMessage(messages.error));
        break;
    }
  }

  handleClick = () => {
    const { createReport, inn } = this.props;
    createReport(inn);
  };

  render() {
    const { classes, intl, task } = this.props;
    const { formatMessage } = intl;

    const loading = this.props.loading || (task && task.processing);

    return (
      <div className={classes.wrapper}>
        <Button
          color="primary"
          startIcon={<GetAppIcon />}
          onClick={this.handleClick}
          disabled={loading}
        >
          {formatMessage(messages.title)}
        </Button>
        {loading && <CircularProgress size={28} className={classes.progress} />}
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(ReportButton));
