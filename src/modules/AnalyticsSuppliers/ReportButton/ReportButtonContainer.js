import React from "react";
import { connect } from "react-redux";

import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { createReport, fetchReport } from "./ReportButtonActions";
import ReportButton from "./ReportButton";
import {
  getEntity,
  getEntityLoading
} from "../../../services/Entities/Entity/EntitySelectors";
import { ENTITY } from "./ReportButtonConstants";

const ReportButtonContainer = props => <ReportButton {...props} />;

const mapStateToProps = state => {
  return {
    loading: getEntityLoading(state, ENTITY),
    task: getEntity(state, ENTITY)
  };
};

export default connect(mapStateToProps, {
  createReport,
  fetchReport,
  showSnackbar
})(ReportButtonContainer);
