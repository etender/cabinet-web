import { clearFilter } from "../Analytics/FilterBuilder/FilterBuilderActions";
import { SUPPLIERS_NAME } from "./AnalyticsSuppliersConstants";

export const cleanSuppliers = () => clearFilter(SUPPLIERS_NAME);
