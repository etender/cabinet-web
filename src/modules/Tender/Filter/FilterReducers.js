import { handleActions } from "redux-actions";

import { successApiType } from "../../../services/Api/ApiHelpers";
import { buildFilter } from "./FilterActions";

const initialState = {
  current: null,
  source: null
};

export default handleActions(
  {
    [buildFilter]: (state, action) => ({
      ...state,
      source: action.payload,
      current: null
    }),
    [successApiType(buildFilter)]: (state, action) => ({
      ...state,
      current: action.payload
    })
  },
  initialState
);
