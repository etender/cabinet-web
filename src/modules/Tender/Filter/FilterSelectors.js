import { createSelector } from "reselect";
import { getCurrentFilter as getFilter } from "../../Dashboard/Filter/FilterSelectors";
import { getCurrentTender } from "../TenderSelectors";
import { normalizeFilter } from "./FilterHelpers";

export const getCurrentFilter = state => state.tender.filter.current;

export const getTenderFilter = createSelector(
  getFilter,
  getCurrentTender,
  (filter, tender) => ({ ...normalizeFilter(filter), tenderId: tender.id })
);
