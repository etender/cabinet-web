import { createAction } from "redux-actions";

export const buildFilter = createAction("TENDER_FILTER_BUILD");
