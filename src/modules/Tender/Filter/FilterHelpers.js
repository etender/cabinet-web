export const normalizeFilter = ({
  customerInn,
  customers,
  suppliers,
  keywords,
  including,
  excluding,
  federalLaw,
  regions,
  okpds,
  categories,
  pubDateFrom,
  pubDateTo
}) => ({
  customerInn: customerInn,
  customers: customers && customers.map(customer => customer.id || customer),
  suppliers: suppliers && suppliers.map(supplier => supplier.id || supplier),
  keywords,
  including,
  excluding,
  federalLaw:
    federalLaw && federalLaw.filter(item => ["fl44", "fl223"].includes(item)),
  regions: regions && regions.map(region => region.id),
  okpds: okpds && okpds.map(okpd => okpd.id),
  categories: categories && categories.map(category => category.id),
  pubDateFrom,
  pubDateTo
});
