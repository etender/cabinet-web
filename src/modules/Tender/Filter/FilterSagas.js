import { takeLatest } from "redux-saga/effects";

import { buildFilter } from "./FilterActions";
import { createEntitySaga } from "../../../services/Entities/EntitiesSagas";

function* buildFilterSaga(action) {
  const { type, payload, meta } = action;
  yield createEntitySaga({
    type,
    payload: { filter: payload },
    meta: { ...meta, entity: "analytics/filters/customers" }
  });
}

export default function* filterSagas() {
  yield takeLatest(buildFilter, buildFilterSaga);
}
