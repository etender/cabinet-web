import React, { Component } from "react";
import { injectIntl } from "react-intl";
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableFooter,
  TablePagination,
  TableRow,
  withStyles
} from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";

import LotItemsPagination from "../LotItemsPagination/LotItemsPagination";

const styles = theme => ({
  root: {
    marginTop: theme.spacing(-2)
  },
  spacer: {
    flex: "1"
  }
});

export class LotItems extends Component {
  state = {
    lotItemsPage: 0
  };

  handleChangePage = (event, newPage) => {
    this.setState({ lotItemsPage: newPage });
  };

  render() {
    const { classes, items } = this.props;

    const { formatNumber } = this.props.intl;

    const page = this.state.lotItemsPage;
    const rowsPerPage = 5;

    return (
      <div className={classes.root}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableHeaderCell}>
                <FormattedMessage
                  id="tender.lot-item.name"
                  defaultMessage="Название"
                />
              </TableCell>
              <TableCell className={classes.tableHeaderCell} align="right">
                <FormattedMessage
                  id="tender.lot-item.quantity"
                  defaultMessage="Количество"
                />
              </TableCell>
              <TableCell className={classes.tableHeaderCell} align="right">
                <FormattedMessage
                  id="tender.lot-item.price"
                  defaultMessage="Цена"
                />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items &&
              items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((lot_item, index) => (
                  <TableRow key={index}>
                    <TableCell className={classes.tableBodyCell}>
                      {lot_item.name}
                    </TableCell>
                    <TableCell className={classes.tableBodyCell} align="right">
                      {lot_item.quantity}
                    </TableCell>
                    <TableCell className={classes.tableBodyCell} align="right">
                      {lot_item.price &&
                        formatNumber(lot_item.price, { style: "decimal" })}
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
          {items && items.length > rowsPerPage && (
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[]}
                  colSpan={3}
                  count={items.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  ActionsComponent={LotItemsPagination}
                  labelDisplayedRows={({ from, to, count }) =>
                    `${from}-${to} из ${count}`
                  }
                  classes={{ spacer: classes.spacer }}
                />
              </TableRow>
            </TableFooter>
          )}
        </Table>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(LotItems));
