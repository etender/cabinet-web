import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Typography,
  Grid,
  Link,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  withStyles,
  IconButton
} from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import withWidth from "@material-ui/core/withWidth";

import { injectIntl } from "react-intl";
import { Close } from "@material-ui/icons";

const styles = theme => ({
  dialog: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(70)
    }
  },
  dialogButton: {
    textAlign: "left"
  },
  label: {
    color: "#888888",
    fontSize: "0.815rem"
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
    [theme.breakpoints.down("xs")]: {
      top: "0px",
      right: "0px"
    }
  },
  actions: {
    padding: theme.spacing(3),
    paddingTop: 0
  }
});

class Customer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, item, width, organizer } = this.props;

    return (
      <div>
        <Link
          component="button"
          onClick={this.handleClickOpen}
          className={classes.dialogButton}
        >
          <Typography color="primary">{item.name}</Typography>
        </Link>
        <Dialog
          fullScreen={width === "xs"}
          open={this.state.open}
          onClose={this.handleClose}
          classes={{ paper: classes.dialog }}
        >
          <DialogTitle>
            <FormattedMessage
              id="tender.customer.contact-title"
              defaultMessage="Контактная информация"
            />
            <IconButton
              aria-label="Закрыть"
              className={classes.closeButton}
              onClick={this.handleClose}
            >
              <Close />
            </IconButton>
          </DialogTitle>
          <DialogContent>
            <Grid container spacing={2}>
              {item.name && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    {organizer ? (
                      <FormattedMessage
                        id="tender.customer.organizer-name"
                        defaultMessage="Организатор"
                      />
                    ) : (
                      <FormattedMessage
                        id="tender.customer.contact-name"
                        defaultMessage="Заказчик"
                      />
                    )}
                  </Typography>
                </Grid>
              )}
              {item.name && (
                <Grid item xs={12} sm={9}>
                  <Typography variant="body1">{item.name}</Typography>
                </Grid>
              )}
              {item.person && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-person"
                      defaultMessage="Контактное лицо"
                    />
                  </Typography>
                </Grid>
              )}
              {item.person && (
                <Grid item xs={12} sm={9}>
                  <Typography variant="body1">{item.person}</Typography>
                </Grid>
              )}
              {item.phone && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-phone"
                      defaultMessage="Телефон"
                    />
                  </Typography>
                </Grid>
              )}
              {item.phone && (
                <Grid item xs={12} sm={9}>
                  <Link href={"tel:" + item.phone}>
                    <Typography color="primary">{item.phone}</Typography>
                  </Link>
                </Grid>
              )}
              {item.email && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-email"
                      defaultMessage="Электропочта"
                    />
                  </Typography>
                </Grid>
              )}
              {item.email && (
                <Grid item xs={12} sm={9}>
                  <Link href={"mailto:" + item.email}>
                    <Typography color="primary">{item.email}</Typography>
                  </Link>
                </Grid>
              )}
              {item.postAddress && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-post-address"
                      defaultMessage="Почтовый адрес"
                    />
                  </Typography>
                </Grid>
              )}
              {item.postAddress && (
                <Grid item xs={12} sm={9}>
                  <Typography variant="body1">{item.postAddress}</Typography>
                </Grid>
              )}
              {item.inn && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-inn"
                      defaultMessage="ИНН"
                    />
                  </Typography>
                </Grid>
              )}
              {item.inn && (
                <Grid item xs={12} sm={9}>
                  <Typography variant="body1">{item.inn}</Typography>
                </Grid>
              )}
              {item.kpp && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-kpp"
                      defaultMessage="КПП"
                    />
                  </Typography>
                </Grid>
              )}
              {item.kpp && (
                <Grid item xs={12} sm={9}>
                  <Typography variant="body1">{item.kpp}</Typography>
                </Grid>
              )}
              {item.ogrn && (
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.label}>
                    <FormattedMessage
                      id="tender.customer.contact-ogrn"
                      defaultMessage="ОГРН"
                    />
                  </Typography>
                </Grid>
              )}
              {item.ogrn && (
                <Grid item xs={12} sm={9}>
                  <Typography variant="body1">{item.ogrn}</Typography>
                </Grid>
              )}
            </Grid>
          </DialogContent>
          <DialogActions className={classes.actions} />
        </Dialog>
      </div>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(Customer)));
