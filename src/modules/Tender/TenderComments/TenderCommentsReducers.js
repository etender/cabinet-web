import { handleActions } from "redux-actions";

import { isEmpty } from "../../../utils/CoreUtils";
import { successApiType } from "../../../services/Api/ApiHelpers";
import {
  fetchComments,
  fetchMoreComments,
  addComment,
  changeComment,
  deleteComment
} from "./TenderCommentsActions";

const initialState = {
  items: [],
  hasMore: false,
  total: null
};

export default handleActions(
  {
    [fetchComments]: (state, action) => {
      if (action.meta.refresh) {
        return state;
      }

      return {
        ...state,
        items: [],
        total: null
      };
    },

    [successApiType(fetchComments)]: (state, action) => ({
      ...state,
      items: action.payload.items,
      hasMore: action.payload.items.length === 25,
      total: action.payload.meta.total
    }),

    [successApiType(fetchMoreComments)]: (state, action) => ({
      ...state,
      items: [...state.items, ...action.payload.items],
      hasMore: action.payload.items.length > 0,
      total: action.payload.meta.total
    }),

    [successApiType(addComment)]: (state, action) => {
      if (isEmpty(action.payload)) {
        return state;
      }

      return {
        ...state,
        items: [action.payload, ...state.items],
        total: state.total++
      };
    },

    [successApiType(changeComment)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.options.id
          ? { ...item, message: action.payload.query.message }
          : item
      )
    }),

    [successApiType(deleteComment)]: (state, action) => ({
      ...state,
      items: state.items.filter(item => item.id !== action.payload.id),
      total: state.total
    })
  },
  initialState
);
