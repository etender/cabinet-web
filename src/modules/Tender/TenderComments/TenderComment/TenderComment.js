import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Avatar,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Link,
  withStyles
} from "@material-ui/core";
import { FormattedMessage, defineMessages } from "react-intl.macro";
import { FormattedDate, FormattedTime, injectIntl } from "react-intl";
import TenderCommentTextField from "../TenderCommentTextField/TenderCommentTextField";

const messages = defineMessages({
  deleteCommentMessageTitle: {
    id: "tender.delete-comment.title",
    defaultMessage: "Удаление коментария"
  },
  deleteCommentMessage: {
    id: "tender.delete-comment.message",
    defaultMessage: "Коментарий будет удален."
  }
});

const styles = theme => ({
  message: {
    paddingLeft: theme.spacing(9)
  },
  link: {
    color: theme.palette.grey["600"],
    marginLeft: theme.spacing(2),
    fontSize: "1em",
    textDecoration: "underline"
  }
});

class TenderComment extends Component {
  static propTypes = {
    comment: PropTypes.object.isRequired,
    editing: PropTypes.bool.isRequired,
    onToggleEdit: PropTypes.func.isRequired,
    changeComment: PropTypes.func.isRequired
  };

  handleDeleteClick = () => {
    const { deleteComment, comment, confirm, intl } = this.props;
    const { formatMessage } = intl;

    confirm(
      formatMessage(messages.deleteCommentMessageTitle),
      formatMessage(messages.deleteCommentMessage),
      () => deleteComment(comment.id)
    );
  };

  handleEditClick = () => {
    const { onToggleEdit, comment } = this.props;
    onToggleEdit(comment);
  };

  handleChange = message => {
    const { changeComment, comment } = this.props;
    changeComment(comment.id, message);

    this.handleEditClick();
  };

  render() {
    const {
      classes,
      comment,
      canDeleteComment,
      canEditComment,
      editing
    } = this.props;

    return (
      <Fragment>
        <ListItem ContainerComponent="div">
          <ListItemAvatar>
            <Avatar
              aria-label={comment.author.fullName}
              src={comment.author.avatarImageUrl}
              className={classes.avatar}
            />
          </ListItemAvatar>
          <ListItemText
            primary={comment.author.fullName}
            secondary={
              <Fragment>
                <FormattedDate
                  year="numeric"
                  month="long"
                  day="numeric"
                  value={comment.createdAt}
                />
                <FormattedTime value={comment.createdAt} />
                {canDeleteComment && (
                  <Link
                    component="button"
                    aria-label="delete"
                    onClick={this.handleDeleteClick}
                    className={classes.link}
                  >
                    <FormattedMessage
                      id="tender.render-comment.delete"
                      defaultMessage="Удалить"
                    />
                  </Link>
                )}
                {canEditComment && (
                  <Link
                    component="button"
                    aria-label="edit"
                    onClick={this.handleEditClick}
                    className={classes.link}
                  >
                    <FormattedMessage
                      id="tender.render-comment.edit"
                      defaultMessage="Редактировать"
                    />
                  </Link>
                )}
              </Fragment>
            }
          />
        </ListItem>
        {editing && (
          <TenderCommentTextField
            value={comment.message}
            onChange={this.handleChange}
          />
        )}
        {!editing && comment.readOnly && (
          <div
            className={classes.message}
            dangerouslySetInnerHTML={{ __html: comment.message }}
          ></div>
        )}
        {!editing && !comment.readOnly && (
          <div className={classes.message}>{comment.message}</div>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(TenderComment));
