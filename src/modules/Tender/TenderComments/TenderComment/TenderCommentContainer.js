import React from "react";
import { connect } from "react-redux";

import TenderComment from "./TenderComment";
import { changeComment, deleteComment } from "../TenderCommentsActions";
import { canDeleteComment, canEditComment } from "../TenderCommentsSelectors";
import { confirm } from "../../../../common/ConfirmationDialog/ConfiramtionActions";

const TenderCommentContainer = props => <TenderComment {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { comment } = ownProps;
  return {
    canEditComment: canEditComment(state, comment),
    canDeleteComment: canDeleteComment(state, comment)
  };
};

export default connect(mapStateToProps, {
  changeComment,
  confirm,
  deleteComment
})(TenderCommentContainer);
