import React from "react";
import { connect } from "react-redux";

import TenderComments from "./TenderComments";
import {
  getComments,
  getTotalComments,
  canReadComment
} from "./TenderCommentsSelectors";

const TenderCommentsContainer = props => <TenderComments {...props} />;

const mapStateToProps = state => {
  return {
    comments: getComments(state),
    total: getTotalComments(state),
    canReadComment: canReadComment(state)
  };
};

export default connect(mapStateToProps)(TenderCommentsContainer);
