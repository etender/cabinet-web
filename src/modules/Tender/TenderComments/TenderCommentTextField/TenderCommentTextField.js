import React, { Component } from "react";
import PropTypes from "prop-types";
import { IconButton, TextField } from "@material-ui/core";
import { Send as SendIcon } from "@material-ui/icons";
import { isEmpty } from "../../../../utils/CoreUtils";

class TenderCommentTextField extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string
  };

  constructor(props) {
    super(props);

    const { value } = this.props;
    this.state = { value: value || "" };
  }

  handleChange = event => {
    const value = event.target.value;
    this.setState({ value });
  };

  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.handleSend();
    }
  };

  handleSend = () => {
    const { value } = this.state;
    if (isEmpty(value)) {
      return;
    }

    this.setState({ value: "" });

    const { onChange } = this.props;
    onChange(value);
  };

  render() {
    const { placeholder } = this.props;
    const { value } = this.state;

    return (
      <TextField
        variant="outlined"
        fullWidth
        value={value}
        onChange={this.handleChange}
        placeholder={placeholder}
        onKeyPress={this.handleKeyPress}
        InputProps={{
          endAdornment: (
            <IconButton
              aria-label="send"
              size="small"
              onClick={this.handleSend}
            >
              <SendIcon />
            </IconButton>
          )
        }}
      />
    );
  }
}

export default TenderCommentTextField;
