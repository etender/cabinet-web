import React, { Component } from "react";
import PropTypes from "prop-types";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Section from "../../../common/Section/Section";
import TenderCommentContainer from "./TenderComment/TenderCommentContainer";
import TenderCommentEditorContainer from "./TenderCommentEditor/TenderCommentEditorContainer";
import { isPresent } from "../../../utils/CoreUtils";

const messages = defineMessages({
  title: {
    id: "tender.tender-comments.title",
    defaultMessage: "Комментарии"
  }
});

class TenderComments extends Component {
  static propTypes = {
    scroller: PropTypes.object,
    comments: PropTypes.arrayOf(PropTypes.object.isRequired),
    total: PropTypes.number.isRequired,
    tenderId: PropTypes.string,
    variant: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      editingComment: null
    };
  }

  handleToggleEdit = comment => {
    this.setState(state => ({
      editingComment: state.editingComment === comment.id ? null : comment.id
    }));
  };

  render() {
    const { comments, variant, canReadComment, intl } = this.props;
    const { formatMessage } = intl;
    const { editingComment } = this.state;

    if (!canReadComment) {
      return "";
    }

    return (
      <Section
        title={isPresent(comments) && formatMessage(messages.title)}
        variant={variant}
      >
        <TenderCommentEditorContainer />
        {comments.map(comment => (
          <TenderCommentContainer
            key={comment.id}
            comment={comment}
            editing={editingComment === comment.id}
            onToggleEdit={this.handleToggleEdit}
          />
        ))}
      </Section>
    );
  }
}

export default injectIntl(TenderComments);
