import { createSelector } from "reselect";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { getCurrentTender, canChangeTender } from "../TenderSelectors";
import { getShared } from "../../Shared/SharedSelectors";

export const getCommentsSlice = state => state.tender.comments;

export const getComments = createSelector(
  getCommentsSlice,
  comments => comments.items
);

export const getTotalComments = createSelector(
  getCommentsSlice,
  comments => comments.total || 0
);

export const getHasMoreComments = createSelector(
  getCommentsSlice,
  comments => comments.hasMore
);

export const canReadComment = createSelector(getShared, shared => !shared);

export const canAddComment = createSelector(
  getCurrentTender,
  canChangeTender,
  (tender, canChangeTender) => tender.favorites && canChangeTender
);

const comment = (state, comment) => comment;

export const canDeleteComment = createSelector(
  getCurrentUser,
  getCurrentTender,
  comment,
  (user, tender, comment) =>
    tender.favorites &&
    !comment.readOnly &&
    (user.admin || user.id === comment.userId)
);

export const canEditComment = canDeleteComment;
