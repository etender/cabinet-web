import React from "react";
import { connect } from "react-redux";

import TenderCommentEditor from "./TenderCommentEditor";
import { getCurrentUser } from "../../../../services/Devise/DeviseSelectors";
import { getCurrentTender } from "../../TenderSelectors";
import { addComment } from "../TenderCommentsActions";
import { canAddComment } from "../TenderCommentsSelectors";

const TenderCommentEditorContainer = props => (
  <TenderCommentEditor {...props} />
);

const mapStateToProps = state => {
  return {
    tenderId: getCurrentTender(state).id,
    canAddComment: canAddComment(state),
    avatarUrl: getCurrentUser(state).avatarImageUrl
  };
};

export default connect(mapStateToProps, {
  addComment
})(TenderCommentEditorContainer);
