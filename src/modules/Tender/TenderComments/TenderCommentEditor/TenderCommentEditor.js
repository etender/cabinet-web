import React, { Component } from "react";
import PropTypes from "prop-types";
import { Avatar, Grid, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import TenderCommentTextField from "../TenderCommentTextField/TenderCommentTextField";

const messages = defineMessages({
  placeholder: {
    id: "tender.tender-comment-editor.placeholder",
    defaultMessage: "Отправьте сообщение"
  }
});

const styles = theme => ({
  avatar: {
    margin: theme.spacing(2)
  }
});

class TenderCommentEditor extends Component {
  static propTypes = {
    tenderId: PropTypes.string,
    avatarUrl: PropTypes.string,
    canAddComment: PropTypes.bool.isRequired
  };

  handleChange = message => {
    const { addComment, tenderId } = this.props;
    addComment(tenderId, message);
  };

  render() {
    const { classes, canAddComment, avatarUrl, intl } = this.props;
    const { formatMessage } = intl;

    if (!canAddComment) {
      return "";
    }

    return (
      <Grid container alignItems="center" wrap="nowrap">
        <Grid item>
          <Avatar src={avatarUrl} className={classes.avatar} />
        </Grid>
        <Grid item xs>
          <TenderCommentTextField
            placeholder={formatMessage(messages.placeholder)}
            onChange={this.handleChange}
          />
        </Grid>
      </Grid>
    );
  }
}

export default injectIntl(withStyles(styles)(TenderCommentEditor));
