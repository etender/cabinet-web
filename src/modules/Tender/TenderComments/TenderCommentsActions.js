import { createAction } from "redux-actions";

export const fetchComments = createAction(
  "TENDER_COMMENTS_FETCH",
  tenderId => ({ options: { tenderId } }),
  (tenderId, refresh) => ({ entity: "comments", refresh })
);

export const fetchMoreComments = createAction(
  "MORE_TENDER_COMMENTS_FETCH",
  query => query,
  () => ({ entity: "comments" })
);

export const addComment = createAction(
  "TENDER_COMMENTS_ADD",
  (tenderId, message) => ({
    options: { tenderId },
    query: { message }
  }),
  () => ({ entity: "comments" })
);

export const changeComment = createAction(
  "TENDER_COMMENT_CHANGE",
  (id, message) => ({
    options: { id },
    query: { message }
  }),
  () => ({ entity: "comments" })
);

export const deleteComment = createAction(
  "TENDER_COMMENT_DELETE",
  id => ({ id }),
  () => ({ entity: "comments" })
);
