import { all, put, takeLatest } from "redux-saga/effects";

import {
  fetchComments,
  fetchMoreComments,
  addComment,
  changeComment,
  deleteComment
} from "./TenderCommentsActions";
import {
  changeUser,
  changeState,
  addFavorites,
  removeFavorites
} from "../../Favorites/FavoritesActions";
import { successApiType } from "../../../services/Api/ApiHelpers";
import {
  fetchEntitiesSaga,
  createEntitySaga,
  updateEntitySaga,
  deleteEntitySaga
} from "../../../services/Entities/EntitiesSagas";

function* fetchCommentsSaga(action) {
  const { skipComments } = action.meta || {};
  if (skipComments === false) {
    return;
  }

  const id = action.payload.favoriteId || action.payload.id || action.payload;
  yield put(fetchComments(id, { refresh: true }));
}

export default function* tendersListSagas() {
  yield all([
    takeLatest(fetchComments, fetchEntitiesSaga),
    takeLatest(fetchMoreComments, fetchEntitiesSaga),
    takeLatest(addComment, createEntitySaga),
    takeLatest(changeComment, updateEntitySaga),
    takeLatest(deleteComment, deleteEntitySaga),
    takeLatest(
      [
        successApiType(addFavorites),
        successApiType(removeFavorites),
        successApiType(changeUser),
        successApiType(changeState)
      ],
      fetchCommentsSaga
    )
  ]);
}
