import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Box,
  Tab,
  Tabs,
  Badge,
  Typography,
  withStyles
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import InfiniteList from "../../common/InfiniteList/InfiniteList";
import LotsContainer from "./Lots/LotsContainer";
import TenderContentContainer from "./TenderContent/TenderContentContainer";
import UserChangeContainer from "../Favorites/UserChange/UserChangeContainer";
import StateChangeContainer from "../Favorites/StateChange/StateChangeContainer";
import TenderSkeleton from "./TenderSkeleton/TenderSkeleton";
import TenderCommentsContainer from "./TenderComments/TenderCommentsContainer";
import TenderCommentsButtonContainer from "./TenderCommentsButton/TenderCommentsButtonContainer";
import AnalyticsTabContainer from "./AnalyticsTab/AnalyticsTabContainer";

const messages = defineMessages({
  commonTab: {
    id: "tender.tender.common-tab",
    defaultMessage: "Извещение"
  },
  analyticTab: {
    id: "tender.tender.analytic-tab",
    defaultMessage: "Аналитика по заказчику"
  }
});

const styles = theme => ({
  header: {
    borderBottomColor: theme.palette.divider,
    borderBottomWidth: 1,
    borderBottomStyle: "solid"
  },
  container: {
    padding: theme.spacing(2),
    width: "100%",
    marginBottom: theme.spacing(3),
    marginRight: "auto",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    }
  },
  tabTitle: {
    textTransform: "none",
    fontSize: "17px"
  },
  tabRoot: {
    maxWidth: "none",
    minWidth: 0,
    overflow: "visible"
  },
  tabButtonsBox: {
    paddingTop: "10px"
  },
  tabBage: {
    borderRadius: "2px",
    right: "-23px"
  }
});

class Tender extends Component {
  static propTypes = {
    // Require to update InfinteList. It's needed to reattache scroll handler
    tender: PropTypes.object,
    fetchTender: PropTypes.func.isRequired,
    hasMoreComments: PropTypes.bool.isRequired,
    fetchComments: PropTypes.func.isRequired,
    fetchMoreComments: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.commentsRef = React.createRef();
    this.state = {
      tab: 0
    };
  }

  componentDidMount() {
    const { fetchTender, fetchComments, tender } = this.props;

    if (tender && tender.id) {
      fetchTender(tender.id);
      fetchComments(tender.id);
    }
  }

  handleLoadMore = page => {
    const { fetchMoreComments, tender } = this.props;
    fetchMoreComments({ tenderId: tender.id, page });
  };

  handleComments = () => {
    this.commentsRef.current.scrollIntoView();
  };

  handleTabChange = (event, value) => {
    this.setState({ tab: value });
  };

  render() {
    const { classes, loaded, hasMoreComments, tender, intl } = this.props;
    const { formatMessage } = intl;
    const { tab } = this.state;

    if (!loaded) {
      return <TenderSkeleton />;
    }

    return (
      <Fragment>
        <UserChangeContainer />
        <StateChangeContainer />
        <InfiniteList
          classes={{ container: classes.container }}
          loadMore={this.handleLoadMore}
          hasMore={hasMoreComments}
        >
          <Box display="flex" className={classes.header}>
            <Box flexGrow={1}>
              <Tabs
                value={tab}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                onChange={this.handleTabChange}
              >
                <Tab
                  label={
                    <Typography className={classes.tabTitle}>
                      {formatMessage(messages.commonTab)}
                    </Typography>
                  }
                  classes={{ root: classes.tabRoot }}
                />
                {tender.area === "zakupki_gov_ru" && (
                  <Tab
                    label={
                      <Badge
                        badgeContent={"new"}
                        color="secondary"
                        classes={{ badge: classes.tabBage }}
                      >
                        <Typography className={classes.tabTitle}>
                          {formatMessage(messages.analyticTab)}
                        </Typography>
                      </Badge>
                    }
                    classes={{ root: classes.tabRoot }}
                  />
                )}
              </Tabs>
            </Box>
            <Box className={classes.tabButtonsBox}>
              {tab === 0 && (
                <TenderCommentsButtonContainer commentsRef={this.commentsRef} />
              )}
            </Box>
          </Box>

          {tab === 0 && (
            <>
              <TenderContentContainer
                variant="filled"
                commentsRef={this.commentsRef}
              />
              <LotsContainer variant="filled" />
              <span ref={this.commentsRef} />
              <TenderCommentsContainer variant="filled" />
            </>
          )}

          <AnalyticsTabContainer active={tab === 1} variant="filled" />
        </InfiniteList>
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(Tender));
