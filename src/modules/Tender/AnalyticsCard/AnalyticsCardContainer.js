import React from "react";
import { connect } from "react-redux";

import AnalyticsCard from "./AnalyticsCard";
import { showTenderAnalytics, toggleTender } from "../TenderActions";
import { getCurrentFilter } from "../Filter/FilterSelectors";
import { clearFilter } from "../../Analytics/FilterBuilder/FilterBuilderActions";

const mapStateToProps = state => {
  return {
    filter: getCurrentFilter(state)
  };
};

const AnalyticsCardContainer = props => <AnalyticsCard {...props} />;

export default connect(mapStateToProps, {
  showTenderAnalytics,
  clearFilter,
  toggleTender
})(AnalyticsCardContainer);
