import React, { Component } from "react";
import { Button, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";
import analyticVisual from "../../../images/analytic_visual.png";

const styles = theme => ({
  root: {
    width: "192px",
    minWidth: "192px",
    padding: "12px 16px 16px 16px",
    borderRadius: theme.spacing(1),
    backgroundColor: "#E0F0FB",
    marginTop: "62px",
    marginLeft: theme.spacing(2)
  },
  analyticVisual: {
    display: "block",
    margin: "0 auto"
  },
  message: {
    marginTop: theme.spacing(1.5),
    marginBottom: theme.spacing(1.5),
    fontSize: "13px"
  }
});

export class AnalyticsCard extends Component {
  handleShareClick = () => {
    const {
      showTenderAnalytics,
      clearFilter,
      toggleTender,
      filter
    } = this.props;
    clearFilter("customers");
    showTenderAnalytics(filter);
    toggleTender();
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <img
          src={analyticVisual}
          alt=""
          width="155"
          className={classes.analyticVisual}
        />

        <div className={classes.message}>
          <FormattedMessage
            id="tender.analytics-card.message"
            defaultMessage="Для ручной настройки параметров выборки и доп. возожностей аналитики"
          />
        </div>

        <Button
          variant="contained"
          color="primary"
          onClick={this.handleShareClick}
        >
          <FormattedMessage
            id="tender.analytics-card.button"
            defaultMessage="Раздел аналитика"
          />
        </Button>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsCard));
