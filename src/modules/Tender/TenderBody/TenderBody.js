import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Grid, Link, Typography, withStyles } from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { FormattedDate, FormattedTime, injectIntl } from "react-intl";

import DocumentCard from "../DocumentCard/DocumentCard";
import Customer from "../Customer/Customer";
import { AccessTime } from "@material-ui/icons";

const messages = defineMessages({
  currency: {
    id: "tender.tender-body.currency",
    defaultMessage: "Российский рубль"
  },
  emptyPrice: {
    id: "tender.tender-body.empty-price",
    defaultMessage: "Уточнить цену закупки"
  },
  endAt: {
    id: "tender.tender-body.end-at",
    defaultMessage: "Прием заявок до"
  }
});

const styles = theme => ({
  label: {
    color: "#888888",
    fontSize: "0.815rem"
  },
  labelBold: {
    color: "#888888",
    fontWeight: 600,
    fontSize: "0.815rem"
  },
  price: {
    fontSize: "16px"
  },
  currency: {
    fontSize: "0.75rem",
    color: "#888888"
  },
  dateInfo: {
    verticalAlign: "middle",
    display: "inline-flex",
    marginTop: "-4px"
  },
  delimiter: {
    color: "#e0e0e0"
  }
});

class Tender extends Component {
  static propTypes = {
    tender: PropTypes.object.isRequired
  };

  renderField(name, value) {
    const { classes } = this.props;
    return (
      <Fragment>
        <Grid item xs={12} sm={3}>
          <Typography className={classes.label}>{name}</Typography>
        </Grid>
        <Grid item xs={12} sm={9}>
          {value}
        </Grid>
      </Fragment>
    );
  }

  renderBaseId() {
    const { tender, classes } = this.props;
    return (
      tender.baseId &&
      this.renderField(
        <FormattedMessage
          id="tender.tender-body.baseId"
          defaultMessage="№ извещения"
        />,
        <Typography className={classes.baseId}>{tender.baseId}</Typography>
      )
    );
  }

  renderPlanNumber() {
    const { tender, classes } = this.props;
    return (
      <>
        {tender.planNumber &&
          this.isFL44Plan() &&
          this.renderField(
            <FormattedMessage
              id="tender.tender-body.plan44-number"
              defaultMessage="№ план-графика"
            />,
            <Typography className={classes.planNumber}>
              {tender.planNumber}
            </Typography>
          )}
        {tender.planNumber &&
          this.isFL223Plan() &&
          this.renderField(
            <FormattedMessage
              id="tender.tender-body.plan223-number"
              defaultMessage="№ плана закупок"
            />,
            <Typography className={classes.planNumber}>
              {tender.planNumber}
            </Typography>
          )}
      </>
    );
  }

  renderPlanPositionNumber() {
    const { tender, classes } = this.props;
    return (
      <>
        {tender.baseId &&
          this.isFL44Plan() &&
          this.renderField(
            <FormattedMessage
              id="tender.tender-body.plan44-position-number"
              defaultMessage="№ позиции план-графика"
            />,
            <Typography className={classes.baseId}>{tender.baseId}</Typography>
          )}
        {tender.baseId &&
          this.isFL223Plan() &&
          this.renderField(
            <FormattedMessage
              id="tender.tender-body.plan223-position-number"
              defaultMessage="№ позиции плана закупок"
            />,
            <Typography className={classes.baseId}>{tender.baseId}</Typography>
          )}
      </>
    );
  }

  renderName() {
    const { tender, classes } = this.props;
    return (
      tender.name &&
      this.renderField(
        <FormattedMessage
          id="tender.tender-body.name"
          defaultMessage="Название"
        />,
        <Typography className={classes.name}>{tender.name}</Typography>
      )
    );
  }

  renderUrl() {
    const { tender } = this.props;
    return this.renderField(
      <FormattedMessage
        id="tender.tender-body.source"
        defaultMessage="Ссылка на источник"
      />,
      <Link href={tender.url} target="_blank" rel="noreferrer">
        <Typography component="span" color="primary">
          <FormattedMessage
            id="tender.tender-body.source_link"
            defaultMessage="Перейти к тендеру"
          />
        </Typography>
      </Link>
    );
  }

  renderAreaName() {
    const { tender, classes } = this.props;
    return this.renderField(
      <FormattedMessage
        id="tender.tender-body.area-name"
        defaultMessage="Площадка"
      />,
      <>
        <Typography component="span">{tender.areaName}</Typography>
        {tender.auctionAreaUrl && (
          <>
            <span className={classes.delimiter}>&nbsp;|&nbsp;</span>
            <Link
              href={tender.auctionAreaUrl}
              color="primary"
              target="_blank"
              rel="noreferrer"
            >
              <Typography component="span">{tender.auctionAreaName}</Typography>
            </Link>
          </>
        )}
      </>
    );
  }

  renderPrice() {
    const { tender, classes, intl } = this.props;
    const { formatNumber, formatMessage } = intl;
    return this.renderField(
      <FormattedMessage
        id="tender.tender-body.price"
        defaultMessage="Начальная цена"
      />,
      tender.price ? (
        <Typography variant="body1">
          <strong className={classes.price}>
            {formatNumber(tender.price, { style: "decimal" })}
          </strong>
          <span className={classes.currency}>
            &nbsp;
            {tender.currency
              ? tender.currency
              : formatMessage(messages.currency)}
          </span>
        </Typography>
      ) : (
        <Typography color="textSecondary">
          {formatMessage(messages.emptyPrice)}
        </Typography>
      )
    );
  }

  renderTypeName() {
    const { tender } = this.props;
    return this.renderField(
      <FormattedMessage
        id="tender.tender-body.type-name"
        defaultMessage="Форма проведения"
      />,
      <Typography variant="body1">
        {tender.typeName === null ? (
          <FormattedMessage
            id="tender.tender-body.type-unset"
            defaultMessage="Не установлена"
          />
        ) : (
          tender.typeName
        )}
      </Typography>
    );
  }

  renderDateTime(name, value) {
    const hours = new Date(value).getUTCHours();

    return (
      value &&
      this.renderField(
        name,
        <Typography variant="body1">
          <strong>
            <FormattedDate value={value} />
            {hours !== 0 && (
              <>
                &nbsp;
                <FormattedTime value={value} />
              </>
            )}
          </strong>
        </Typography>
      )
    );
  }

  renderEndAt() {
    const { tender, intl } = this.props;
    const { formatMessage } = intl;

    return (
      tender.stage === "tender" &&
      this.renderDateTime(formatMessage(messages.endAt), tender.endAt)
    );
  }

  renderEndAtWithTimeLeft() {
    const { tender, classes, intl } = this.props;
    const { formatMessage } = intl;
    const hours = new Date(tender.endAt).getUTCHours();

    return (
      tender.stage === "tender" &&
      this.renderField(
        formatMessage(messages.endAt),
        <>
          <Typography variant="body1">
            <strong>
              <FormattedDate value={tender.endAt} />
              {hours !== 0 && (
                <>
                  &nbsp;
                  <FormattedTime value={tender.endAt} />
                </>
              )}
            </strong>
            &nbsp;
            <span className={classes.dateInfo}>
              <AccessTime />
              &nbsp;
              <FormattedMessage
                id="tenders.tender-body.time-left"
                defaultMessage="Осталось"
              />
              &nbsp;
              {tender.timeLeft}
            </span>
          </Typography>
        </>
      )
    );
  }

  renderAuctionAt() {
    const { tender } = this.props;
    return this.renderDateTime(
      <FormattedMessage
        id="tender.tender-body.auction-at"
        defaultMessage="Срок проведения"
      />,
      tender.durationAt
    );
  }

  renderPurchaseGraphPlacing() {
    const { tender } = this.props;
    return (
      tender.stage === "purchase_plan" &&
      this.renderDateTime(
        <FormattedMessage
          id="tender.tender-body.purchase-graph-placing"
          defaultMessage="Срок осуществления закупки"
        />,
        tender.purchaseGraphPlacing
      )
    );
  }

  renderPurchaseGraphExecution() {
    const { tender } = this.props;
    return (
      tender.stage === "purchase_plan" &&
      this.renderDateTime(
        <FormattedMessage
          id="tender.tender-body.purchase-graph-execution"
          defaultMessage="Срок договора"
        />,
        tender.purchaseGraphExecution
      )
    );
  }

  renderCustomer() {
    const { tender } = this.props;
    return (
      tender.customer &&
      tender.customer.name &&
      this.renderField(
        <FormattedMessage
          id="tender.tender-body.customer"
          defaultMessage="Заказчик"
        />,
        <Customer item={tender.customer} />
      )
    );
  }

  renderOrganizer() {
    const { tender } = this.props;
    return (
      tender.organizer &&
      tender.organizer.name &&
      this.renderField(
        <FormattedMessage
          id="tender.tender-body.organizer"
          defaultMessage="Организатор"
        />,
        <Customer item={tender.organizer} organizer />
      )
    );
  }

  renderRegion() {
    const { tender } = this.props;
    return (
      tender.region &&
      this.renderField(
        <FormattedMessage
          id="tender.tender-body.region"
          defaultMessage="Регион"
        />,
        <Typography variant="body1">{tender.region}</Typography>
      )
    );
  }

  renderDocuments() {
    const { tender } = this.props;
    return (
      tender.documents &&
      this.renderField(
        <FormattedMessage
          id="tender.tender-body.documents"
          defaultMessage="Документы"
        />,
        tender.documents.map((item, index) => (
          <DocumentCard document={item} key={index} />
        ))
      )
    );
  }

  renderGuarantee(name, value) {
    const { tender, classes, intl } = this.props;
    const { formatNumber, formatMessage } = intl;
    return this.renderField(
      name,
      <Typography variant="body1">
        {formatNumber(value, { style: "decimal" })}
        &nbsp;
        <span className={classes.currency}>
          {tender.currency ? tender.currency : formatMessage(messages.currency)}
        </span>
      </Typography>
    );
  }

  renderGuaranteeApp() {
    const { tender } = this.props;
    return (
      tender.stage === "tender" &&
      tender.guaranteeApp &&
      this.renderGuarantee(
        <FormattedMessage
          id="tender.tender-body.provision-bit"
          defaultMessage="Обеспечение заявки"
        />,
        tender.lots[0].customerRequirements[0].guaranteeApp.amount
      )
    );
  }

  renderGuaranteeContract() {
    const { tender } = this.props;
    return (
      tender.stage === "tender" &&
      tender.guaranteeContract &&
      this.renderGuarantee(
        <FormattedMessage
          id="tender.tender-body.provision-contract"
          defaultMessage="Обеспечение контракта"
        />,
        tender.lots[0].customerRequirements[0].guaranteeContract.amount
      )
    );
  }

  isPlan() {
    const { tender } = this.props;
    return tender.stage === "purchase_plan";
  }

  isFL44Plan() {
    const { tender } = this.props;
    return tender.stage === "purchase_plan" && tender.federalLaw === "fl44";
  }

  isFL223Plan() {
    const { tender } = this.props;
    return tender.stage === "purchase_plan" && tender.federalLaw === "fl223";
  }

  showTimeLeft() {
    const { tender } = this.props;
    return (
      tender.stage === "tender" &&
      tender.status !== "completed" &&
      tender.status !== "canceled" &&
      tender.timeLeft
    );
  }

  render() {
    const { shared } = this.props;
    return (
      <Grid container spacing={2}>
        {this.isPlan() && this.renderPlanNumber()}
        {this.isPlan() && this.renderPlanPositionNumber()}
        {!this.isPlan() && this.renderBaseId()}
        {this.renderName()}
        {this.renderUrl()}
        {this.renderAreaName()}
        {this.renderTypeName()}
        {this.renderPrice()}
        {this.renderGuaranteeApp()}
        {this.renderGuaranteeContract()}
        {this.renderOrganizer()}
        {this.renderCustomer()}
        {this.renderRegion()}
        {this.showTimeLeft()
          ? this.renderEndAtWithTimeLeft()
          : this.renderEndAt()}
        {this.renderPurchaseGraphPlacing()}
        {this.renderPurchaseGraphExecution()}
        {this.renderAuctionAt()}
        {!shared && this.renderDocuments()}
      </Grid>
    );
  }
}

export default injectIntl(withStyles(styles)(Tender));
