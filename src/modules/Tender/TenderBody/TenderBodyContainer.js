import React from "react";
import { connect } from "react-redux";

import { getShared } from "../../Shared/SharedSelectors";
import TenderBody from "./TenderBody";
import { getCurrentTender } from "../TenderSelectors";

const TenderBodyContainer = props => <TenderBody {...props} />;

const mapStateToProps = state => {
  return {
    tender: getCurrentTender(state),
    shared: getShared(state)
  };
};

export default connect(mapStateToProps)(TenderBodyContainer);
