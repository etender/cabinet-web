import { combineReducers } from "redux";
import { handleActions } from "redux-actions";

import { requestApiType, successApiType } from "../../services/Api/ApiHelpers";
import { fetchTender, showTender } from "./TenderActions";
import { addFavorites, removeFavorites } from "../Favorites/FavoritesActions";
import { changeState } from "../Favorites/StateChange/StateChangeActions";
import { changeUser } from "../Favorites/UserChange/UserChangeActions";

import comments from "./TenderComments/TenderCommentsReducers";
import filter from "./Filter/FilterReducers";

const initialState = null;

const current = handleActions(
  {
    [showTender]: (state, action) => action.payload,

    [requestApiType(fetchTender)]: state => ({ ...state, loading: true }),

    [successApiType(fetchTender)]: (state, action) => ({
      ...action.payload,
      loading: false
    }),

    [successApiType(addFavorites)]: (state, action) =>
      state && state.id === action.payload.id
        ? { ...state, ...action.payload }
        : state,

    [successApiType(removeFavorites)]: (state, action) =>
      state && state.id === action.payload
        ? {
            ...state,
            favorites: false,
            user: null,
            state: null,
            isOwner: false
          }
        : state,

    [successApiType(changeState)]: (state, action) =>
      state && state.id === action.payload.favoriteId
        ? { ...state, state: action.payload.id }
        : state,

    [successApiType(changeUser)]: (state, action) =>
      state && state.id === action.payload.favoriteId
        ? { ...state, user: action.payload.user }
        : state
  },
  initialState
);

export default combineReducers({
  current,
  comments,
  filter
});
