import React, { Component } from "react";
import PropTypes from "prop-types";
import { Chip, withStyles } from "@material-ui/core";
import { Icon } from "office-ui-fabric-react";
import { initializeFileTypeIcons } from "@uifabric/file-type-icons";
import { getFileTypeIconProps } from "@uifabric/file-type-icons";

import { injectIntl } from "react-intl";

initializeFileTypeIcons();

const styles = theme => ({
  chip: {
    marginRight: theme.spacing(),
    marginBottom: theme.spacing(),
    overflow: "hidden",
    textOverflow: "ellipsis",
    justifyContent: "left",
    maxWidth: "135px"
  },
  label: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "135px",
    display: "inline-block"
  },
  icon: {
    minWidth: "16px",
    minHeight: "16px"
  }
});

class DocumentCard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  handleClick = () => {
    const { document } = this.props;
    window.open(document.url);
  };

  render() {
    const { classes, document } = this.props;

    return (
      <Chip
        icon={
          <Icon
            {...getFileTypeIconProps({
              extension: document.name ? document.name.split(".").pop() : "doc",
              size: 16
            })}
            className={classes.icon}
          />
        }
        label={document.name}
        title={document.name}
        clickable
        className={classes.chip}
        variant="outlined"
        onClick={this.handleClick}
        classes={{
          label: classes.label
        }}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(DocumentCard));
