import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Typography, Grid, withStyles } from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { grey } from "@material-ui/core/colors";
import LotItems from "../LotItems/LotItems";
import LotParticipants from "../LotParticipants/LotParticipants";

const messages = defineMessages({
  currency: {
    id: "dashboard.lot-card.currency",
    defaultMessage: "Российский рубль"
  }
});

const styles = theme => ({
  actions: {
    display: "flex",
    padding: theme.spacing(3),
    paddingTop: "0"
  },
  label: {
    color: "#888888",
    fontSize: "0.815rem"
  },
  header: {
    fontWeight: 600,
    color: "#888888",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  lotName: {
    backgroundColor: grey[100]
  },
  lotPrice: {
    textAlign: "right"
  },
  currency: {
    fontSize: "0.75rem",
    color: "#888888"
  }
});

class LotCard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      deleteOpen: false
    };
  }

  render() {
    const { classes, item } = this.props;
    const { formatNumber, formatMessage } = this.props.intl;

    return (
      <Fragment>
        <Grid container spacing={2} className={classes.lotName}>
          <Grid item xs={12} sm={9}>
            <Typography>
              <strong>№{item["ordinalNumber"]}</strong> &nbsp;{item["name"]}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3}>
            {item.price && (
              <Typography className={classes.lotPrice}>
                <strong>
                  {formatNumber(item.price, { style: "decimal" })}
                </strong>
              </Typography>
            )}
          </Grid>
        </Grid>
        {item.lotItems && (
          <Fragment>
            <Typography className={classes.header}>
              <FormattedMessage
                id="tender.lot-card.lot-items"
                defaultMessage="Классификация товара"
              />
            </Typography>
            <LotItems items={item.lotItems} />
          </Fragment>
        )}

        {item && item["customerRequirements"] && (
          <Fragment>
            <Typography className={classes.header}>
              <FormattedMessage
                id="tender.lot-card.customer-requirements"
                defaultMessage="Требования заказчика"
              />
            </Typography>
            {item["customerRequirements"].map((item, index) => (
              <Grid container spacing={2} key={index}>
                {item.deliveryPlace && (
                  <Fragment>
                    <Grid item xs={12} sm={3}>
                      <Typography className={classes.label}>
                        <FormattedMessage
                          id="tender.lot-card.delivery-place"
                          defaultMessage="Место доставки"
                        />
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                      <Typography variant="body1">
                        {item.deliveryPlace}
                      </Typography>
                    </Grid>
                  </Fragment>
                )}

                {item.deliveryAt && (
                  <Fragment>
                    <Grid item xs={12} sm={3}>
                      <Typography className={classes.label}>
                        <FormattedMessage
                          id="tender.lot-card.delivery-at"
                          defaultMessage="Срок поставки"
                        />
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                      <Typography variant="body1">{item.deliveryAt}</Typography>
                    </Grid>
                  </Fragment>
                )}

                {item.guaranteeApp && parseFloat(item.guaranteeApp.amount) > 0 && (
                  <Fragment>
                    <Grid item xs={12} sm={3}>
                      <Typography className={classes.label}>
                        <FormattedMessage
                          id="tender.lot-card.provision-bit"
                          defaultMessage="Обеспечение заявки"
                        />
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                      <Typography variant="body1">
                        {formatNumber(item.guaranteeApp.amount, {
                          style: "decimal"
                        })}
                        &nbsp;
                        <span className={classes.currency}>
                          {item.currency
                            ? item.currency
                            : formatMessage(messages.currency)}
                        </span>
                      </Typography>
                    </Grid>
                  </Fragment>
                )}

                {item.guaranteeContract &&
                  parseFloat(item.guaranteeContract.amount) > 0 && (
                    <Fragment>
                      <Grid item xs={12} sm={3}>
                        <Typography className={classes.label}>
                          <FormattedMessage
                            id="tender.lot-card.provision-contract"
                            defaultMessage="Обеспечение контракта"
                          />
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={9}>
                        <Typography variant="body1">
                          {formatNumber(item.guaranteeContract.amount, {
                            style: "decimal"
                          })}
                          &nbsp;
                          <span className={classes.currency}>
                            {item.currency
                              ? item.currency
                              : formatMessage(messages.currency)}
                          </span>
                        </Typography>
                      </Grid>
                    </Fragment>
                  )}
              </Grid>
            ))}
          </Fragment>
        )}

        {item.participants && (
          <Fragment>
            <Typography className={classes.header}>
              <FormattedMessage
                id="tender.lot-card.lot-participants"
                defaultMessage="Участники"
              />
            </Typography>
            <LotParticipants items={item.participants} price={item.price} />
          </Fragment>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(LotCard));
