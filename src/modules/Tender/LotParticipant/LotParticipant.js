import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Box, TableCell, TableRow, withStyles } from "@material-ui/core";
import {
  Star as StarIcon,
  RemoveCircleOutline as RemoveCircleIcon,
  StarBorder as StarBorderIcon
} from "@material-ui/icons";
import { FormattedMessage } from "react-intl.macro";

const styles = theme => ({
  winnerIcon: {
    color: "#e44143",
    marginRight: theme.spacing(1)
  },
  statusIcon: {
    marginRight: theme.spacing(1),
    color: "#888"
  },
  appSecondInfo: {
    color: "#888888",
    marginTop: theme.spacing(0.5)
  },
  currency: {
    color: "#888888",
    whiteSpace: "nowrap"
  }
});

export class LotParticipant extends Component {
  render() {
    const { classes, item, price } = this.props;
    const address = item.factAddress || item.postAddress;
    const { formatNumber } = this.props.intl;

    var details = [];
    if (item.inn) {
      details.push("ИНН " + item.inn);
    }

    if (item.kpp) {
      details.push("КПП " + item.kpp);
    }

    return (
      <TableRow>
        <TableCell>
          <Box display="flex" flexDirection="row">
            <Box>
              {item.winner && <StarIcon className={classes.winnerIcon} />}
              {!item.winner &&
                (item.admitted || typeof item.admitted === "undefined") && (
                  <StarBorderIcon className={classes.statusIcon} />
                )}
              {!item.winner && item.admitted === false && (
                <RemoveCircleIcon className={classes.statusIcon} />
              )}
            </Box>
            <Box flexGrow={1}>
              {item.name ? (
                <>{item.name}</>
              ) : (
                <>
                  <FormattedMessage
                    id="tender.lot-participant.name"
                    defaultMessage="Участник"
                  />
                  &nbsp;{item.applicationNumber}
                </>
              )}
              <div className={classes.appSecondInfo}>
                {address || item.inn ? (
                  <>
                    {address && <div>{address}</div>}
                    {details && <div>{details.join(", ")}</div>}
                  </>
                ) : (
                  <>—</>
                )}
              </div>
            </Box>
          </Box>
        </TableCell>
        <TableCell align="right">
          {item.price && price ? (
            <>
              {formatNumber(100 - (item.price / price) * 100, {
                style: "decimal",
                minimumFractionDigits: 0,
                maximumFractionDigits: 2
              })}
            </>
          ) : (
            <>—</>
          )}
        </TableCell>
        <TableCell align="right">
          {item.price ? (
            <>
              {formatNumber(item.price, {
                style: "decimal",
                minimumFractionDigits: 0,
                maximumFractionDigits: 2
              })}
            </>
          ) : (
            <>—</>
          )}
          <div className={classes.currency}>{item.currency}</div>
        </TableCell>
      </TableRow>
    );
  }
}

export default injectIntl(withStyles(styles)(LotParticipant));
