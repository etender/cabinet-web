import React from "react";
import { connect } from "react-redux";

import TenderHeader from "./TenderHeader";
import {
  toggleUserChange,
  toggleStateChange
} from "../../Favorites/FavoritesActions";
import { getCurrentTender } from "../TenderSelectors";
import { canChangeTenderState, canChangeTenderUser } from "../TenderSelectors";

const TenderHeaderContainer = props => <TenderHeader {...props} />;

const mapStateToProps = state => {
  return {
    tender: getCurrentTender(state),
    canChangeTenderState: canChangeTenderState(state),
    canChangeTenderUser: canChangeTenderUser(state)
  };
};

export default connect(mapStateToProps, {
  toggleUserChange,
  toggleStateChange
})(TenderHeaderContainer);
