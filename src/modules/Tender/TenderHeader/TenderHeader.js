import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Avatar,
  Button,
  Typography,
  Hidden,
  withStyles
} from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { messages as stateMessages } from "../../Favorites/StateChange/StateChange";
import StateColorDot from "../../Favorites/StateColorDot/StateColorDot";

const styles = theme => ({
  header: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    display: "flex",
    backgroundColor: "#e6f1f8",
    minHeight: theme.spacing(7),
    [theme.breakpoints.down("xs")]: {
      paddingLeft: theme.spacing(1),
      paddingRight: 0
    }
  },
  label: {
    color: "#888888",
    fontSize: "0.815rem"
  },
  coordinator: {
    marginRight: 0,
    padding: theme.spacing(1),
    display: "flex",
    alignItems: "center"
  },
  avatarButton: {
    margin: 0,
    width: theme.spacing(2),
    height: theme.spacing(2),
    marginRight: theme.spacing(1)
  },
  avatarText: {
    margin: 0,
    width: theme.spacing(2.5),
    height: theme.spacing(2.5),
    marginRight: theme.spacing(1),
    display: "inline-block",
    marginBottom: "-5px"
  },
  state: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingRight: theme.spacing(4),
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      paddingRight: 0
    }
  },
  stateIcon: {
    minWidth: "auto",
    marginRight: "12px",
    textAlign: "left",
    display: "inline-block",
    lineHeight: "12px"
  }
});

class TenderHeader extends Component {
  static propTypes = {
    tender: PropTypes.object.isRequired,
    toggleStateChange: PropTypes.func.isRequired,
    toggleUserChange: PropTypes.func.isRequired,
    canChangeTenderState: PropTypes.bool,
    canChangeTenderUser: PropTypes.bool
  };

  handleChangeState = () => {
    const { tender, toggleStateChange } = this.props;
    toggleStateChange(tender.id);
  };

  handleChangeUser = () => {
    const { tender, toggleUserChange } = this.props;
    toggleUserChange(tender.id);
  };

  render() {
    const {
      classes,
      tender,
      canChangeTenderState,
      canChangeTenderUser,
      intl
    } = this.props;
    const { formatMessage } = intl;

    return (
      <div className={classes.header}>
        {tender.state && (
          <div className={classes.state}>
            <Hidden only="xs">
              <Typography className={classes.label} display={"inline"}>
                <FormattedMessage
                  id="tender.tender-header.state"
                  defaultMessage="Статус"
                />
                :
              </Typography>
              &nbsp;
            </Hidden>
            {canChangeTenderState ? (
              <Button onClick={this.handleChangeState} color="primary">
                <div className={classes.stateIcon}>
                  <StateColorDot state={tender.state} />
                </div>
                {formatMessage(stateMessages[tender.state])}
              </Button>
            ) : (
              <>
                <div className={classes.stateIcon}>
                  <StateColorDot state={tender.state} />
                </div>
                <Typography display={"inline"}>
                  {formatMessage(stateMessages[tender.state])}
                </Typography>
              </>
            )}
          </div>
        )}
        {tender.user && (
          <div className={classes.coordinator}>
            <Hidden only="xs">
              <Typography className={classes.label} display={"inline"}>
                <FormattedMessage
                  id="tender.tender-header.coordinator"
                  defaultMessage="Координатор"
                />
                :
              </Typography>
              &nbsp;
            </Hidden>
            {canChangeTenderUser ? (
              <Button onClick={this.handleChangeUser} color="primary">
                <Avatar
                  aria-label={tender.user.fullName}
                  src={tender.user.avatarImageUrl}
                  className={classes.avatarButton}
                />
                {tender.user.fullName}
              </Button>
            ) : (
              <Fragment>
                <Avatar
                  aria-label={tender.user.fullName}
                  src={tender.user.avatarImageUrl}
                  className={classes.avatarText}
                />
                <Typography display={"inline"}>
                  {tender.user.fullName}
                </Typography>
              </Fragment>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(TenderHeader));
