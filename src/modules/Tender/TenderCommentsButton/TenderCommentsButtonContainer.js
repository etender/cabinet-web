import React from "react";
import { connect } from "react-redux";

import TenderCommentsButton from "./TenderCommentsButton";
import { getTotalComments } from "../TenderComments/TenderCommentsSelectors";

const TenderCommentsButtonContainer = props => (
  <TenderCommentsButton {...props} />
);

const mapStateToProps = state => {
  return {
    totalComments: getTotalComments(state)
  };
};

export default connect(mapStateToProps, {})(TenderCommentsButtonContainer);
