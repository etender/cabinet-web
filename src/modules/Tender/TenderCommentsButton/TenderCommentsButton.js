import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import {
  CommentOutlined as CommentIcon,
  ModeCommentOutlined as ModeCommentIcon
} from "@material-ui/icons";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import LinkButton from "../../../common/LinkButton/LinkButton";

const messages = defineMessages({
  title: {
    id: "tender.tender-comments-button.title",
    defaultMessage: "Комментарии"
  }
});

const styles = theme => ({
  icon: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      marginRight: 0
    }
  }
});

class TenderContent extends Component {
  static propTypes = {
    commentsRef: PropTypes.object.isRequired
  };

  handleClick = () => {
    const { commentsRef } = this.props;
    commentsRef.current.scrollIntoView();
  };

  render() {
    const { intl, totalComments, classes } = this.props;
    const { formatMessage } = intl;
    return (
      <LinkButton
        onClick={this.handleClick}
        title={formatMessage(messages.title)}
      >
        {totalComments > 0 && (
          <CommentIcon color="primary" className={classes.icon} />
        )}
        {totalComments <= 0 && (
          <ModeCommentIcon className={classes.icon} color="primary" />
        )}
      </LinkButton>
    );
  }
}

export default injectIntl(withStyles(styles)(TenderContent));
