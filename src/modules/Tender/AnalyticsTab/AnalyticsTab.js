import React, { Component } from "react";
import { withStyles } from "@material-ui/core";
import { injectIntl } from "react-intl";

import FilterDescription from "../FilterDescription/FilterDescription";
import PurchasesSummaryContainer from "../../AnalyticsCustomers/PurchasesSummary/PurchasesSummaryContainer";
import SuppliersSummaryContainer from "../../AnalyticsCustomers/SuppliersSummary/SuppliersSummaryContainer";
import ControlResultsContainer from "../../AnalyticsCustomers/ControlResults/ControlResultsContainer";
import TendersSummaryContainer from "../../AnalyticsCustomers/TendersSummary/TendersSummaryContainer";
import AnalyticsCardContainer from "../AnalyticsCard/AnalyticsCardContainer";

const styles = theme => ({
  container: {
    display: "flex"
  },
  block: {
    flex: 1
  }
});
export class AnalyticsTab extends Component {
  componentDidUpdate(prevProps) {
    const { active, tenderFilter, buildFilter } = this.props;

    if (!active) {
      return;
    }

    if (!this.filterBuilded) {
      this.filterBuilded = true;
      buildFilter(tenderFilter);
      return;
    }
  }

  render() {
    const { active, filter, variant, classes } = this.props;

    if (!active) {
      return "";
    }

    return (
      <>
        <div>
          {/* <div className={classes.container}> */}
          <FilterDescription filter={filter} variant={variant} />
          {/* <div className={classes.block}>
            <FilterDescription filter={filter} variant={variant} />
          </div> */}
          {/* <AnalyticsCardContainer /> */}
        </div>
        <TendersSummaryContainer filter={filter} variant={variant} />
        <ControlResultsContainer filter={filter} variant={variant} />
        <SuppliersSummaryContainer filter={filter} variant={variant} />
        <PurchasesSummaryContainer filter={filter} variant={variant} />
      </>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsTab));
