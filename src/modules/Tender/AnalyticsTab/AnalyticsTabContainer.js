import React from "react";
import { connect } from "react-redux";

import AnalyticsTab from "./AnalyticsTab";
import { getCurrentFilter, getTenderFilter } from "../Filter/FilterSelectors";
import { buildFilter } from "../Filter/FilterActions";

const AnalyticsTabContainer = props => <AnalyticsTab {...props} />;

const mapStateToProps = state => {
  return {
    filter: getCurrentFilter(state),
    tenderFilter: getTenderFilter(state)
  };
};

export default connect(mapStateToProps, {
  buildFilter
})(AnalyticsTabContainer);
