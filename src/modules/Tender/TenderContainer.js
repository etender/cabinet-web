import React from "react";
import { connect } from "react-redux";

import "./TenderSagas";
import "../Favorites/FavoritesSagas";
import Tender from "./Tender";
import { fetchTender } from "./TenderActions";
import {
  getCurrentTender,
  getTenderLoaded,
  getTenderFromLocation
} from "./TenderSelectors";
import {
  fetchComments,
  fetchMoreComments
} from "./TenderComments/TenderCommentsActions";
import {
  getComments,
  getHasMoreComments
} from "./TenderComments/TenderCommentsSelectors";

const TenderContainer = props => <Tender {...props} />;

const mapStateToProps = state => {
  return {
    tender: getCurrentTender(state) || getTenderFromLocation(state),
    comments: getComments(state),
    loaded: getTenderLoaded(state),
    hasMoreComments: getHasMoreComments(state)
  };
};

export default connect(mapStateToProps, {
  fetchTender,
  fetchComments,
  fetchMoreComments
})(TenderContainer);
