import React, { Component } from "react";
import { Grid, Box, Tab, Tabs, Card, withStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    padding: theme.spacing(2)
  },
  cardContent: {
    marginTop: theme.spacing(3)
  },
  boxContent: {
    padding: theme.spacing(3)
  },
  header: {
    borderBottomColor: theme.palette.divider,
    borderBottomWidth: 1,
    borderBottomStyle: "solid"
  }
});

class TenderSkeleton extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Box display="flex" className={classes.header}>
          <Tabs
            value={0}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            onChange={this.handleTabChange}
          >
            <Tab
              label={<Skeleton variant="text" width="80px" height="23px" />}
            />
            <Tab
              label={<Skeleton variant="text" width="80px" height="23px" />}
            />
          </Tabs>
        </Box>
        <Card className={classes.cardContent}>
          <Box className={classes.boxContent}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={3}>
                <Skeleton width="30%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="50%" />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton width="70%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="80%" />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton width="60%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="90%" />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton width="30%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="50%" />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton width="70%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="70%" />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton width="50%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton width="50%" />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="30%" />
              </Grid>
              <Grid item xs={12} sm={3}>
                <Skeleton />
              </Grid>
              <Grid item xs={12} sm={9}>
                <Skeleton width="10%" />
              </Grid>
            </Grid>
          </Box>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(TenderSkeleton);
