import React, { Component } from "react";
import { Typography, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { grey } from "@material-ui/core/colors";

import StringFieldDescription from "../../Dashboard/FilterDescription/StringFieldDescription";
import ArrayFieldDescription from "../../Dashboard/FilterDescription/ArrayFieldDescription";
import CollectionFieldDescription from "../../Dashboard/FilterDescription/CollectionFieldDescription";
import DateRangeFieldDescription from "../../Dashboard/FilterDescription/DateRangeFieldDescription";
import FederalLawFieldDescription from "../../Dashboard/FilterDescription/FederalLawFieldDescription";
import FilterDescriptionSkeleton from "./FilterDescriptionSkeleton";

const messages = defineMessages({
  customer: {
    id: "tender.filter-description.contact",
    defaultMessage: "заказчик:"
  },
  keywords: {
    id: "tender.filter-description.keywords",
    defaultMessage: "со словами:"
  },
  including: {
    id: "tender.filter-description.including",
    defaultMessage: "включая слова:"
  },
  excluding: {
    id: "tender.filter-description.excluding",
    defaultMessage: "исключая слова:"
  },
  regions: {
    id: "tender.filter-description.regions",
    defaultMessage: "по регионам:"
  },
  pubDate: {
    id: "tender.filter-description.pub-date",
    defaultMessage: "дата публикации:"
  },
  okpds: {
    id: "tender.filter-description.okpds",
    defaultMessage: "по ОКПД2:"
  },
  categories: {
    id: "tender.filter-description.categories",
    defaultMessage: "по категориям:"
  }
});

const styles = theme => ({
  root: {
    color: grey[600]
  }
});

class FilterDescriptionContent extends Component {
  render() {
    const { classes, value, intl } = this.props;
    const { formatMessage } = intl;

    const {
      customerName,
      keywords,
      including,
      excluding,
      federalLaw,
      deal,
      regions,
      categories,
      okpds,
      pubDateFrom,
      pubDateTo
    } = value || {};

    return (
      <>
        {!value && <FilterDescriptionSkeleton />}
        {value && (
          <Typography className={classes.root}>
            <StringFieldDescription
              title={formatMessage(messages.customer)}
              value={customerName}
            />
            <ArrayFieldDescription
              title={formatMessage(messages.keywords)}
              value={keywords}
            />
            <ArrayFieldDescription
              title={formatMessage(messages.including)}
              value={including}
            />
            <ArrayFieldDescription
              title={formatMessage(messages.excluding)}
              value={excluding}
            />
            <CollectionFieldDescription
              title={formatMessage(messages.regions)}
              value={regions}
              field="title"
            />
            <FederalLawFieldDescription value={federalLaw} deal={deal} />
            <DateRangeFieldDescription
              title={formatMessage(messages.pubDate)}
              from={pubDateFrom}
              to={pubDateTo}
            />
            <CollectionFieldDescription
              title={formatMessage(messages.categories)}
              value={categories}
            />
            <CollectionFieldDescription
              title={formatMessage(messages.okpds)}
              value={okpds}
            />
          </Typography>
        )}
      </>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterDescriptionContent));
