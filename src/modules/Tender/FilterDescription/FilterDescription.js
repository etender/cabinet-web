import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { defineMessages } from "react-intl.macro";

import Section from "../../../common/Section/Section";

import FilterDescriptionContent from "./FilterDescriptionContent";

const messages = defineMessages({
  title: {
    id: "tender.filter-description.title",
    defaultMessage: "Параметры построения"
  },
  helpButtonBody: {
    id: "tender.filter-description.help-button-body",
    defaultMessage: `Вы получаете аналитику на основе настроенного фильтра в личном кабинете. 
    Результат поиска показывает статистику по завершенным закупкам заказчика, где есть победитель.`
  }
});
export class FilterDescription extends Component {
  render() {
    const { filter, intl, variant } = this.props;
    const { formatMessage } = intl;

    return (
      <Section
        variant={variant}
        title={formatMessage(messages.title)}
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        <FilterDescriptionContent value={filter} />
      </Section>
    );
  }
}

export default injectIntl(FilterDescription);
