import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { OpenInNew as OpenInNewIcon } from "@material-ui/icons";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import LinkButton from "../../../common/LinkButton/LinkButton";

const messages = defineMessages({
  title: {
    id: "tender.share-button.title",
    defaultMessage: "В новом окне"
  }
});

const styles = theme => ({
  newWindowIcon: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      marginRight: 0
    }
  },
  root: {
    marginRight: "10px"
  }
});

class ShareButton extends Component {
  static propTypes = {
    tender: PropTypes.object,
    shareTender: PropTypes.func.isRequired,
    fitler: PropTypes.object
  };

  handleClick = () => {
    const { shareTender, filter, tender } = this.props;
    shareTender(tender.id, filter);
  };

  render() {
    const { classes, tender, intl } = this.props;
    const { formatMessage } = intl;

    if (!tender) {
      return "";
    }

    return (
      <LinkButton
        onClick={this.handleClick}
        title={formatMessage(messages.title)}
        classes={{ linkButton: classes.root }}
      >
        <OpenInNewIcon color="primary" className={classes.newWindowIcon} />
      </LinkButton>
    );
  }
}

export default injectIntl(withStyles(styles)(ShareButton));
