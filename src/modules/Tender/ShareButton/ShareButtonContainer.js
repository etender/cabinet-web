import React from "react";
import { connect } from "react-redux";

import { shareTender } from "../TenderActions";
import { getPathFilter } from "../../Dashboard/Filter/FilterSelectors";
import { getCurrentTender } from "../TenderSelectors";
import ShareButton from "./ShareButton";

const ShareContainer = props => <ShareButton {...props} />;

const mapStateToProps = state => ({
  tender: getCurrentTender(state),
  filter: getPathFilter(state)
});

export default connect(mapStateToProps, {
  shareTender
})(ShareContainer);
