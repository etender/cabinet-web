import { all, put, fork, takeLatest } from "redux-saga/effects";
import { buildPath } from "../../services/Router/RouterHelpers";
import { push } from "connected-react-router";

import sagaMiddleware from "../../utils/SagaUtils";

import { openLink } from "../../services/Api/ApiActions";
import { fetchTender, shareTender, showTenderAnalytics } from "./TenderActions";
import { fetchEntitiesSaga } from "../../services/Entities/EntitiesSagas";
import commentsSagas from "./TenderComments/TenderCommentsSagas";
import filterSagas from "./Filter/FilterSagas";
import { TENDER_PATH } from "./TenderConstants";
import { CUSTOMERS_PATH } from "../AnalyticsCustomers/AnalyticsCustomersConstants";

function* shareTenderSaga(action) {
  yield put(
    openLink({
      url: buildPath(TENDER_PATH, action.payload),
      target: action.payload.id
    })
  );
}

function* showTenderAnalyticsSaga(action) {
  const { customerInn, customerName } = action.payload;
  const filter = {
    ...action.payload,
    customers: [{ inn: customerInn, title: customerName }]
  };
  delete filter.id;
  delete filter.customerInn;
  delete filter.customerName;

  const url = buildPath(CUSTOMERS_PATH, filter);
  yield put(push(url));
}

function* tenderSagas() {
  yield all([
    fork(commentsSagas),
    fork(filterSagas),
    takeLatest(fetchTender, fetchEntitiesSaga),
    takeLatest(shareTender, shareTenderSaga),
    takeLatest(showTenderAnalytics, showTenderAnalyticsSaga)
  ]);
}

sagaMiddleware.run(tenderSagas);
