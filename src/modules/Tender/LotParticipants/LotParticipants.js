import React, { Component } from "react";
import { injectIntl } from "react-intl";
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableRow,
  SvgIcon,
  withStyles
} from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import LotParticipant from "../LotParticipant/LotParticipant";

const styles = theme => ({
  root: {
    marginTop: theme.spacing(-2)
  },
  spacer: {
    flex: "1"
  },
  winnerIcon: {
    color: "#e44143",
    marginRight: theme.spacing(1)
  },
  statusIcon: {
    marginRight: theme.spacing(1),
    color: "#888"
  },
  appSecondInfo: {
    color: "#888888"
  },
  currency: {
    color: "#888888"
  },
  iconArrow: {
    display: "inline-block",
    marginRight: "5px",
    marginBottom: "-3px",
    width: "7px",
    height: "13px"
  }
});

export class LotParticipants extends Component {
  state = {
    lotItemsPage: 0
  };

  handleChangePage = (event, newPage) => {
    this.setState({ lotItemsPage: newPage });
  };

  render() {
    const { classes, items, price } = this.props;

    return (
      <div className={classes.root}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <FormattedMessage
                  id="tender.lot-participants.name"
                  defaultMessage="Наименование участника"
                />
              </TableCell>
              <TableCell align="right">
                <SvgIcon viewBox="0 0 8 14" className={classes.iconArrow}>
                  <path d="M8 10H5V0H3V10H0L4 14L8 10Z" />
                </SvgIcon>
                <FormattedMessage
                  id="tender.lot-participants.quantity"
                  defaultMessage="%"
                />
              </TableCell>
              <TableCell align="right">
                <FormattedMessage
                  id="tender.lot-participants.price"
                  defaultMessage="Сумма"
                />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items &&
              items.map((item, index) => (
                <LotParticipant item={item} price={price} key={index} />
              ))}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(LotParticipants));
