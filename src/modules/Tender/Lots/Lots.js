import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Section from "../../../common/Section/Section";
import LotCard from "../LotCard/LotCard";

const messages = defineMessages({
  title: {
    id: "tender.lots.title",
    defaultMessage: "Лоты"
  }
});

const styles = theme => ({
  content: {
    padding: theme.spacing(3)
  }
});

export class Lots extends Component {
  static propTypes = {
    lots: PropTypes.arrayOf(PropTypes.object),
    variant: PropTypes.string
  };

  render() {
    const { lots, variant, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <Section title={formatMessage(messages.title)} variant={variant}>
        {(lots || []).map(lot => (
          <LotCard
            key={lot.ordinalNumber}
            item={lot}
            lotsNumber={lots.length}
          />
        ))}
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(Lots));
