import { createSelector } from "reselect";
import { isPresent } from "../../../utils/CoreUtils";
import { getCurrentTender } from "../TenderSelectors";

export const getLots = createSelector(getCurrentTender, tender => tender.lots);

export const shouldShowLots = createSelector(
  getCurrentTender,
  tender =>
    (isPresent(tender.showLots) &&
      tender.stage !== "purchase_plan" &&
      isPresent(tender.lots)) ||
    false
);
