import React from "react";
import { connect } from "react-redux";

import Lots from "./Lots";
import { getLots, shouldShowLots } from "../Lots/LotsSelectors";

const LotsContainer = props =>
  props.shouldShowLots ? <Lots {...props} /> : "";

const mapStateToProps = state => {
  return {
    lots: getLots(state),
    shouldShowLots: shouldShowLots(state)
  };
};

export default connect(mapStateToProps)(LotsContainer);
