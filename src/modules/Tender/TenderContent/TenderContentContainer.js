import React from "react";
import { connect } from "react-redux";

import { confirm } from "../../../common/ConfirmationDialog/ConfiramtionActions";
import TenderContent from "./TenderContent";
import {
  addFavorites,
  removeFavorites
} from "../../Favorites/FavoritesActions";
import {
  getCurrentTender,
  getTenderFromLocation,
  canChangeTender,
  canAddTender
} from "../TenderSelectors";

const TenderContentContainer = props => <TenderContent {...props} />;

const mapStateToProps = state => {
  const tender = getCurrentTender(state) || getTenderFromLocation(state);
  return {
    tender,
    canChangeTender: canChangeTender(state, tender),
    canAddTender: canAddTender(state)
  };
};

export default connect(mapStateToProps, {
  addFavorites,
  confirm,
  removeFavorites
})(TenderContentContainer);
