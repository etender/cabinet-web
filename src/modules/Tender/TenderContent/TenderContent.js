import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Section from "../../../common/Section/Section";
import TenderHeaderContainer from "../TenderHeader/TenderHeaderContainer";
import TenderBodyContainer from "../TenderBody/TenderBodyContainer";
import { messages as favoriteDeleteMessages } from "../../Favorites/FavoriteDelete/FavoriteDeleteConstants";
import { Favorite, FavoriteBorder } from "@material-ui/icons";

const messages = defineMessages({
  title: {
    id: "tender.tender-content.title",
    defaultMessage: "Извещение №{id}"
  },
  commonTab: {
    id: "tender.tender-content.common-tab",
    defaultMessage: "Общая информация"
  },
  analyticTab: {
    id: "tender.tender-content.analytic-tab",
    defaultMessage: "Аналитика по заказчику"
  },
  removeFavorites: {
    id: "tender.tender-content.remove-favorites",
    defaultMessage: "Снять с контроля"
  },
  addFavorites: {
    id: "tender.tender-content.add-favorites",
    defaultMessage: "На контроль"
  }
});

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    width: "100%",
    marginBottom: theme.spacing(3),
    marginRight: "auto",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 110px)"
    }
  },
  cardTitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  disabledFavorites: {
    backgroundColor: theme.palette.primary.main + " !important",
    color: theme.palette.common.white + " !important",
    opacity: "0.7"
  },
  tenderDescription: {
    marginTop: theme.spacing(3)
  }
});

class TenderContent extends Component {
  static propTypes = {
    tender: PropTypes.object,
    canChangeTender: PropTypes.bool.isRequired,
    addFavorites: PropTypes.func.isRequired,
    confirm: PropTypes.func.isRequired,
    removeFavorites: PropTypes.func.isRequired,
    variant: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = {
      tab: 0
    };
  }

  handleFavoritesClick = () => {
    const { tender, addFavorites, confirm, removeFavorites, intl } = this.props;
    const { formatMessage } = intl;

    if (tender.favorites) {
      confirm(
        formatMessage(favoriteDeleteMessages.title),
        formatMessage(favoriteDeleteMessages.message),
        () => removeFavorites(tender.id)
      );
    } else {
      addFavorites(tender.id);
    }
  };

  render() {
    const {
      classes,
      tender,
      canChangeTender,
      canAddTender,
      variant,
      intl
    } = this.props;
    const { formatMessage } = intl;

    return (
      <Section
        variant={variant}
        actions={
          <Button
            variant="contained"
            onClick={this.handleFavoritesClick}
            color="primary"
            disabled={tender.favorites ? !canChangeTender : !canAddTender}
            classes={{
              disabled: tender.favorites && classes.disabledFavorites
            }}
            startIcon={
              tender.favorites ? (
                <Favorite className={classes.favoriteIcon} />
              ) : (
                <FavoriteBorder className={classes.favoriteIcon} />
              )
            }
          >
            {tender.favorites
              ? formatMessage(messages.removeFavorites)
              : formatMessage(messages.addFavorites)}
          </Button>
        }
        header={tender.state && tender.user ? <TenderHeaderContainer /> : ""}
        classes={{ card: classes.tenderDescription }}
      >
        <TenderBodyContainer />
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(TenderContent));
