import { createAction } from "redux-actions";
import { toggleDialog } from "../../common/Dialog/DialogActions";

export const fetchTender = createAction(
  "TENDER_FETCH",
  id => ({ id }),
  () => ({ entity: "tender" })
);

export const showTender = createAction(
  "TENDER_SHOW",
  tender => tender,
  () => ({ entity: "tender" })
);

export const shareTender = createAction(
  "TENDER_SHARE",
  (id, filter) => ({ id, filter }),
  () => ({ entity: "tender" })
);

export const toggleTender = () => toggleDialog("tender");

export { fetchComments } from "./TenderComments/TenderCommentsActions";

export const showTenderAnalytics = createAction(
  "TENDER_ANALYTICS_SHOW",
  filter => filter
);
