import React from "react";
import { connect } from "react-redux";

import "../TenderSagas";
import { getShared } from "../../Shared/SharedSelectors";
import { confirm } from "../../../common/ConfirmationDialog/ConfiramtionActions";
import { fetchTender, shareTender } from "../TenderActions";
import {
  addFavorites,
  removeFavorites
} from "../../Favorites/FavoritesActions";
import {
  fetchComments,
  fetchMoreComments
} from "../TenderComments/TenderCommentsActions";
import {
  getCurrentTender,
  canChangeTender,
  canAddTender
} from "../TenderSelectors";
import {
  getComments,
  getHasMoreComments,
  canReadComment
} from "../TenderComments/TenderCommentsSelectors";
import TenderDialog from "./TenderDialog";

const TenderDialogContainer = props => <TenderDialog {...props} />;

const mapStateToProps = state => {
  const tender = getCurrentTender(state);
  return {
    tender,
    comments: getComments(state),
    canReadComment: canReadComment(state),
    hasMoreComments: getHasMoreComments(state),
    canChangeTender: canChangeTender(state, tender),
    canAddTender: canAddTender(state),
    shared: getShared(state)
  };
};

export default connect(mapStateToProps, {
  addFavorites,
  shareTender,
  confirm,
  removeFavorites,
  fetchTender,
  fetchComments,
  fetchMoreComments
})(TenderDialogContainer);
