import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Tabs,
  Tab,
  Typography,
  withStyles,
  withWidth
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import NewFeatureBadge from "../../../common/NewFeatureBadge/NewFeatureBadge";
import InfiniteList from "../../../common/InfiniteList/InfiniteList";
import DialogContainer from "../../../common/Dialog/DialogContainer";
import UserChangeContainer from "../../Favorites/UserChange/UserChangeContainer";
import StateChangeContainer from "../../Favorites/StateChange/StateChangeContainer";
import TenderBodyContainer from "../TenderBody/TenderBodyContainer";
import TenderHeaderContainer from "../TenderHeader/TenderHeaderContainer";
import LotsContainer from "../Lots/LotsContainer";
import { messages as favoriteDeleteMessages } from "../../Favorites/FavoriteDelete/FavoriteDeleteConstants";
import TenderCommentsContainer from "../TenderComments/TenderCommentsContainer";
import TenderCommentsButtonContainer from "../TenderCommentsButton/TenderCommentsButtonContainer";
import AnalyticsTabContainer from "../AnalyticsTab/AnalyticsTabContainer";
import ShareButtonContainer from "../ShareButton/ShareButtonContainer";
import { Favorite, FavoriteBorder } from "@material-ui/icons";

const messages = defineMessages({
  mainTab: {
    id: "tender.tender-dialog.main-tab",
    defaultMessage: "Извещение"
  },
  analyticTab: {
    id: "tender.tender-dialog.analytic-tab",
    defaultMessage: "Аналитика по заказчику"
  },
  share: {
    id: "tender.tender-dialog.share",
    defaultMessage: "В новом окне"
  }
});

const styles = theme => ({
  root: {
    width: "100%",
    maxWidth: "1024px",
    height: "100%",
    overflow: "hidden"
  },
  header: {
    marginRight: theme.spacing(3),
    marginLeft: theme.spacing(3),
    [theme.breakpoints.down("xs")]: {
      marginRight: 0,
      marginLeft: 0
    }
  },
  headerControl: {
    borderTopColor: theme.palette.divider,
    borderTopWidth: 1,
    borderTopStyle: "solid"
  },
  title: {
    paddingTop: 20,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0
  },
  container: {
    padding: theme.spacing(3)
  },
  disabledFavorites: {
    backgroundColor: theme.palette.primary.main + " !important",
    color: theme.palette.common.white + " !important",
    opacity: "0.7"
  },
  favorite: {
    // marginLeft: "auto"
  },
  actions: {
    padding: theme.spacing(3)
  },
  content: {
    padding: "0px"
  },
  tabTitle: {
    textTransform: "none",
    fontSize: "17px"
  },
  tabButtons: {
    marginRight: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      marginRight: theme.spacing(3),
      marginLeft: theme.spacing(1)
    }
  },
  tabRoot: {
    maxWidth: "none",
    minWidth: 0,
    marginRight: theme.spacing(3),
    overflow: "visible"
  },
  tabsBox: {
    [theme.breakpoints.down("xs")]: {
      maxWidth: "235px"
    }
  },
  newWindowIcon: {
    marginRight: theme.spacing(1),
    [theme.breakpoints.down("sm")]: {
      marginRight: 0
    }
  },
  openInNewWindowLink: {
    marginRight: "10px"
  }
});

class TenderDialog extends Component {
  static propTypes = {
    tender: PropTypes.object,
    fetchTender: PropTypes.func.isRequired,
    // Require to update InfinteList. It's needed to reattache scroll handler
    comments: PropTypes.array.isRequired,
    hasMoreComments: PropTypes.bool.isRequired,
    fetchComments: PropTypes.func.isRequired,
    fetchMoreComments: PropTypes.func.isRequired,
    shareTender: PropTypes.func.isRequired,
    addFavorites: PropTypes.func.isRequired,
    removeFavorites: PropTypes.func.isRequired,
    confirm: PropTypes.func.isRequired,
    width: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.commentsRef = React.createRef();
    this.state = {
      tab: 0
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { fetchTender, fetchComments, tender, shared } = this.props;

    if (tender && (!prevProps.tender || prevProps.tender.id !== tender.id)) {
      fetchTender(tender.id);
      if (!shared) {
        fetchComments(tender.id);
      }
    }
  }

  handleShareClick = () => {
    const { shareTender, tender } = this.props;
    shareTender(tender.id);
  };

  handleCommentClick = () => {
    window.scrollTo(0, this.commentRef.current.offsetTop);
  };

  handleFavoritesClick = () => {
    const { tender, addFavorites, confirm, removeFavorites, intl } = this.props;
    const { formatMessage } = intl;

    if (tender.favorites) {
      confirm(
        formatMessage(favoriteDeleteMessages.title),
        formatMessage(favoriteDeleteMessages.message),
        () => removeFavorites(tender.id)
      );
    } else {
      addFavorites(tender.id);
    }
  };

  handleLoadMore = page => {
    const { fetchMoreComments, tender } = this.props;
    fetchMoreComments({ tenderId: tender.id, page });
  };

  handleTabChange = (event, value) => {
    this.setState({ tab: value });
  };

  handleClose = event => {
    this.setState({ tab: 0 });
  };

  render() {
    const {
      classes,
      tender,
      canChangeTender,
      canAddTender,
      hasMoreComments,
      shared,
      intl
    } = this.props;
    const { formatMessage } = intl;
    const loading = !tender || tender.loading;

    if (!tender) {
      return "";
    }

    const { tab } = this.state;
    return (
      <Fragment>
        <UserChangeContainer />
        <StateChangeContainer />
        <DialogContainer
          name="tender"
          classes={{
            paper: classes.root,
            content: classes.content,
            title: classes.title
          }}
          loading={loading}
          formDisabled
          actionsDisabled
          fullScreen
          onClose={this.handleClose}
          title={
            <>
              <Box display="flex" className={classes.header}>
                <Box flexGrow={1} className={classes.tabsBox}>
                  <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    onChange={this.handleTabChange}
                  >
                    <Tab
                      label={
                        <Typography className={classes.tabTitle}>
                          {formatMessage(messages.mainTab)}
                        </Typography>
                      }
                      classes={{ root: classes.tabRoot }}
                    />
                    {tender.area === "zakupki_gov_ru" && (
                      <Tab
                        label={
                          <NewFeatureBadge>
                            <Typography className={classes.tabTitle}>
                              {formatMessage(messages.analyticTab)}
                            </Typography>
                          </NewFeatureBadge>
                        }
                        classes={{ root: classes.tabRoot }}
                      />
                    )}
                  </Tabs>
                </Box>
                {!shared && (
                  <Box
                    display="flex"
                    alignItems="center"
                    className={classes.tabButtons}
                  >
                    <ShareButtonContainer />
                    {tab === 0 && (
                      <TenderCommentsButtonContainer
                        commentsRef={this.commentsRef}
                      />
                    )}
                  </Box>
                )}
              </Box>
              {!shared && tender.state && tender.user && (
                <TenderHeaderContainer
                  classes={{ header: classes.headerControl }}
                />
              )}
            </>
          }
          buttons={
            !shared && (
              <Fragment>
                <Button
                  className={classes.favorite}
                  onClick={this.handleFavoritesClick}
                  disabled={tender.favorites ? !canChangeTender : !canAddTender}
                  color="primary"
                  startIcon={
                    tender.favorites ? (
                      <Favorite className={classes.favoriteIcon} />
                    ) : (
                      <FavoriteBorder className={classes.favoriteIcon} />
                    )
                  }
                  classes={{
                    disabled: tender.favorites && classes.disabledFavorites
                  }}
                >
                  {tender.favorites ? (
                    <FormattedMessage
                      id="tender.tender-dialog.remove-favorites"
                      defaultMessage="Снять с контроля"
                    />
                  ) : (
                    <FormattedMessage
                      id="tender.tender-dialog.add-favorites"
                      defaultMessage="На контроль"
                    />
                  )}
                </Button>
              </Fragment>
            )
          }
        >
          {tab === 0 && (
            <InfiniteList
              classes={{ container: classes.container }}
              loadMore={this.handleLoadMore}
              hasMore={hasMoreComments}
            >
              <TenderBodyContainer />
              <LotsContainer />
              <span ref={this.commentsRef} />
              <TenderCommentsContainer onComments={this.handleComments} />
            </InfiniteList>
          )}
          <Box px={3}>
            <AnalyticsTabContainer active={tab === 1} />
          </Box>
        </DialogContainer>
      </Fragment>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(TenderDialog)));
