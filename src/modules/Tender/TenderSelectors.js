import { createSelector } from "reselect";
import {
  getLocationHash,
  getLocationPathname
} from "../../services/Router/RouterSelectors";
import { parseHash } from "../../services/Router/RouterHelpers";
import { getCurrentUser } from "../../services/Devise/DeviseSelectors";

export const getCurrentTender = state => state.tender.current;

export const getTender = (state, tender) => tender || getCurrentTender(state);

export const getTenderLoaded = createSelector(
  getCurrentTender,
  tender => tender && tender.loading === false
);

export const isTenderPath = createSelector(getLocationPathname, path =>
  path.includes("/tender")
);

export const getTenderFromLocation = createSelector(
  isTenderPath,
  getLocationHash,
  (path, locationHash) => path && parseHash(locationHash)
);

export const canChangeTender = createSelector(
  getCurrentUser,
  getTender,
  (user, tender) =>
    (!user || !user.readOnly) &&
    Boolean((user && user.admin) || (tender && tender.isOwner))
);

export const canAddTender = createSelector(
  getCurrentUser,
  user => user && !user.readOnly
);

export const canChangeTenderState = createSelector(
  canChangeTender,
  canChangeTender => canChangeTender
);

export const canChangeTenderUser = createSelector(getCurrentUser, user =>
  user ? !user.readOnly && user.admin : false
);
