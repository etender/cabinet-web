import { getArrayEntities } from "../../../services/Entities/EntitiesSelectors";
import { getArrayEntitiesLoading } from "../../../services/Entities/ArrayEntities/ArrayEntitiesSelectors";
import { ENTITY } from "./ControlResultsConstants";

export const getControlResults = state => getArrayEntities(state, ENTITY);

export const getControlResultsLoading = state => {
  const loading = getArrayEntitiesLoading(state, ENTITY);

  return loading === undefined ? true : loading;
};
