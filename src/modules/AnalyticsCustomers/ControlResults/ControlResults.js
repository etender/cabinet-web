import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableRow,
  TableContainer
} from "@material-ui/core";
import { FormattedMessage, defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../../utils/CoreUtils";
import Section from "../../../common/Section/Section";
import ExportButtonContainer from "../../../common/ExportButton/ExportButtonContainer";
import { normalizeFilter } from "../../Tender/Filter/FilterHelpers";
import ControlResultsSkeleton from "./ControlResultsSkeleton";
import { EXPORT_ENTITY } from "./ControlResultsConstants";

const messages = defineMessages({
  title: {
    id: "analytics-customers.control-results.title",
    defaultMessage: "Результаты контроля"
  },
  helpButtonBody: {
    id: "analytics-customers.control-results.help-button-body",
    defaultMessage: `Позволяет оценить поведение заказчика на рынке.
    Небольшое число «признанных» или «частично признанных» жалоб говорит о
    честности торгов (заказчик не ограничивает конкуренцию). Также это
    число повышает вероятность успешной работы с закупочной документацией.`
  }
});

class ControlResults extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
  };

  constructor(props) {
    super(props);

    this.query = {};
  }

  componentDidMount() {
    const { autofetch, cleanСheckResults } = this.props;
    if (autofetch) {
      cleanСheckResults();
      this.fetch();
    }
  }

  componentDidUpdate(prevProps) {
    const { filter, cleanСheckResults } = this.props;

    if (isEmpty(filter)) {
      if (!this.cleaned) {
        this.cleaned = true;
        cleanСheckResults();
      }

      return;
    }

    if (prevProps.filter !== filter) {
      this.fetch();
    }
  }

  fetch() {
    const { fetchСheckResults, filter } = this.props;

    if (isEmpty(filter)) {
      return;
    }

    this.query = this.buildQuery();
    fetchСheckResults(this.query);
  }

  buildQuery() {
    const { filter } = this.props;

    return { search: filter && normalizeFilter(filter) };
  }

  render() {
    const { items, loading, variant, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <Section
        variant={variant}
        title={formatMessage(messages.title)}
        titleAction={
          <ExportButtonContainer name={EXPORT_ENTITY} query={this.query} />
        }
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  <FormattedMessage
                    id="analytics.control-results.type-of-event"
                    defaultMessage="Вид мероприятия"
                  />
                </TableCell>
                <TableCell align="right">
                  <FormattedMessage
                    id="analytics.control-results.total-verified"
                    defaultMessage="Всего проверено"
                  />
                </TableCell>
                <TableCell align="right">
                  <FormattedMessage
                    id="analytics.control-results.violations-recognized"
                    defaultMessage="Нарушений признано"
                  />
                </TableCell>
                <TableCell align="right">
                  <FormattedMessage
                    id="analytics.control-results.violations-partially-recognized"
                    defaultMessage="Нарушений частично признано"
                  />
                </TableCell>
                <TableCell align="right">
                  <FormattedMessage
                    id="analytics.control-results.no-violations"
                    defaultMessage="Нарушений нет"
                  />
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading && <ControlResultsSkeleton />}
              {!loading &&
                items &&
                items.map((item, index) => (
                  <TableRow key={index}>
                    <TableCell>
                      {item.type === "complaint" && (
                        <FormattedMessage
                          id="analytics.control-results.complaint"
                          defaultMessage="Жалобы"
                        />
                      )}
                      {item.type === "unplanned_check" && (
                        <FormattedMessage
                          id="analytics.control-results.unplanned-check"
                          defaultMessage="Проверки"
                        />
                      )}
                    </TableCell>
                    <TableCell align="right">{item.count}</TableCell>
                    <TableCell align="right">
                      {item.violationsRecognized}
                    </TableCell>
                    <TableCell align="right">
                      {item.violationsPartiallyRecognized}
                    </TableCell>
                    <TableCell align="right">{item.noViolations}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Section>
    );
  }
}

export default injectIntl(ControlResults);
