import React, { Component } from "react";
import { TableCell, TableRow } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { Skeleton } from "@material-ui/lab";
import { injectIntl } from "react-intl";

class ControlResultsSkeleton extends Component {
  render() {
    return (
      <>
        <TableRow>
          <TableCell>
            <FormattedMessage
              id="analytics.control-results-skeleton.complaint"
              defaultMessage="Жалобы"
            />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            <FormattedMessage
              id="analytics.control-results-skeleton.unplanned-check"
              defaultMessage="Проверки"
            />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell align="right">
            <Skeleton width="100%" height="30%" />
          </TableCell>
        </TableRow>
      </>
    );
  }
}

export default injectIntl(ControlResultsSkeleton);
