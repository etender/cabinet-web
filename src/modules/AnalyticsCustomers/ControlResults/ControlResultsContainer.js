import React from "react";
import { connect } from "react-redux";

import ControlResults from "./ControlResults";
import {
  getControlResults,
  getControlResultsLoading
} from "./ControlResultsSelectors";
import { fetchСheckResults, cleanСheckResults } from "./ControlResultsActions";

const ControlResultsContainer = props => <ControlResults {...props} />;

const mapStateToProps = state => {
  return {
    items: getControlResults(state),
    loading: getControlResultsLoading(state)
  };
};

export default connect(mapStateToProps, {
  fetchСheckResults,
  cleanСheckResults
})(ControlResultsContainer);
