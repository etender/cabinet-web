import {
  fetchArrayEntities,
  cleanArrayEntities
} from "../../../services/Entities/EntitiesActions";
import { ENTITY } from "./ControlResultsConstants";

export const fetchСheckResults = query => fetchArrayEntities(ENTITY, query);

export const cleanСheckResults = () => cleanArrayEntities(ENTITY);
