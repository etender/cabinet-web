import { createSharedPath } from "../Shared/SharedHelpers";

export const CUSTOMERS_PATH = createSharedPath("/analytics/customers");
export const CUSTOMERS_NAME = "customers";
export const CUSTOMERS_DEFAULT_FILTER = { federalLaw: ["fl223", "fl44"] };
