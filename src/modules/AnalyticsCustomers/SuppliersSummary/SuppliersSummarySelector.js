import { getPaginateEntities } from "../../../services/Entities/EntitiesSelectors";
import { getArrayEntitiesLoading } from "../../../services/Entities/ArrayEntities/ArrayEntitiesSelectors";
import { ENTITY } from "./SuppliersSummaryConstants";

export const getSuppliersSummary = state => getPaginateEntities(state, ENTITY);

export const getSuppliersSummaryLoading = state => {
  const loading = getArrayEntitiesLoading(state, ENTITY);

  return loading === undefined ? true : loading;
};
