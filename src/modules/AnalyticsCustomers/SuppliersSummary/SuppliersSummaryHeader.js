import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  withStyles
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

const messages = defineMessages({
  name: {
    id: "analytics-customers.suppliers-summary-header.name",
    defaultMessage: "Поставщик"
  },
  inn: {
    id: "analytics-customers.suppliers-summary-header.inn",
    defaultMessage: "ИНН"
  },
  participationCount: {
    id: "analytics-customers.suppliers-summary-header.participation-count",
    defaultMessage: "Участий"
  },
  winsCount: {
    id: "analytics-customers.suppliers-summary-header.wins-count",
    defaultMessage: "Побед"
  },
  price: {
    id: "analytics-customers.suppliers-summary-header.price",
    defaultMessage: "Сумма контрактов"
  },
  priceDrop: {
    id: "analytics-customers.suppliers-summary-header.price-drop",
    defaultMessage: "% снижения"
  }
});

const styles = theme => ({
  inn: {
    width: 70
  },
  participationCount: {
    width: 55
  },
  winsCount: {
    width: 55
  },
  price: {
    width: 120
  },
  priceDrop: {
    width: 80
  },
  tableSortLabel: {
    flexDirection: "initial"
  },
  tableSortLabelIcon: {
    marginRight: "0px"
  }
});

const cells = [
  { id: "name", name: "name" },
  { id: "inn", name: "inn" },
  { id: "participation_count", name: "participationCount" },
  { id: "wins_count", name: "winsCount" },
  { id: "price", name: "price" },
  { id: "price_drop", name: "priceDrop" }
];

const defaultOrder = "desc";
export class SuppliersSummaryHeader extends Component {
  static propTypes = {
    orderBy: PropTypes.string.isRequired,
    order: PropTypes.string.isRequired
  };

  createSortHandler(newOrderBy) {
    return event => {
      const { order, onSort, orderBy } = this.props;
      const newOrder =
        orderBy === newOrderBy
          ? order === defaultOrder
            ? "asc"
            : defaultOrder
          : defaultOrder;

      onSort(event, newOrderBy, newOrder);
    };
  }

  render() {
    const { orderBy, order, classes, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <TableHead>
        <TableRow>
          {cells.map(cell => (
            <TableCell
              key={cell.id}
              align={cell.name === "name" ? "left" : "right"}
              className={classes[cell.name]}
              sortDirection={orderBy === cell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === cell.id}
                direction={orderBy === cell.id ? order : defaultOrder}
                onClick={this.createSortHandler(cell.id)}
                classes={{
                  root: classes.tableSortLabel,
                  icon: classes.tableSortLabelIcon
                }}
              >
                <span className={classes[cell.name]}>
                  {formatMessage(messages[cell.name])}
                </span>
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }
}

export default injectIntl(withStyles(styles)(SuppliersSummaryHeader));
