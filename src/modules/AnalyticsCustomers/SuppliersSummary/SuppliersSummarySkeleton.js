import React, { Component } from "react";
import { TableCell, TableRow } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { injectIntl } from "react-intl";

class SuppliersSummarySkeleton extends Component {
  render() {
    return (
      <>
        <TableRow>
          <TableCell>
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell>
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell>
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell>
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell>
            <Skeleton width="100%" height="30%" />
          </TableCell>
          <TableCell>
            <Skeleton width="100%" height="30%" />
          </TableCell>
        </TableRow>
      </>
    );
  }
}

export default injectIntl(SuppliersSummarySkeleton);
