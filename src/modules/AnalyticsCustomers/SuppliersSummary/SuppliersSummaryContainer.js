import React from "react";
import { connect } from "react-redux";

import SuppliersSummary from "./SuppliersSummary";
import {
  getSuppliersSummary,
  getSuppliersSummaryLoading
} from "./SuppliersSummarySelector";
import {
  fetchSuppliersSummary,
  cleanSuppliersSummary
} from "./SuppliersSummaryActions";

const SuppliersSummaryContainer = props => <SuppliersSummary {...props} />;

const mapStateToProps = state => {
  return {
    suppliers: getSuppliersSummary(state),
    loading: getSuppliersSummaryLoading(state)
  };
};

export default connect(mapStateToProps, {
  fetchSuppliersSummary,
  cleanSuppliersSummary
})(SuppliersSummaryContainer);
