import {
  fetchPaginateEntities,
  cleanPaginateEntities
} from "../../../services/Entities/EntitiesActions";
import { ENTITY } from "./SuppliersSummaryConstants";

export const fetchSuppliersSummary = query =>
  fetchPaginateEntities(ENTITY, query);

export const cleanSuppliersSummary = () => cleanPaginateEntities(ENTITY);
