import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  withStyles
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../../utils/CoreUtils";
import Section from "../../../common/Section/Section";
import ExportButtonContainer from "../../../common/ExportButton/ExportButtonContainer";
import { normalizeFilter } from "../../Tender/Filter/FilterHelpers";
import SuppliersSummaryHeader from "./SuppliersSummaryHeader";
import SuppliersSummarySkeleton from "./SuppliersSummarySkeleton";
import { EXPORT_ENTITY } from "./SuppliersSummaryConstants";

const messages = defineMessages({
  title: {
    id: "analytics-customers.suppliers-summary.title",
    defaultMessage: "Топ 10 поставщиков заказчика по указанной тематике"
  },
  helpButtonBody: {
    id: "analytics-customers.suppliers-summary.help-button-body",
    defaultMessage: `Отчет показывает, на какую сумму поставщики заключают
    контракты, и какое снижение цены стоит ожидать. Видно, кто чаще побеждает и на каких условиях.`
  }
});

const styles = theme => ({
  nowrapCell: {
    whiteSpace: "nowrap"
  }
});

export class SuppliersSummary extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
  };

  constructor(props) {
    super(props);

    this.query = {};

    this.state = {
      page: 0,
      orderBy: "wins_count",
      order: "desc"
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const { filter, cleanSuppliersSummary } = this.props;
    const { page, orderBy, order } = this.state;

    if (isEmpty(filter)) {
      if (!this.cleaned) {
        this.cleaned = true;
        cleanSuppliersSummary();
      }

      return;
    }

    if (prevProps.filter !== filter) {
      this.fetch();
      return;
    }

    if (
      prevState.page !== page ||
      prevState.orderBy !== orderBy ||
      prevState.order !== order
    ) {
      this.fetch();
    }
  }

  fetch() {
    const { fetchSuppliersSummary } = this.props;

    this.query = this.buildQuery();
    fetchSuppliersSummary(this.query);
  }

  buildQuery() {
    const { filter } = this.props;
    const { page, orderBy, order } = this.state;

    return {
      search: filter && normalizeFilter(filter),
      page,
      sort: `${orderBy} ${order}`
    };
  }

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  handleSort = (event, orderBy, order) => {
    this.setState({ orderBy, order });
  };

  render() {
    const {
      suppliers: { items },
      loading,
      variant,
      intl
    } = this.props;
    const { orderBy, order } = this.state;
    const { formatNumber, formatMessage } = intl;

    return (
      <Section
        variant={variant}
        title={formatMessage(messages.title)}
        titleAction={
          <ExportButtonContainer name={EXPORT_ENTITY} query={this.query} />
        }
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        <TableContainer>
          <Table>
            <SuppliersSummaryHeader
              onSort={this.handleSort}
              orderBy={orderBy}
              order={order}
            />
            <TableBody>
              {loading && <SuppliersSummarySkeleton />}
              {!loading &&
                items &&
                items.map((item, index) => (
                  <TableRow key={index}>
                    <TableCell>{item.name}</TableCell>
                    <TableCell align="right">{item.inn}</TableCell>
                    <TableCell align="right">
                      {item.participationCount}
                    </TableCell>
                    <TableCell align="right">{item.winsCount}</TableCell>
                    <TableCell align="right">
                      {formatNumber(item.price, { style: "decimal" })}
                    </TableCell>
                    <TableCell align="right">
                      {item.priceDrop === 100 ? "-" : `${item.priceDrop}`}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(SuppliersSummary));
