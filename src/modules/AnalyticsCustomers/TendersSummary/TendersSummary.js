import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isEmpty } from "../../../utils/CoreUtils";
import Section from "../../../common/Section/Section";
import { normalizeFilter } from "../../Tender/Filter/FilterHelpers";
import TendersSummaryCard from "./TendersSummaryCard";
import TendersSummarySkeleton from "./TendersSummarySkeleton";

const messages = defineMessages({
  title: {
    id: "analytics-customers.tenders-summary.title",
    defaultMessage: "Статистика по заказчику"
  },
  helpButtonBody: {
    id: "analytics-customers.tenders-summary.help-button-body",
    defaultMessage: `Отчет наглядно покажет всю тендерную деятельность заказчика:
    <ol>
      <li>Количество активных закупок</li>
      <li>Насколько обычно снижается цена контракта</li>
      <li>Среднее количество участников</li>
    </ol>`
  },
  activeTenders: {
    id: "analytics-customers.tenders-summary.active-tenders",
    defaultMessage: "активных тендера <br/>на {sum, number, rub}"
  },
  averagePrice: {
    id: "analytics-customers.tenders-summary.average-price",
    defaultMessage: "средняя цена <br/>завершенного тендера"
  },
  averageSuppliersCount: {
    id: "analytics-customers.tenders-summary.average-suppliers-count",
    defaultMessage: "среднее количество <br/>участников"
  },
  averagePriceDrop: {
    id: "analytics-customers.tenders-summary.average-price-drop",
    defaultMessage: "среднее снижение <br/>цены"
  }
});

const styles = theme => ({
  summary: {
    display: "flex",
    flexDirection: "row"
  },
  summaryItem: {
    marginRight: theme.spacing(3),
    minWidth: "175px"
  }
});

export class TendersSummary extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
  };

  componentDidMount() {
    const { autofetch, cleanTendersSummary } = this.props;
    if (autofetch) {
      cleanTendersSummary();
      this.fetch();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { filter, cleanTendersSummary } = this.props;

    if (isEmpty(filter)) {
      if (!this.cleaned) {
        this.cleaned = true;
        cleanTendersSummary();
      }

      return;
    }

    if (prevProps.filter !== filter) {
      this.fetch();
    }
  }

  fetch() {
    const { fetchTendersSummary, filter } = this.props;

    if (isEmpty(filter)) {
      return;
    }

    fetchTendersSummary(this.buildQuery());
  }

  buildQuery() {
    const { filter } = this.props;

    return {
      search: normalizeFilter(filter)
    };
  }

  render() {
    const { summary, classes, loaded, variant, intl } = this.props;
    const { formatMessage, formatNumber } = intl;

    return (
      <Section
        variant={variant}
        title={formatMessage(messages.title)}
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        {!loaded && <TendersSummarySkeleton />}
        {loaded && (
          <div className={classes.summary}>
            {summary &&
              Object.keys(summary).map(key => (
                <div key={key} className={classes.summaryItem}>
                  <TendersSummaryCard
                    value={formatNumber(summary[key]["value"], {
                      style: "decimal"
                    })}
                    title={formatMessage(messages[key], {
                      sum: summary[key]["sum"]
                    })}
                    unit={summary[key]["unit"]}
                    key={key}
                  />
                </div>
              ))}
          </div>
        )}
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(TendersSummary));
