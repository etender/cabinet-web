import React from "react";
import { connect } from "react-redux";

import TendersSummary from "./TendersSummary";
import {
  getTendersSummary,
  getTendersSummaryLoaded
} from "./TendersSummarySelector";
import {
  fetchTendersSummary,
  cleanTendersSummary
} from "./TendersSummaryActions";

const TendersSummaryContainer = props => <TendersSummary {...props} />;

const mapStateToProps = state => {
  return {
    summary: getTendersSummary(state),
    loaded: getTendersSummaryLoaded(state)
  };
};

export default connect(mapStateToProps, {
  fetchTendersSummary,
  cleanTendersSummary
})(TendersSummaryContainer);
