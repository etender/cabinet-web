import React, { Component } from "react";
import { withStyles } from "@material-ui/core";

import { Skeleton } from "@material-ui/lab";

const styles = theme => ({
  tendersSummary: {
    display: "flex",
    flexDirection: "row"
  },
  summaryItem: {
    marginRight: theme.spacing(3),
    minWidth: "175px"
  }
});

export class TendersSummarySkeleton extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.tendersSummary}>
        <div className={classes.summaryItem}>
          <Skeleton width="40px" height="60px" />
          <Skeleton width="150px" height="28px" />
          <Skeleton width="150px" height="28px" />
        </div>
        <div className={classes.summaryItem}>
          <Skeleton width="170px" height="60px" />
          <Skeleton width="150px" height="28px" />
          <Skeleton width="150px" height="28px" />
        </div>
        <div className={classes.summaryItem}>
          <Skeleton width="40px" height="60px" />
          <Skeleton width="150px" height="28px" />
          <Skeleton width="150px" height="28px" />
        </div>
        <div className={classes.summaryItem}>
          <Skeleton width="40px" height="60px" />
          <Skeleton width="150px" height="28px" />
          <Skeleton width="150px" height="28px" />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TendersSummarySkeleton);
