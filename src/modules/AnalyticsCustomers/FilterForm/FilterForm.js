import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import AutocompleteFieldContainer from "../../../common/AutocompleteField/AutocompleteFieldContainer";
import FilterFormContainer from "../../Analytics/FilterForm/FilterFormContainer";
import FilterFormContent from "../../Analytics/FilterForm/FilterFormContent";

const messages = defineMessages({
  customersField: {
    id: "analytics-customers.filter-form.customers-field",
    defaultMessage: "Заказчик"
  },
  customersFieldHelpMessage: {
    id: "analytics-customers.filter-form.customers-field-help-message",
    defaultMessage: `<p>Введите наименование или ИНН организации-заказчика. Вы можете указать одно или сразу несколько значений.</p>`
  },
  helpButtonBody: {
    id: "analytics-customers.filter-form.help-button-body",
    defaultMessage: `Вы получаете аналитику на основе настроенного фильтра в личном кабинете. 
    Результат поиска показывает статистику по завершенным закупкам заказчика, где есть победитель.`
  }
});

class FilterForm extends Component {
  render() {
    const { entity, path, defaultFilter, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <FilterFormContainer
        entity={entity}
        path={path}
        defaultFilter={defaultFilter}
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        {props => (
          <FilterFormContent {...props}>
            {({ field, getValue, handleChange }) => (
              <AutocompleteFieldContainer
                fullWidth
                label={formatMessage(messages.customersField)}
                name="customers"
                fieldRef={field}
                value={getValue("customers")}
                onChange={value => handleChange("customers", value)}
                helpButtonBody={formatMessage(
                  messages.customersFieldHelpMessage
                )}
              />
            )}
          </FilterFormContent>
        )}
      </FilterFormContainer>
    );
  }
}

export default injectIntl(FilterForm);
