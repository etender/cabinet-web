import { clearFilter } from "../Analytics/FilterBuilder/FilterBuilderActions";
import { CUSTOMERS_NAME } from "./AnalyticsCustomersConstants";

export const cleanCustomers = () => clearFilter(CUSTOMERS_NAME);
