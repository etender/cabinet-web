import React from "react";
import { connect } from "react-redux";

import {
  getFilter,
  getIsEditing
} from "../Analytics/FilterBuilder/FilterBuilderSelectors";
import {
  CUSTOMERS_NAME,
  CUSTOMERS_PATH,
  CUSTOMERS_DEFAULT_FILTER
} from "./AnalyticsCustomersConstants";
import AnalyticsCustomers from "./AnalyticsCustomers";

const AnalyticsCustomersContainer = props => <AnalyticsCustomers {...props} />;

const mapStateToProps = state => {
  const filter = getFilter(state, CUSTOMERS_NAME);
  return {
    filter: filter,
    editing: getIsEditing(state, CUSTOMERS_NAME),
    entity: CUSTOMERS_NAME,
    entityPath: CUSTOMERS_PATH,
    defaultFilter: CUSTOMERS_DEFAULT_FILTER
  };
};

export default connect(mapStateToProps)(AnalyticsCustomersContainer);
