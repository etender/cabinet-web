import React from "react";
import { connect } from "react-redux";

import PurchasesSummary from "./PurchasesSummary";
import {
  getPurchasesSummary,
  getPurchasesSummaryLoading
} from "./PurchasesSummarySelector";
import {
  fetchPurchasesSummary,
  cleanPurchasesSummary
} from "./PurchasesSummaryActions";

const PurchasesSummaryContainer = props => <PurchasesSummary {...props} />;

const mapStateToProps = state => {
  return {
    purchases: getPurchasesSummary(state),
    loading: getPurchasesSummaryLoading(state)
  };
};

export default connect(mapStateToProps, {
  fetchPurchasesSummary,
  cleanPurchasesSummary
})(PurchasesSummaryContainer);
