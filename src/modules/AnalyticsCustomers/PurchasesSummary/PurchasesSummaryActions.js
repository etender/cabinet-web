import {
  fetchPaginateEntities,
  cleanPaginateEntities
} from "../../../services/Entities/EntitiesActions";
import { ENTITY } from "./PurchasesSummaryConstants";

export const fetchPurchasesSummary = query =>
  fetchPaginateEntities(ENTITY, query);

export const cleanPurchasesSummary = () => cleanPaginateEntities(ENTITY);
