import { getPaginateEntities } from "../../../services/Entities/EntitiesSelectors";
import { getArrayEntitiesLoading } from "../../../services/Entities/ArrayEntities/ArrayEntitiesSelectors";
import { ENTITY } from "./PurchasesSummaryConstants";

export const getPurchasesSummary = state => getPaginateEntities(state, ENTITY);

export const getPurchasesSummaryLoading = state => {
  const loading = getArrayEntitiesLoading(state, ENTITY);

  return loading === undefined ? true : loading;
};
