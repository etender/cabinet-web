import React, { Component } from "react";
import SimpleBar from "simplebar-react";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";

import FilterForm from "./FilterForm/FilterForm";
import FilterDescriptionContainer from "../Analytics/FilterDescription/FilterDescriptionContainer";
import TendersSummaryContainer from "./TendersSummary/TendersSummaryContainer";
import ControlResultsContainer from "./ControlResults/ControlResultsContainer";
import PurchasesSummaryContainer from "./PurchasesSummary/PurchasesSummaryContainer";

const messages = defineMessages({
  helpButtonBody: {
    id: "analytics-customers.filter-description.help-button-body",
    defaultMessage: `Вы получаете аналитику на основе настроенного фильтра в личном кабинете. 
    Результат поиска показывает статистику по завершенным закупкам заказчика, где есть победитель.`
  }
});

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    overflowY: "auto",

    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    }
  }
});

export class AnalyticsCustomers extends Component {
  render() {
    const {
      filter,
      editing,
      entity,
      entityPath,
      defaultFilter,
      intl,
      classes
    } = this.props;
    const { formatMessage } = intl;

    return (
      <SimpleBar style={{ height: "100%" }}>
        <div className={classes.root}>
          {editing && (
            <FilterForm
              entity={entity}
              path={entityPath}
              defaultFilter={defaultFilter}
            />
          )}
          {!editing && (
            <>
              <FilterDescriptionContainer
                entity={entity}
                path={entityPath}
                defaultFilter={defaultFilter}
                helpButtonBody={formatMessage(messages.helpButtonBody)}
              />
              <TendersSummaryContainer
                filter={filter}
                variant="filled"
                autofetch
              />
              <ControlResultsContainer
                filter={filter}
                variant="filled"
                autofetch
              />
              <PurchasesSummaryContainer
                filter={filter}
                variant="filled"
                autofetch
              />
            </>
          )}
        </div>
      </SimpleBar>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsCustomers));
