import React, { Component } from "react";
import PropTypes from "prop-types";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  IconButton,
  Grid,
  Tooltip,
  Typography,
  withStyles
} from "@material-ui/core";
import { FilterNone as CopyIcon } from "@material-ui/icons";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import DialogContainer from "../../../common/Dialog/DialogContainer";

const messages = defineMessages({
  title: {
    id: "dashboard.filter-share.title",
    defaultMessage: "Поделиться фильтром"
  },
  emailField: {
    id: "dashboard.filter-share.email-field",
    defaultMessage: "Имя"
  },
  shareButton: {
    id: "dashboard.filter-share.share-button",
    defaultMessage: "Поделиться"
  },
  copyTooltip: {
    id: "dashboard.filter-share.copy-tooltip",
    defaultMessage: "Скопировать в буфер обмена"
  }
});

const styles = theme => ({
  paper: {
    minHeight: theme.spacing(10),

    [theme.breakpoints.up("md")]: {
      minWidth: "500px"
    }
  },

  url: {
    lineHeight: "44px"
  },

  content: {
    paddingLeft: 0,
    paddingRight: 0
  }
});

export class FilterShare extends Component {
  static propTypes = {
    url: PropTypes.string
  };

  render() {
    const { classes, url, onCopy, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogContainer
        name="filterShare"
        classes={{ paper: classes.paper }}
        title={formatMessage(messages.title)}
        actionsDisabled
        formDisabled
      >
        <Grid container spacing={0}>
          <Grid item xs={11}>
            <Typography noWrap className={classes.url}>
              {url}
            </Typography>
          </Grid>
          <Grid item xs={1}>
            <CopyToClipboard text={url} onCopy={onCopy}>
              <Tooltip
                title={formatMessage(messages.copyTooltip)}
                aria-label="Скопировать"
              >
                <IconButton edge="end" aria-label="Скопировать">
                  <CopyIcon />
                </IconButton>
              </Tooltip>
            </CopyToClipboard>
          </Grid>
        </Grid>
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterShare));
