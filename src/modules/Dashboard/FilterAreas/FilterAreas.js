import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { areasHash } from "./FilterAreasSelectors";
import DialogField from "../../../common/DialogField/DialogField";
import { isPresent } from "../../../utils/CoreUtils";
import FilterBaseAreas from "../FilterBaseAreas/FilterBaseAreas";
import FilterCommonAreas from "../FilterCommonAreas/FilterCommonAreas";

const messages = defineMessages({
  name: {
    id: "dashboard.filter-areas.name",
    defaultMessage: "Площадки"
  },
  helpMessage: {
    id: "dashboard.filter-areas.help-message",
    defaultMessage: `<p>Выберите из списка любые электронные торговые площадки. Например, если вы ориентированы только на государственные закупки, выберите только госплощадки, тем самым исключая из поисков все коммерческие. По умолчанию, поиск осуществляется по всем подключенным площадкам.</p>`
  }
});

class FilterAreas extends Component {
  static propTypes = {
    value: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onlyBase: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.state = {
      suggestions: null
    };
  }

  serialize = value => {
    return Object.values(value)
      .filter(item => item)
      .map(item => item.id);
  };

  deserialize(value) {
    return (isPresent(value) ? value : []).map(id => areasHash[id]);
  }

  deserializeSelected(value) {
    return value
      ? value.reduce((result, item) => ({ ...result, [item.id]: item }), {})
      : {};
  }

  renderContent(props) {
    const { onlyBase } = this.props;
    return (
      <Fragment>
        <FilterBaseAreas {...props} />
        {!onlyBase && <FilterCommonAreas {...props} />}
      </Fragment>
    );
  }

  render() {
    const { value, onChange, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogField
        title={formatMessage(messages.name)}
        serialize={this.serialize}
        deserialize={this.deserialize}
        deserializeSelected={this.deserializeSelected}
        onChange={onChange}
        suggestionsComponent={({ search, selected, handleToggleSelected }) =>
          this.renderContent({
            search,
            value: selected,
            onChange: handleToggleSelected
          })
        }
        value={value}
        helpButtonBody={formatMessage(messages.helpMessage)}
      >
        {({ selected, handleToggleSelected }) =>
          this.renderContent({
            value: selected,
            onChange: handleToggleSelected
          })
        }
      </DialogField>
    );
  }
}

export default injectIntl(FilterAreas);
