export const areas = [
  {
    id: "zakupki_gov_ru",
    name: "Официальный сайт ЕИС в сфере закупок",
    href: "http://zakupki.gov.ru",
    included: [
      {
        name: "ЕЭТП",
        href: "http://roseltorg.ru"
      },
      {
        name: "РТС-тендер",
        href: "http://rts-tender.ru"
      },
      {
        name: "Сбербанк-АСТ",
        href: "http://sberbank-ast.ru"
      },
      {
        name: "ТЭК-Торг",
        href: "http://tektorg.ru"
      },
      {
        name: "ЭТП ГПБ",
        href: "http://etp.gpb.ru"
      },
      {
        name: "ЭТП НЭП",
        href: "http://etp-ets.ru"
      },
      {
        name: "ЭТП РАД",
        href: "http://lot-online.ru"
      },
      {
        name: "ZakazRF",
        href: "http://etp.zakazrf.ru"
      }
    ]
  },
  {
    id: "akd_ru",
    name: "Аукционный Конкурсный Дом",
    href: "http://a-k-d.ru"
  },
  {
    id: "atctrade_ru",
    name: "Аукционный тендерный центр",
    href: "http://atctrade.ru"
  },
  {
    id: "zakupki_bashkirenergo_ru",
    name: "Башкирэнерго",
    href: "http://zakupki.bashkirenergo.ru"
  },
  {
    id: "gazneftetorg_ru",
    name: "ГАЗНЕФТЕТОРГ.РУ",
    href: "http://gazneftetorg.ru"
  },
  {
    id: "gazprom_ru",
    name: "ГАЗПРОМ",
    href: "http://gazprom.ru"
  },
  {
    id: "tender_gazprom_neft_ru",
    name: "ГАЗПРОМ НЕФТЬ",
    href: "http://zakupki.gazprom-neft.ru"
  },
  {
    id: "etp_gpb_ru",
    name: "Группа Газпромбанка",
    href: "http://etp.gpb.ru"
  },
  {
    id: "roseltorg_ru",
    name: "Единая электронная торговая площадка",
    href: "http://roseltorg.ru"
  },
  {
    id: "irkutskoil_ru",
    name: "Иркутская нефтяная компания",
    href: "http://irkutskoil.ru"
  },
  {
    id: "cpc_ru",
    name: "Каспийский трубопроводный консорциум",
    href: "http://cpc.ru"
  },
  {
    id: "zakupki_komos_ru",
    name: "КОМОС групп",
    href: "http://zakupkikomos.ru"
  },
  {
    id: "lukoil_ru",
    name: "ЛУКОЙЛ",
    href: "http://lukoil.ru"
  },
  {
    id: "mmk_ru",
    name: "Магнитогорский металлургический комбинат",
    href: "http://mmk.ru"
  },
  {
    id: "tenders_mts_ru",
    name: "МТС",
    href: "http://tenders.mts.ru"
  },
  {
    id: "etp_ets_ru",
    name: "Национальная электронная площадка",
    href: "http://etp-ets.ru"
  },
  {
    id: "zf_norilsknickel_ru",
    name: "Норильский никель Заполярный филиал",
    href: "http://zf.norilsknickel.ru"
  },
  {
    id: "tender_otc_ru",
    name: "ОТС",
    href: "http://tender.otc.ru"
  },
  {
    id: "torgi_gov_ru_xml",
    name: "Официальный сайт РФ о проведении торгов",
    href: "http://torgi.gov.ru"
  },
  {
    id: "zakupki_mos_ru",
    name: "Портал поставщиков",
    href: "http://zakupki.mos.ru"
  },
  {
    id: "pptk_mos_ru",
    name: "ППТК",
    href: "http://pptk-mos.ru"
  },
  {
    id: "zakupki_rosneft_ru",
    name: "РОСНЕФТЬ ЗАКУПКИ",
    href: "http://zakupki.rosneft.ru"
  },
  {
    id: "rts_tender_ru",
    name: "РТС-тендер",
    href: "http://rts-tender.ru"
  },
  {
    id: "sng_ru",
    name: "Саратовнефтегаз",
    href: "http://sng.ru"
  },
  {
    id: "utp_sberbank_ast_ru_api",
    name: "Сбербанк-АСТ",
    href: "http://utp.sberbank-ast.ru"
  },
  {
    id: "agro_zakupki_tomsk_ru",
    name: "Сибирская Аграрная Группа",
    href: "http://agro.zakupki.tomsk.ru"
  },
  {
    id: "etp_tatneft_ru",
    name: "Татнефть",
    href: "http://etp.tatneft.ru"
  },
  {
    id: "torgi223_ru",
    name: "Торги 223",
    href: "http://torgi223.ru"
  },
  {
    id: "tektorg_ru",
    name: "ТЭК-Торг",
    href: "http://tektorg.ru"
  },
  {
    id: "uralchem_ru",
    name: "УРАЛХИМ",
    href: "http://uralchem.ru"
  },
  {
    id: "fabrikant_ru",
    name: "Фабрикант",
    href: "http://fabrikant.ru"
  },
  {
    id: "bankrupt_centerr_ru",
    name: "Центр реализации",
    href: "http://bankrupt.centerr.ru"
  },
  {
    id: "market_mosreg_ru",
    name: "Электронный магазин Московской области",
    href: "http://market.mosreg.ru"
  },
  {
    id: "etprf_ru",
    name: "ЭТП",
    href: "http://etprf.ru"
  },
  {
    id: "b2b_sibur_ru",
    name: "B2B-Сибур",
    href: "http://b2b.sibur.ru"
  },
  {
    id: "b2b_energo_ru",
    name: "B2B-Энерго (B2B-Центр)",
    href: "http://b2b-energo.ru"
  },
  {
    id: "b2b_center_ru",
    name: "B2B-Центр (банкротство)",
    href: "http://b2b-center.ru/debt"
  },
  {
    id: "bankruptcy_lot_online_ru",
    name: "Lot-online",
    href: "http://bankruptcy.lot-online.ru"
  },
  {
    id: "onlinecontract_ru",
    name: "ONLINECONTRACT",
    href: "http://onlinecontract.ru"
  },
  {
    id: "tender_pro",
    name: "Tender.Pro",
    href: "http://tender.pro"
  },
  {
    id: "reg_zakazrf_ru",
    name: "ZakazRF",
    href: "http://etp.zakazrf.ru"
  },
  {
    id: "bp_zakazrf_ru",
    name: "ZakazRF Биржевая площадка",
    href: "http://bp.zakazrf.ru"
  },
  {
    id: "agregatoreat_ru",
    name: "ЕАТ Березка",
    href: "https://agregatoreat.ru"
  },
  {
    id: "stroytorgi_ru",
    name: "СТРОЙТОРГИ (только архив)",
    href: "http://stroytorgi.ru"
  },
  {
    id: "oborontorg_ru",
    name: "ОБОРОНТОРГ (только архив)",
    href: "http://oborontorg.ru"
  },
  {
    id: "zakupki_bashneft_ru",
    name: "БАШНЕФТЬ (только архив)",
    href: "http://zakupki.bashneft.ru"
  },
  {
    id: "bankrot_fedresurs_ru",
    name: "Единый федеральный реестр сведений о банкротстве (только архив)",
    href: "http://fedresurs.ru"
  },
  {
    id: "tzselektra_ru",
    name: "ТЗС Электра (только архив)",
    href: "http://tzselektra.ru"
  },
  {
    id: "tenders_generation_ru",
    name: "Генерация (только архив)",
    href: "http://tenders.generation.ru"
  },
  {
    id: "zakup_rk_ru",
    name: "Госзакупки в Республике Крым (только архив)",
    href: "http://zakup-rk.ru"
  },
  {
    id: "kku_ru",
    name: "КРАСНОЯРСК КРАЙУГОЛЬ (только архив)",
    href: "http://kku.ru"
  },
  {
    id: "omk_zakupim_ru",
    name: "Объединенная Металлургическая компания (только архив)",
    href: "http://omk.zakupim.ru"
  },
  {
    id: "ritek_ru",
    name: "РИТЭК (только архив)",
    href: "http://ritek.ru"
  },
  {
    id: "sibneftegaz_ru",
    name: "Сибнефтегаз (только архив)",
    href: "http://Только архивные"
  },
  {
    id: "transnefteproduct_transneft_ru",
    name: "Транснефтепродукт (только архив)",
    href: "http://transnefteproduct.transneft.ru"
  }
];

export const baseArea = areas[0];
export const commonAreas = areas.slice(1);
export const areasHash = areas.reduce((result, item) => {
  result[item.id] = item;
  return result;
}, {});
