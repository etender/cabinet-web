import React from "react";
import { connect } from "react-redux";

import { getShared } from "../../Shared/SharedSelectors";
import { signOutUser } from "../../../services/Devise/DeviseActions";
import { showProfile } from "../../Profile/ProfileActions";
import { showMembers } from "../../Members/MembersActions";
import { toggleDrawerOpen } from "../Drawer/DrawerActions";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { editFilter } from "../Filter/FilterActions";
import Toolbar from "./Toolbar";
import { push } from "connected-react-router";
import { DASHBOARD_PATH } from "../DashboardConstants";

const ToolbarContainer = props => <Toolbar {...props} />;

const mapStateToProps = state => {
  return {
    user: getCurrentUser(state),
    shared: getShared(state),
    homePath: DASHBOARD_PATH
  };
};

export default connect(mapStateToProps, {
  signOut: signOutUser,
  showProfile,
  showMembers,
  editFilter,
  toggleDrawerOpen,
  push
})(ToolbarContainer);
