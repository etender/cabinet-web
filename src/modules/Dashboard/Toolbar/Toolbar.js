import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  AppBar,
  Toolbar as MuiToolbar,
  IconButton,
  Tooltip,
  Typography,
  Avatar,
  Popover,
  Button,
  Divider,
  withStyles
} from "@material-ui/core";

import {
  AccountCircle as AccountCircleIcon,
  Menu as MenuIcon,
  Help as HelpIcon,
  Search as SearchIcon
} from "@material-ui/icons";

import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import FastSearchContainer from "../FastSearch/FastSearchContainer";
import etLogo from "../../../images/logo-white.svg";
import { defaultFilter } from "../Filter/FilterHelpers";

const messages = defineMessages({
  etTitle: {
    id: "dashboard.toolbar.et-title",
    defaultMessage: "Гарант Тендер"
  },
  menu: {
    id: "dashboard.toolbar.menu",
    defaultMessage: "Меню"
  },
  notices: {
    id: "dashboard.toolbar.notices",
    defaultMessage: "Уведомления"
  },
  profile: {
    id: "dashboard.toolbar.profile",
    defaultMessage: "Профиль пользователя"
  },
  settings: {
    id: "dashboard.toolbar.settings",
    defaultMessage: "Настройки"
  },
  exit: {
    id: "dashboard.toolbar.exit",
    defaultMessage: "Выход"
  },
  help: {
    id: "dashboard.toolbar.help",
    defaultMessage: "Помощь"
  },
  members: {
    id: "dashboard.toolbar.members",
    defaultMessage: "Управление пользователями"
  },
  advancedSearch: {
    id: "dashboard.toolbar.advanced-search",
    defaultMessage: "Точный поиск"
  }
});

const styles = theme => ({
  toolbar: {
    [theme.breakpoints.down("sm")]: {
      marginLeft: 0
    }
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  title: {
    paddingLeft: theme.spacing(2)
  },
  logoBlock: {
    minWidth: theme.spacing(32),
    display: "flex",
    position: "relative",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      minWidth: "auto"
    }
  },
  logo: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    cursor: "pointer",
    [theme.breakpoints.down("sm")]: {
      marginLeft: 0
    }
  },
  logoShared: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    cursor: "pointer"
  },
  search: {
    marginRight: "auto",
    marginLeft: "0",
    [theme.breakpoints.down("xs")]: {
      display: "none"
    }
  },
  menuButton: {
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  buttons: {
    display: "flex",
    justifyContent: "right",
    marginLeft: "auto",
    paddingLeft: theme.spacing(1.5),
    paddingRight: theme.spacing(1.5),
    [theme.breakpoints.down("xs")]: {
      paddingLeft: 0,
      paddingRight: 0
    }
  },
  left: {
    flex: "0 0 auto",
    display: "flex",
    minWidth: theme.spacing(34),
    alignItems: "center"
  },
  name: {
    textAlign: "center",
    margin: "0 auto",
    fontSize: "1.4em"
  },
  email: {
    textAlign: "center",
    margin: "0 auto",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden"
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4)
  },
  avatarBig: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    margin: theme.spacing(2) + "px auto"
  },
  userMenuAppbar: {
    minWidth: theme.spacing(35)
  },
  userMenuAppbarEmail: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  settingsMenuButton: {
    display: "block",
    margin: theme.spacing(2) + "px auto"
  },
  searchButton: {
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  }
});

export class Toolbar extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
  }

  handleDrawerToggle = () => {
    const { toggleDrawerOpen } = this.props;
    toggleDrawerOpen();
  };

  render() {
    const {
      classes,
      signOut,
      showProfile,
      showMembers,
      user,
      shared,
      push,
      homePath
    } = this.props;
    const { formatMessage } = this.props.intl;
    let { anchorEl } = this.state;

    const handleShowProfile = () => {
      this.setState({ anchorEl: null });
      showProfile();
    };

    const handleShowMembers = () => {
      this.setState({ anchorEl: null });
      showMembers();
    };

    const handleClick = event => {
      this.setState({ anchorEl: event.currentTarget });
    };

    const handleClose = () => {
      this.setState({ anchorEl: null });
    };

    const handleAdvancedSearch = () => {
      const { editFilter } = this.props;
      let options = { ...defaultFilter };
      editFilter(options);
    };

    return (
      <AppBar position="absolute" className={classes.appBar}>
        <MuiToolbar disableGutters className={classes.toolbar}>
          <div className={classes.logoBlock}>
            {!shared && (
              <IconButton
                color="inherit"
                aria-label={formatMessage(messages.menu)}
                onClick={this.handleDrawerToggle}
                className={classes.menuButton}
              >
                <MenuIcon />
              </IconButton>
            )}
            <img
              src={etLogo}
              width={120}
              className={shared ? classes.logoShared : classes.logo}
              alt={formatMessage(messages.etTitle)}
              onClick={() => push(homePath)}
            />
          </div>
          <FastSearchContainer className={classes.search} />
          <div className={classes.buttons}>
            <Tooltip
              title={formatMessage(messages.advancedSearch)}
              className={classes.searchButton}
            >
              <IconButton color="inherit" onClick={handleAdvancedSearch}>
                <SearchIcon />
              </IconButton>
            </Tooltip>

            <Tooltip title={formatMessage(messages.help)}>
              <IconButton color="inherit" target="_blank" href="/help/">
                <HelpIcon />
              </IconButton>
            </Tooltip>

            {!shared && (
              <>
                <Tooltip title={formatMessage(messages.settings)}>
                  <IconButton color="inherit" onClick={handleClick}>
                    <AccountCircleIcon />
                  </IconButton>
                </Tooltip>
                <Popover
                  id="user-menu-appbar"
                  open={Boolean(anchorEl)}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                >
                  <div className={classes.userMenuAppbar}>
                    <Avatar
                      aria-label={user.fullName}
                      src={user.avatarImageUrl}
                      className={classes.avatarBig}
                    />
                    <Typography className={classes.name}>
                      {user.name} {user.surname}
                    </Typography>
                    <div className={classes.userMenuAppbarEmail}>
                      <Typography
                        color="textSecondary"
                        className={classes.email}
                      >
                        {user.email}
                      </Typography>
                    </div>
                    <Button
                      className={classes.settingsMenuButton}
                      onClick={handleShowProfile}
                      color="primary"
                    >
                      {formatMessage(messages.profile)}
                    </Button>
                    {!user.foreign && (
                      <Fragment>
                        <Divider />
                        <Button
                          className={classes.settingsMenuButton}
                          onClick={handleShowMembers}
                          color="primary"
                        >
                          {formatMessage(messages.members)}
                        </Button>
                      </Fragment>
                    )}
                    <Divider />
                    <Button
                      className={classes.settingsMenuButton}
                      onClick={signOut}
                      color="primary"
                    >
                      {formatMessage(messages.exit)}
                    </Button>
                  </div>
                </Popover>
              </>
            )}
          </div>
        </MuiToolbar>
      </AppBar>
    );
  }
}

export default injectIntl(withStyles(styles)(Toolbar));
