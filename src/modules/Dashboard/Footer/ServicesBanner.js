import React, { Component } from "react";
import classNames from "classnames";
import { Button, Slide, Typography, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import bgServices from "../../../images/services/bg_services.png";

const styles = theme => ({
  root: {
    position: "absolute",
    zIndex: theme.zIndex.appBar,
    height: 71,
    left: 0,
    right: 0,
    bottom: 0,
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.getContrastText(theme.palette.primary.main),
    overflow: "hidden",
    backgroundImage: `url(${bgServices})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right"
  },
  title: {
    textTransform: "uppercase",
    paddingRight: "40px",
    fontWeight: "500",
    fontSize: "17px"
  }
});

export const getServicesBannerViewed = () => {
  return localStorage.getItem("services_banner_viewed");
};

const setServicesBannerViewed = value => {
  localStorage.setItem("services_banner_viewed", value);
};

export class ServicesBanner extends Component {
  constructor(props) {
    super(props);

    this.state = { opened: false };
  }

  componentDidMount() {
    this.viewed = getServicesBannerViewed();
    if (this.viewed) {
      return;
    }

    window.setTimeout(() => {
      this.setState({ opened: true });
    }, 1000);
  }

  handleClick = () => {
    setServicesBannerViewed(true);

    this.setState({ opened: false });

    const { onClick } = this.props;
    onClick && onClick();
  };

  render() {
    const { classes, className } = this.props;
    const { opened } = this.state;

    if (this.viewed) {
      return "";
    }

    return (
      <Slide direction="up" in={opened} mountOnEnter unmountOnExit>
        <div className={classNames(classes.root, className)}>
          <Typography className={classes.title}>
            <FormattedMessage
              id="dashboard.services-banner.title"
              defaultMessage="В вашем профессиональном комплекте также доступны другие сервисы"
            />
          </Typography>
          <Button variant="contained" size="medium" onClick={this.handleClick}>
            <FormattedMessage
              id="dashboard.services-banner.show"
              defaultMessage="Посмотреть"
            />
          </Button>
        </div>
      </Slide>
    );
  }
}

export default withStyles(styles)(ServicesBanner);
