import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { IconButton, withStyles, withWidth } from "@material-ui/core";
import {
  NavigateBefore as NavigateBeforeIcon,
  NavigateNext as NavigateNextIcon
} from "@material-ui/icons";
import ServiceButton from "./ServiceButton";

const styles = theme => ({
  root: {
    position: "relative",
    height: 72,
    minHeight: 72,
    display: "flex",
    paddingLeft: theme.spacing(1.5),
    paddingRight: theme.spacing(1),
    paddingTop: "6px",
    paddingBottom: "6px",
    backgroundColor: theme.palette.background.paper,
    overflow: "hidden",
    borderTop: "1px solid #e0e0e0"
  },
  servicesBody: {
    display: "flex",
    position: "relative",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    },
    width: "100%"
  },
  navigateButton: {
    position: "absolute",
    top: "14px",
    width: theme.spacing(4),
    height: theme.spacing(4),
    backgroundColor: "#dbdbdb"
  },
  navigateBefore: {
    left: "-5px"
  },
  navigateNext: {
    right: "-5px"
  }
});

export class Services extends Component {
  static propTypes = {
    width: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.state = this.buildState();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { width } = this.props;

    if (prevProps.width !== width) {
      this.setState(this.buildState());
    }
  }

  buildState(options = {}) {
    const { services, user } = this.props;

    var myServices = services;
    if (user.foreign) {
      myServices = services.filter(item => {
        return item.foreign === true ? false : true;
      });
    }

    const from = options.from || 0;
    const size = this.getVisibleServicesCount();
    const showBefore = from > 0;
    const showNext = from < myServices.length - size;

    return {
      from,
      size,
      services: myServices.slice(from, from + size),
      showBefore,
      showNext
    };
  }

  getVisibleServicesCount() {
    const { width } = this.props;
    // xs: 0,
    // sm: 600,
    // md: 960,
    // lp: 1024,
    // lg: 1280,
    // lpl: 1440,
    // lpxl: 1600,
    // xl: 1920,

    switch (width) {
      case "md":
        return 3;
      case "lp":
        return 4;
      case "lg":
        return 6;
      case "lpl":
        return 7;
      case "lpxl":
        return 7;
      default:
        return 7;
    }
  }

  handleBeforeClick = () => {
    const { from } = this.state;
    this.setState(this.buildState({ from: from - 1 }));
  };

  handleNextClick = () => {
    const { from } = this.state;
    this.setState(this.buildState({ from: from + 1 }));
  };

  render() {
    const { classes, className, docConstructor } = this.props;
    const { services, showBefore, showNext } = this.state;

    return (
      <div className={classNames(classes.root, className)}>
        <div className={classes.servicesBody}>
          {services.map(service => (
            <ServiceButton
              key={service.id}
              value={service}
              docConstructor={docConstructor}
            />
          ))}

          {showBefore && (
            <IconButton
              className={classNames(
                classes.navigateButton,
                classes.navigateBefore
              )}
              onClick={this.handleBeforeClick}
            >
              <NavigateBeforeIcon />
            </IconButton>
          )}
          {showNext && (
            <IconButton
              className={classNames(
                classes.navigateButton,
                classes.navigateNext
              )}
              onClick={this.handleNextClick}
            >
              <NavigateNextIcon />
            </IconButton>
          )}
        </div>
      </div>
    );
  }
}

export default withWidth()(withStyles(styles)(Services));
