import React, { Component } from "react";
import { withWidth } from "@material-ui/core";

import ServicesContainer from "./ServicesContainer";
import ServicesBanner, { getServicesBannerViewed } from "./ServicesBanner";

export class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = { services: getServicesBannerViewed() };
  }

  handleClick = () => {
    this.setState({ services: getServicesBannerViewed() });
  };

  render() {
    const { width } = this.props;
    const { services } = this.state;

    if (["sm", "xs"].includes(width)) {
      return "";
    }

    return (
      <>
        {services && <ServicesContainer />}
        <ServicesBanner onClick={this.handleClick} />
      </>
    );
  }
}

export default withWidth()(Footer);
