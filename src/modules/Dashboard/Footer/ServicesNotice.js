import React, { Component } from "react";
import classNames from "classnames";
import moment from "moment";
import { Fade, Typography, withStyles, withWidth } from "@material-ui/core";
import {
  PlayArrow as PlayArrowIcon,
  Close as CloseIcon
} from "@material-ui/icons";
import { FormattedMessage } from "react-intl.macro";

import { getServicesBannerViewed } from "./ServicesBanner";

const styles = theme => ({
  root: {
    position: "absolute",
    zIndex: theme.zIndex.drawer,
    width: theme.spacing(30),
    left: 0,
    bottom: 0,
    display: "flex",
    alignItems: "center",
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.getContrastText(theme.palette.primary.main),
    overflow: "hidden"
  },
  title: {
    textTransform: "uppercase",
    fontWeight: "500"
  },
  closeIcon: {
    cursor: "pointer",
    position: "absolute",
    top: theme.spacing(2),
    right: theme.spacing(2)
  },
  playArrowIcon: {
    position: "absolute",
    zIndex: theme.zIndex.drawer,
    right: theme.spacing(-1.7),
    bottom: theme.spacing(3),
    color: theme.palette.primary.main
  }
});

export class ServicesNotice extends Component {
  constructor(props) {
    super(props);

    this.state = { opened: false };
  }

  componentDidMount() {
    if (!this.visible()) {
      return;
    }

    let scheduleAt = this.getScheduledAt();
    const today = moment();

    if (!scheduleAt) {
      return this.schedule();
    }

    if (today.unix() < scheduleAt) {
      return;
    }

    window.setTimeout(() => {
      this.setState({ opened: true });
    }, 1000);
  }

  schedule() {
    const today = moment();
    const scheduleAt = today
      .add(14, "days")
      .set({ hour: 7, minute: 0 })
      .unix();
    this.setScheduledAt(scheduleAt);
  }

  getScheduledAt() {
    const scheduledAt = localStorage.getItem("services_notice_scheduled_at");
    return scheduledAt && parseInt(scheduledAt);
  }

  setScheduledAt(value) {
    localStorage.setItem("services_notice_scheduled_at", value);
  }

  handleCloseClick = () => {
    this.setState({ opened: false });
    this.schedule();
  };

  visible() {
    const { width } = this.props;

    return getServicesBannerViewed() && !["sm", "xs"].includes(width);
  }

  render() {
    const { classes, className } = this.props;
    const { opened } = this.state;

    if (!this.visible()) {
      return "";
    }

    return (
      <>
        <Fade in={opened} mountOnEnter unmountOnExit>
          <div className={classNames(classes.root, className)}>
            <Typography className={classes.title}>
              <FormattedMessage
                id="dashboard.services-banner.title"
                defaultMessage="В вашем профессиональном комплекте также доступны другие сервисы"
              />
            </Typography>
            <CloseIcon
              className={classes.closeIcon}
              onClick={this.handleCloseClick}
            />
          </div>
        </Fade>
        <Fade in={opened} mountOnEnter unmountOnExit>
          <PlayArrowIcon className={classes.playArrowIcon} />
        </Fade>
      </>
    );
  }
}

export default withWidth()(withStyles(styles)(ServicesNotice));
