import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Avatar,
  Button,
  ClickAwayListener,
  Popper,
  withStyles
} from "@material-ui/core";
import OtherServiceCard from "../../OtherServices/OtherServiceCard";
import classNames from "classnames";

const styles = theme => ({
  root: {
    flex: "1 1 auto",
    margin: theme.spacing(0.5),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    fontSize: "0.8rem",
    textTransform: "none",
    fontWeight: theme.typography.fontWeightRegular,
    backgroundColor: theme.palette.background.paper + " !important",
    width: "25%",
    marginRight: "8px",
    boxShadow:
      "0px 3px 1px -2px rgba(0,0,0,0.2), 0px 1px 3px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12);"
  },
  header: {
    display: "flex",
    alignItems: "center"
  },
  label: {
    justifyContent: "flex-start",
    fontSize: "0.85rem",
    lineHeight: "normal",
    textAlign: "left"
  },
  avatarImage: {
    width: "38px",
    height: "auto"
  },
  startIcon: {
    width: "38px",
    height: "auto"
  },
  card: {
    paddingBottom: theme.spacing()
  },
  serviceCard: {
    boxShadow:
      "0px 4px 8px rgba(0, 0, 0, 0.13), 0px 2px 10px rgba(0, 0, 0, 0.13)",
    minWidth: "200px"
  },
  buttonOpen: {
    boxShadow:
      "0px 4px 8px rgba(0, 0, 0, 0.13), 0px 2px 10px rgba(0, 0, 0, 0.13)",
    backgroundColor: "transparent"
  },
  "buttonOpen:hover": {
    boxShadow:
      "0px 4px 8px rgba(0, 0, 0, 0.13), 0px 2px 10px rgba(0, 0, 0, 0.13)"
  }
});

export class ServiceButton extends Component {
  static propTypes = {
    value: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.ref = React.createRef();

    this.state = {
      open: false,
      anchorEl: null
    };
  }

  componentDidMount() {
    this.setState({ anchorEl: this.ref.current });
  }

  handleClick = () => {
    this.setState({ open: true });
  };

  handleClickAway = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, value, docConstructor } = this.props;
    const { open, anchorEl } = this.state;

    return (
      <>
        <Button
          ref={this.ref}
          variant="contained"
          size="large"
          startIcon={
            <Avatar
              aria-label={value.name}
              src={value.icon}
              className={classes.avatarImage}
            />
          }
          className={classNames(classes.root, open ? classes.buttonOpen : "")}
          onClick={this.handleClick}
          classes={{ label: classes.label, startIcon: classes.startIcon }}
          disableFocusRipple
          disableRipple
        >
          {value.name}
        </Button>
        <Popper placement="top-start" open={open} anchorEl={anchorEl}>
          <ClickAwayListener onClickAway={this.handleClickAway}>
            <div className={classes.card}>
              <OtherServiceCard
                value={value}
                anchorEl={anchorEl}
                docConstructor={docConstructor}
                classes={{
                  description: classes.description,
                  card: classes.serviceCard
                }}
              />
            </div>
          </ClickAwayListener>
        </Popper>
      </>
    );
  }
}

export default withStyles(styles)(ServiceButton);
