import React from "react";
import { connect } from "react-redux";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { docConstructor } from "../../OtherServices/OtherServicesActions";
import { getServices } from "../../OtherServices/OtherServicesHelpers";
import Services from "./Services";

const ServicesContainer = props => <Services {...props} />;

const mapStateToProps = state => {
  return {
    user: getCurrentUser(state),
    services: getServices()
  };
};

export default connect(mapStateToProps, {
  docConstructor
})(ServicesContainer);
