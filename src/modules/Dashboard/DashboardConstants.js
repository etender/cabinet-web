import { createSharedPath } from "../Shared/SharedHelpers";

export const DASHBOARD_PATH = createSharedPath("/");
export const SEARCH_PATH = createSharedPath("/search");
export const FILTER_PATH = createSharedPath("/filter");
export const SERVICES_PATH = "/services";

export {
  ANALYTICS_PATH,
  ANALYTICS_FILTER_PATH
} from "../Analytics/AnalyticsConstants";
export { TENDERS_PATH } from "../Tenders/TendersConstants";
export { FAVORITES_PATH } from "../Favorites/FavoritesConstants";
export { MEMBERS_PATH } from "../Members/MembersConstants";
export { PROFILE_PATH } from "../Profile/ProfileConstants";
