import React, { Component } from "react";
import PropTypes from "prop-types";
import { List, withStyles } from "@material-ui/core";
import { injectIntl } from "react-intl";

import { isPresent } from "../../../utils/CoreUtils";
import { forms } from "../FilterForms/FilterFormsSelectors";

import FilterItem from "../FilterItem/FilterItem";

const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  }
});

class FilterBaseAuctions extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = this.formsState();
  }

  formsState() {
    const { search } = this.props;

    let items = forms;
    if (isPresent(search)) {
      const re = new RegExp(search, "i");
      items = forms.filter(item => !search || re.test(item.name));
    }

    return { forms: items, enable: items.length > 0 };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { search } = this.props;

    if (prevProps.search !== search) {
      this.setState(this.formsState());
    }
  }

  render() {
    const { classes, value, onChange } = this.props;
    const { enable, forms } = this.state;

    if (!enable) {
      return "";
    }

    return (
      <List className={classes.root}>
        {forms.map(item => (
          <FilterItem
            key={item.id}
            onChange={onChange}
            value={item}
            selected={value}
          />
        ))}
      </List>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterBaseAuctions));
