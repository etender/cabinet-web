import React from "react";
import { connect } from "react-redux";
import { getVisible } from "../WelcomeDialog/WelcomeDialogSelectors";
import { showJivosite } from "./JivositeActions";
import Jivosite from "./Jivosite";

const JivositeContainer = props => <Jivosite {...props} />;

const mapStateToProps = state => {
  return {
    visible: getVisible(state)
  };
};

export default connect(mapStateToProps, {
  showJivosite
})(JivositeContainer);
