import { handleActions } from "redux-actions";

import { showJivosite } from "./JivositeActions";

const initialState = {
  attached: false
};

export default handleActions(
  {
    [showJivosite]: state => {
      if (state.attached) {
        return state;
      }
      const script = document.createElement("script");
      script.src = "https://code.jivosite.com/widget.js";
      script.async = true;
      script.dataset.jvId = "KhXXbP98BS";

      document.body.appendChild(script);
      return { ...state, attached: true };
    }
  },
  initialState
);
