import { Component } from "react";
import { withWidth } from "@material-ui/core";

class Jivosite extends Component {
  componentDidMount() {
    const { visible, width, showJivosite } = this.props;
    const isMobile = width === "xs" || width === "sm";
    const production = process.env.NODE_ENV === "production";

    if (!production || (isMobile && visible)) {
      return;
    }
    showJivosite();
  }

  render() {
    return "";
  }
}

export default withWidth()(Jivosite);
