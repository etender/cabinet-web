import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemAvatar,
  ListItemText,
  withStyles,
  withWidth
} from "@material-ui/core";
import { injectIntl } from "react-intl";
import { Avatar } from "@material-ui/core";

import NewFeatureBadge from "../../../common/NewFeatureBadge/NewFeatureBadge";

const styles = theme => ({
  title: {
    fontWeight: "500"
  },
  menuItem: {
    paddingTop: "9px",
    paddingBottom: "9px",
    color: "#0071B8"
  },
  menuItemIcon: {
    width: "auto",
    minWidth: "auto",
    marginRight: theme.spacing(2),
    color: "#0071B8"
  },
  avatar: {
    width: "20px",
    height: "20px"
  },
  itemAvatar: {
    minWidth: "auto",
    marginRight: theme.spacing(2)
  },
  newFeatureBadge: {
    borderRadius: "2px",
    right: -theme.spacing(3)
  }
});

export class Menu extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    classes: PropTypes.object.isRequired
  };

  renderItem(item) {
    const { classes, selected, action, width } = this.props;
    const { formatMessage } = this.props.intl;
    const { icon: Icon, avatar } = item;

    if (item.visibleWidth) {
      if (["md", "lp", "lg", "lpl", "lpxl", "xl"].includes(width)) {
        return "";
      }
    }

    return (
      <MenuItem
        key={item.id}
        selected={selected === item.id}
        className={classes.menuItem}
        onClick={() => action(item)}
      >
        {Icon && (
          <ListItemIcon className={classes.menuItemIcon}>
            <Icon />
          </ListItemIcon>
        )}
        {avatar && (
          <ListItemAvatar className={classes.itemAvatar}>
            <Avatar src={avatar} variant="square" className={classes.avatar} />
          </ListItemAvatar>
        )}
        <ListItemText
          primary={
            <>
              {item.newFeature && (
                <NewFeatureBadge>{formatMessage(item.name)}</NewFeatureBadge>
              )}
              {!item.newFeature && formatMessage(item.name)}
            </>
          }
          classes={{ primary: classes.title }}
        />
      </MenuItem>
    );
  }

  render() {
    const { className, items } = this.props;

    return (
      <MenuList className={className}>
        {items.map(item => this.renderItem(item))}
      </MenuList>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(Menu)));
