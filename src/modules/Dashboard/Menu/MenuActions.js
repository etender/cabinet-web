import { createAction } from "redux-actions";

import { openLink } from "../../../services/Api/ApiActions";

export const openSign = createAction(openLink.toString(), () => ({
  url: "https://www.garantexpress.ru/services/electronic_signature/",
  target: "electronic_signature"
}));

export const openGuarantee = createAction(openLink.toString(), () => ({
  url:
    "https://www.garantexpress.ru/services/electronic_trading/bank_guarantee/",
  target: "bank_guarantee"
}));
