import React, { Component } from "react";
import PropTypes from "prop-types";
import { List, ListSubheader, ListItem, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { baseArea } from "../FilterAreas/FilterAreasSelectors";
import FilterItem from "../FilterItem/FilterItem";
import FilterItemText from "../FilterItemText/FilterItemText";

const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  item: {
    paddingLeft: theme.spacing(8),
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: theme.spacing(),
    [theme.breakpoints.down("sm")]: {
      paddingLeft: theme.spacing(8)
    }
  }
});

class FilterBaseArea extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = this.enableState();
  }

  enableState() {
    const { search } = this.props;
    const re = new RegExp(search, "i");
    return {
      enable:
        re.test(baseArea.name) ||
        baseArea.included.some(item => !search || re.test(item.name))
    };
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { search } = this.props;

    if (prevProps.search !== search) {
      this.setState(this.enableState());
    }
  }

  render() {
    const { classes, value, onChange } = this.props;
    const { enable } = this.state;

    if (!enable) {
      return "";
    }

    return (
      <List
        className={classes.root}
        subheader={
          <ListSubheader>
            <FormattedMessage
              id="dashboard.filter-base-areas.title"
              defaultMessage="Государственные ресурсы"
            />
          </ListSubheader>
        }
      >
        <FilterItem onChange={onChange} value={baseArea} selected={value} />

        {baseArea.included.map(item => (
          <ListItem key={item.name} className={classes.item}>
            <FilterItemText primary={item.name} secondary={item.href} />
          </ListItem>
        ))}
      </List>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterBaseArea));
