import React from "react";
import { connect } from "react-redux";
import { fetchFilters } from "./FiltersActions";
import { getFilters } from "./FiltersSelectors";
import { getCurrentFilter } from "../Filter/FilterSelectors";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { showFilter } from "../Filter/FilterActions";
import Filters from "./Filters";

const FiltersContainer = props => <Filters {...props} />;

const mapStateToProps = state => {
  return {
    user: getCurrentUser(state),
    filters: getFilters(state),
    selected: getCurrentFilter(state)
  };
};

export default connect(mapStateToProps, {
  fetchFilters,
  showFilter
})(FiltersContainer);
