import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  MenuList,
  Collapse,
  ListItem,
  ListItemIcon,
  ListSubheader,
  ListItemText,
  withStyles
} from "@material-ui/core";
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon
} from "@material-ui/icons";
import FiltersItem from "../FiltersItem/FiltersItem";
import { FormattedMessage } from "react-intl.macro";

const styles = theme => ({
  title: {
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden"
  },
  header: {
    backgroundColor: theme.palette.background.paper,
    textTransform: "uppercase",
    paddingLeft: theme.spacing(2),
    fontSize: "0.85rem",

    "&:focus": {
      outline: "none"
    }
  },
  container0: {
    minHeight: `${theme.spacing(6)}px !important`
  },
  container1: {
    minHeight: `${theme.spacing(6)}px !important`
  },
  container2: {
    minHeight: `${theme.spacing(2 * 6)}px !important`
  },
  container3: {
    minHeight: `${theme.spacing(3 * 6)}px !important`
  },
  container4: {
    minHeight: `${theme.spacing(4 * 6)}px !important`
  },
  container5: {
    minHeight: `${theme.spacing(5 * 6)}px !important`
  },
  button: {
    "&:focus": {
      backgroundColor: theme.palette.background.paper
    }
  },
  hidden: {
    visibility: "visible"
  },
  menuListContainer: {
    position: "relative"
  }
});

export class Filters extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    fetchFilters: PropTypes.func.isRequired,
    showFilter: PropTypes.func.isRequired,
    filters: PropTypes.arrayOf(PropTypes.object).isRequired,
    selected: PropTypes.object,
    onSelect: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      collapsed: true
    };
  }

  componentDidMount() {
    const { fetchFilters } = this.props;
    fetchFilters();
  }

  handleClick = () => {
    const { collapsed } = this.state;
    this.setState({ collapsed: !collapsed });
  };

  handleSelect = filter => {
    const { showFilter, onSelect } = this.props;
    showFilter(filter.id);
    onSelect(filter);
  };

  render() {
    const { classes, filters, selected } = this.props;
    const { collapsed } = this.state;
    const present = filters.length > 6;
    const count = filters.length - 6;
    return (
      <div className={classes.menuListContainer}>
        <MenuList>
          <ListSubheader
            className={classNames(classes.title, classes.header)}
            inset
          >
            <FormattedMessage
              id="dashboard.filters.title"
              defaultMessage="Мои фильтры"
            />
          </ListSubheader>
          <Collapse
            in={!collapsed}
            classes={{
              container:
                classes[
                  "container" + (filters.length >= 5 ? 5 : filters.length)
                ],
              hidden: classes.hidden
            }}
          >
            {filters.map(filter => (
              <FiltersItem
                key={filter.id}
                filter={filter}
                selected={selected && selected.id === filter.id}
                onSelect={this.handleSelect}
              />
            ))}
          </Collapse>
        </MenuList>
        {present && (
          <ListItem
            ContainerComponent="div"
            divider
            button
            classes={{ button: classes.button }}
            onClick={this.handleClick}
          >
            <ListItemIcon>
              {collapsed ? <ExpandMoreIcon /> : <ExpandLessIcon />}
            </ListItemIcon>
            <ListItemText
              primary={
                collapsed ? (
                  <FormattedMessage
                    id="dashboard.filters.expand"
                    defaultMessage="Показать еще {count}"
                    values={{ count }}
                  />
                ) : (
                  <FormattedMessage
                    id="dashboard.filters.collapse"
                    defaultMessage="Скрыть"
                  />
                )
              }
            />
          </ListItem>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(Filters);
