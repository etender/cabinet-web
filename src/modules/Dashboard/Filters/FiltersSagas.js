import { takeLatest } from "redux-saga/effects";

import { fetchEntitiesSaga } from "../../../services/Entities/EntitiesSagas";
import { fetchFilters } from "./FiltersActions";

export default function* filtersSagas() {
  yield takeLatest(fetchFilters, fetchEntitiesSaga);
}
