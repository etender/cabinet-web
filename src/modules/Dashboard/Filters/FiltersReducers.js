import { handleActions } from "redux-actions";

import { successApiType } from "../../../services/Api/ApiHelpers";
import {
  showFilter,
  deleteFilter,
  updateFilterName
} from "../Filter/FilterActions";
import { fetchFilters } from "./FiltersActions";
import { signOutUser } from "../../../services/Devise/DeviseActions";

const initialState = [];

export default handleActions(
  {
    [successApiType(deleteFilter)]: (state, action) =>
      state.filter(item => item.id !== action.payload.id),

    [successApiType(showFilter)]: (state, action) =>
      state.map(item =>
        item.id === action.payload.id
          ? { ...item, count: action.payload.count }
          : item
      ),

    [successApiType(updateFilterName)]: (state, action) =>
      state.map(item =>
        item.id === action.payload.id
          ? { ...item, name: action.payload.name }
          : item
      ),

    [successApiType(fetchFilters)]: (state, action) => action.payload,

    [successApiType(signOutUser)]: (state, action) => []
  },
  initialState
);
