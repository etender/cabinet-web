import { createAction } from "redux-actions";

export const fetchFilters = createAction(
  "DASHBOARD_FILTERS_FETCH",
  id => ({ userId: id }),
  () => ({ entity: "filters" })
);
