import { createSelector } from "reselect";
import { isPresent } from "../../../utils/CoreUtils";
import { getLocationPathname } from "../../../services/Router/RouterSelectors";

export const getFastSearch = state => state.dashboard.fastSearch;

export const isSearchPath = createSelector(getLocationPathname, path =>
  path.includes("/search")
);

export const shouldFastSearchClean = createSelector(
  isSearchPath,
  getFastSearch,
  (isSearchPath, search) => {
    return isPresent(search) && !isSearchPath;
  }
);
