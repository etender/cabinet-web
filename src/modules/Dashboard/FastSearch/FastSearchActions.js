import { createAction } from "redux-actions";

export const applySearch = createAction("DASHBOARD_SEARCH_APPLY");
export const clearSearch = createAction("DASHBOARD_SEARCH_CLEAR");
