import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { fade } from "@material-ui/core/styles/colorManipulator";
import { Link, InputBase, withStyles } from "@material-ui/core";
import { Search as SearchIcon } from "@material-ui/icons";
import { injectIntl } from "react-intl";
import { FormattedMessage, defineMessages } from "react-intl.macro";

import FastSearchSuggestions from "../FastSearchSuggestions/FastSearchSuggestions";
import { defaultFilter } from "../Filter/FilterHelpers";
import { isEmpty, isArray, isPresent } from "../../../utils/CoreUtils";

const messages = defineMessages({
  placeholder: {
    id: "dashboard.fast-search.placeholder",
    defaultMessage: "Введите название товара (услуги) или номер закупки"
  }
});

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.25),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.3)
    },
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  searchIcon: {
    flex: "0 0 auto",
    marginLeft: theme.spacing(),
    marginRight: theme.spacing(),
    marginTop: "5px"
  },
  arrowDropDown: {
    flex: "0 0 auto",
    marginLeft: theme.spacing(),
    marginRight: theme.spacing()
  },
  inputRoot: {
    color: "inherit",
    marginTop: theme.spacing(),
    marginBottom: theme.spacing(),
    flex: "1 1 auto"
  },
  inputInput: {
    padding: theme.spacing(),
    transition: theme.transitions.create("width"),
    width: "100%"
  },
  container: {
    width: "100%",
    maxWidth: theme.spacing(96),
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  extendedSearch: {
    marginRight: theme.spacing(),
    color: "inherit",
    opacity: "0.5",
    textDecoration: "underline",
    textDecorationStyle: "dashed",
    "&:hover": {
      textDecoration: "underline",
      textDecorationStyle: "dashed",
      opacity: "0.8"
    }
  }
});

class FastSearch extends Component {
  static propTypes = {
    fetchSuggestions: PropTypes.func.isRequired,
    cleanSuggestions: PropTypes.func.isRequired,
    suggestions: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);

    this.ref = React.createRef();

    const { search } = this.props;
    this.state = {
      anchorEl: null,
      value: search || "",
      suggestion: null
    };
  }

  componentDidMount() {
    this.setState({ anchorEl: this.ref.current });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { search, shouldFastSearchClean, clearSearch } = this.props;

    if (shouldFastSearchClean) {
      return clearSearch("");
    }

    if (prevProps.search !== search) {
      this.setState({ value: search || "" });
    }
  }

  handleInputChange = event => {
    const value = event.target.value;
    this.setState({ value });

    if (this.timeout) {
      window.clearTimeout(this.timeout);
    }

    this.timeout = window.setTimeout(() => {
      this.setState({ suggestion: value });
    }, 500);
  };

  handleInputKeyPress = event => {
    if (event.key === "Enter") {
      if (this.timeout) {
        window.clearTimeout(this.timeout);
      }

      this.applyFilter();
    }
  };

  handleBlur = () => {
    const { search } = this.props;
    this.setState({ value: search });
  };

  handleSuggestionsSelect = item => {
    this.applyFilter(item.title);
  };

  handleFilterClick = () => {
    const { editFilter } = this.props;
    const { value } = this.state;
    let options = { ...defaultFilter };
    if (isPresent(value)) {
      options.keywords = [value];
    }
    editFilter(options);
  };

  applyFilter(value) {
    if (value) {
      this.setState({ value });
    } else {
      value = this.state.value;
    }

    const { cleanSuggestions } = this.props;
    cleanSuggestions();

    if (isEmpty(value)) {
      return;
    }

    const { applyFilter, applySearch } = this.props;
    applySearch(value);
    applyFilter({
      ...defaultFilter,
      keywords: isArray(value) ? value : [value],
      deal: true
    });
  }

  render() {
    const {
      classes,
      className,
      suggestions,
      fetchSuggestions,
      cleanSuggestions,
      intl
    } = this.props;
    const { anchorEl, value, suggestion } = this.state;
    const { formatMessage } = intl;

    return (
      <div className={classNames(classes.root, className)} ref={this.ref}>
        <div className={classes.searchIcon}>
          <SearchIcon />
        </div>
        <InputBase
          placeholder={formatMessage(messages.placeholder)}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput
          }}
          value={value}
          onBlur={this.handleBlur}
          onChange={this.handleInputChange}
          onKeyPress={this.handleInputKeyPress}
        />

        <Link
          component="button"
          variant="body1"
          onClick={this.handleFilterClick}
          className={classes.extendedSearch}
        >
          <FormattedMessage
            id="dashboard.fast-search.extended"
            defaultMessage="Точный поиск"
          />
        </Link>

        <FastSearchSuggestions
          className={classes.container}
          anchorEl={anchorEl}
          items={suggestions}
          fetch={fetchSuggestions}
          clean={cleanSuggestions}
          value={suggestion}
          onSelect={this.handleSuggestionsSelect}
        />
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(FastSearch));
