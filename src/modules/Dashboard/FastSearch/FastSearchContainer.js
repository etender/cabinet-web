import React from "react";
import { connect } from "react-redux";

import { applyFilter, editFilter } from "../Filter/FilterActions";
import {
  fetchSuggestions,
  cleanSuggestions
} from "../../../common/Suggestions/SuggestionsActions";
import { getSuggestions } from "../../../common/Suggestions/SuggestionsSelectors";
import { applySearch, clearSearch } from "./FastSearchActions";
import { getFastSearch, shouldFastSearchClean } from "./FastSearchSelectors";
import FastSearch from "./FastSearch";

const FastSearchContainer = props => <FastSearch {...props} />;

const mapStateToProps = state => {
  return {
    suggestions: getSuggestions(state, "search"),
    search: getFastSearch(state),
    shouldFastSearchClean: shouldFastSearchClean(state)
  };
};

export default connect(mapStateToProps, {
  fetchSuggestions: query => fetchSuggestions("search", query),
  cleanSuggestions: () => cleanSuggestions("search"),
  applyFilter,
  editFilter,
  applySearch,
  clearSearch
})(FastSearchContainer);
