import { handleActions } from "redux-actions";

import { applySearch, clearSearch } from "./FastSearchActions";

const initialState = "";

export default handleActions(
  {
    [applySearch]: (state, action) => action.payload,

    [clearSearch]: state => ""
  },
  initialState
);
