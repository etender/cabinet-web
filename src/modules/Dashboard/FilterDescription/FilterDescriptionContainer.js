import React from "react";
import { connect } from "react-redux";

import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import {
  createFilter,
  updateFilter,
  editFilter,
  deleteFilter,
  shareFilter
} from "../Filter/FilterActions";
import {
  getCurrentFilter,
  canCreateFilter,
  canChangeFilter,
  canDeleteFilter,
  canShareFilter,
  getShareUrl
} from "../Filter/FilterSelectors";
import { toggleDialog } from "../../../common/Dialog/DialogActions";
import { shouldShowFilterDescription } from "./FilterDescriptionSelectors";
import FilterDescription from "./FilterDescription";

const FilterDescriptionContainer = props => <FilterDescription {...props} />;

const mapStateToProps = state => {
  return {
    value: getCurrentFilter(state),
    shareUrl: getShareUrl(state),
    visible: shouldShowFilterDescription(state),
    canCreateFilter: canCreateFilter(state),
    canChangeFilter: canChangeFilter(state),
    canDeleteFilter: canDeleteFilter(state),
    canShareFilter: canShareFilter(state)
  };
};

export default connect(mapStateToProps, {
  toggleFilterName: () => toggleDialog("filterName"),
  toggleFilterShare: () => toggleDialog("filterShare"),
  createFilter,
  updateFilter,
  editFilter,
  deleteFilter,
  shareFilter,
  showSnackbar
})(FilterDescriptionContainer);
