import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { areasHash } from "../FilterAreas/FilterAreasSelectors";
import StringFieldDescription from "./StringFieldDescription";

const messages = defineMessages({
  title: {
    id: "dashboard.areas-field-description.title",
    defaultMessage: "по площадкам:"
  }
});

class AreasFieldDescription extends Component {
  constructor(props) {
    super(props);

    this.state = this.buildState();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState(this.buildState());
    }
  }

  buildState() {
    const { value } = this.props;
    return {
      value: value ? value.map(item => areasHash[item].name).join(", ") : null
    };
  }

  render() {
    const { intl } = this.props;
    const { formatMessage } = intl;
    const { value } = this.state;

    if (!value) {
      return "";
    }

    return (
      <StringFieldDescription
        title={formatMessage(messages.title)}
        value={value}
      />
    );
  }
}

export default injectIntl(AreasFieldDescription);
