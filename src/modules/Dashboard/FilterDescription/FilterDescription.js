import React, { Component } from "react";
import {
  Typography,
  Card,
  CardContent,
  CardActions,
  Button,
  IconButton,
  withStyles,
  withWidth
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import FilterDescriptionContent from "./FilterDescriptionContent";
import FilterDelete from "../FilterDelete/FilterDelete";
import FilterName from "../FilterName/FilterName";
import FilterShare from "../FilterShare/FilterShare";
import { grey } from "@material-ui/core/colors";
import { Share as ShareIcon } from "@material-ui/icons";

const messages = defineMessages({
  name: {
    id: "dashboard.filter-description.name",
    defaultMessage: "Фильтр"
  },
  title: {
    id: "dashboard.filter-description.title",
    defaultMessage: "Точный поиск"
  },
  shareUrlCopy: {
    id: "dashboard.filter-description.shar-url-copy",
    defaultMessage: "Ссыслка скопирована в буфер обмена"
  }
});

const styles = theme => ({
  card: {
    marginBottom: theme.spacing(3),
    marginRight: "auto"
  },
  cardContent: {
    padding: theme.spacing(3),
    paddingBottom: theme.spacing(1)
  },
  cardActions: {
    padding: theme.spacing(3),
    paddingTop: theme.spacing(1)
  },
  description: {
    color: grey[600]
  },
  floatRight: {
    display: "flex",
    marginLeft: "auto !important"
  },
  filterName: {
    color: grey[600],
    marginBottom: theme.spacing(2)
  }
});

class FilterDescription extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.state.nameOpen = false;
    this.state.deleteOpen = false;
  }

  handleEditClick = () => {
    const { editFilter, value } = this.props;
    editFilter(value);
  };

  handleFilterDeleteToggle = () => {
    this.setState(state => ({ deleteOpen: !state.deleteOpen }));
  };

  handleFilterDelete = () => {
    const { deleteFilter, value } = this.props;

    deleteFilter(value);
  };

  handleSaveClick = () => {
    const { updateFilter, value, toggleFilterName } = this.props;

    if (value.persisted) {
      updateFilter(value);
    } else {
      toggleFilterName();
    }
  };

  handleShareClick = () => {
    const { shareFilter, toggleFilterShare, value } = this.props;
    shareFilter(value);
    toggleFilterShare();
  };

  handleShareUrlCopy = () => {
    const { showSnackbar, toggleFilterShare, intl } = this.props;
    const { formatMessage } = intl;
    showSnackbar(formatMessage(messages.shareUrlCopy));

    toggleFilterShare();
  };

  createFilter = options => {
    const { createFilter, value, toggleFilterName } = this.props;

    createFilter({ ...value, ...options });
    toggleFilterName();
  };

  render() {
    const {
      classes,
      value,
      visible,
      shareUrl,
      canCreateFilter,
      canChangeFilter,
      canDeleteFilter,
      canShareFilter,
      width,
      intl
    } = this.props;
    const { deleteOpen } = this.state;
    const { formatMessage } = intl;

    if (!visible) {
      return "";
    }

    return (
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <FilterShare url={shareUrl} onCopy={this.handleShareUrlCopy} />
          <FilterName value={value} change={this.createFilter} />
          <FilterDelete
            open={deleteOpen}
            onAction={this.handleFilterDelete}
            onClose={this.handleFilterDeleteToggle}
          />
          {value.name && (
            <Typography variant={"h6"} className={classes.filterName}>
              {formatMessage(messages.name) + ": " + value.name}
            </Typography>
          )}
          <FilterDescriptionContent value={value} />
        </CardContent>
        <CardActions className={classes.cardActions}>
          {canShareFilter && (
            <>
              {width !== "xs" && (
                <Button color="primary" onClick={this.handleShareClick}>
                  <FormattedMessage
                    id="dashboard.filter-description.share"
                    defaultMessage="Поделиться"
                  />
                </Button>
              )}
              {width === "xs" && (
                <IconButton
                  aria-label="Поделиться"
                  className={classes.shareButton}
                  onClick={this.handleShareClick}
                >
                  <ShareIcon />
                </IconButton>
              )}
            </>
          )}
          <div className={classes.floatRight}>
            {canChangeFilter && (
              <Button color="primary" onClick={this.handleEditClick}>
                <FormattedMessage
                  id="dashboard.filter-description.edit"
                  defaultMessage="Изменить"
                />
              </Button>
            )}
            {canCreateFilter && (
              <Button color="primary" onClick={this.handleSaveClick}>
                <FormattedMessage
                  id="dashboard.filter-description.save"
                  defaultMessage="Сохранить"
                />
              </Button>
            )}
            {value && value.persisted && canDeleteFilter && (
              <Button color="primary" onClick={this.handleFilterDeleteToggle}>
                <FormattedMessage
                  id="dashboard.filter-description.delete"
                  defaultMessage="Удалить"
                />
              </Button>
            )}
          </div>
        </CardActions>
      </Card>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(FilterDescription)));
