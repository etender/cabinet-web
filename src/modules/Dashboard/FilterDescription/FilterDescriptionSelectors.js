import { createSelector } from "reselect";
import { getPathFilter } from "../Filter/FilterSelectors";
import { isEmpty } from "../../../utils/CoreUtils";

export const shouldShowFilterDescription = createSelector(
  getPathFilter,
  pathFilter => !isEmpty(pathFilter)
);
