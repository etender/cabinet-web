import React, { Component } from "react";
import { Typography, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { grey } from "@material-ui/core/colors";

import ArrayFieldDescription from "../FilterDescription/ArrayFieldDescription";
import CollectionFieldDescription from "../FilterDescription/CollectionFieldDescription";
import FormsFieldDescription from "../FilterDescription/FormsFieldDescription";
import AreasFieldDescription from "../FilterDescription/AreasFieldDescription";
import BoolFieldDescription from "../FilterDescription/BoolFieldDescription";
import StageFieldDescription from "../FilterDescription/StageFieldDescription";
import PriceRangeFieldDescription from "../FilterDescription/PriceRangeFieldDescription";
import DateRangeFieldDescription from "../FilterDescription/DateRangeFieldDescription";
import FederalLawFieldDescription from "../FilterDescription/FederalLawFieldDescription";

const messages = defineMessages({
  keywords: {
    id: "dashboard.filter-description.keywords",
    defaultMessage: "со словами:"
  },
  including: {
    id: "dashboard.filter-description.including",
    defaultMessage: "включая слова:"
  },
  excluding: {
    id: "dashboard.filter-description.excluding",
    defaultMessage: "исключая слова:"
  },
  attachmentSearch: {
    id: "dashboard.filter-description.attachmentSearch",
    defaultMessage: "искать внутри документации"
  },
  hasPayment: {
    id: "dashboard.filter-description.has-payment",
    defaultMessage: "искать с авансом"
  },
  regions: {
    id: "dashboard.filter-description.regions",
    defaultMessage: "по регионам:"
  },
  okpds: {
    id: "dashboard.filter-description.okpds",
    defaultMessage: "по ОКПД2:"
  },
  categories: {
    id: "dashboard.filter-description.categories",
    defaultMessage: "по категориям:"
  },
  price: {
    id: "dashboard.filter-description.price",
    defaultMessage: "цена:"
  },
  contact: {
    id: "dashboard.filter-description.contact",
    defaultMessage: "заказчик:"
  },
  pubDate: {
    id: "dashboard.filter-description.pub-date",
    defaultMessage: "дата публикации:"
  },
  endDate: {
    id: "dashboard.filter-description.end-date",
    defaultMessage: "дата окончания приема заявок: "
  },
  okved: {
    id: "dashboard.filter-description.okved2",
    defaultMessage: "ОКВЭД2"
  }
});

const styles = theme => ({
  root: {
    color: grey[600]
  }
});

class FilterDescriptionContent extends Component {
  render() {
    const { classes, value, intl } = this.props;
    const { formatMessage } = intl;

    if (!value) {
      return "";
    }

    const {
      contact,
      keywords,
      including,
      excluding,
      attachmentSearch,
      hasPayment,
      stage,
      regions,
      forms,
      areas,
      priceFrom,
      priceTo,
      pubDateFrom,
      pubDateTo,
      endDateFrom,
      endDateTo,
      federalLaw,
      deal,
      categories,
      okpds,
      okved
    } = value;

    return (
      <Typography className={classes.root}>
        <ArrayFieldDescription
          title={formatMessage(messages.contact)}
          value={contact}
        />
        <ArrayFieldDescription
          title={formatMessage(messages.keywords)}
          value={keywords}
        />
        <ArrayFieldDescription
          title={formatMessage(messages.including)}
          value={including}
        />
        <ArrayFieldDescription
          title={formatMessage(messages.excluding)}
          value={excluding}
        />
        <BoolFieldDescription
          title={formatMessage(messages.attachmentSearch)}
          value={attachmentSearch}
        />
        <BoolFieldDescription
          title={formatMessage(messages.hasPayment)}
          value={hasPayment}
        />
        <CollectionFieldDescription
          title={formatMessage(messages.regions)}
          value={regions}
          field="title"
        />
        <FederalLawFieldDescription value={federalLaw} deal={deal} />
        <StageFieldDescription value={stage} />
        <FormsFieldDescription value={forms} />
        <AreasFieldDescription value={areas} />
        <PriceRangeFieldDescription
          title={formatMessage(messages.price)}
          from={priceFrom}
          to={priceTo}
        />
        <DateRangeFieldDescription
          title={formatMessage(messages.pubDate)}
          from={pubDateFrom}
          to={pubDateTo}
        />
        <DateRangeFieldDescription
          title={formatMessage(messages.endDate)}
          from={endDateFrom}
          to={endDateTo}
        />

        <CollectionFieldDescription
          title={formatMessage(messages.categories)}
          value={categories}
        />
        <CollectionFieldDescription
          title={formatMessage(messages.okpds)}
          value={okpds}
        />
        <ArrayFieldDescription
          title={formatMessage(messages.okved)}
          value={okved}
        />
      </Typography>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterDescriptionContent));
