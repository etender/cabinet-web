import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import StringFieldDescription from "./StringFieldDescription";

const messages = defineMessages({
  title: {
    id: "dashboard.stage-field-description.title",
    defaultMessage: "этап:"
  }
});

class StageFieldDescription extends Component {
  stages = {
    plan: "Планируется",
    application: "Подача заявок",
    archival: "Архивные"
  };

  constructor(props) {
    super(props);

    this.state = this.buildState();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState(this.buildState());
    }
  }

  buildState() {
    const { value } = this.props;
    return { value: value ? this.stages[value] : null };
  }

  render() {
    const { intl } = this.props;
    const { formatMessage } = intl;
    const { value } = this.state;

    if (!value) {
      return "";
    }

    return (
      <StringFieldDescription
        title={formatMessage(messages.title)}
        value={value}
      />
    );
  }
}

export default injectIntl(StageFieldDescription);
