import React, { Component } from "react";
import moment from "moment";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import StringFieldDescription from "./StringFieldDescription";

const messages = defineMessages({
  from: {
    id: "dashboard.date-range-field-description.from",
    defaultMessage: "с: {date, date, medium}"
  },
  to: {
    id: "dashboard.date-range-field-description.to",
    defaultMessage: "по: {date, date, medium}"
  }
});

class PriceRangeFieldDescription extends Component {
  render() {
    const { title, intl } = this.props;
    let { from, to } = this.props;
    const { formatMessage } = intl;

    if (!from && !to) {
      return "";
    }

    from = from && moment(from);
    to = to && moment(to);

    return (
      <StringFieldDescription
        title={title}
        value={
          <>
            {from && formatMessage(messages.from, { date: from })}
            {from && to && "\u00A0"}
            {to && formatMessage(messages.to, { date: to })}
          </>
        }
      />
    );
  }
}

export default injectIntl(PriceRangeFieldDescription);
