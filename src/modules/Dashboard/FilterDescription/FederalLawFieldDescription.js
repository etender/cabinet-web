import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import StringFieldDescription from "./StringFieldDescription";

const messages = defineMessages({
  title: {
    id: "dashboard.federal-law-field-description.title",
    defaultMessage: "тип проведения:"
  }
});

class FederalLawFieldDescription extends Component {
  federalLaws = {
    fl223: "ФЗ №223",
    fl44: "ФЗ №44",
    smpchs: "Малые закупки",
    fl176: "ПП №615",
    com: "Коммерческий",
    deal: "Торги по банкротству"
  };

  constructor(props) {
    super(props);

    this.state = this.buildState();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState(this.buildState());
    }
  }

  buildState() {
    const { value, deal } = this.props;
    let federalLaws = [];

    if (value) {
      for (const [key, title] of Object.entries(this.federalLaws)) {
        if (value.includes(key)) {
          federalLaws.push(title);
        }
      }
    }

    if (deal && !federalLaws.includes("deal")) {
      federalLaws.push(this.federalLaws.deal);
    }

    federalLaws = federalLaws.join(", ");
    return { value: federalLaws };
  }

  render() {
    const { intl } = this.props;
    const { formatMessage } = intl;
    const { value } = this.state;

    if (!value) {
      return "";
    }

    return (
      <StringFieldDescription
        title={formatMessage(messages.title)}
        value={value}
      />
    );
  }
}

export default injectIntl(FederalLawFieldDescription);
