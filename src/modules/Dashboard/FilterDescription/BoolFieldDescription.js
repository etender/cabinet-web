import React from "react";
import StringFieldDescription from "./StringFieldDescription";

export default props => {
  const { value, title } = props;

  if (!value) {
    return "";
  }

  return <StringFieldDescription title={title} />;
};
