import React, { Component } from "react";

import StringFieldDescription from "./StringFieldDescription";

class ObjectFieldDescription extends Component {
  constructor(props) {
    super(props);

    this.state = this.buildState();
  }

  componentDidUpdate(prevProps) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState(this.buildState());
    }
  }

  buildState() {
    const { value } = this.props;
    let { field } = this.props;
    field = field || "title";
    return { value: value ? value[field] : null };
  }

  render() {
    const { title } = this.props;
    const { value } = this.state;

    if (!value) {
      return "";
    }

    return <StringFieldDescription title={title} value={value} />;
  }
}

export default ObjectFieldDescription;
