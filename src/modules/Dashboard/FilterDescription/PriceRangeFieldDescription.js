import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import StringFieldDescription from "./StringFieldDescription";

const messages = defineMessages({
  from: {
    id: "dashboard.price-range-filter-description.from",
    defaultMessage: "от: {price, number, rub}"
  },
  to: {
    id: "dashboard.price-range-filter-description.to",
    defaultMessage: "до: {price, number, rub}"
  }
});

class PriceRangeFieldDescription extends Component {
  render() {
    const { from, to, title, intl } = this.props;
    const { formatMessage } = intl;

    if (!from && !to) {
      return "";
    }

    return (
      <StringFieldDescription
        title={title}
        value={
          <>
            {from && formatMessage(messages.from, { price: from })}
            {from && to && "\u00A0"}
            {to && formatMessage(messages.to, { price: to })}
          </>
        }
      />
    );
  }
}

export default injectIntl(PriceRangeFieldDescription);
