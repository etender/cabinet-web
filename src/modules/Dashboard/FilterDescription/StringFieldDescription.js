import React from "react";

export default props => {
  const { value, title } = props;

  return (
    <span>
      <strong style={{ fontStyle: "italic" }}>{title}</strong>
      {value && <>&nbsp;{value}</>};&nbsp;
    </span>
  );
};
