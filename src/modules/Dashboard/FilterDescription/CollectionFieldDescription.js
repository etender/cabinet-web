import React, { Component } from "react";

import StringFieldDescription from "./StringFieldDescription";

class CollectionFieldDescription extends Component {
  constructor(props) {
    super(props);

    this.state = this.buildState();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState(this.buildState());
    }
  }

  buildState() {
    const { value } = this.props;
    let { field } = this.props;
    field = field || "name";
    return {
      value: value ? value.map(item => item[field] || item).join(", ") : null
    };
  }

  render() {
    const { title } = this.props;
    const { value } = this.state;

    if (!value) {
      return "";
    }

    return <StringFieldDescription title={title} value={value} />;
  }
}

export default CollectionFieldDescription;
