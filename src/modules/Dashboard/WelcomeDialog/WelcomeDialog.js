import React, { Component } from "react";
import {
  Typography,
  MobileStepper,
  Dialog,
  DialogContent,
  Hidden,
  IconButton,
  Button,
  withStyles
} from "@material-ui/core";
import { Close as CloseIcon } from "@material-ui/icons";
import { FormattedMessage } from "react-intl.macro";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

const styles = theme => ({
  paper: {
    width: "100%",
    maxWidth: "1196px",
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(2)
    }
  },
  img: {
    maxWidth: "100%",
    maxHeight: "100%"
  },
  body: {
    fontWeight: "400",
    fontSize: "15px",
    lineHeight: "145%",
    color: "#212121",
    [theme.breakpoints.down("xs")]: {
      fontSize: "13px"
    }
  },
  bodyCenter: {
    fontWeight: "400",
    fontSize: "15px",
    lineHeight: "145%",
    textAlign: "center",
    color: "#212121",
    [theme.breakpoints.down("xs")]: {
      fontSize: "13px"
    }
  },
  container: {
    marginTop: "0px",
    marginBottom: "0px",
    height: "405px",
    display: "flex",
    flexDirection: "row",
    alignItems: "stretch",
    [theme.breakpoints.down("md")]: {
      height: "inherit"
    },
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column"
    }
  },
  leftContent: {
    width: "384px",
    marginRight: "22px",
    [theme.breakpoints.down("sm")]: {
      width: "34%"
    },
    [theme.breakpoints.down("xs")]: {
      marginRight: "0px",
      marginBottom: "22px",
      width: "100%"
    }
  },
  rightContent: {
    width: "720px",
    [theme.breakpoints.down("sm")]: {
      width: "66%"
    },
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    }
  },
  leftBlock: {
    backgroundColor: "rgba(0, 173, 236, 0.2)",
    height: "100%",
    paddingLeft: "37px",
    paddingRight: "37px",
    paddingTop: "37px",
    display: "flex",
    justifyContent: "center",
    // alignItems: "center",
    [theme.breakpoints.up("lg")]: {
      paddingTop: "80px"
    },
    [theme.breakpoints.down("sm")]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      paddingBottom: theme.spacing(2),
      paddingTop: theme.spacing(2),
      height: "calc(100% - 5px)"
    }
  },
  header: {
    color: "#0071B8",
    fontSize: "24px",
    lineHeight: "28px",
    textAlign: "center",
    fontWeight: "bold",
    paddingBottom: "12px",
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(2)
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "23px"
    }
  },
  welcomeHeader1: {
    color: "#0071B8",
    fontSize: "24px",
    lineHeight: "28px",
    fontWeight: "bold"
  },
  welcomeHeader2: {
    color: "#00ADEC",
    fontSize: "16px",
    lineHeight: "19px",
    fontWeight: "normal",
    marginBottom: "15px"
  },
  welcomeHeader3: {
    color: "#0071B8",
    fontSize: "18px",
    lineHeight: "145%",
    fontWeight: "bold"
  },
  logo: {
    marginBottom: "25px"
  },
  icon: {
    display: "block",
    margin: "0 auto",
    marginBottom: "23px",
    [theme.breakpoints.down("sm")]: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
      paddingBottom: "0"
    }
  },
  firstSlideContainer: {
    backgroundImage: `url(${"/welcome/sl1-background-cut.svg"})`,
    backgroundRepeat: "no-repeat",
    backgroundColor: "rgba(0, 173, 236, 0.2)",
    backgroundPosition: "left top",
    backgroundSize: "100% 100%",
    height: "405px",
    [theme.breakpoints.down("xs")]: {
      backgroundImage: "none"
    },
    [theme.breakpoints.down("md")]: {
      height: "auto"
    }
  },
  firstSlideLeftContent: {
    width: "50%",
    height: "100%",
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    }
  },
  firstSlideRightContent: {
    width: "50%",
    height: "100%",
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    }
  },
  content: {
    padding: "35px 35px 22px 35px",
    paddingTop: "35px !important",
    [theme.breakpoints.down("sm")]: {
      padding: "20px"
    }
  },
  contentPadding: {
    padding: "50px 30px 30px 60px",
    [theme.breakpoints.down("sm")]: {
      padding: "20px"
    }
  },
  list: {
    padding: "0"
  },
  listItem: {
    listStyle: "none",
    backgroundImage: `url(${"/welcome/sl1-list-icon.svg"})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "left top",
    backgroundSize: "20px",
    paddingLeft: "30px",
    marginBottom: "15px"
  },
  button: {
    marginLeft: "15px",
    marginRight: "15px",
    textTransform: "none",
    fontWeight: "400",
    [theme.breakpoints.down("xs")]: {
      marginLeft: "0px",
      marginRight: "0px"
    },
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  closeButton: {
    position: "absolute",
    right: "2px",
    top: "2px",
    color: theme.palette.grey[500]
  },
  dot: {
    margin: "0 9px",
    [theme.breakpoints.down("xs")]: {
      margin: "0 2px"
    }
  },
  buttonDisabled: {
    color: "#888 !important"
  }
});

const welcomeSteps = [
  {
    title: "Добро пожаловать",
    logoPath: "/welcome/sl1-logo.svg",
    backgroundPath: "/welcome/sl1-background-cut.svg",
    firstStep: true
  },
  {
    title: "Быстрый поиск",
    body:
      "Это быстрый поиск. Введите в поле слова, словосочетания или номер извещения. Для указания дополнительных параметров поиска выберите «Точный поиск»",
    imgPath: "/welcome/sl2-image.png",
    iconPath: "/welcome/sl2-icon.svg"
  },
  {
    title: "Точный поиск",
    body:
      "Создайте подробный и точный фильтр с помощью дополнительных параметров поиска закупок. Заполните нужные поля и сохраните фильтр.",
    imgPath: "/welcome/sl3-image.png",
    iconPath: "/welcome/sl3-icon.svg"
  },
  {
    title: "Мои фильтры",
    body: `Здесь расположены сохраненные фильтры. Рядом с каждым из них — счетчик процедур, его 
      показания обновляются при появлении новых закупок, удовлетворяющих параметрам фильтра.`,
    imgPath: "/welcome/sl4-image.png",
    iconPath: "/welcome/sl4-icon.svg"
  },
  {
    title: "Закупки на контроле",
    body:
      "Управляйте закупками на контроле. Назначайте координаторов, следите за сроками и статусом тендера, выгружайте список закупок в Excel.",
    imgPath: "/welcome/sl5-image.png",
    iconPath: "/welcome/sl5-icon.svg"
  },
  {
    title: "Личный кабинет",
    body:
      "Редактируйте личные данные, подключайте оповещения и следите за сроком действия подписки.",
    imgPath: "/welcome/sl6-image.png",
    iconPath: "/welcome/sl6-icon.svg"
  },
  {
    title: "Пользователи",
    body: `Управляйте пользователями и правами доступа. Приглашайте новых членов команды для 
      совместной работы, назначайте каждому роль (Координатор, Администратор) в системе.`,
    imgPath: "/welcome/sl7-image.png",
    iconPath: "/welcome/sl7-icon.svg"
  },
  {
    title: "Другие сервисы",
    body:
      "Воспользуйтесь дополнительными сервисами, которые будут полезны в работе специалиста по электронным торгам.",
    imgPath: "/welcome/sl8-image.png",
    iconPath: "/welcome/sl8-icon.svg"
  }
];

export class WelcomeDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      open: true
    };
  }

  handleNext = () => {
    const { activeStep } = this.state;
    this.setState({ activeStep: activeStep + 1 });
  };

  handleBack = () => {
    const { activeStep } = this.state;
    this.setState({ activeStep: activeStep - 1 });
  };

  handleAction = () => {
    const { toggleDialog } = this.props;
    toggleDialog();
  };

  handleClose = () => {
    const { showJivosite, pushRoot } = this.props;

    pushRoot();
    this.setState({ open: false });
    localStorage.setItem("tourViewed", true);
    showJivosite();
  };

  handleChange = step => {
    this.setState({ activeStep: step });
  };

  render() {
    const { classes } = this.props;
    const maxSteps = welcomeSteps.length;
    const { activeStep, open } = this.state;

    return (
      <Dialog
        open={open}
        onClose={this.handleClose}
        classes={{ paper: classes.paper }}
      >
        <IconButton
          aria-label="Закрыть"
          className={classes.closeButton}
          onClick={this.handleClose}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent classes={{ root: classes.content }}>
          {welcomeSteps[activeStep].firstStep && (
            <div className={classes.firstSlideContainer}>
              <div className={classes.container}>
                <div className={classes.firstSlideLeftContent}>
                  <div className={classes.contentPadding}>
                    <Hidden xsDown>
                      <img
                        className={classes.logo}
                        src={welcomeSteps[activeStep].logoPath}
                        alt={welcomeSteps[activeStep].title}
                        width={272}
                      />
                    </Hidden>
                    <Typography variant="h1" className={classes.welcomeHeader1}>
                      Добро пожаловать в Гарант Тендер!
                    </Typography>
                    <Typography variant="h2" className={classes.welcomeHeader2}>
                      для поиска торгов и эффективной работы с ними
                    </Typography>
                    <Typography className={classes.body}>
                      Хотите охватить большее количество торгов и опередить
                      конкурентов? Быть уверенным в том, что закупочная
                      процедура будет проведена без ошибок, а выбранный
                      контрагент – надежен? Мы организуем вашу работу с
                      тендерами «под ключ»
                    </Typography>
                  </div>
                </div>
                <div className={classes.firstSlideRightContent}>
                  <div className={classes.contentPadding}>
                    <Typography variant="h3" className={classes.welcomeHeader3}>
                      С нами удобно!
                    </Typography>
                    <ul className={classes.list}>
                      <li className={classes.listItem}>
                        <Typography className={classes.body}>
                          Профессиональный сервис для поиска закупок в составе
                          информационно-правового обеспечения ГАРАНТ
                        </Typography>
                      </li>
                      <li className={classes.listItem}>
                        <Typography className={classes.body}>
                          Актуальные данные по закупкам на государственных и
                          коммерческих электронных площадках «в одном окне»
                        </Typography>
                      </li>
                      <li className={classes.listItem}>
                        <Typography className={classes.body}>
                          Дружелюбный интерфейс и гибкий, интуитивно понятный
                          поиск
                        </Typography>
                      </li>
                      <li className={classes.listItem}>
                        <Typography className={classes.body}>
                          Возможность настройки правил поиска для регулярного
                          использования в «один клик»
                        </Typography>
                      </li>
                      <li className={classes.listItem}>
                        <Typography className={classes.body}>
                          Передача закупок «на контроль» и организация работы
                          сотрудников с ними
                        </Typography>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          )}
          {!welcomeSteps[activeStep].firstStep && (
            <div className={classes.container}>
              <div className={classes.leftContent}>
                <div className={classes.leftBlock}>
                  <div>
                    <Hidden smDown>
                      <img
                        className={classes.icon}
                        src={welcomeSteps[activeStep].iconPath}
                        alt={welcomeSteps[activeStep].title}
                        width={60}
                      />
                    </Hidden>
                    <Typography className={classes.header}>
                      {welcomeSteps[activeStep].title}
                    </Typography>
                    <Typography className={classes.bodyCenter}>
                      {welcomeSteps[activeStep].body}
                    </Typography>
                  </div>
                </div>
              </div>
              <div className={classes.rightContent}>
                <img
                  className={classes.img}
                  src={welcomeSteps[activeStep].imgPath}
                  alt={welcomeSteps[activeStep].title}
                />
              </div>
            </div>
          )}
        </DialogContent>
        <MobileStepper
          steps={maxSteps}
          position="static"
          variant="dots"
          activeStep={activeStep}
          classes={{ dot: classes.dot }}
          nextButton={
            <Button
              onClick={this.handleNext}
              disabled={activeStep === maxSteps - 1}
              color="primary"
              className={classes.button}
              disableRipple
              classes={{ disabled: classes.buttonDisabled }}
            >
              <FormattedMessage
                id="dashboard.welcome-dialog.next"
                defaultMessage="Далее"
              />
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button
              onClick={this.handleBack}
              disabled={activeStep === 0}
              color="primary"
              className={classes.button}
              disableRipple
              classes={{ disabled: classes.buttonDisabled }}
            >
              <KeyboardArrowLeft />
              <FormattedMessage
                id="dashboard.welcome-dialog.back"
                defaultMessage="Назад"
              />
            </Button>
          }
        />
      </Dialog>
    );
  }
}

export default withStyles(styles)(WelcomeDialog);
