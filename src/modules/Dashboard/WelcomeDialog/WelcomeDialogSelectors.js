import { createSelector } from "reselect";
import { getLocationPathname } from "../../../services/Router/RouterSelectors";

export const getVisible = createSelector(
  getLocationPathname,
  path => path.includes("/tour") || !localStorage.getItem("tourViewed")
);
