import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import { DASHBOARD_PATH } from "../DashboardConstants";
import { getVisible } from "./WelcomeDialogSelectors";
import { showJivosite } from "../Jivosite/JivositeActions";
import WelcomeDialog from "./WelcomeDialog";

let visible = false;

const WelcomeDialogContainer = props => {
  if (props.visible) {
    visible = true;
  }
  return visible ? <WelcomeDialog {...props} /> : "";
};

const mapStateToProps = state => {
  return {
    visible: getVisible(state)
  };
};

export default connect(mapStateToProps, {
  pushRoot: () => push(DASHBOARD_PATH),
  showJivosite
})(WelcomeDialogContainer);
