import { combineReducers } from "redux";

import drawer from "./Drawer/DrawerReducers";
import filter from "./Filter/FilterReducers";
import filters from "./Filters/FiltersReducers";
import filtersUsers from "./FiltersUsers/FiltersUsersReducers";
import userFilters from "./UserFilters/UserFiltersReducers";
import fastSearch from "./FastSearch/FastSearchReducers";
import jivosite from "./Jivosite/JivositeReducers";

export default combineReducers({
  drawer,
  filter,
  filters,
  filtersUsers,
  userFilters,
  fastSearch,
  jivosite
});
