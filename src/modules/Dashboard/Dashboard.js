import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import DrawerContainer from "./Drawer/DrawerContainer";
import FilterContainer from "./Filter/FilterContainer";
import ToolbarContainer from "./Toolbar/ToolbarContainer";
import ProgressBarContainer from "./ProgressBar/ProgressBarContainer";
import Router from "../../services/Router/Router";
import ConfirmationDialogContainer from "../../common/ConfirmationDialog/ConfirmationDialogContainer";
import WelcomeDialogContainer from "./WelcomeDialog/WelcomeDialogContainer";
import ForeignNoticeContainer from "./ForeignNotice/ForeignNoticeContainer";
import JivositeContainer from "./Jivosite/JivositeContainer";
import Footer from "./Footer/Footer";

const styles = theme => ({
  root: {
    display: "flex",
    width: "100%"
  },
  title: {
    flexGrow: 0
  },
  drawer: {
    maxWidth: theme.spacing(36)
  },
  container: {
    position: "relative",
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "stretch"
  },
  content: {
    flexGrow: 1,
    flexShrink: 1,
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    overflow: "hidden"
  },
  footer: {
    flexGrow: 0,
    flexShrink: 0
  },
  progressBar: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    zIndex: theme.zIndex.drawer + 2
  }
});

export class Dashboard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { classes, routes, shared } = this.props;

    return (
      <div className={classes.root}>
        {!shared && (
          <>
            <ForeignNoticeContainer />
            <ConfirmationDialogContainer />
            <WelcomeDialogContainer />
            <DrawerContainer className={classes.drawer} />
          </>
        )}
        <FilterContainer />
        <ToolbarContainer />
        <ProgressBarContainer />
        <div className={classes.container}>
          <div className={classes.content}>
            <Router routes={routes} />
          </div>
          {!shared && <Footer className={classes.footer} />}
        </div>
        {!shared && <JivositeContainer />}
      </div>
    );
  }
}

export default withStyles(styles)(Dashboard);
