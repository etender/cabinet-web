import React from "react";
import { connect } from "react-redux";

import { getShared } from "../../Shared/SharedSelectors";
import {
  createFilter,
  applyFilter,
  editFilter,
  copyFilter,
  updateFilter,
  updateFilterName
} from "../Filter/FilterActions";
import { getCurrentFilter, canCreateFilter } from "../Filter/FilterSelectors";
import { isTariffLight } from "../../../services/Devise/DeviseSelectors";
import { toggleDialog } from "../../../common/Dialog/DialogActions";

import FilterForm from "./FilterForm";

const FilterFormContainer = props => <FilterForm {...props} />;

const mapStateToProps = state => {
  return {
    value: getCurrentFilter(state),
    tariffLight: isTariffLight(state),
    shared: getShared(state),
    canCreateFilter: canCreateFilter(state)
  };
};

export default connect(mapStateToProps, {
  toggleFilterName: () => toggleDialog("filterName"),
  createFilter,
  applyFilter,
  editFilter,
  copyFilter,
  updateFilter,
  updateFilterName
})(FilterFormContainer);
