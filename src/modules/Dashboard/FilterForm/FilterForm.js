import React, { Component, Fragment } from "react";
import SimpleBar from "simplebar-react";
import NumberFormat from "react-number-format";
import {
  Typography,
  FormGroup,
  FormLabel,
  FormControl,
  FormControlLabel,
  TextField,
  Radio,
  RadioGroup,
  Checkbox,
  Button,
  InputAdornment,
  IconButton,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  withStyles
} from "@material-ui/core";
import {
  ExpandMore as ExpandMoreIcon,
  Edit as EditIcon,
  Event as EventIcon
} from "@material-ui/icons";
import { DatePicker } from "@material-ui/pickers";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Form from "../../../common/Form/Form";
import AutocompleteField from "../../../common/AutocompleteField/AutocompleteField";
import AutocompleteFieldContainer from "../../../common/AutocompleteField/AutocompleteFieldContainer";
import FilterName from "../FilterName/FilterName";
import FilterAreas from "../FilterAreas/FilterAreas";
import FilterAuctions from "../FilterForms/FilterForms";
import TreeViewDialogFieldContainer from "../../../common/TreeViewDialogField/TreeViewDialogFieldContainer";
import { isEmpty, isPresent } from "../../../utils/CoreUtils";
import { defaultFilter } from "../Filter/FilterHelpers";
import HelpButton from "../../../common/HelpButton/HelpButton";

const messages = defineMessages({
  title: {
    id: "dashboard.filter-form.title",
    defaultMessage: "Точный поиск"
  },
  titleWithFilter: {
    id: "dashboard.filter-form.title-with-filter",
    defaultMessage: "Фильтр"
  },
  regionsField: {
    id: "dashboard.filter-form.regions",
    defaultMessage: "Регион или город"
  },
  regionsFieldHelpMessage: {
    id: "dashboard.filter-form.regions-help-message",
    defaultMessage: `<p>Чтобы найти тендеры в интересующем регионе или городе, начните вбивать в поле его название. Система предложит подходящие варианты из списка. Выберите один или несколько регионов, в которых нужно искать тендеры.</p>`
  },
  deliveryPlacesField: {
    id: "dashboard.filter-form.delivery-places",
    defaultMessage: "Место доставки"
  },
  keywordsField: {
    id: "dashboard.filter-form.keywords-field",
    defaultMessage: "Cо словами"
  },
  keywordsFieldHelpMessage: {
    id: "dashboard.filter-form.keywords-field-help-message",
    defaultMessage: `
    <p>Введите ключевые слова, содержащиеся в извещении процедуры, внутри документации, перечне товаров или лотов. Ключевым словом или словосочетанием может быть название отрасли, товара или услуги.</p>
    <p>Введите слова через запятую, чтобы найти процедуру хотя бы с одним словом. Например: <strong>колеса, шины, резина</strong>. Введите слова, разделяя пробелом, чтобы найти процедуры, содержащие все слова сразу. Например: (ищем) <strong>автомобильные шины</strong> — (получаем) поставка <strong>шин</strong> и <strong>дисков автомобильных</strong>.</p>
    <p>Введите словосочетание в кавычках, чтобы найти процедуры с точным соответствием искомой фразе, сохраняя порядок слов.</p>
    <p>Система применяет правила морфологии: каждое слово автоматически склоняется по падежам и числам. Правила применяются для всех полей.</p>`
  },
  includingField: {
    id: "dashboard.filter-form.including-field",
    defaultMessage: "Включая слова"
  },
  includingFieldHelpMessage: {
    id: "dashboard.filter-form.including-field-help-message",
    defaultMessage: `<p>Введите любые слова или словосочетания, чтобы уточнить параметры, указанные в поле “Со словами”. Это сузит круг поиска, потому что эти слова обязательно будут присутствовать в описании процедуры, лотов и внутри документации. Например: если вы производите шины для грузовых автомобилей, то для уточнения результатов введите в поле <strong>Камаз, Маз</strong> и т.д.</p>`
  },
  excludingField: {
    id: "dashboard.filter-form.excluding-field",
    defaultMessage: "Исключая слова"
  },
  excludingFieldHelpMessage: {
    id: "dashboard.filter-form.excluding-field-help-message",
    defaultMessage: `<p>
    Введите слова, которые не должны присутствовать в описании процедуры. Тендеры с этими словами не попадут в поисковую выдачу. Например: если в поле Со словами вы ввели <strong>автозапчасти</strong>, но не хотите, чтобы в результаты попали закупки шин и резины, потому что вы их не производите или не поставляете, то в поле Исключая слова введите <strong>шины, резины</strong>.</p>`
  },
  priceFromField: {
    id: "dashboard.filter-form.price-start-field",
    defaultMessage: "Цена, от"
  },
  priceToField: {
    id: "dashboard.filter-form.price-end-field",
    defaultMessage: "Цена, до"
  },
  okpd2Field: {
    id: "dashboard.filter-form.okpd2-field",
    defaultMessage: "ОКПД2"
  },
  okpd2FieldHelpMessage: {
    id: "dashboard.filter-form.okpd2-field-help-message",
    defaultMessage: `<p>ОКПД2 — Общероссийский классификатор продукции по видам экономической деятельности. Предназначен для поиска и подбора кодов в известной группе без ввода ключевых слов. Любая государственная закупка имеет код ОКПД2. Если же требуется подобрать виды продукции, опираясь только на названия услуг и типов товаров, введите наименование позиции в расположенное в верхней части окна поле поиска.</p>`
  },
  categoriesField: {
    id: "dashboard.filter-form.categories-field",
    defaultMessage: "Категории"
  },
  categoriesFieldHelpMessage: {
    id: "dashboard.filter-form.categories-field-help-message",
    defaultMessage: `<p>Воспользуйтесь справочником “Категории”, если хотите найти закупки по различным сферам  деятельности. Введите наименование деятельности, услуги или типа товаров, либо укажите код классификатора ОКПД2.</p>`
  },
  okved2Field: {
    id: "dashboard.filter-form.okved2-field",
    defaultMessage: "ОКВЭД2"
  },
  okved2FieldHelpMessage: {
    id: "dashboard.filter-form.okved2-field-help-message",
    defaultMessage: `<p>ОКВЭД2 — Общероссийский классификатор видов экономической деятельности, коды которого указываются при регистрации индивидуального предпринимателя и общества с ограниченной ответственностью. Если вас интересуют закупки по конкретной отрасли, то заполните это поле, и получите нужный результат без ввода ключевых слов.</p>`
  },
  nameField: {
    id: "dashboard.filter-form.name-field",
    defaultMessage: "Название фильтра"
  },
  contactField: {
    id: "dashboard.filter-form.customer-field",
    defaultMessage: "Заказчик"
  },
  contactFieldHelpMessage: {
    id: "dashboard.filter-form.customer-field-help-message",
    defaultMessage: `<p>Введите наименование или ИНН организации-заказчика. Вы можете указать одно или сразу несколько значений.</p>`
  },
  pubDateFromField: {
    id: "dashboard.filter-form.pub-date-from-field",
    defaultMessage: "Дата публикации, от"
  },
  pubDateToField: {
    id: "dashboard.filter-form.pub-date-to-field",
    defaultMessage: "Дата публикации, до"
  },
  pubDateFieldHelpMessage: {
    id: "dashboard.filter-form.pub-date-to-field-help-message",
    defaultMessage: `
    <p>Выберите с помощью календаря дату публикации процедуры.</p>
    <p>Дата публикации процедуры на площадке поможет вам найти процедуры, по конкретной дате.</p>`
  },
  endDateFromField: {
    id: "dashboard.filter-form.end-date-from-field",
    defaultMessage: "Дата окончания приема заявок, от"
  },
  endDateToField: {
    id: "dashboard.filter-form.end-date-to-field",
    defaultMessage: "Дата окончания приема заявок, до"
  },
  endDateFieldHelpMessage: {
    id: "dashboard.filter-form.end-date-to-field-help-message",
    defaultMessage: `
    <p>С помощью календаря выберите дату окончания приема заявок.</p>
    <p>Срок приема заявок важная дата для участника закупок. К этой дате вам нужно внимательно изучить процедуру, зарегистрироваться на площадке и подготовить документы.</p>`
  },
  clear: {
    id: "dashboard.filter-form.clear",
    defaultMessage: "Очистить"
  },
  cancel: {
    id: "dashboard.filter-form.cancel",
    defaultMessage: "Отмена"
  },
  stagePlan: {
    id: "dashboard.filter-form.plan-stage",
    defaultMessage: "Планируется"
  },
  stageApplication: {
    id: "dashboard.filter-form.application-stage",
    defaultMessage: "Подача заявок"
  },
  stageArchival: {
    id: "dashboard.filter-form.archival-stage",
    defaultMessage: "Архивные"
  }
});

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    overflowY: "auto"
  },
  card: {
    marginRight: "auto",
    width: "100%",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    },
    overflow: "unset"
  },
  formControl: {
    marginTop: theme.spacing(3)
  },
  dateFilter: {
    width: "calc((100% / 2) - 22px)",
    marginTop: "0px",
    "&:first-child": {
      marginRight: theme.spacing(3)
    }
  },
  priceFilter: {
    width: "calc((100% / 2) - 12px)",
    "&:first-child": {
      marginRight: theme.spacing(3)
    }
  },
  accordionDetails: {
    display: "block",
    paddingLeft: 0,
    paddingRight: 0
  },
  accordion: {
    boxShadow: "none",
    "&:before": {
      backgroundColor: "transparent"
    }
  },
  accordionSummary: {
    paddingLeft: 0,
    paddingRight: 0
  },
  nameEditIcon: {
    padding: 7
  },
  dateHelpBotton: {
    marginTop: theme.spacing(1)
  }
});

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={value => {
        onChange(value.value);
      }}
      thousandSeparator={" "}
    />
  );
}

class FilterForm extends Component {
  constructor(props) {
    super(props);

    this.filterRef = React.createRef();
  }

  handleFormAction = filter => {
    const { applyFilter } = this.props;
    applyFilter(filter);
  };

  moreFiltersExpanded = () => {
    const { value } = this.props;
    if (
      isPresent(value.pubDateFrom) ||
      isPresent(value.pubDateTo) ||
      isPresent(value.endDateFrom) ||
      isPresent(value.endDateTo) ||
      isPresent(value.priceFrom) ||
      isPresent(value.priceTo) ||
      isPresent(value.contact)
    ) {
      return true;
    }
    return false;
  };

  handleSaveClick = () => {
    const { updateFilter, value } = this.props;

    if (value.persisted) {
      const values = this.filterRef.current.getValues();
      updateFilter({ ...value, ...values });
    } else {
      this.handleNameToggle();
    }
  };

  handleCopyClick = () => {
    const { copyFilter, value } = this.props;
    copyFilter(value.id);
  };

  handleCleanClick = () => {
    const { applyFilter } = this.props;
    this.filterRef.current.clean();

    applyFilter(defaultFilter);
  };

  handleNameToggle = () => {
    const { toggleFilterName } = this.props;
    toggleFilterName();
  };

  changeName = (options, form) => {
    const { createFilter, updateFilterName, value } = this.props;

    const values = this.filterRef.current.getValues();
    if (value.persisted) {
      updateFilterName(value, options.name, form);
    } else {
      createFilter({ ...values, ...options }, form);
    }
  };

  render() {
    const {
      classes,
      value,
      intl,
      tariffLight,
      shared,
      canCreateFilter
    } = this.props;
    const { formatMessage } = intl;

    if (isEmpty(value)) {
      return "";
    }

    return (
      <SimpleBar style={{ height: "100%" }}>
        <div className={classes.root}>
          <FilterName value={value} change={this.changeName} />

          <Form
            action={this.handleFormAction}
            value={value}
            ref={this.filterRef}
          >
            {({
              field,
              arrayField,
              getBooleanValue,
              getValue,
              getArrayValue,
              getDateTimeValue,
              handleChange,
              handleInputChange,
              handleArrayChange
            }) => (
              <Fragment>
                <Card className={classes.card}>
                  {value.persisted && (
                    <CardHeader
                      title={
                        <span>
                          {formatMessage(messages.titleWithFilter)}:{" "}
                          {value.name}
                          <IconButton
                            onClick={this.handleNameToggle}
                            className={classes.nameEditIcon}
                          >
                            <EditIcon fontSize="small" />
                          </IconButton>
                        </span>
                      }
                    />
                  )}
                  {!value.persisted && (
                    <CardHeader title={formatMessage(messages.title)} />
                  )}
                  <CardContent>
                    <AutocompleteFieldContainer
                      fullWidth
                      label={formatMessage(messages.keywordsField)}
                      name="keywords"
                      fieldRef={field}
                      value={getValue("keywords")}
                      onChange={value => handleChange("keywords", value)}
                      helpButtonBody={formatMessage(
                        messages.keywordsFieldHelpMessage
                      )}
                    />

                    <AutocompleteFieldContainer
                      fullWidth
                      label={formatMessage(messages.includingField)}
                      name="including"
                      fieldRef={field}
                      value={getValue("including")}
                      onChange={value => handleChange("including", value)}
                      helpButtonBody={formatMessage(
                        messages.includingFieldHelpMessage
                      )}
                    />

                    <AutocompleteFieldContainer
                      fullWidth
                      label={formatMessage(messages.excludingField)}
                      name="excluding"
                      fieldRef={field}
                      value={getValue("excluding")}
                      onChange={value => handleChange("excluding", value)}
                      helpButtonBody={formatMessage(
                        messages.excludingFieldHelpMessage
                      )}
                    />

                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Checkbox
                            name="attachmentSearch"
                            color="primary"
                            inputRef={field}
                            checked={getBooleanValue("attachmentSearch")}
                            onChange={handleInputChange}
                          />
                        }
                        label={
                          <FormattedMessage
                            id="dashboard.filter-form.attachment-search-field"
                            defaultMessage="Искать внутри документации"
                          />
                        }
                      />

                      <FormControlLabel
                        control={
                          <Checkbox
                            name="hasPayment"
                            color="primary"
                            inputRef={field}
                            checked={getBooleanValue("hasPayment")}
                            onChange={handleInputChange}
                          />
                        }
                        label={
                          <FormattedMessage
                            id="dashboard.filter-form.has-payment-field"
                            defaultMessage="Искать с авансом"
                          />
                        }
                      />
                    </FormGroup>

                    <AutocompleteFieldContainer
                      objectValue
                      selectOnly
                      fullWidth
                      label={formatMessage(messages.regionsField)}
                      name="regions"
                      fieldRef={field}
                      value={getValue("regions", [])}
                      onChange={value => handleChange("regions", value)}
                      helpButtonBody={formatMessage(
                        messages.regionsFieldHelpMessage
                      )}
                    />

                    <FormControl
                      component="fieldset"
                      className={classes.formControl}
                    >
                      <FormLabel component="legend">
                        <FormattedMessage
                          id="dashboard.filter-form.stage"
                          defaultMessage="Этап"
                        />
                      </FormLabel>
                      <RadioGroup
                        name="stage"
                        value={getValue("stage")}
                        onChange={event =>
                          handleChange("stage", event.target.value)
                        }
                        style={{ flexDirection: "row", display: "flex" }}
                      >
                        <FormControlLabel
                          value="plan"
                          control={<Radio color="primary" />}
                          label={formatMessage(messages.stagePlan)}
                        />
                        <FormControlLabel
                          value="application"
                          control={<Radio color="primary" />}
                          label={formatMessage(messages.stageApplication)}
                        />
                        {!shared && (
                          <FormControlLabel
                            value="archival"
                            control={<Radio color="primary" />}
                            label={formatMessage(messages.stageArchival)}
                          />
                        )}
                      </RadioGroup>
                    </FormControl>
                    <br />
                    <FormControl
                      component="fieldset"
                      className={classes.formControl}
                    >
                      <FormLabel component="legend">
                        <FormattedMessage
                          id="dashboard.filter-form.type"
                          defaultMessage="Тип проведения"
                        />
                      </FormLabel>
                      <FormGroup>
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="federalLaw"
                              value="com"
                              color="primary"
                              inputRef={arrayField}
                              checked={getArrayValue("federalLaw", "com")}
                              onChange={handleArrayChange}
                            />
                          }
                          label={
                            <FormattedMessage
                              id="dashboard.filter-form.com-field"
                              defaultMessage="Коммерческий"
                            />
                          }
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="federalLaw"
                              value="fl223"
                              color="primary"
                              inputRef={arrayField}
                              checked={getArrayValue("federalLaw", "fl223")}
                              onChange={handleArrayChange}
                            />
                          }
                          label={
                            <FormattedMessage
                              id="dashboard.filter-form.fl223-field"
                              defaultMessage="ФЗ №223"
                            />
                          }
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="federalLaw"
                              value="fl44"
                              color="primary"
                              inputRef={arrayField}
                              checked={getArrayValue("federalLaw", "fl44")}
                              onChange={handleArrayChange}
                            />
                          }
                          label={
                            <FormattedMessage
                              id="dashboard.filter-form.fl44-field"
                              defaultMessage="ФЗ №44"
                            />
                          }
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="federalLaw"
                              value="smpchs"
                              color="primary"
                              inputRef={arrayField}
                              checked={getArrayValue("federalLaw", "smpchs")}
                              onChange={handleArrayChange}
                            />
                          }
                          label={
                            <FormattedMessage
                              id="dashboard.filter-form.smpchs-field"
                              defaultMessage="Малые закупки"
                            />
                          }
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="federalLaw"
                              value="fl176"
                              color="primary"
                              inputRef={arrayField}
                              checked={getArrayValue("federalLaw", "fl176")}
                              onChange={handleArrayChange}
                            />
                          }
                          label={
                            <FormattedMessage
                              id="dashboard.filter-form.fl176-field"
                              defaultMessage="ПП №615"
                            />
                          }
                        />
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="deal"
                              color="primary"
                              inputRef={field}
                              checked={getBooleanValue("deal")}
                              onChange={handleInputChange}
                            />
                          }
                          label={
                            <FormattedMessage
                              id="dashboard.filter-form.bankruptcy-field"
                              defaultMessage="Торги по банкротству"
                            />
                          }
                        />
                      </FormGroup>
                    </FormControl>

                    <div>
                      <FilterAuctions
                        value={getValue("forms", [])}
                        onChange={value => handleChange("forms", value)}
                      />
                    </div>

                    <div>
                      <FilterAreas
                        value={getValue("areas", [])}
                        onChange={value => handleChange("areas", value)}
                        onlyBase={tariffLight}
                      />
                    </div>

                    <div>
                      <TreeViewDialogFieldContainer
                        name="categories"
                        title={formatMessage(messages.categoriesField)}
                        value={getValue("categories", [])}
                        onChange={value => handleChange("categories", value)}
                        helpButtonBody={formatMessage(
                          messages.categoriesFieldHelpMessage
                        )}
                      />
                    </div>

                    <div>
                      <TreeViewDialogFieldContainer
                        name="okpds"
                        title={formatMessage(messages.okpd2Field)}
                        value={getValue("okpds", [])}
                        onChange={value => handleChange("okpds", value)}
                        helpButtonBody={formatMessage(
                          messages.okpd2FieldHelpMessage
                        )}
                      />
                    </div>

                    <Accordion
                      className={classes.accordion}
                      defaultExpanded={this.moreFiltersExpanded()}
                    >
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        className={classes.accordionSummary}
                      >
                        <Typography color="primary">
                          <FormattedMessage
                            id="dashboard.filter-form.more-filters"
                            defaultMessage="Больше фильтров"
                          />
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails className={classes.accordionDetails}>
                        <div>
                          <TextField
                            name="priceFrom"
                            label={formatMessage(messages.priceFromField)}
                            className={classes.priceFilter}
                            value={getValue("priceFrom")}
                            onChange={value => handleChange("priceFrom", value)}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  ₽
                                </InputAdornment>
                              ),
                              inputComponent: NumberFormatCustom
                            }}
                          />
                          <TextField
                            name="priceTo"
                            label={formatMessage(messages.priceToField)}
                            className={classes.priceFilter}
                            value={getValue("priceTo")}
                            onChange={value => handleChange("priceTo", value)}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  ₽
                                </InputAdornment>
                              ),
                              inputComponent: NumberFormatCustom
                            }}
                          />
                        </div>

                        <AutocompleteField
                          fullWidth
                          label={formatMessage(messages.okved2Field)}
                          name="okved"
                          ref={field}
                          value={getValue("okved")}
                          onChange={value => handleChange("okved", value)}
                          helpButtonBody={formatMessage(
                            messages.okved2FieldHelpMessage
                          )}
                        />

                        <AutocompleteFieldContainer
                          fullWidth
                          label={formatMessage(messages.contactField)}
                          name="contact"
                          fieldRef={field}
                          value={getValue("contact")}
                          onChange={value => handleChange("contact", value)}
                          helpButtonBody={formatMessage(
                            messages.contactFieldHelpMessage
                          )}
                        />

                        <div>
                          <DatePicker
                            clearable
                            autoOk
                            disableFuture
                            margin="normal"
                            format="DD.MM.YYYY"
                            label={formatMessage(messages.pubDateFromField)}
                            value={getDateTimeValue("pubDateFrom")}
                            onChange={value =>
                              handleChange("pubDateFrom", value)
                            }
                            clearLabel={formatMessage(messages.clear)}
                            cancelLabel={formatMessage(messages.cancel)}
                            className={classes.dateFilter}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <EventIcon />
                                  </IconButton>
                                </InputAdornment>
                              )
                            }}
                          />
                          <DatePicker
                            clearable
                            autoOk
                            disableFuture
                            margin="normal"
                            format="DD.MM.YYYY"
                            label={formatMessage(messages.pubDateToField)}
                            value={getDateTimeValue("pubDateTo")}
                            onChange={value => handleChange("pubDateTo", value)}
                            clearLabel={formatMessage(messages.clear)}
                            cancelLabel={formatMessage(messages.cancel)}
                            className={classes.dateFilter}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <EventIcon />
                                  </IconButton>
                                </InputAdornment>
                              )
                            }}
                          />
                          <HelpButton
                            body={formatMessage(
                              messages.pubDateFieldHelpMessage
                            )}
                            classes={{ root: classes.dateHelpBotton }}
                          />
                        </div>

                        <div>
                          <DatePicker
                            clearable
                            autoOk
                            margin="normal"
                            format="DD.MM.YYYY"
                            label={formatMessage(messages.endDateFromField)}
                            value={getDateTimeValue("endDateFrom")}
                            onChange={value =>
                              handleChange("endDateFrom", value)
                            }
                            clearLabel={formatMessage(messages.clear)}
                            cancelLabel={formatMessage(messages.cancel)}
                            className={classes.dateFilter}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <EventIcon />
                                  </IconButton>
                                </InputAdornment>
                              )
                            }}
                          />
                          <DatePicker
                            clearable
                            autoOk
                            margin="normal"
                            format="DD.MM.YYYY"
                            label={formatMessage(messages.endDateToField)}
                            value={getDateTimeValue("endDateTo")}
                            onChange={value => handleChange("endDateTo", value)}
                            clearLabel={formatMessage(messages.clear)}
                            cancelLabel={formatMessage(messages.cancel)}
                            className={classes.dateFilter}
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end">
                                  <IconButton>
                                    <EventIcon />
                                  </IconButton>
                                </InputAdornment>
                              )
                            }}
                          />
                          <HelpButton
                            body={formatMessage(
                              messages.endDateFieldHelpMessage
                            )}
                            classes={{ root: classes.dateHelpBotton }}
                          />
                        </div>
                      </AccordionDetails>
                    </Accordion>
                  </CardContent>
                  <CardActions>
                    <Button
                      type="submit"
                      variant="contained"
                      color="primary"
                      size="medium"
                    >
                      <FormattedMessage
                        id="dashboard.filter-form.search"
                        defaultMessage="Поиск"
                      />
                    </Button>
                    {canCreateFilter && (
                      <>
                        <Button
                          variant="contained"
                          size="medium"
                          onClick={this.handleSaveClick}
                        >
                          <FormattedMessage
                            id="dashboard.filter-form.save"
                            defaultMessage="Сохранить"
                          />
                        </Button>
                        {value.persisted && (
                          <Button
                            variant="contained"
                            size="medium"
                            onClick={this.handleCopyClick}
                          >
                            <FormattedMessage
                              id="dashboard.filter-form.copy"
                              defaultMessage="Копировать"
                            />
                          </Button>
                        )}
                      </>
                    )}
                    <Button
                      variant="contained"
                      size="medium"
                      onClick={this.handleCleanClick}
                    >
                      <FormattedMessage
                        id="dashboard.filter-form.clean"
                        defaultMessage="Сбросить"
                      />
                    </Button>
                  </CardActions>
                </Card>
              </Fragment>
            )}
          </Form>
        </div>
      </SimpleBar>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterForm));
