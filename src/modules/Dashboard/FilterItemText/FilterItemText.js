import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { ListItemText, withStyles } from "@material-ui/core";
import { injectIntl } from "react-intl";

const styles = theme => ({
  root: {
    display: "flex",
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  },
  primary: {
    flex: "70%",
    display: "block"
  },
  secondary: {
    flex: "30%",
    display: "block"
  }
});

class FilterItemText extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { primary, secondary, classes, className } = this.props;

    return (
      <ListItemText
        primary={primary}
        secondary={secondary}
        className={classes.root}
        classes={{
          primary: classNames(classes.primary, className),
          secondary: classes.secondary
        }}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(FilterItemText));
