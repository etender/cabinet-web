import { all, fork } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";

import filterSagas from "./Filter/FilterSagas";
import filtersSagas from "./Filters/FiltersSagas";
import filtersUsersSagas from "./FiltersUsers/FiltersUsersSagas";
import userFiltersSagas from "./UserFilters/UserFiltersSagas";

function* dashboardSagas() {
  yield all([
    fork(filterSagas),
    fork(filtersSagas),
    fork(filtersUsersSagas),
    fork(userFiltersSagas)
  ]);
}

sagaMiddleware.run(dashboardSagas);
