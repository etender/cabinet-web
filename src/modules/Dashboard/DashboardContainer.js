import React, { lazy } from "react";
import { connect } from "react-redux";

import "./DashboardSagas";
import { getShared } from "../Shared/SharedSelectors";
import {
  DASHBOARD_PATH,
  TENDERS_PATH,
  FILTER_PATH,
  FAVORITES_PATH,
  ANALYTICS_PATH,
  MEMBERS_PATH,
  PROFILE_PATH,
  SERVICES_PATH
} from "./DashboardConstants";
import Dashboard from "./Dashboard";

const FavoritesContainer = lazy(() =>
  import("../Favorites/FavoritesContainer")
);
const AnalyticsContainer = lazy(() =>
  import("../Analytics/AnalyticsContainer")
);
const TenderContainer = lazy(() => import("../Tender/TenderContainer"));
const TendersContainer = lazy(() => import("../Tenders/TendersContainer"));
const FilterFormContainer = lazy(() =>
  import("./FilterForm/FilterFormContainer")
);
const ProfileContainer = lazy(() => import("../Profile/ProfileContainer"));
const MembersContainer = lazy(() => import("../Members/MembersContainer"));
const OtherServicesContainer = lazy(() =>
  import("../OtherServices/OtherServicesContainer")
);

const routes = {
  [FAVORITES_PATH]: { container: FavoritesContainer },
  [ANALYTICS_PATH]: { container: AnalyticsContainer },
  [TENDERS_PATH]: { container: TenderContainer },
  [FILTER_PATH]: { container: FilterFormContainer },
  [PROFILE_PATH]: { container: ProfileContainer },
  [MEMBERS_PATH]: { container: MembersContainer },
  [SERVICES_PATH]: { container: OtherServicesContainer },
  [DASHBOARD_PATH]: { container: TendersContainer }
};

const DashboardContainer = props => <Dashboard {...props} />;

const mapStateToProps = state => {
  return {
    routes,
    shared: getShared(state)
  };
};

export default connect(mapStateToProps)(DashboardContainer);
