import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { ListItem, withStyles } from "@material-ui/core";
import { injectIntl } from "react-intl";
import grey from "@material-ui/core/colors/grey";

import FilterItemText from "../FilterItemText/FilterItemText";

const styles = theme => ({
  root: {
    [theme.breakpoints.down("sm")]: {
      paddingBottom: theme.spacing()
    }
  },
  selected: {
    backgroundColor: grey["300"],

    "&:hover": {
      backgroundColor: grey["400"]
    }
  },
  listItem: {
    marginBottom: theme.spacing(0.5),
    borderRadius: theme.spacing(2)
  }
});

class FilterItem extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: this.isSelected()
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value, selected } = this.props;

    if (prevProps.value !== value || prevProps.selected !== selected) {
      this.setState({
        selected: this.isSelected()
      });
    }
  }

  isSelected() {
    const { selected, value } = this.props;
    return !!(selected || {})[value.id];
  }

  handleClick = () => {
    let { selected } = this.state;
    selected = !selected;

    this.setState({ selected });

    const { onChange, value } = this.props;
    onChange(value, selected);
  };

  render() {
    const { value, classes } = this.props;
    const { selected } = this.state;

    return (
      <ListItem
        button
        onClick={this.handleClick}
        className={classNames(
          selected && classes.selected,
          classes.root,
          classes.listItem
        )}
      >
        <FilterItemText
          primary={value.name}
          secondary={value.href}
          className={classes.primary}
        />
      </ListItem>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterItem));
