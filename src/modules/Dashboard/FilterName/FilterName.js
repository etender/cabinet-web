import React, { Component } from "react";
import { Button, TextField, withStyles } from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import DialogContainer from "../../../common/Dialog/DialogContainer";

const messages = defineMessages({
  title: {
    id: "dashboard.filter-name.title",
    defaultMessage: "Название фильтра"
  },
  nameField: {
    id: "dashboard.filter-name.name-field",
    defaultMessage: "Имя"
  }
});

const styles = theme => ({
  paper: {
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(2)
    }
  }
});

export class FilterName extends Component {
  handleAction = (value, form) => {
    const { change } = this.props;

    if (value.name !== this.props.value.name) {
      change({ name: value.name }, form);
    }
  };

  render() {
    const { classes, intl, value } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogContainer
        name="filterName"
        classes={{ paper: classes.paper }}
        title={formatMessage(messages.title)}
        value={value}
        onAction={this.handleAction}
        buttons={
          <Button type="submit" color="primary" autoFocus>
            <FormattedMessage
              id="dashboard.filter-name.ok"
              defaultMessage="ОК"
            />
          </Button>
        }
      >
        {({ field, getValue, handleInputChange }) => (
          <TextField
            name="name"
            inputRef={field}
            value={getValue("name")}
            onChange={handleInputChange}
            required
            fullWidth
            label={formatMessage(messages.nameField)}
            InputLabelProps={{ required: false }}
            autoFocus
          />
        )}
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterName));
