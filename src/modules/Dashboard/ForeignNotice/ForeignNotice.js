import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Checkbox,
  FormControlLabel,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { DASHBOARD_FOREIGN_NOTICE } from "./ForeignNoticeConstants";
import DialogContainer from "../../../common/Dialog/DialogContainer";

const messages = defineMessages({
  title: {
    id: "dashboard.foreign-notice.title",
    defaultMessage: "Начальные настройки"
  },
  emailField: {
    id: "dashboard.foreign-notice.email-field",
    defaultMessage: "Электронный адрес"
  },
  continueButton: {
    id: "dashboard.foreign-notice.ok",
    defaultMessage: "Продолжить"
  },
  message: {
    id: "dashboard.foreign-notice.message",
    defaultMessage:
      "Для настройки уведомлений укажите Ваш адрес электронной почты и отметьте рассылки, которые Вы хотели бы получать"
  },
  tendersNewSubscription: {
    id: "dashboard.foreign-notice.tenders-new-subscription",
    defaultMessage: "Ежедневная рассылка о новых аукционах"
  },
  tendersChangeSubscription: {
    id: "dashboard.foreign-notice.tenders-change-subscription",
    defaultMessage: "Ежедневная рассылка об изменениях в тендерах"
  },
  needConfirmation: {
    id: "dashboard.foreign-notice.need-confirmation",
    defaultMessage:
      "На указанный электроный адрес отправлено письмо с сылкой подверждения."
  }
});

const styles = theme => ({});

export class ForeignNotice extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    shouldShow: PropTypes.bool.isRequired,
    toggleDialog: PropTypes.func.isRequired,
    changeProfile: PropTypes.func.isRequired,
    showSnackbar: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.value = {
      tendersNewSubscription: true,
      tendersChangeSubscription: true
    };
  }
  componentDidMount() {
    const { toggleDialog, shouldShow } = this.props;
    if (shouldShow) {
      toggleDialog();
    }
  }

  handleAction = (value, form) => {
    const { changeProfile } = this.props;
    changeProfile(value, form);
  };

  handleSubmitted = () => {
    const { showSnackbar, intl } = this.props;
    const { formatMessage } = intl;
    showSnackbar(formatMessage(messages.needConfirmation));
  };

  render() {
    const { classes, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogContainer
        name={DASHBOARD_FOREIGN_NOTICE}
        classes={{ paper: classes.paper }}
        title={formatMessage(messages.title)}
        value={this.value}
        onAction={this.handleAction}
        onSubmitted={this.handleSubmitted}
        buttons={
          <Button type="submit" color="primary">
            {formatMessage(messages.continueButton)}
          </Button>
        }
      >
        {({ field, getValue, handleInputChange, getBooleanValue }) => (
          <>
            <Typography>{formatMessage(messages.message)}</Typography>
            <TextField
              name="email"
              inputRef={field}
              value={getValue("email")}
              onChange={handleInputChange}
              required
              fullWidth
              label={formatMessage(messages.emailField)}
              InputLabelProps={{ required: false }}
              autoFocus
            />

            <FormControlLabel
              control={
                <Checkbox
                  name="tendersNewSubscription"
                  color="primary"
                  inputRef={field}
                  checked={getBooleanValue("tendersNewSubscription")}
                  onChange={handleInputChange}
                />
              }
              label={formatMessage(messages.tendersNewSubscription)}
            />

            <FormControlLabel
              control={
                <Checkbox
                  name="tendersChangeSubscription"
                  color="primary"
                  inputRef={field}
                  checked={getBooleanValue("tendersChangeSubscription")}
                  onChange={handleInputChange}
                />
              }
              label={formatMessage(messages.tendersChangeSubscription)}
            />
          </>
        )}
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(ForeignNotice));
