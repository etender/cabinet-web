import React from "react";
import { connect } from "react-redux";

import { updateUser } from "../../../services/Devise/DeviseActions";
import { toggleDialog } from "../../../common/Dialog/DialogActions";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";

import { DASHBOARD_FOREIGN_NOTICE } from "./ForeignNoticeConstants";
import { shouldShow } from "./ForeignNoticeSelectors";
import ForeignNotice from "./ForeignNotice";

const ForeignNoticeContainer = props => <ForeignNotice {...props} />;

const mapStateToProps = state => {
  return {
    shouldShow: shouldShow(state)
  };
};

export default connect(mapStateToProps, {
  toggleDialog: () => toggleDialog(DASHBOARD_FOREIGN_NOTICE),
  changeProfile: updateUser,
  showSnackbar
})(ForeignNoticeContainer);
