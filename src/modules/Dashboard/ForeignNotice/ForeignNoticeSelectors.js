import { createSelector } from "reselect";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { isEmpty } from "../../../utils/CoreUtils";

export const shouldShow = createSelector(
  getCurrentUser,
  user => user.foreign && !user.emailChanged && isEmpty(user.unconfirmedEmail)
);
