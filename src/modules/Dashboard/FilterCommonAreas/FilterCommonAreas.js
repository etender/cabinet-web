import React, { Component } from "react";
import PropTypes from "prop-types";
import { List, ListSubheader, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { isPresent } from "../../../utils/CoreUtils";
import { commonAreas } from "../FilterAreas/FilterAreasSelectors";

import FilterItem from "../FilterItem/FilterItem";

const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  }
});

class FilterCommonAreas extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = this.areasState();
  }

  areasState() {
    const { search } = this.props;

    let areas = commonAreas;
    if (isPresent(search)) {
      const re = new RegExp(search, "i");
      areas = commonAreas.filter(
        item => !search || re.test(item.name) || re.test(item.href)
      );
    }

    return { areas, enable: areas.length > 0 };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { search } = this.props;

    if (prevProps.search !== search) {
      this.setState(this.areasState());
    }
  }

  render() {
    const { classes, value, onChange } = this.props;
    const { enable, areas } = this.state;

    if (!enable) {
      return "";
    }

    return (
      <List
        className={classes.root}
        subheader={
          <ListSubheader>
            <FormattedMessage
              id="dashboard.filter-common-areas.title"
              defaultMessage="Остальные площадки"
            />
          </ListSubheader>
        }
      >
        {areas.map(item => (
          <FilterItem
            key={item.id}
            onChange={onChange}
            value={item}
            selected={value}
          />
        ))}
      </List>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterCommonAreas));
