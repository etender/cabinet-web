import React, { Component } from "react";
import PropTypes from "prop-types";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core";

import { formsHash } from "./FilterFormsSelectors";
import DialogField from "../../../common/DialogField/DialogField";
import { isPresent } from "../../../utils/CoreUtils";
import FilterBaseForms from "../FilterBaseForms/FilterBaseForms";

const messages = defineMessages({
  name: {
    id: "dashboard.filter-forms.name",
    defaultMessage: "Форма проведения"
  },
  helpMessage: {
    id: "dashboard.filter-forms.help-message",
    defaultMessage: `<p>Выберите из списка форму проведения закупки. Используйте это поле, чтобы ограничить поиск по различным конкурентным формам (по умолчанию Гарант Тендер ищет по всем формам, кроме закупок у единственного поставщика)</p>`
  }
});

const styles = theme => ({
  root: {},
  item: {}
});

class FilterForms extends Component {
  static propTypes = {
    value: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  };

  serialize = value => {
    return Object.values(value)
      .filter(item => item)
      .map(item => item.id);
  };

  deserialize(value) {
    return (isPresent(value) ? value : []).map(id => formsHash[id]);
  }

  deserializeSelected(value) {
    return value
      ? value.reduce((result, item) => ({ ...result, [item.id]: item }), {})
      : {};
  }

  renderContent(props) {
    return <FilterBaseForms {...props} />;
  }

  render() {
    const { value, onChange, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogField
        title={formatMessage(messages.name)}
        serialize={this.serialize}
        deserialize={this.deserialize}
        deserializeSelected={this.deserializeSelected}
        onChange={onChange}
        value={value}
        helpButtonBody={formatMessage(messages.helpMessage)}
        suggestionsComponent={({ search, selected, handleToggleSelected }) => {
          return this.renderContent({
            search,
            value: selected,
            onChange: handleToggleSelected
          });
        }}
      >
        {({ selected, handleToggleSelected }) =>
          this.renderContent({
            value: selected,
            onChange: handleToggleSelected
          })
        }
      </DialogField>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterForms));
