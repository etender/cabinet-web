import React, { Component } from "react";
import {
  Typography,
  Dialog,
  withStyles,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button
} from "@material-ui/core";

import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

const styles = theme => ({
  paper: {
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      margin: theme.spacing(2)
    }
  }
});

export class FilterDelete extends Component {
  handleClose = () => {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
  };

  handleOk = () => {
    this.handleClose();

    const { onAction } = this.props;
    if (onAction) {
      onAction();
    }
  };

  render() {
    const { classes, open } = this.props;

    return (
      <Dialog
        open={open}
        onClose={this.handleClose}
        aria-labelledby="title"
        classes={{ paper: classes.paper }}
      >
        <DialogTitle id="title">
          <FormattedMessage
            id="dashboard.filter-delete.title"
            defaultMessage="Удаление фильтра"
          />
        </DialogTitle>
        <DialogContent>
          <Typography variant="body1">
            <FormattedMessage
              id="dashboard.filter-delete.message"
              defaultMessage="Фильтр будет удален."
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="primary">
            <FormattedMessage
              id="dashboard.filter-delete.cancel"
              defaultMessage="Отмена"
            />
          </Button>
          <Button color="primary" autoFocus onClick={this.handleOk}>
            <FormattedMessage
              id="dashboard.filter-delete.ok"
              defaultMessage="ОК"
            />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterDelete));
