import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Typography,
  MenuItem,
  ListItemText,
  Tooltip,
  withStyles
} from "@material-ui/core";
import classNames from "classnames";

const styles = theme => ({
  name: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis"
  },
  count: {
    display: "inline-block",
    position: "relative",
    verticalAlign: "middle",
    textAlign: "right",
    flex: "2"
  },
  title: {
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden",
    maxWidth: "160px",
    marginRight: theme.spacing(1)
  },
  selected: {
    backgroundColor: "#f5f5f5"
  },
  tooltip: {
    fontSize: "1em",
    marginTop: "-8px"
  }
});

class FiltersItem extends Component {
  static propTypes = {
    filter: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired
  };

  handleClick = () => {
    const { onSelect, filter } = this.props;
    onSelect(filter);
  };

  renderMenuItem = () => {
    const { classes, filter, selected } = this.props;

    return (
      <MenuItem
        selected={selected && selected.id === filter.id}
        onClick={this.handleClick}
        className={classNames(selected ? classes.selected : "")}
      >
        <ListItemText
          primary={filter.name}
          primaryTypographyProps={{ className: classes.name }}
          className={classes.title}
        />
        <Typography color="textSecondary" className={classes.count}>
          {filter.count}
        </Typography>
      </MenuItem>
    );
  };

  render() {
    const { classes, filter } = this.props;

    return (
      <>
        {filter.name.length + filter.count.toString().length >= 20 ? (
          <Tooltip title={filter.name} classes={{ tooltip: classes.tooltip }}>
            {this.renderMenuItem()}
          </Tooltip>
        ) : (
          <>{this.renderMenuItem()}</>
        )}
      </>
    );
  }
}

export default withStyles(styles)(FiltersItem);
