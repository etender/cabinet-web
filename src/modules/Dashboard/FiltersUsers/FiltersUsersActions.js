import { createAction } from "redux-actions";

export const fetchFiltersUsers = createAction(
  "DASHBOARD_FILTERS_USERS_FETCH",
  () => ({ withFilters: true }),
  () => ({ entity: "users" })
);
