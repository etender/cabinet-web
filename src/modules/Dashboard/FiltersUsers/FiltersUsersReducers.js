import { handleActions } from "redux-actions";

import { successApiType } from "../../../services/Api/ApiHelpers";
import { fetchFiltersUsers } from "./FiltersUsersActions";

const initialState = [];

export default handleActions(
  {
    [successApiType(fetchFiltersUsers)]: (state, action) => action.payload
  },
  initialState
);
