import { takeLatest } from "redux-saga/effects";
import { fetchEntitiesSaga } from "../../../services/Entities/EntitiesSagas";
import { fetchFiltersUsers } from "./FiltersUsersActions";

export default function* filtersUsersSagas() {
  yield takeLatest(fetchFiltersUsers, fetchEntitiesSaga);
}
