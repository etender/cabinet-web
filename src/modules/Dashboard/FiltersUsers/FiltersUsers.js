import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { MenuList, ListSubheader, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import UserFiltersContainer from "../UserFilters/UserFiltersContainer";

const styles = theme => ({
  title: {
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    overflow: "hidden"
  },
  header: {
    backgroundColor: theme.palette.background.paper,
    textTransform: "uppercase",
    paddingLeft: theme.spacing(2),
    fontSize: "0.85rem",

    "&:focus": {
      outline: "none"
    }
  }
});

export class FiltersUsers extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    fetchFiltersUsers: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { fetchFiltersUsers } = this.props;
    fetchFiltersUsers();
  }

  handleSelect = filter => {
    const { onSelect } = this.props;
    onSelect(filter);
  };

  render() {
    const { classes, users } = this.props;
    return (
      <Fragment>
        {users && (
          <MenuList>
            <ListSubheader
              className={classNames(classes.title, classes.header)}
              inset
            >
              <FormattedMessage
                id="dashboard.filters-users.title"
                defaultMessage="Общие фильтры"
              />
            </ListSubheader>
            {users.map(user => (
              <UserFiltersContainer
                user={user}
                key={user.id}
                onSelect={this.handleSelect}
              />
            ))}
          </MenuList>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(FiltersUsers));
