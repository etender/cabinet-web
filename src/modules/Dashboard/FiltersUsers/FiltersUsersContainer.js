import React from "react";
import { connect } from "react-redux";

import { getFiltersUsers } from "./FiltersUsersSelectors";
import { fetchFiltersUsers } from "./FiltersUsersActions";
import FiltersUsers from "./FiltersUsers";

const FiltersUsersContainer = props => <FiltersUsers {...props} />;

const mapStateToProps = state => {
  return {
    users: getFiltersUsers(state)
  };
};

export default connect(mapStateToProps, {
  fetchFiltersUsers
})(FiltersUsersContainer);
