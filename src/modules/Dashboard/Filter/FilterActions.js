import { createAction } from "redux-actions";

export const showFilter = createAction(
  "DASHBOARD_FILTER_SHOW",
  id => id,
  (id, userId) => ({ userId })
);
export const editFilter = createAction("DASHBOARD_FILTER_EDIT");
export const deleteFilter = createAction("DASHBOARD_FILTER_DELETE");
export const applyFilter = createAction("DASHBOARD_FILTER_APPLY");
export const createFilter = createAction(
  "DASHBOARD_FILTER_CREATE",
  options => options,
  (options, form) => ({ form })
);
export const buildFilter = createAction("DASHBOARD_FILTER_BUILD");
export const copyFilter = createAction("DASHBOARD_FILTER_COPY");
export const updateFilter = createAction("DASHBOARD_FILTER_UPDATE");
export const updateFilterName = createAction(
  "DASHBOARD_FILTER_NAME_UPDATE",
  (filter, name) => ({ filter, name }),
  (id, name, form) => ({ form })
);
export const shareFilter = createAction(
  "DASHBOARD_FILTER_SHARE",
  options => options,
  () => ({ entity: "filterShare" })
);
