import React from "react";
import { connect } from "react-redux";

import { getPathFilter, shouldBuildFilter } from "./FilterSelectors";
import { buildFilter } from "./FilterActions";
import Filter from "./Filter";

const FilterContainer = props => <Filter {...props} />;

const mapStateToProps = state => {
  return {
    filter: getPathFilter(state),
    shouldBuildFilter: shouldBuildFilter(state)
  };
};

export default connect(mapStateToProps, {
  buildFilter
})(FilterContainer);
