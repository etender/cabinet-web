import { all, put, call, takeLatest } from "redux-saga/effects";
import { push } from "connected-react-router";

import { callApi } from "../../../services/Api/ApiHelpers";
import { buildPath } from "../../../services/Router/RouterHelpers";
import { isPresent } from "../../../utils/CoreUtils";
import {
  requestApi,
  successApi,
  failureApi
} from "../../../services/Api/ApiActions";
import { fetchFilters } from "../Filters/FiltersActions";
import { fetchUserFilters } from "../UserFilters/UserFiltersActions";
import { clearSearch } from "../FastSearch/FastSearchActions";
import { createEntitySaga } from "../../../services/Entities/EntitiesSagas";
import {
  showFilter,
  applyFilter,
  buildFilter,
  createFilter,
  copyFilter,
  updateFilter,
  updateFilterName,
  editFilter,
  deleteFilter,
  shareFilter
} from "./FilterActions";
import {
  API_FILTER_PATH,
  API_FILTER_CREATE_PATH,
  API_FILTER_DELETE_PATH,
  API_FILTER_BUILD_PATH,
  API_FILTER_COPY_PATH,
  API_FILTER_UPDATE_PATH
} from "./FilterConstants";
import {
  DASHBOARD_PATH,
  SEARCH_PATH,
  FILTER_PATH
} from "../DashboardConstants";
import { defaultFilter, normalizeFilter } from "./FilterHelpers";

function* showFilterSaga(action) {
  yield put(clearSearch());
  yield put(push(buildPath(SEARCH_PATH, { id: action.payload })));
  yield put(requestApi(showFilter));

  try {
    const response = yield call(callApi, API_FILTER_PATH, {
      id: action.payload
    });
    yield put(successApi(showFilter, response, action.meta));
  } catch (error) {
    yield put(failureApi(showFilter, error, action.meta));
  }
}

function* applyFilterSaga(action) {
  yield call(putFilterPath, action, SEARCH_PATH);
}

function* buildFilterSaga(action) {
  yield put(requestApi(showFilter));

  try {
    const filter = isPresent(action.payload) ? action.payload : defaultFilter;
    const response = yield call(callApi, API_FILTER_BUILD_PATH, { filter });
    yield put(successApi(showFilter, response));
  } catch (error) {
    yield put(failureApi(showFilter, error));
  }
}

function* createFilterSaga(action) {
  yield put(requestApi(createFilter, action.payload, action.meta));

  try {
    const filter = normalizeFilter(action.payload);
    filter.name = action.payload.name;

    const response = yield call(callApi, API_FILTER_CREATE_PATH, { filter });
    yield put(successApi(createFilter, response, action.meta));
    yield put(fetchFilters());
    yield put(showFilter(response.id));
  } catch (error) {
    yield put(failureApi(createFilter, error, action.meta));
  }
}

function* editFilterSaga(action) {
  yield call(putFilterPath, action, FILTER_PATH);
}

function* copyFilterSaga(action) {
  yield put(requestApi(copyFilter));

  try {
    const response = yield call(callApi, API_FILTER_COPY_PATH, {
      filter_id: action.payload
    });
    yield put(successApi(copyFilter, response));
    yield put(fetchFilters());
    yield put(editFilter(response));
  } catch (error) {
    yield put(failureApi(copyFilter, error));
  }
}

function* updateFilterSaga(action) {
  yield put(requestApi(updateFilter));

  try {
    const { id } = action.payload;
    const filter = normalizeFilter(action.payload);
    yield call(callApi, API_FILTER_UPDATE_PATH, { id }, { filter });
    yield put(successApi(updateFilter, action.payload));
    yield put(fetchFilters());
    yield put(fetchUserFilters(filter.userId));
    yield put(showFilter(id));
  } catch (error) {
    yield put(failureApi(updateFilter, error));
  }
}

function* updateFilterNameSaga(action) {
  yield put(requestApi(updateFilterName));

  const {
    payload: {
      filter: { id },
      name
    },
    meta: { form }
  } = action;

  // Enable form submitting
  form.submitting();

  try {
    yield call(callApi, API_FILTER_UPDATE_PATH, { id }, { filter: { name } });

    // Disable form submitting
    form.submitted();

    yield put(successApi(updateFilterName, action.payload));
  } catch (error) {
    // Disable form submitting
    form.submitted(error.message);

    yield put(failureApi(updateFilterName, error));
  }
}

function* deleteFilterSaga(action) {
  yield put(requestApi(deleteFilter));

  try {
    const { id } = action.payload;
    yield call(callApi, API_FILTER_DELETE_PATH, null, { id: id });
    yield put(successApi(deleteFilter, action.payload));
    yield put(push(buildPath(DASHBOARD_PATH)));
  } catch (error) {
    yield put(failureApi(deleteFilter, error));
  }
}

function* shareFilterSaga(action) {
  const payload = normalizeFilter(action.payload);
  const path = [window.location.origin, SEARCH_PATH].join("");
  const url = buildPath(path, payload);

  yield createEntitySaga({ ...action, payload: { url: url } });
}

function* putFilterPath(action, path) {
  const payload = normalizeFilter(action.payload);
  yield put(push(buildPath(path, payload)));
}

export default function* filterSagas() {
  yield all([
    takeLatest(showFilter, showFilterSaga),
    takeLatest(applyFilter, applyFilterSaga),
    takeLatest(buildFilter, buildFilterSaga),
    takeLatest(createFilter, createFilterSaga),
    takeLatest(editFilter, editFilterSaga),
    takeLatest(copyFilter, copyFilterSaga),
    takeLatest(updateFilter, updateFilterSaga),
    takeLatest(updateFilterName, updateFilterNameSaga),
    takeLatest(deleteFilter, deleteFilterSaga),
    takeLatest(shareFilter, shareFilterSaga)
  ]);
}
