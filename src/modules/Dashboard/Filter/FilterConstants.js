export const API_FILTER_PATH = { method: "GET", url: "/api/filters/:id" };
export const API_FILTER_CREATE_PATH = { method: "POST", url: "/api/filters" };
export const API_FILTER_DELETE_PATH = {
  method: "DELETE",
  url: "/api/filters/:id"
};
export const API_FILTER_BUILD_PATH = {
  method: "POST",
  url: "/api/filters/entity"
};
export const API_FILTER_COPY_PATH = {
  method: "GET",
  url: "/api/filters/:filter_id/copy"
};
export const API_FILTER_UPDATE_PATH = {
  method: "PUT",
  url: "/api/filters/:id"
};
