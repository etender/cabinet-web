export const defaultFilter = {
  federalLaw: ["fl176", "fl223", "fl44", "smpchs", "com"],
  attachmentSearch: false,
  stage: "application"
};

export const normalizeFilter = (filter, options) => {
  let result = {
    ...filter,
    regions: filter.regions && filter.regions.map(region => region.id),
    okpds: filter.okpds && filter.okpds.map(okpd => okpd.id),
    categories:
      filter.categories && filter.categories.map(category => category.id),
    ...options
  };

  delete result.name;
  delete result.count;

  return result;
};
