import { Component } from "react";

// Component allows to get full filter (with regions and other dictionaries models)
// from server and cache it.
class Filter extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { filter, buildFilter, shouldBuildFilter } = this.props;

    if (shouldBuildFilter) {
      buildFilter(filter);
    }
  }

  componentDidMount() {
    const { filter, buildFilter, shouldBuildFilter } = this.props;
    if (shouldBuildFilter) {
      buildFilter(filter);
    }
  }

  render() {
    return null;
  }
}

export default Filter;
