import { handleActions } from "redux-actions";

import {
  requestApiType,
  successApiType
} from "../../../services/Api/ApiHelpers";
import {
  applyFilter,
  createFilter,
  editFilter,
  updateFilter,
  copyFilter,
  showFilter,
  updateFilterName,
  shareFilter
} from "./FilterActions";

const initialState = {
  current: null,
  shareUrl: null
};

export default handleActions(
  {
    [applyFilter]: (state, action) => ({
      ...state,
      current: action.payload
    }),
    [successApiType(createFilter)]: (state, action) => ({
      ...state,
      current: action.payload
    }),
    [successApiType(updateFilter)]: (state, action) => ({
      ...state,
      current: action.payload
    }),
    [successApiType(showFilter)]: (state, action) => ({
      ...state,
      current: action.payload
    }),
    [successApiType(copyFilter)]: (state, action) => ({
      ...state,
      current: action.payload
    }),
    [successApiType(updateFilterName)]: (state, action) => ({
      ...state,
      current: { ...state.current, name: action.payload.name }
    }),
    [editFilter]: (state, action) => ({
      ...state,
      current: action.payload
    }),
    [requestApiType(shareFilter)]: (state, action) => ({
      ...state,
      shareUrl: null
    }),
    [successApiType(shareFilter)]: (state, action) => ({
      ...state,
      shareUrl: action.payload.url
    })
  },
  initialState
);
