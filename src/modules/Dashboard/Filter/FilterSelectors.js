import { createSelector } from "reselect";
import {
  getLocationHash,
  getLocationPathname,
  isLocationPopOrReplace
} from "../../../services/Router/RouterSelectors";
import { parseHash } from "../../../services/Router/RouterHelpers";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { getShared } from "../../Shared/SharedSelectors";
import { defaultFilter, normalizeFilter } from "./FilterHelpers";

export const getCurrentFilter = state =>
  state.dashboard.filter.current || defaultFilter;

export const getNormalizeCurrentFilter = createSelector(
  getCurrentFilter,
  filter => normalizeFilter(filter)
);

export const isFilterPath = createSelector(
  getLocationPathname,
  path =>
    path === "/" ||
    path.includes("/search") ||
    path.includes("/filter") ||
    path.includes("/tender")
);

export const getPathFilter = createSelector(
  isFilterPath,
  getLocationHash,
  (path, locationHash) => {
    if (!path) {
      return {};
    }

    const options = parseHash(locationHash);
    return options.filter || options || {};
  }
);

export const shouldBuildFilter = createSelector(
  isFilterPath,
  isLocationPopOrReplace,
  getCurrentFilter,
  (path, popOrReplace, currentFilter) =>
    path && (popOrReplace || !currentFilter)
);

export const canChangeFilter = createSelector(
  getCurrentUser,
  getCurrentFilter,
  getShared,
  (user, filter, shared) =>
    (user &&
      !user.readOnly &&
      (user.admin || filter.userId === user.id || !filter.userId)) ||
    (!user && !filter.userId)
);

export const canCreateFilter = createSelector(
  getCurrentUser,
  getShared,
  (user, shared) => !shared && !user.readOnly
);

export const canDeleteFilter = createSelector(
  getCurrentUser,
  getCurrentFilter,
  getShared,
  (user, filter, shared) =>
    !shared && !user.readOnly && filter.userId === user.id
);

export const canShareFilter = createSelector(getShared, (user, shared) => {
  return !shared;
});

export const getShareUrl = state => state.dashboard.filter.shareUrl;
