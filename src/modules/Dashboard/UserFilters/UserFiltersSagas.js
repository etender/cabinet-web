import { takeLatest } from "redux-saga/effects";
import { fetchEntitiesSaga } from "../../../services/Entities/EntitiesSagas";
import { fetchUserFilters } from "./UserFiltersActions";

export default function* userFiltersSagas() {
  yield takeLatest(fetchUserFilters, fetchEntitiesSaga);
}
