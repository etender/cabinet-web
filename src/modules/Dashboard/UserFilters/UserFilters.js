import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  MenuList,
  MenuItem,
  Collapse,
  ListItemIcon,
  ListItemText,
  withStyles
} from "@material-ui/core";
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon
} from "@material-ui/icons";
import FiltersItem from "../FiltersItem/FiltersItem";

const styles = theme => ({
  expandIcon: {
    minWidth: "30px"
  }
});

export class UserFilters extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    filters: PropTypes.arrayOf(PropTypes.object).isRequired,
    selected: PropTypes.object.isRequired,
    fetchUserFilters: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      collapsed: true
    };
  }

  handleClick = () => {
    const { collapsed } = this.state;
    this.setState({ collapsed: !collapsed });

    const { fetchUserFilters, user } = this.props;
    if (collapsed) {
      fetchUserFilters(user.id);
    }
  };

  handleSelect = filter => {
    const { showFilter, onSelect, user } = this.props;
    showFilter(filter.id, user.id);
    onSelect(filter);
  };

  render() {
    const { classes, user, selected, filters } = this.props;
    const { collapsed } = this.state;
    return (
      <Fragment>
        <MenuItem onClick={this.handleClick}>
          <ListItemIcon className={classes.expandIcon}>
            {collapsed ? <ExpandMoreIcon /> : <ExpandLessIcon />}
          </ListItemIcon>
          <ListItemText primary={user.fullName} />
        </MenuItem>

        <Collapse in={!collapsed}>
          <MenuList>
            {filters &&
              filters.map(filter => (
                <FiltersItem
                  key={filter.id}
                  filter={filter}
                  selected={selected && selected.id === filter.id}
                  onSelect={this.handleSelect}
                />
              ))}
          </MenuList>
        </Collapse>
      </Fragment>
    );
  }
}

export default withStyles(styles)(UserFilters);
