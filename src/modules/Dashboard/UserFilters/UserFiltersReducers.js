import { handleActions } from "redux-actions";

import {
  requestApiType,
  successApiType
} from "../../../services/Api/ApiHelpers";
import { fetchUserFilters } from "./UserFiltersActions";

import {
  deleteFilter,
  updateFilterName,
  showFilter
} from "../Filter/FilterActions";

const initialState = {};

export default handleActions(
  {
    [successApiType(showFilter)]: (state, action) => {
      const { userId } = action.meta || {};

      if (!userId) {
        return state;
      }

      return {
        ...state,
        [userId]: state[userId]
          ? state[userId].map(item =>
              item.id === action.payload.id
                ? { ...item, count: action.payload.count }
                : item
            )
          : null
      };
    },

    [successApiType(deleteFilter)]: (state, action) => {
      const { userId, id } = action.payload;

      return {
        ...state,
        [userId]: state[userId]
          ? state[userId].filter(item => item.id !== id)
          : null
      };
    },

    [requestApiType(fetchUserFilters)]: (state, action) => {
      const { userId } = action.payload;
      let filters = state[userId] || [];
      filters.loading = true;

      return {
        ...state,
        [userId]: filters
      };
    },

    [successApiType(fetchUserFilters)]: (state, action) => {
      const userId = action.payload[0].userId;
      return {
        ...state,
        [userId]: action.payload
      };
    },

    [successApiType(updateFilterName)]: (state, action) => {
      const userId = action.payload.filter.userId;
      if (!state[userId]) {
        return state;
      }

      return {
        ...state,
        [userId]: state[userId].map(item =>
          item.id === action.payload.filter.id
            ? { ...item, name: action.payload.name }
            : item
        )
      };
    }
  },
  initialState
);
