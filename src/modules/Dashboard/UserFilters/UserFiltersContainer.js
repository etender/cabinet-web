import React from "react";
import { connect } from "react-redux";

import { getUserFilters } from "./UserFiltersSelectors";
import { fetchUserFilters } from "./UserFiltersActions";
import { getCurrentFilter } from "../Filter/FilterSelectors";
import { showFilter } from "../Filter/FilterActions";
import UserFilters from "./UserFilters";

const UserFiltersContainer = props => <UserFilters {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { user } = ownProps;
  return {
    filters: getUserFilters(state, user.id),
    selected: getCurrentFilter(state)
  };
};

export default connect(mapStateToProps, {
  fetchUserFilters,
  showFilter
})(UserFiltersContainer);
