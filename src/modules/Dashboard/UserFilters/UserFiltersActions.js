import { createAction } from "redux-actions";

export const fetchUserFilters = createAction(
  "DASHBOARD_USER_FILTERS_FETCH",
  id => ({ userId: id }),
  () => ({ entity: "filters" })
);
