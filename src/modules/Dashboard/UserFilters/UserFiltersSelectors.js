const defaultUserFilters = [];
export const getUserFilters = (state, userId) =>
  state.dashboard.userFilters[userId] || defaultUserFilters;
