import React, { Component } from "react";
import classNames from "classnames";
import {
  Popper,
  Paper,
  MenuList,
  MenuItem,
  ListItemText,
  ClickAwayListener,
  withStyles
} from "@material-ui/core";

import { isEmpty } from "../../../utils/CoreUtils";

const styles = theme => ({
  root: {
    padding: theme.spacing(2)
  }
});

class FastSearchSuggestions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: true
    };
  }

  componentDidMount() {
    const { value, fetch } = this.props;
    if (this.valid(value)) {
      fetch(value);
    }
  }

  componentDidUpdate(prevProps) {
    const { value, fetch, clean } = this.props;

    if (value !== prevProps.value) {
      if (this.valid(value)) {
        this.setState({ open: true });
        fetch(value);
      } else {
        clean();
      }
    }
  }

  valid(value) {
    return !isEmpty(value) && value.length > 3;
  }

  handleClick(item) {
    const { onSelect } = this.props;
    if (onSelect) {
      onSelect(item);
    }
  }

  handleClickAway = () => {
    this.setState({ open: false });
  };

  render() {
    const { anchorEl, classes, items, className, value } = this.props;

    if (isEmpty(items)) {
      return "";
    }

    let { open } = this.state;
    open = open && this.valid(value);

    return (
      <Popper placement="bottom-start" open={open} anchorEl={anchorEl}>
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <Paper className={classNames(classes.root, className)}>
            <MenuList>
              {items.map(item => {
                return (
                  <MenuItem
                    key={item.id}
                    onClick={this.handleClick.bind(this, item)}
                  >
                    <ListItemText primary={item.title} />
                  </MenuItem>
                );
              })}
            </MenuList>
          </Paper>
        </ClickAwayListener>
      </Popper>
    );
  }
}

export default withStyles(styles)(FastSearchSuggestions);
