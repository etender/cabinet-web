import React from "react";
import { connect } from "react-redux";

import { getLoading } from "../../../services/Api/ApiSelectors";
import ProgressBar from "./ProgressBar";

const ProgressBarContainer = props => <ProgressBar {...props} />;

const mapStateToProps = state => {
  return {
    active: getLoading(state)
  };
};

export default connect(mapStateToProps)(ProgressBarContainer);
