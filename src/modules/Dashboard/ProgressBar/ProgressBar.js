import React, { Component } from "react";
import { LinearProgress, withStyles } from "@material-ui/core";

const styles = theme => ({
  root: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    zIndex: theme.zIndex.drawer + 2
  }
});

export class ProgressBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      completed: true
    };
  }

  componentDidUpdate(prevProps) {
    const { active } = this.props;
    if (prevProps.active !== active) {
      if (active) {
        if (this.completeTimer) {
          clearInterval(this.completeTimer);
          this.completeTimer = null;
        }

        if (this.timer) {
          return;
        }

        this.setState({ progress: 0, completed: false });
        this.timer = setInterval(this.progress, 500);
      } else {
        if (!this.timer || this.completeTimer) {
          return;
        }

        this.completeTimer = setTimeout(() => {
          clearInterval(this.timer);
          this.timer = null;
          this.setState({ progress: 100 });
          setTimeout(
            () => this.setState({ progress: 0, completed: true }),
            100
          );
        }, 300);
      }
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  progress = () => {
    let { progress } = this.state;
    const delta = (70 - progress) / 2;
    const diff = Math.random() * delta;
    this.setState({ progress: Math.round(progress + diff) });
  };

  render() {
    const { classes } = this.props;
    const { progress, completed } = this.state;

    if (completed) {
      return null;
    }

    return (
      <LinearProgress
        color="secondary"
        variant="determinate"
        value={progress}
        className={classes.root}
      />
    );
  }
}

export default withStyles(styles)(ProgressBar);
