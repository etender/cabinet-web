import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";
import {
  Drawer as MuiDrawer,
  Divider,
  Hidden,
  Link,
  withStyles,
  withWidth
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Menu from "../Menu/Menu";
import FiltersContainer from "../Filters/FiltersContainer";
import FiltersUsersContainer from "../FiltersUsers/FiltersUsersContainer";
import garantLogo from "../../../images/garant-logo.png";
import SirvicesNotice from "../Footer/ServicesNotice";
import PhoneIcon from "@material-ui/icons/Phone";

const messages = defineMessages({
  title: {
    id: "dashboard.title",
    defaultMessage: "Гартендер"
  },
  garantTitle: {
    id: "dashboard.drawer.garant-title",
    defaultMessage: "В составе Гарант"
  },
  etTitle: {
    id: "dashboard.drawer.et-title",
    defaultMessage: "Гарант Тендер"
  }
});

const styles = theme => ({
  root: {
    position: "relative"
  },
  drawer: {
    flexShrink: 0,
    width: theme.spacing(30)
  },
  drawerPaper: {
    overflow: "hidden",
    maxWidth: theme.spacing(30),
    display: "flex",
    paddingTop: theme.spacing(9),
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(1)
    }
  },
  garantLogo: {
    width: 185,
    marginLeft: theme.spacing(),
    marginTop: theme.spacing(1)
  },
  infoBlock: {
    marginTop: "auto",
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  favorite: {
    display: "block",
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(1.5),
    fontSize: "1.1em",
    textTransform: "uppercase"
  },
  phone: {
    color: theme.palette.grey["600"],
    marginLeft: theme.spacing(1),
    fontSize: "1.1em",
    marginBottom: theme.spacing(1),
    display: "flex",
    alignItems: "center"
  },
  phoneIcon: {
    marginRight: theme.spacing(1)
  },
  commonFilters: {
    minHeight: "200px"
  },
  parentBox: {
    display: "flex",
    height: "100%",
    flexDirection: "column"
  },
  topBox: {
    height: "calc(100% - 147px)"
  },
  bottomBox: {
    minHeight: "147px",
    position: "relative"
  },
  gradient: {
    position: "absolute",
    left: "0px",
    top: "-55px",
    height: "55px",
    width: "100%",
    background: "linear-gradient(transparent, #fff)",
    pointerEvents: "none"
  }
});

export class Drawer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    fetchFilters: PropTypes.func.isRequired,
    showFilter: PropTypes.func.isRequired,
    width: PropTypes.string.isRequired
  };

  actionMenu = item => {
    const { url, action } = item;
    const { toggleDrawerOpen, width } = this.props;

    if (width === "sm" || width === "xs") {
      toggleDrawerOpen();
    }

    if (url) {
      const { push } = this.props;
      push(url);
    }

    if (action) {
      const actionHandler = this.props[action];
      actionHandler && actionHandler();
    }
  };

  handleDrawerToggle = () => {
    const { toggleDrawerOpen } = this.props;

    toggleDrawerOpen();
  };

  handleFilterSelect = filter => {
    const { toggleDrawerOpen, width } = this.props;
    if (width === "sm" || width === "xs") {
      toggleDrawerOpen();
    }
  };

  handleScroll = () => {
    const gradient = document.getElementById("gradient");
    const scrollElement = document
      .getElementById("scroll")
      .SimpleBar.getScrollElement();
    if (
      scrollElement.scrollHeight - scrollElement.scrollTop ===
      scrollElement.clientHeight
    ) {
      gradient.style.display = "none";
    } else {
      gradient.style.display = "block";
    }
  };

  renderDrawer() {
    const { classes, menu, roles, section, filter } = this.props;
    const { formatMessage } = this.props.intl;
    const selected = (!filter || !filter.persisted) && section;

    return (
      <div className={classes.parentBox}>
        <div className={classes.topBox}>
          <SimpleBar
            style={{ width: 240, height: "100%" }}
            id="scroll"
            onScroll={this.handleScroll}
          >
            <Menu items={menu} selected={selected} action={this.actionMenu} />
            <Divider />
            <FiltersContainer onSelect={this.handleFilterSelect} />
            {roles.admin && (
              <Fragment>
                <Divider />
                <FiltersUsersContainer onSelect={this.handleFilterSelect} />
              </Fragment>
            )}
          </SimpleBar>
        </div>
        <div className={classes.bottomBox}>
          <div className={classes.gradient} id="gradient"></div>
          <div className={classes.infoBlock}>
            <Link href={"tel:88003338888"} className={classes.phone}>
              <PhoneIcon className={classes.phoneIcon} /> 8 800 333 8888
            </Link>
            <Link
              href={"https://www.garantexpress.ru/#commentsWrp"}
              target={"_blank"}
              className={classes.favorite}
            >
              оставить отзыв
            </Link>
            <Link href={"https://www.garantexpress.ru/"} target={"_blank"}>
              <img
                src={garantLogo}
                alt={formatMessage(messages.garantTitle)}
                className={classes.garantLogo}
                width="185"
              />
            </Link>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { classes, open, shared } = this.props;

    return (
      <div className={classes.root}>
        <Hidden lgUp implementation="css">
          <MuiDrawer
            variant="temporary"
            className={classes.drawer}
            classes={{
              paper: classNames(
                classes.drawerPaper,
                !open && classes.drawerPaperClose,
                this.isEdge && classes.disableScroll
              )
            }}
            open={open}
            onClose={this.handleDrawerToggle}
          >
            {this.renderDrawer()}
          </MuiDrawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <MuiDrawer
            variant="permanent"
            className={classes.drawer}
            classes={{
              paper: classNames(
                classes.drawerPaper,
                this.isEdge && classes.disableScroll
              )
            }}
            open
          >
            {this.renderDrawer()}
          </MuiDrawer>
        </Hidden>
        {!shared && <SirvicesNotice />}
      </div>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(Drawer)));
