import { createSelector } from "reselect";
import { getLocationPathname } from "../../../services/Router/RouterSelectors";

export const getOpen = state => state.dashboard.drawer.open;

export const getSection = createSelector(
  getLocationPathname,
  locationPathname =>
    locationPathname === "/" ? "root" : locationPathname.match(/^\/([^/]+)/)[1]
);
