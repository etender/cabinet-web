import { createAction } from "redux-actions";

export const toggleDrawerOpen = createAction(
  "DASHBOARD_DRAWER_OPEN_TOGGLE",
  () => {}
);
