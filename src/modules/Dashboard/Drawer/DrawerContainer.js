import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import HomeIcon from "@material-ui/icons/Home";
import FavoriteIcon from "@material-ui/icons/Favorite";
import AnalyticsIcon from "@material-ui/icons/Assessment";
import { defineMessages } from "react-intl.macro";

import Drawer from "./Drawer";
import { getShared } from "../../Shared/SharedSelectors";
import { getOpen, getSection } from "./DrawerSelectors";
import { getCurrentFilter } from "../Filter/FilterSelectors";
import { buildFilter, showFilter } from "../Filter/FilterActions";
import { fetchFilters } from "../Filters/FiltersActions";
import { openSign, openGuarantee } from "../Menu/MenuActions";
import { toggleDrawerOpen } from "./DrawerActions";
import { getCurrentUserRoles } from "../../../services/Devise/DeviseSelectors";
import otherServicesIcon from "../../../images/icons/services.png";

import {
  DASHBOARD_PATH,
  FAVORITES_PATH,
  ANALYTICS_FILTER_PATH,
  SERVICES_PATH
} from "../DashboardConstants";
// import otherServices from "../../../images/icons/services.svg";

const messages = defineMessages({
  home: {
    id: "dashboard.drawer-container.home",
    defaultMessage: "Главная"
  },
  favorites: {
    id: "dashboard.drawer-container.favorites",
    defaultMessage: "Закупки на контроле"
  },
  analytics: {
    id: "dashboard.drawer-container.analytics",
    defaultMessage: "Аналитика"
  },
  sirvices: {
    id: "dashboard.drawer-container.sirvices",
    defaultMessage: "Другие сервисы"
  }
});

const DrawerContainer = props => <Drawer {...props} />;

const mapStateToProps = state => {
  return {
    open: getOpen(state),
    section: getSection(state),
    filters: state.dashboard.filters.items,
    publicFilters: state.dashboard.filters.publicItems,
    filter: getCurrentFilter(state),
    roles: getCurrentUserRoles(state),
    shared: getShared(state),
    menu: [
      {
        id: "root",
        name: messages.home,
        icon: HomeIcon,
        url: DASHBOARD_PATH,
        action: "buildFilter"
      },
      {
        id: "favorites",
        name: messages.favorites,
        icon: FavoriteIcon,
        url: FAVORITES_PATH
      },
      {
        id: "analytics",
        name: messages.analytics,
        icon: AnalyticsIcon,
        url: ANALYTICS_FILTER_PATH,
        newFeature: true
      },
      {
        id: "sirvices",
        name: messages.sirvices,
        avatar: otherServicesIcon,
        url: SERVICES_PATH,
        visibleWidth: "sm"
      }
    ]
  };
};

export default connect(mapStateToProps, {
  push,
  fetchFilters,
  toggleDrawerOpen,
  showFilter,
  openSign,
  openGuarantee,
  buildFilter
})(DrawerContainer);
