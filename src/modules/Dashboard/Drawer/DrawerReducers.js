import { handleActions } from "redux-actions";

import { toggleDrawerOpen } from "./DrawerActions";

const initialState = {
  open: false
};

export default handleActions(
  {
    [toggleDrawerOpen]: (state, action) => ({
      ...state,
      open: !state.open
    })
  },
  initialState
);
