import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { injectIntl } from "react-intl";

import InfiniteList from "../../common/InfiniteList/InfiniteList";
import FilterDescriptionContainer from "../Dashboard/FilterDescription/FilterDescriptionContainer";
import TenderDialogContainer from "../Tender/TenderDialog/TenderDialogContainer";

const styles = theme => ({
  container: {
    padding: theme.spacing(2),
    width: "100%",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    }
  },

  sharedContainer: {
    padding: theme.spacing(2),
    width: "100%"
  }
});

export class Tenders extends Component {
  static propTypes = {
    // Require to update InfinteList. It's needed to reattache scroll handler
    tenders: PropTypes.array.isRequired,
    filterContainer: PropTypes.any.isRequired,
    listContainer: PropTypes.any.isRequired,
    fetchTenders: PropTypes.func.isRequired,
    fetchMoreTenders: PropTypes.func.isRequired,
    hasMore: PropTypes.bool.isRequired,
    exportable: PropTypes.bool
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { fetchTenders, filter, filterQuery, outdated } = this.props;
    const {
      filter: prevFilter,
      filterQuery: prevFilterQuery,
      outdated: prevOutdated
    } = prevProps;

    if (
      (outdated !== prevOutdated && outdated) ||
      (filter && filter !== prevFilter) ||
      (filterQuery && filterQuery !== prevFilterQuery)
    ) {
      fetchTenders({ search: filter, ...filterQuery });
    }
  }

  componentDidMount() {
    const { fetchTenders, filter, filterQuery } = this.props;
    fetchTenders({ search: filter, ...filterQuery });
  }

  handleLoadMore = page => {
    const { fetchMoreTenders, filter, filterQuery } = this.props;
    fetchMoreTenders({ search: filter, page, ...filterQuery });
  };

  render() {
    const {
      classes,
      exportable,
      hasMore,
      shared,
      filterContainer: FilterContainer,
      listContainer: ListContainer
    } = this.props;

    return (
      <Fragment>
        <TenderDialogContainer />
        <InfiniteList
          classes={{
            container: shared ? classes.sharedContainer : classes.container
          }}
          loadMore={this.handleLoadMore}
          hasMore={hasMore}
        >
          <FilterDescriptionContainer />
          <FilterContainer />
          <ListContainer exportable={exportable} />
        </InfiniteList>
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(Tenders));
