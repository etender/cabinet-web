import React from "react";
import { connect } from "react-redux";

import { getNormalizeCurrentFilter } from "../../Dashboard/Filter/FilterSelectors";
import { getFilterQuery } from "../TendersList/TendersListSelectors";
import ExportButtonContainer from "../../../common/ExportButton/ExportButtonContainer";

const TendersExportContainer = props => (
  <ExportButtonContainer {...props} name="tendersExport" />
);

const mapStateToProps = state => {
  return {
    query: {
      search: getNormalizeCurrentFilter(state),
      ...getFilterQuery(state)
    }
  };
};

export default connect(mapStateToProps)(TendersExportContainer);
