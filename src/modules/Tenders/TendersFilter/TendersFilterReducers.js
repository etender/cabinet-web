import { handleActions } from "redux-actions";

import { applyFilter } from "./TendersFilterActions";

const initialState = {
  price: "none",
  date: "published_at desc",
  period: "all",
  status: "none"
};

export default handleActions(
  {
    [applyFilter]: (state, action) => ({ ...state, ...action.payload })
  },
  initialState
);
