import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../TendersFilterActions";
import { getFilter } from "../TendersFilterSelectors";
import TendersFilterPeriod from "./TendersFilterPeriod";

const TendersFilterPeriodContainer = props => (
  <TendersFilterPeriod {...props} />
);

const mapStateToProps = state => {
  return {
    value: getFilter(state).period,
    startedAt: getFilter(state).startedAt,
    endedAt: getFilter(state).endedAt
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(TendersFilterPeriodContainer);
