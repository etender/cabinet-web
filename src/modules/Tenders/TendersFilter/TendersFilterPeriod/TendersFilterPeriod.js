import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import {
  MenuItem,
  MenuList,
  Grid,
  Divider,
  withStyles
} from "@material-ui/core";
import { DatePicker } from "@material-ui/pickers";
import { defineMessages } from "react-intl.macro";
import { FormattedDate, injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  all: {
    id: "tenders.tenders-filter.all",
    defaultMessage: "За весь период"
  },
  today: {
    id: "tenders.tenders-filter.today",
    defaultMessage: "Текущие сутки"
  },
  last7: {
    id: "tenders.tenders-filter.last7",
    defaultMessage: "Последние 7 дней"
  },
  last30: {
    id: "tenders.tenders-filter.last30",
    defaultMessage: "Последние 30 дней"
  },
  month: {
    id: "tenders.tenders-filter.month",
    defaultMessage: "Текущий месяц "
  },
  startedAt: {
    id: "tenders.tenders-filter.started-at",
    defaultMessage: "с"
  },
  endedAt: {
    id: "tenders.tenders-filter.ended-at",
    defaultMessage: "по"
  }
});

const styles = theme => ({
  popover: {
    padding: theme.spacing(2)
  },
  menuItem: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2)
  },
  container: {
    width: 845
  }
});

export class TendersFilterPeriod extends Component {
  static propTypes = {
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.state = this.createState();
    this.menu = ["all", "today", "last7", "last30", "month"];
  }

  componentDidUpdate(prevProps, prevState) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState(this.createState());
    }
  }

  createState() {
    const { value } = this.props;
    const period = this.getPeriod(value);
    const startedAt = period.startedAt || moment().subtract(1, "month");
    const endedAt = period.endedAt || moment();

    return {
      value,
      startedAt,
      endedAt,
      minDate: startedAt,
      maxDate: endedAt
    };
  }

  getPeriod(value) {
    if (value === "today") {
      return {
        startedAt: moment().subtract(1, "d"),
        endedAt: moment()
      };
    }

    if (value === "last7") {
      return {
        startedAt: moment().subtract(7, "d"),
        endedAt: moment()
      };
    }

    if (value === "last30") {
      return {
        startedAt: moment().subtract(30, "d"),
        endedAt: moment()
      };
    }

    if (value === "month") {
      return {
        startedAt: moment().startOf("month"),
        endedAt: moment().endOf("month")
      };
    }

    return {
      startedAt: null,
      endedAt: null
    };
  }

  handleClose = event => {
    const { applyFilter } = this.props;
    const { startedAt, endedAt, value } = this.state;

    if (value === "none") {
      applyFilter({ period: "none", startedAt, endedAt });
    }
  };

  handleMenuClick = item => {
    const { applyFilter } = this.props;
    applyFilter({ period: item, ...this.getPeriod(item) });
  };

  handleStartChange = value => {
    const { startedAt } = this.state;
    if (startedAt !== value) {
      this.setState({ value: "none", startedAt: value, minDate: value });
    }
  };

  handleEndChange = value => {
    const { endedAt } = this.state;
    if (endedAt !== value) {
      this.setState({ value: "none", endedAt: value, maxDate: value });
    }
  };

  render() {
    const { classes, intl, value: period } = this.props;
    const { formatMessage } = intl;
    const { value, startedAt, endedAt, minDate, maxDate } = this.state;

    return (
      <Dropdown
        onClose={this.handleClose}
        classes={{ popover: classes.popover }}
        title={
          period === "none" ? (
            <span>
              <FormattedDate month="short" day="numeric" value={startedAt} />
              -
              <FormattedDate month="short" day="numeric" value={endedAt} />
            </span>
          ) : (
            formatMessage(messages[period])
          )
        }
      >
        {({ popoverClose }) => (
          <Grid container spacing={2} className={classes.container}>
            <Grid item>
              <MenuList>
                {this.menu.map(item => (
                  <MenuItem
                    key={item}
                    value={item}
                    selected={item === value}
                    className={classes.menuItem}
                    onClick={() => {
                      popoverClose();
                      this.handleMenuClick(item);
                    }}
                  >
                    {formatMessage(messages[item])}
                  </MenuItem>
                ))}
              </MenuList>
            </Grid>
            <Grid item>
              <Divider orientation="vertical" />
            </Grid>
            <Grid item xs sm>
              <DatePicker
                variant="static"
                value={startedAt}
                maxDate={maxDate}
                disableFuture
                disableToolbar
                onChange={this.handleStartChange}
              />
            </Grid>
            <Grid item xs={6} sm>
              <DatePicker
                variant="static"
                value={endedAt}
                minDate={minDate}
                disableToolbar
                onChange={this.handleEndChange}
              />
            </Grid>
          </Grid>
        )}
      </Dropdown>
    );
  }
}

export default injectIntl(withStyles(styles)(TendersFilterPeriod));
