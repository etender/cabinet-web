import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  ArrowUpward as ArrowUpwardIcon,
  ArrowDownward as ArrowDownwardIcon
} from "@material-ui/icons";
import { ListItemIcon, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  dateField: {
    id: "tenders.tenders-filter.date-field",
    defaultMessage: "Дата"
  },
  publishedAtField: {
    id: "tenders.tenders-filter.published-at-field",
    defaultMessage: "Дата публикации"
  },
  endAtField: {
    id: "tenders.tenders-filter.end-at-field",
    defaultMessage: "Дата подачи заявок"
  }
});

const styles = theme => ({
  menuItemIcon: {
    minWidth: theme.spacing(3),
    marginRight: 0,
    marginLeft: "auto",
    justifyContent: "flex-end"
  },
  menuItemIconSvg: {
    fontSize: "1em"
  }
});

export class TendersFilterDate extends Component {
  static propTypes = {
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    const { intl, classes } = this.props;
    const { formatMessage } = intl;

    this.menu = [
      {
        id: "published_at asc",
        name: (
          <Fragment>
            {formatMessage(messages.publishedAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowUpwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "published_at desc",
        name: (
          <Fragment>
            {formatMessage(messages.publishedAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowDownwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "end_at asc",
        name: (
          <Fragment>
            {formatMessage(messages.endAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowUpwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "end_at desc",
        name: (
          <Fragment>
            {formatMessage(messages.endAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowDownwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      }
    ];
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ date: item.id, price: "none" });
  };

  render() {
    const { intl, value } = this.props;
    const { formatMessage } = intl;

    return (
      <Dropdown
        title={formatMessage(messages.dateField)}
        value={value}
        menu={this.menu}
        onChange={this.handleChange}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(TendersFilterDate));
