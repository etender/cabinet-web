import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../TendersFilterActions";
import { getFilter } from "../TendersFilterSelectors";
import TendersFilterDate from "./TendersFilterDate";

const TendersFilterDateContainer = props => <TendersFilterDate {...props} />;

const mapStateToProps = state => {
  return {
    value: getFilter(state).date
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(TendersFilterDateContainer);
