import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../TendersFilterActions";
import { getFilter } from "../TendersFilterSelectors";
import { getShared } from "../../../Shared/SharedSelectors";
import TendersFilterStatus from "./TendersFilterStatus";

const TendersFilterStatusContainer = props => (
  <TendersFilterStatus {...props} />
);

const mapStateToProps = state => {
  return {
    value: getFilter(state).status,
    shared: getShared(state)
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(TendersFilterStatusContainer);
