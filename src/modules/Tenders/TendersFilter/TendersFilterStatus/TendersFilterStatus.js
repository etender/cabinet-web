import React, { Component } from "react";
import PropTypes from "prop-types";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  statusField: {
    id: "tenders.tenders-filter.state-field",
    defaultMessage: "Любой статус"
  },
  new: {
    id: "tenders.tenders-filter.new",
    defaultMessage: "Только новые"
  },
  winner: {
    id: "tenders.tenders-filter.winner",
    defaultMessage: "С победителями"
  },
  canceled: {
    id: "tenders.tenders-filter.canceled",
    defaultMessage: "Отмененные"
  }
});

export class TendersFilterStatus extends Component {
  static propTypes = {
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    const { intl, shared } = this.props;
    const { formatMessage } = intl;

    this.menu = [
      {
        id: "none",
        name: formatMessage(messages.statusField)
      },
      {
        id: "new",
        name: formatMessage(messages.new)
      },
      {
        id: "winner",
        name: formatMessage(messages.winner)
      },
      {
        id: "canceled",
        name: formatMessage(messages.canceled)
      }
    ];

    if (shared) {
      this.menu = [
        {
          id: "none",
          name: formatMessage(messages.statusField)
        },
        {
          id: "new",
          name: formatMessage(messages.new)
        }
      ];
    }
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ status: item.id });
  };

  render() {
    const { intl, value } = this.props;
    const { formatMessage } = intl;

    return (
      <Dropdown
        title={formatMessage(messages.statusField)}
        value={value}
        menu={this.menu}
        onChange={this.handleChange}
      />
    );
  }
}

export default injectIntl(TendersFilterStatus);
