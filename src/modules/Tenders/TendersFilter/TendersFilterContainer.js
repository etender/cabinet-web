import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "./TendersFilterActions";
import { getFilter } from "./TendersFilterSelectors";
import TendersFilter from "./TendersFilter";

const TendersFilterContainer = props => <TendersFilter {...props} />;

const mapStateToProps = state => {
  return {
    ...getFilter(state)
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(TendersFilterContainer);
