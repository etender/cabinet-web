import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  ArrowUpward as ArrowUpwardIcon,
  ArrowDownward as ArrowDownwardIcon
} from "@material-ui/icons";
import { ListItemIcon, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  priceField: {
    id: "tenders.tenders-filter.price-field",
    defaultMessage: "Цена"
  }
});

const styles = theme => ({
  menuItemIcon: {
    minWidth: theme.spacing(3),
    marginRight: 0,
    marginLeft: "auto",
    justifyContent: "flex-end"
  },
  menuItemIconSvg: {
    fontSize: "1em"
  }
});

export class TendersFilterPrice extends Component {
  static propTypes = {
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    const { intl, classes } = this.props;
    const { formatMessage } = intl;

    this.menu = [
      {
        id: "price asc",
        name: (
          <Fragment>
            {formatMessage(messages.priceField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowUpwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "price desc",
        name: (
          <Fragment>
            {formatMessage(messages.priceField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowDownwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      }
    ];
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ price: item.id, date: "none" });
  };

  render() {
    const { intl, value } = this.props;
    const { formatMessage } = intl;

    return (
      <Dropdown
        title={formatMessage(messages.priceField)}
        value={value}
        menu={this.menu}
        onChange={this.handleChange}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(TendersFilterPrice));
