import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../TendersFilterActions";
import { getFilter } from "../TendersFilterSelectors";
import TendersFilterPrice from "./TendersFilterPrice";

const TendersFilterPriceContainer = props => <TendersFilterPrice {...props} />;

const mapStateToProps = state => {
  return {
    value: getFilter(state).price
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(TendersFilterPriceContainer);
