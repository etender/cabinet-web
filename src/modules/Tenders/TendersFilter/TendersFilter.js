import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import TendersFilterPriceContainer from "./TendersFilterPrice/TendersFilterPriceContainer";
import TendersFilterDateContainer from "./TendersFilterDate/TendersFilterDateContainer";
import TendersFilterPeriodContainer from "./TendersFilterPeriod/TendersFilterPeriodContainer";
import TendersFilterStatusContainer from "./TendersFilterStatus/TendersFilterStatusContainer";

const styles = theme => ({
  root: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(1)
  },
  formControl: {
    marginBottom: theme.spacing(3),
    minWidth: 100
  }
});

export class TendersFilter extends Component {
  static propTypes = {
    price: PropTypes.string,
    applyFilter: PropTypes.func.isRequired
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <TendersFilterPriceContainer />
        <TendersFilterDateContainer />
        <TendersFilterPeriodContainer />
        <TendersFilterStatusContainer />
      </div>
    );
  }
}

export default withStyles(styles)(TendersFilter);
