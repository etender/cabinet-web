import { all, fork } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";

import listSagas from "./TendersList/TendersListSagas";

function* tendersSagas() {
  yield all([fork(listSagas)]);
}

sagaMiddleware.run(tendersSagas);
