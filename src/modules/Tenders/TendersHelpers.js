import { handleActions } from "redux-actions";

import {
  successApiType,
  failureApiType,
  requestApiType
} from "../../services/Api/ApiHelpers";

export const createExportReducer = action => {
  const initialState = {
    loading: false
  };

  return handleActions(
    {
      [requestApiType(action)]: (state, action) => ({
        ...state,
        loading: true
      }),

      [successApiType(action)]: (state, action) => ({
        ...state,
        loading: false
      }),

      [failureApiType(action)]: (state, action) => ({
        ...state,
        loading: false
      })
    },
    initialState
  );
};
