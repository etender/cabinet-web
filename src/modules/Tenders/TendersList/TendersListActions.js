import { createAction } from "redux-actions";

export const fetchTenders = createAction(
  "TENDERS_FETCH",
  query => query,
  () => ({ entity: "tenders" })
);

export const fetchMoreTenders = createAction(
  "MORE_TENDERS_FETCH",
  query => query,
  () => ({ entity: "tenders" })
);
