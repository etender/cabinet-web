import { all, takeLatest } from "redux-saga/effects";

import { fetchTenders, fetchMoreTenders } from "./TendersListActions";
import { fetchEntitiesSaga } from "../../../services/Entities/EntitiesSagas";

export default function* tendersListSagas() {
  yield all([
    takeLatest(fetchTenders, fetchEntitiesSaga),
    takeLatest(fetchMoreTenders, fetchEntitiesSaga)
  ]);
}
