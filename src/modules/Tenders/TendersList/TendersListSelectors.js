import { createSelector } from "reselect";
import { getFilter } from "../TendersFilter/TendersFilterSelectors";

const getTendersList = state => state.tenders.list;

export const getTenders = createSelector(getTendersList, list => list.items);

export const getTotalTenders = createSelector(
  getTendersList,
  list => list.total || 0
);

export const getHasMoreTenders = createSelector(
  getTendersList,
  list => list.hasMore
);

export const getFilterQuery = createSelector(getFilter, filter => ({
  sort: filter.price !== "none" ? filter.price : filter.date,
  period:
    filter.startedAt || filter.endedAt
      ? {
          startedAt: filter.startedAt.toString(),
          endedAt: filter.endedAt.toString()
        }
      : null,
  status: filter.status !== "none" ? filter.status : null
}));

export const getTendersLoading = state => state.tenders.list.loading;
