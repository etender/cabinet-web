import React from "react";
import { connect } from "react-redux";

import TendersList from "./TendersList";
import {
  getTenders,
  getTotalTenders,
  getTendersLoading
} from "./TendersListSelectors";
import TendersExportContainer from "../TendersExport/TendersExportContainer";

const TendersListContainer = props => (
  <TendersList {...props} exportContainer={TendersExportContainer} />
);

const mapStateToProps = state => {
  return {
    items: getTenders(state),
    total: getTotalTenders(state),
    loading: getTendersLoading(state)
  };
};

export default connect(mapStateToProps, {})(TendersListContainer);
