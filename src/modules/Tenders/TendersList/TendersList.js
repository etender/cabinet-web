import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Typography, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";
import { isEmpty } from "../../../utils/CoreUtils";
import TenderCardContainer from "../TenderCard/TenderCardContainer";
import TenderCardSkeleton from "../TenderCardSkeleton/TenderCardSkeleton";
import { Skeleton } from "@material-ui/lab";

const styles = theme => ({
  totalBlock: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    minHeight: "60px"
  },
  totalSkeleton: {
    display: "inline-block"
  }
});

export class Tenders extends Component {
  static propTypes = {
    scroller: PropTypes.object,
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    total: PropTypes.number.isRequired,
    exportable: PropTypes.bool
  };

  renderTenders() {
    const { items } = this.props;
    return items.map(tender => (
      <TenderCardContainer key={tender.originId} tender={tender} />
    ));
  }

  handleExportToExcel = () => {
    const { exportToExcel } = this.props;
    exportToExcel();
  };

  render() {
    const {
      classes,
      items,
      total,
      loading,
      exportContainer: ExportContainer,
      intl
    } = this.props;
    const { formatNumber } = intl;

    return (
      <Fragment>
        <div className={classes.totalBlock}>
          <Typography className={classes.total}>
            <FormattedMessage
              id="tenders.tenders.total"
              defaultMessage="Найдено процедур"
            />
            &nbsp;
            {isEmpty(items) && loading ? (
              <Skeleton
                width={70}
                height={23}
                className={classes.totalSkeleton}
              />
            ) : (
              <strong>{formatNumber(total, { style: "decimal" })}</strong>
            )}
          </Typography>
          {isEmpty(items) && loading ? (
            <Skeleton
              width={133}
              height={33}
              className={classes.exportSkeleton}
            />
          ) : (
            <ExportContainer />
          )}
        </div>

        {isEmpty(items) && loading ? (
          <TenderCardSkeleton />
        ) : (
          this.renderTenders()
        )}
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(Tenders));
