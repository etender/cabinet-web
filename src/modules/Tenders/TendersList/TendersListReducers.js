import { handleActions } from "redux-actions";

import { successApiType } from "../../../services/Api/ApiHelpers";
import {
  addFavorites,
  removeFavorites
} from "../../Favorites/FavoritesActions";
import { changeState } from "../../Favorites/StateChange/StateChangeActions";
import { changeUser } from "../../Favorites/UserChange/UserChangeActions";
import { fetchTender } from "../../Tender/TenderActions";
import { fetchTenders, fetchMoreTenders } from "./TendersListActions";

const initialState = {
  items: [],
  hasMore: false,
  total: null
};

export default handleActions(
  {
    [successApiType(fetchTender)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.id ? { ...item, ...action.payload } : item
      )
    }),

    [fetchTenders]: (state, action) => ({
      ...state,
      items: [],
      total: null,
      loading: true
    }),

    [successApiType(fetchTenders)]: (state, action) => ({
      ...state,
      items: action.payload.items,
      hasMore: action.payload.items.length === 25,
      total: action.payload.meta.total,
      loading: false
    }),

    [successApiType(fetchMoreTenders)]: (state, action) => ({
      ...state,
      items: [...state.items, ...action.payload.items],
      hasMore: action.payload.items.length > 0,
      total: action.payload.meta.total
    }),

    [successApiType(addFavorites)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.id ? { ...item, ...action.payload } : item
      )
    }),

    [successApiType(removeFavorites)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload
          ? {
              ...item,
              favorites: false,
              user: null,
              state: null,
              isOwner: false
            }
          : item
      )
    }),

    [successApiType(changeState)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.favoriteId
          ? { ...item, state: action.payload.id }
          : item
      )
    }),

    [successApiType(changeUser)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.favoriteId
          ? { ...item, user: action.payload.user }
          : item
      )
    })
  },
  initialState
);
