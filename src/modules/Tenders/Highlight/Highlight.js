import React, { Component } from "react";
import { Typography, Link, withStyles, Grid } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";

import { isEmpty } from "../../../utils/CoreUtils";
import Collapse from "../../../common/Collapse/Collapse";

const styles = theme => ({
  root: {
    overflow: "hidden",
    "& em": {
      backgroundColor: "#dfef64",
      fontStyle: "normal",
      padding: "0 3px"
    }
  },
  documentsHighlight: {
    wordBreak: "break-all"
  },
  caption: {
    fontWeight: "600",
    marginRight: theme.spacing(1)
  },
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1)
  },
  grayLabel: {
    color: "#757575",
    fontSize: "0.75rem",
    verticalAlign: "middle",
    display: "inline-flex",
    lineHeight: "20.6px"
  }
});

export class Highlight extends Component {
  render() {
    const { classes, tender } = this.props;
    const { highlight, documentsHighlight, hasPaymentsHighlight } = tender;
    const {
      lotsCustomerRequirementsDeliveryPlace,
      contactPostAddress,
      customerPostAddress,
      lotItemsName,
      lotItemsNameExact,
      lotsName,
      lotsNameExact,
      region
    } = highlight || {};
    const highlightKeys = highlight ? Object.keys(highlight) : [];
    const highlightKeysHasName = highlightKeys.indexOf("name") >= 0;

    return (
      <>
        {((highlight && highlightKeysHasName && highlightKeys.length > 1) ||
          (!highlightKeysHasName && highlightKeys.length > 0) ||
          (documentsHighlight && documentsHighlight.length > 0) ||
          hasPaymentsHighlight) && (
          <div className={classes.root}>
            <Typography variant="body1" className={classes.title}>
              <FormattedMessage
                id="tenders.highlight.finding"
                defaultMessage="Найдено"
              />
              :
            </Typography>
            <Grid container spacing={1}>
              {region && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.region"
                        defaultMessage="В регионе"
                      />
                      :
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: region.join("; ")
                        }}
                      />
                    </Typography>
                  </Grid>
                </>
              )}

              {lotsCustomerRequirementsDeliveryPlace && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.delivery-place"
                        defaultMessage="В месте поставки"
                      />
                      :
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: lotsCustomerRequirementsDeliveryPlace.join(
                            "; "
                          )
                        }}
                      />
                    </Typography>
                  </Grid>
                </>
              )}

              {(contactPostAddress || customerPostAddress) && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.contact-post-address"
                        defaultMessage="В почтовом адресе"
                      />
                      :
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography>
                      {customerPostAddress && (
                        <span
                          dangerouslySetInnerHTML={{
                            __html: customerPostAddress.join("; ")
                          }}
                        />
                      )}
                      {!customerPostAddress && contactPostAddress && (
                        <span
                          dangerouslySetInnerHTML={{
                            __html: contactPostAddress.join("; ")
                          }}
                        />
                      )}
                    </Typography>
                  </Grid>
                </>
              )}

              {(lotsName || lotsNameExact) && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.lots-name"
                        defaultMessage="В названии лота"
                      />
                      :
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography>
                      {lotsName && (
                        <span dangerouslySetInnerHTML={{ __html: lotsName }} />
                      )}
                      {lotsNameExact && (
                        <span
                          dangerouslySetInnerHTML={{
                            __html: lotsNameExact.join("; ")
                          }}
                        />
                      )}
                    </Typography>
                  </Grid>
                </>
              )}

              {(lotItemsName || lotItemsNameExact) && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.lots-lot-items-name"
                        defaultMessage="В товарах"
                      />
                      :
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography>
                      {lotItemsName && (
                        <span
                          dangerouslySetInnerHTML={{ __html: lotItemsName }}
                        />
                      )}
                      {lotItemsNameExact && (
                        <span
                          dangerouslySetInnerHTML={{
                            __html: lotItemsNameExact.join("; ")
                          }}
                        />
                      )}
                    </Typography>
                  </Grid>
                </>
              )}

              {!isEmpty(documentsHighlight) && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.document"
                        defaultMessage="В документах"
                      />
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    {documentsHighlight.map((item, index) => (
                      <div key={index}>
                        <Collapse collapsedHeight={80}>
                          <Typography>
                            <Link
                              href={item.attachmentUrl}
                              className={classes.documentName}
                            >
                              {item.attachmentFilename}
                            </Link>
                          </Typography>
                          <Typography>
                            {item.attachmentContent && (
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: item.attachmentContent
                                }}
                              />
                            )}
                            {item.attachmentContentExact && (
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: item.attachmentContentExact
                                }}
                              />
                            )}
                          </Typography>
                        </Collapse>
                      </div>
                    ))}
                  </Grid>
                </>
              )}

              {!isEmpty(hasPaymentsHighlight) && (
                <>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.highlight.has-payment"
                        defaultMessage="С авансом"
                      />
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    {hasPaymentsHighlight.map((item, index) => {
                      return (
                        <div key={index} className={classes.highlihjtItem}>
                          <Typography>
                            <Link href={item.attachmentUrl}>
                              {item.attachmentFilename}
                            </Link>
                            <span
                              dangerouslySetInnerHTML={{
                                __html: item.attachmentContent
                              }}
                            />
                          </Typography>
                        </div>
                      );
                    })}
                  </Grid>
                </>
              )}
            </Grid>
          </div>
        )}
      </>
    );
  }
}

export default withStyles(styles)(Highlight);
