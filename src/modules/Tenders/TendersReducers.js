import { combineReducers } from "redux";

import list from "./TendersList/TendersListReducers";
import filter from "./TendersFilter/TendersFilterReducers";

export default combineReducers({
  list,
  filter
});
