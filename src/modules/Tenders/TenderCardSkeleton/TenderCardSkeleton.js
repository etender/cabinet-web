import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardContent,
  CardActions,
  Grid,
  withStyles
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { injectIntl } from "react-intl";

const styles = theme => ({
  cardContent: {
    padding: theme.spacing(3),
    paddingBottom: theme.spacing(0.5)
  },
  favorite: {
    marginLeft: "auto"
  },
  price: {
    marginLeft: "auto",
    [theme.breakpoints.down("sm")]: {
      marginRight: "auto"
    }
  }
});

class TenderCardSkeleton extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <Card>
        <CardContent className={classes.cardContent}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={3}>
              <Skeleton width="30%" height="30%" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Skeleton width="70%" />
            </Grid>
            <Grid item xs={12} sm={3} className={classes.price}>
              <Grid container spacing={1}>
                <Grid item xs={12} sm={6} />
                <Grid item xs={12} sm={6}>
                  <Skeleton height="80%" />
                </Grid>
                <Grid item xs={12} sm={6} />
                <Grid item xs={12} sm={6}>
                  <Skeleton height="80%" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Skeleton width="50%" height="15%" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Skeleton width="100%" />
              <Skeleton width="80%" />
              <Skeleton width="90%" />
            </Grid>
            <Grid item xs={12} sm={3}>
              <Skeleton height="50%" width="20%" className={classes.price} />
            </Grid>
            <Grid item xs={12} sm={3}>
              <Skeleton width="20%" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Skeleton width="90%" height="30%" />
              <Skeleton width="50%" height="30%" />
              <Skeleton width="60%" height="30%" />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions className={classes.actions} disableSpacing>
          <Skeleton
            variant="rect"
            width={120}
            height={40}
            className={classes.favorite}
          />
        </CardActions>
      </Card>
    );
  }
}

export default injectIntl(withStyles(styles)(TenderCardSkeleton));
