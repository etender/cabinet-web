import React from "react";
import { connect } from "react-redux";

import "./TendersSagas";
import "../Favorites/FavoritesSagas";
import {
  fetchTenders,
  fetchMoreTenders
} from "./TendersList/TendersListActions";
import {
  getTenders,
  getHasMoreTenders,
  getFilterQuery
} from "./TendersList/TendersListSelectors";
import { getPathFilter } from "../Dashboard/Filter/FilterSelectors";
import { getShared } from "../Shared/SharedSelectors";
import Tenders from "./Tenders";
import TendersFilterContainer from "./TendersFilter/TendersFilterContainer";
import TendersListContainer from "./TendersList/TendersListContainer";

const TendersContainer = props => (
  <Tenders
    {...props}
    filterContainer={TendersFilterContainer}
    listContainer={TendersListContainer}
    exportable
  />
);

const mapStateToProps = state => {
  return {
    tenders: getTenders(state),
    hasMore: getHasMoreTenders(state),
    filter: getPathFilter(state),
    filterQuery: getFilterQuery(state),
    shared: getShared(state)
  };
};

export default connect(mapStateToProps, {
  fetchTenders,
  fetchMoreTenders
})(TendersContainer);
