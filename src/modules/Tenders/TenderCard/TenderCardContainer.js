import React from "react";
import { connect } from "react-redux";

import { getShared } from "../../Shared/SharedSelectors";
import { confirm } from "../../../common/ConfirmationDialog/ConfiramtionActions";
import TenderCard from "./TenderCard";
import { showTender, toggleTender } from "../../Tender/TenderActions";
import {
  addFavorites,
  removeFavorites
} from "../../Favorites/FavoritesActions";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";
import { canChangeTender, canAddTender } from "../../Tender/TenderSelectors";

const TenderCardContainer = props => <TenderCard {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { tender } = ownProps;

  return {
    user: getCurrentUser(state),
    shared: getShared(state),
    canChangeTender: canChangeTender(state, tender),
    canAddTender: canAddTender(state)
  };
};

export default connect(mapStateToProps, {
  showTender,
  toggleTender,
  addFavorites,
  confirm,
  removeFavorites
})(TenderCardContainer);
