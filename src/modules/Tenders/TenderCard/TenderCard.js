import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Card,
  CardContent,
  Button,
  Grid,
  Link,
  Typography,
  withStyles,
  Chip,
  Avatar
} from "@material-ui/core";
import { FormattedMessage, defineMessages } from "react-intl.macro";
import { FormattedDate, injectIntl } from "react-intl";
import withWidth from "@material-ui/core/withWidth";

import Highlight from "../Highlight/Highlight";
import { messages as stateMessages } from "../../Favorites/StateChange/StateChange";
import StateColorDot from "../../Favorites/StateColorDot/StateColorDot";
import { messages as favoriteDeleteMessages } from "../../Favorites/FavoriteDelete/FavoriteDeleteConstants";

import { Favorite, FavoriteBorder, AccessTime } from "@material-ui/icons";

export const messages = defineMessages({
  canceled: {
    id: "tenders.tender-card.canceled",
    defaultMessage: "Отменён"
  },
  submission_app: {
    id: "tenders.tender-card.submission-app",
    defaultMessage: "Подача заявок"
  },
  completed: {
    id: "tenders.tender-card.completed",
    defaultMessage: "Подача заявок завершена"
  },
  commission: {
    id: "tenders.tender-card.commission",
    defaultMessage: "Работа комиссии"
  },
  is_new: {
    id: "tenders.tender-card.is-new",
    defaultMessage: "Новый"
  },
  purchase_plan: {
    id: "tenders.tender-card.purchase-plan",
    defaultMessage: "Планируется"
  }
});

const styles = theme => ({
  card: {
    marginBottom: theme.spacing(3),
    marginRight: "auto",
    "&:hover": {
      boxShadow:
        "0px 1px 1px 0px rgba(0,0,0,0.2), 0px 2px 3px 1px rgba(0,0,0,0.14), 0px 2px 3px 2px rgba(0,0,0,0.12)"
    }
  },
  cardContentLeft: {
    padding: theme.spacing(3),
    height: "100%"
  },
  cardContentRight: {
    padding: theme.spacing(3),
    height: "100%",
    position: "relative",
    paddingBottom: "60px !important"
  },
  favorite: {
    position: "absolute",
    bottom: theme.spacing(3),
    right: theme.spacing(3),

    [theme.breakpoints.down("xs")]: {
      right: "auto"
    }
  },
  favoriteIcon: {
    marginLeft: "2px"
  },
  disabledFavorites: {
    opacity: "0.7"
  },
  customerName: {
    textTransform: "uppercase"
  },
  rightCardBlock: {
    textAlign: "right",
    borderLeft: "1px solid #e0e0e0",

    [theme.breakpoints.down("xs")]: {
      textAlign: "left"
    }
  },
  price: {
    marginBottom: theme.spacing(2)
  },
  priceValue: {
    fontSize: "1.3rem",
    fontWeight: "500",
    lineHeight: "27.8px"
  },
  priceFraction: {
    color: "#757575",
    fontSize: "16.8px",
    fontWeight: "500",
    lineHeight: "27.8px"
  },
  name: {
    textAlign: "left",
    marginTop: 0,
    marginBottom: theme.spacing(2),
    fontSize: "16px",
    lineHeight: "23px",

    "& em": {
      fontWeight: 600
    }
  },
  browsed: {
    color: "#609",
    "& p": {
      color: "#609"
    }
  },
  dateInfo: {
    textAlign: "right",
    overflowWrap: "break-word",
    marginBottom: theme.spacing(0.5),

    [theme.breakpoints.down("xs")]: {
      textAlign: "left"
    }
  },
  grayLabel: {
    color: "#757575",
    fontSize: "0.75rem",
    verticalAlign: "middle",
    display: "inline-flex",
    lineHeight: "20.6px"
  },
  grayLabelDate: {
    lineHeight: "21px"
  },
  grayLabelData: {
    textAlign: "right",
    whiteSpace: "nowrap",
    fontSize: "0.75rem",

    [theme.breakpoints.down("xs")]: {
      textAlign: "left"
    }
  },
  cardInfoChip: {
    marginRight: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5),
    backgroundColor: "#dbdbdb",
    borderRadius: "4px",
    height: "36px",
    fontSize: "12px",
    fontWeight: "500"
  },
  addMarginRight: {
    marginRight: theme.spacing(3) + "px !important"
  },
  stateIconAvatar: {
    backgroundColor: "transparent",
    width: "auto",
    height: "auto",
    marginLeft: "8px",
    marginRight: "-4px"
  },
  avatarChip: {
    width: "36px",
    height: "36px"
  },
  cardStatusChip: {
    borderRadius: "4px",
    marginRight: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5),
    height: "36px",
    fontSize: "12px",
    fontWeight: "500"
  },
  cardStatusChipDefault: {
    backgroundColor: "#dbdbdb"
  },
  cardStatusChipPP: {
    backgroundColor: "#00adec",
    color: "#ffffff",
    borderRadius: "4px",
    marginRight: theme.spacing(0.5),
    marginBottom: theme.spacing(0.5),
    height: "36px",
    fontSize: "12px",
    fontWeight: "500"
  },
  chipLabel: {
    paddingLeft: "16px",
    paddingRight: "16px",
    textOverflow: "unset"
  },
  titleInfoSection: {
    display: "inline-block",
    marginBottom: "20px"
  },
  titleInfoBlock: {
    display: "inline-block",
    marginRight: theme.spacing(3),
    marginBottom: theme.spacing(0.5),
    verticalAlign: "middle",
    lineHeight: "18px",
    fontWeight: "500"
  },
  titleInfoBlockLabel: {
    color: theme.palette.grey["600"],
    fontWeight: "400"
  },
  cardIsFavorite: {
    backgroundColor: "#ebf4fa"
  },
  delimiter: {
    color: "#e0e0e0",
    display: "inline-block",
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),

    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
  areaLink: {
    [theme.breakpoints.down("md")]: {
      display: "block",
      marginTop: theme.spacing(1)
    }
  },
  timeIcon: {
    marginRight: theme.spacing(0.5)
  },
  currency: {
    lineHeight: "unset"
  }
});

function TenderName(props) {
  const { tender } = props;
  if (!tender.highlight) {
    return tender.name;
  }

  const tenderName = tender.highlight.nameExact || tender.highlight.name;
  if (tenderName) {
    return <span dangerouslySetInnerHTML={{ __html: tenderName[0] }} />;
  }

  return tender.name;
}

class TenderCard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  handleClick = () => {
    const { showTender, toggleTender, tender } = this.props;
    showTender(tender);
    toggleTender();
  };

  handleFavorites = () => {
    const { tender, addFavorites, confirm, removeFavorites, intl } = this.props;
    const { formatMessage } = intl;

    if (tender.favorites) {
      confirm(
        formatMessage(favoriteDeleteMessages.title),
        formatMessage(favoriteDeleteMessages.message),
        () => removeFavorites(tender.id, { skipComments: false })
      );
    } else {
      addFavorites(tender.id);
    }
  };

  render() {
    const {
      classes,
      tender,
      shared,
      canChangeTender,
      canAddTender,
      intl
    } = this.props;
    const { formatMessage, formatNumber } = intl;
    const priceInt = String(tender.price && tender.price).split(".")[0];
    const priceFraction = String(tender.price && tender.price.toFixed(2)).split(
      "."
    )[1];

    return (
      <Fragment>
        <Card
          className={classNames(
            tender.favorites && classes.cardIsFavorite,
            classes.card
          )}
        >
          <Grid container spacing={0}>
            <Grid item xs={12} sm={8} lg={9}>
              <CardContent className={classes.cardContentLeft}>
                <Grid item sm={12}>
                  <div className={classes.titleInfoSection}>
                    {tender.stage === "tender" &&
                      tender.status !== "submission_app" && (
                        <Chip
                          className={classNames(
                            classes.cardStatusChip,
                            classes.cardStatusChipDefault,
                            !tender.state ? classes.addMarginRight : ""
                          )}
                          label={formatMessage(messages[tender.status])}
                          color={"default"}
                          classes={{ label: classes.chipLabel }}
                        />
                      )}

                    {tender.stage === "tender" &&
                      tender.status === "submission_app" &&
                      (!tender.recent || tender.favorites) && (
                        <Chip
                          className={classNames(
                            classes.cardStatusChip,
                            !tender.state ? classes.addMarginRight : ""
                          )}
                          label={formatMessage(messages["submission_app"])}
                          color={"primary"}
                          classes={{ label: classes.chipLabel }}
                        />
                      )}

                    {tender.stage === "tender" &&
                      tender.status === "submission_app" &&
                      tender.recent &&
                      !tender.favorites && (
                        <Chip
                          className={classNames(
                            classes.cardStatusChip,
                            classes.addMarginRight
                          )}
                          label={formatMessage(messages["is_new"])}
                          color="secondary"
                          classes={{ label: classes.chipLabel }}
                        />
                      )}

                    {tender.stage === "purchase_plan" && (
                      <Chip
                        className={classNames(
                          classes.cardStatusChipPP,
                          !tender.state ? classes.addMarginRight : ""
                        )}
                        label={formatMessage(messages["purchase_plan"])}
                        classes={{ label: classes.chipLabel }}
                      />
                    )}

                    {tender.state && (
                      <Chip
                        className={classes.cardInfoChip}
                        label={formatMessage(stateMessages[tender.state])}
                        avatar={
                          <Avatar>
                            <StateColorDot state={tender.state} />
                          </Avatar>
                        }
                        classes={{
                          avatar: classes.stateIconAvatar,
                          label: classes.chipLabel
                        }}
                      />
                    )}

                    {tender.user && (
                      <Chip
                        className={classNames(
                          classes.cardInfoChip,
                          classes.addMarginRight
                        )}
                        label={tender.user.fullName}
                        avatar={
                          <Avatar
                            aria-label={tender.user.fullName}
                            src={tender.user.avatarImageUrl}
                            className={classes.avatarChip}
                          />
                        }
                        classes={{ label: classes.chipLabel }}
                      />
                    )}
                  </div>

                  <div className={classes.titleInfoSection}>
                    <div className={classes.titleInfoBlock}>
                      {tender.baseId}
                      <div className={classes.titleInfoBlockLabel}>
                        <FormattedMessage
                          id="tenders.tender-card.notification-number"
                          defaultMessage="№ извещения"
                        />
                      </div>
                    </div>

                    <div className={classes.titleInfoBlock}>
                      {tender.federalLawName}
                      <div className={classes.titleInfoBlockLabel}>
                        <FormattedMessage
                          id="tenders.tender-card.federal-low"
                          defaultMessage="Тип проведения"
                        />
                      </div>
                    </div>
                  </div>
                </Grid>

                <Grid item sm={12}>
                  <Link
                    component="button"
                    onClick={this.handleClick}
                    className={classNames({
                      [classes.browsed]: tender.browsed
                    })}
                  >
                    <Typography color="primary" className={classes.name}>
                      <TenderName tender={tender} />
                    </Typography>
                  </Link>
                </Grid>

                <Grid container spacing={1}>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.tender-card.customer"
                        defaultMessage="Заказчик"
                      />
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography
                      variant="body1"
                      className={classes.customerName}
                    >
                      {tender.customer.name}
                    </Typography>
                  </Grid>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.tender-card.area"
                        defaultMessage="Площадка"
                      />
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography variant="body1">
                      {tender.areaName}
                      {tender.auctionAreaUrl && (
                        <>
                          <span className={classes.delimiter}>|</span>
                          <Link
                            href={tender.auctionAreaUrl}
                            className={classes.areaLink}
                            color="primary"
                            target="_blank"
                            rel="noreferrer"
                          >
                            <Typography component="span">
                              {tender.auctionAreaName}
                            </Typography>
                          </Link>
                        </>
                      )}
                    </Typography>
                  </Grid>
                  <Grid item sm={3} lg={2} xs={12}>
                    <Typography className={classes.grayLabel}>
                      <FormattedMessage
                        id="tenders.tender-card.type"
                        defaultMessage="Форма проведения"
                      />
                    </Typography>
                  </Grid>
                  <Grid item sm={9} lg={10} xs={12}>
                    <Typography variant="body1">
                      {tender.typeName === null ? (
                        <FormattedMessage
                          id="tender.tender-card.type-unset"
                          defaultMessage="Не установлена"
                        />
                      ) : (
                        tender.typeName
                      )}
                    </Typography>
                  </Grid>
                </Grid>
                <Highlight tender={tender} />
              </CardContent>
            </Grid>
            <Grid item xs={12} sm={4} lg={3} className={classes.rightCardBlock}>
              <CardContent className={classes.cardContentRight}>
                <div className={classes.price}>
                  {tender.price ? (
                    <>
                      <Typography>
                        <span className={classes.priceValue}>
                          {formatNumber(priceInt, {
                            style: "decimal"
                          })}
                        </span>
                        {priceFraction && (
                          <span className={classes.priceFraction}>
                            ,{priceFraction}
                          </span>
                        )}
                      </Typography>
                      <Typography variant="body1">
                        <span
                          className={classNames(
                            classes.grayLabel,
                            classes.currency
                          )}
                        >
                          {tender.currency &&
                          !tender.currency.match(/руб|rub/i) ? (
                            tender.currency
                          ) : (
                            <FormattedMessage
                              id="tenders.tender-card.default-currency"
                              defaultMessage="Российский рубль"
                            />
                          )}
                        </span>
                      </Typography>
                    </>
                  ) : (
                    <Typography>
                      <FormattedMessage
                        id="tenders.tender-card.check-price"
                        defaultMessage="Уточнить цену закупки"
                      />
                    </Typography>
                  )}
                </div>
                <Typography className={classes.dateInfo}>
                  <span
                    className={classNames(
                      classes.grayLabel,
                      classes.grayLabelDate
                    )}
                  >
                    <FormattedMessage
                      id="tenders.tender-card.published-at"
                      defaultMessage="Опубликовано"
                    />
                  </span>
                  <span className={classes.grayLabelData}>
                    &nbsp;
                    <FormattedDate
                      id="tenders.tender-card.date-format"
                      year="numeric"
                      month="long"
                      day="numeric"
                      value={tender.publishedAt}
                    />
                  </span>
                </Typography>
                {tender.status !== "canceled" &&
                  tender.stage === "tender" &&
                  tender.endAt && (
                    <Typography className={classes.dateInfo}>
                      <span
                        className={classNames(
                          classes.grayLabel,
                          classes.grayLabelDate
                        )}
                      >
                        <FormattedMessage
                          id="tenders.tender-card.end-at"
                          defaultMessage="Срок подачи"
                        />
                      </span>
                      <span className={classes.grayLabelData}>
                        &nbsp;
                        <FormattedDate
                          id="tenders.tender-card.date-format"
                          year="numeric"
                          month="long"
                          day="numeric"
                          value={tender.endAt}
                        />
                      </span>
                    </Typography>
                  )}
                {tender.stage === "purchase_plan" &&
                  tender.purchaseGraphExecution && (
                    <Typography className={classes.dateInfo}>
                      <span
                        className={classNames(
                          classes.grayLabel,
                          classes.grayLabelDate
                        )}
                      >
                        <FormattedMessage
                          id="tenders.tender-card.execution-at"
                          defaultMessage="Срок договора"
                        />
                      </span>
                      <span className={classes.grayLabelData}>
                        &nbsp;
                        <FormattedDate
                          id="tenders.tender-card.date-format"
                          year="numeric"
                          month="long"
                          value={tender.purchaseGraphExecution}
                        />
                      </span>
                    </Typography>
                  )}
                {tender.stage === "tender" &&
                  tender.status !== "completed" &&
                  tender.status !== "canceled" &&
                  tender.timeLeft && (
                    <Typography className={classes.dateInfo}>
                      <span
                        className={classNames(
                          classes.grayLabel,
                          classes.grayLabelDate
                        )}
                      >
                        <AccessTime className={classes.timeIcon} />
                        <FormattedMessage
                          id="tenders.tender-card.time-left"
                          defaultMessage="Осталось"
                        />
                      </span>
                      <span className={classes.grayLabelData}>
                        &nbsp;
                        {tender.timeLeft}
                      </span>
                    </Typography>
                  )}
                {tender.stage === "purchase_plan" && tender.purchasePeriod && (
                  <Typography className={classes.dateInfo}>
                    <span
                      className={classNames(
                        classes.grayLabel,
                        classes.grayLabelDate
                      )}
                    >
                      <FormattedMessage
                        id="tenders.tender-card.purchase-period"
                        defaultMessage="Плановый период"
                      />
                    </span>
                    <span className={classes.grayLabelData}>
                      &nbsp;
                      <FormattedDate
                        id="tenders.tender-card.date-format"
                        year="numeric"
                        month="long"
                        value={tender.endAt}
                      />
                    </span>
                  </Typography>
                )}
                {!shared && (
                  <Button
                    className={classes.favorite}
                    onClick={this.handleFavorites}
                    color="primary"
                    disabled={
                      tender.favorites ? !canChangeTender : !canAddTender
                    }
                    startIcon={
                      tender.favorites ? (
                        <Favorite className={classes.favoriteIcon} />
                      ) : (
                        <FavoriteBorder className={classes.favoriteIcon} />
                      )
                    }
                    classes={{
                      disabled: tender.favorites && classes.disabledFavorites
                    }}
                  >
                    {tender.favorites ? (
                      <FormattedMessage
                        id="tenders.tender-card.remove-favorites"
                        defaultMessage="Снять с контроля"
                      />
                    ) : (
                      <FormattedMessage
                        id="tenders.tender-card.add-favorites"
                        defaultMessage="На контроль"
                      />
                    )}
                  </Button>
                )}
              </CardContent>
            </Grid>
          </Grid>
        </Card>
      </Fragment>
    );
  }
}

export default injectIntl(withWidth()(withStyles(styles)(TenderCard)));
