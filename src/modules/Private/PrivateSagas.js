import { all, fork } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";
import entitiesSagas from "../../services/Entities/EntitiesSagas";
import formSagas from "../../common/Form/FormSagas";
import suggestionsSagas from "../../common/Suggestions/SuggestionsSagas";
import otherServicesSagas from "../OtherServices/OtherServicesSagas";
import registrationExpiredSagas from "../RegistrationExpired/RegistrationExpiredSagas";

import "../../services/Api/ApiSagas";
import "../../services/Devise/DeviseSagas";
import { fetchSuggestions } from "../../common/Suggestions/SuggestionsActions";
import {
  changeComment,
  deleteComment
} from "../Tender/TenderComments/TenderCommentsActions";
import {
  createEntity,
  fetchEntity
} from "../../services/Entities/EntitiesActions";

// Override default entity api by action type and entity name
// default entity api { method: 'GET', url: `/api/${ entity }`}
const entities = {
  tender: "/api/tenders/:id",
  comments: "/api/tenders/:tenderId/comments",
  invitation: "/api/user/invitation",
  invitations: "/api/user/invitations",
  tendersExport: "/api/tenders/exports.xlsx",
  favoritesState: "/api/favorites/:favoriteId/state/:id",
  favoritesUser: "/api/favorites/:favoriteId/user/:id",
  favoritesExport: "/api/favorites/exports.xlsx",
  filterShare: "/api/short_links",
  "analytics/customers/purchasesSummary": "/api/analytics/customers/purchases",
  "analytics/customers/purchasesSummaryExport":
    "/api/analytics/customers/purchases/exports.xlsx",
  "analytics/customers/suppliersSummary": "/api/analytics/customers/suppliers",
  "analytics/customers/suppliersSummaryExport":
    "/api/analytics/customers/suppliers/exports.xlsx",
  "analytics/customers/checkResults": "/api/analytics/customers/check_results",
  "analytics/customers/checkResultsExport":
    "/api/analytics/customers/check_results/exports.xlsx",
  "analytics/customers/tendersSummary": "/api/analytics/customers/tenders",

  "analytics/suppliers/purchasesSummary": "/api/analytics/suppliers/purchases",
  "analytics/suppliers/tendersSummary": "/api/analytics/suppliers/tenders",
  "analytics/suppliers/supplierDescription":
    "/api/analytics/suppliers/supplier_description",
  "analytics/suppliers/purchasesSummaryExport":
    "/api/analytics/suppliers/purchases/exports.xlsx",

  [createEntity]: {
    "analytics/suppliers/supplierDescription/reports":
      "/api/analytics/suppliers/supplier_description/reports"
  },

  [fetchEntity]: {
    "analytics/suppliers/supplierDescription/reports":
      "/api/analytics/suppliers/supplier_description/reports/:id"
  },

  [changeComment]: {
    comments: "/api/comments/:id"
  },

  [deleteComment]: {
    comments: "/api/comments/:id"
  },

  [fetchSuggestions]: {
    search: "/api/suggestions",
    keywords: "/api/suggestions",
    including: "/api/suggestions",
    excluding: "/api/suggestions",
    customers: "/api/organizations",
    contact: "/api/organizations",
    region: "/api/regions",
    suppliers: "/api/organizations"
  }
};

function* privateSagas() {
  yield all([
    fork(entitiesSagas(entities)),
    fork(formSagas),
    fork(suggestionsSagas),
    fork(otherServicesSagas),
    fork(registrationExpiredSagas)
  ]);
}

sagaMiddleware.run(privateSagas);
