import React, { lazy } from "react";
import { connect } from "react-redux";

import "./PrivateSagas";

import { DEVISE_PATH } from "../../services/Devise/DeviseConstants";
import { REGISTRATION_EXPIRED_PATH } from "../RegistrationExpired/RegistrationExpiredConstants";
import { DASHBOARD_PATH } from "../Dashboard/DashboardConstants";
import { getAuth, getVerified } from "../../services/Devise/DeviseSelectors";
import { getEnabled } from "../RegistrationExpired/RegistrationExpiredSelectors";

import Private from "./Private";

const DeviseContainer = lazy(() =>
  import("../../services/Devise/DeviseContainer")
);
const DashboardContainer = lazy(() =>
  import("../Dashboard/DashboardContainer")
);
const RegistrationExpiredContainer = lazy(() =>
  import("../RegistrationExpired/RegistrationExpiredContainer")
);

const routes = {
  [REGISTRATION_EXPIRED_PATH]: {
    container: RegistrationExpiredContainer,
    expired: true
  },
  [DEVISE_PATH]: { container: DeviseContainer },
  [DASHBOARD_PATH]: { container: DashboardContainer, secure: true }
};

const PrivateContainer = props => <Private {...props} />;

const mapStateToProps = state => {
  return {
    routes,
    verified: getVerified(state),
    auth: getAuth(state),
    active: !getEnabled(state)
  };
};

export default connect(mapStateToProps)(PrivateContainer);
