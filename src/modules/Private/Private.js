import React, { Component } from "react";
import PropTypes from "prop-types";
import Router from "../../services/Router/Router";
import SplashScreen from "../../common/SplashScreen/SplashScreen";
import { YMInitializer } from "react-yandex-metrika";

class Private extends Component {
  static propTypes = {
    auth: PropTypes.bool.isRequired,
    verified: PropTypes.bool,
    active: PropTypes.bool,
    routes: PropTypes.shape({}).isRequired
  };

  render() {
    const { routes, auth, verified, active } = this.props;
    const production = process.env.NODE_ENV === "production";

    return (
      <>
        <Router
          routes={routes}
          auth={auth}
          verified={verified}
          active={active}
          fallback={<SplashScreen />}
        />
        {production && (
          <YMInitializer
            accounts={[62639791]}
            options={{
              webvisor: true,
              clickmap: true,
              trackLinks: true,
              accurateTrackBounce: true
            }}
            version="2"
          />
        )}
      </>
    );
  }
}

export default Private;
