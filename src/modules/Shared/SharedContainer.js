import React, { lazy } from "react";
import { connect } from "react-redux";

import "./SharedSagas";
import Shared from "./Shared";

import { DASHBOARD_PATH } from "../Dashboard/DashboardConstants";

const DashboardContainer = lazy(() =>
  import("../Dashboard/DashboardContainer")
);

const routes = {
  [DASHBOARD_PATH]: { container: DashboardContainer }
};
const SharedContainer = props => <Shared {...props} />;

const mapStateToProps = state => {
  return {
    routes
  };
};

export default connect(mapStateToProps)(SharedContainer);
