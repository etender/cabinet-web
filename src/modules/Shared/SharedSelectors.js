import { createSelector } from "reselect";
import { getLocationPathname } from "../../services/Router/RouterSelectors";

export const getShared = createSelector(getLocationPathname, path =>
  path.includes("/shared")
);
