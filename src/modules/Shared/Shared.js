import React, { Component } from "react";
import PropTypes from "prop-types";

import Router from "../../services/Router/Router";
import SplashScreen from "../../common/SplashScreen/SplashScreen";
import { YMInitializer } from "react-yandex-metrika";

window.shared = true;

class Shared extends Component {
  static propTypes = {
    routes: PropTypes.shape({}).isRequired
  };

  componentDidMount() {
    if (process.env.NODE_ENV === "production") {
      this.attachBitrixForm();
    }
  }

  attachBitrixForm() {
    const script = document.createElement("script");
    script.id = "bx24_form_delay";
    script.innerHTML =
      "(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;(w[b].forms=w[b].forms||[]).push(arguments[0])};if(w[b]['forms']) return;var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})(window,document,'https://cifrator.bitrix24.ru/bitrix/js/crm/form_loader.js','b24form');b24form({'id':'61','lang':'ru','sec':'a5nrwb','type':'delay','delay':60});";

    document.body.appendChild(script);
  }

  render() {
    const { routes } = this.props;
    const production = process.env.NODE_ENV === "production";

    return (
      <>
        <Router routes={routes} fallback={<SplashScreen />} />
        {production && (
          <YMInitializer
            accounts={[63480892]}
            options={{
              webvisor: true,
              clickmap: true,
              trackLinks: true,
              accurateTrackBounce: true
            }}
            version="2"
          />
        )}
      </>
    );
  }
}

export default Shared;
