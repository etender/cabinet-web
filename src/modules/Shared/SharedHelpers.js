export const createSharedPath = path => (window.shared ? "/shared" : "") + path;
