import { all, fork } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";
import entitiesSagas from "../../services/Entities/EntitiesSagas";
import formSagas from "../../common/Form/FormSagas";
import suggestionsSagas from "../../common/Suggestions/SuggestionsSagas";

import "../../services/Api/ApiSagas";

import { fetchSuggestions } from "../../common/Suggestions/SuggestionsActions";

// Override default entity api by action type and entity name
// default entity api { method: 'GET', url: `/api/${ entity }`}
const entities = {
  tender: "/api/shared/tenders/:id",
  tenders: "/api/shared/tenders",
  tendersExport: "/api/shared/tenders/exports.xlsx",
  categories: "/api/shared/categories",
  okpds: "/api/shared/okpds",
  tenderFilter: "/api/shared/analytics/filters/entity",

  "analytics/customers/purchasesSummary":
    "/api/shared/analytics/customers/purchases",
  "analytics/customers/purchasesSummaryExport":
    "/api/shared/analytics/customers/purchases/exports.xlsx",
  "analytics/customers/suppliersSummary":
    "/api/shared/analytics/customers/suppliers",
  "analytics/customers/suppliersSummaryExport":
    "/api/shared/analytics/customers/suppliers/exports.xlsx",
  "analytics/customers/checkResults":
    "/api/shared/analytics/customers/check_results",
  "analytics/customers/checkResultsExport":
    "/api/shared/analytics/customers/check_results/exports.xlsx",
  "analytics/customers/tendersSummary":
    "/api/shared/analytics/customers/tenders",

  // purchasesSummary: "/api/shared/analytics/purchases",
  // purchasesSummaryExport: "/api/shared/analytics/purchases/exports.xlsx",
  // suppliersSummary: "/api/shared/analytics/suppliers",
  // suppliersSummaryExport: "/api/shared/analytics/suppliers/exports.xlsx",
  // tendersSummary: "/api/shared/analytics/tenders",
  // checkResults: "/api/shared/analytics/check_results",
  // checkResultsExport: "/api/shared/analytics/check_results/exports.xlsx",

  [fetchSuggestions]: {
    search: "/api/shared/suggestions",
    keywords: "/api/shared/suggestions",
    including: "/api/shared/suggestions",
    excluding: "/api/shared/suggestions",
    regions: "/api/shared/regions",
    categories: "/api/shared/categories",
    okpds: "/api/shared/okpds"
  }
};

function* privateSagas() {
  yield all([
    fork(entitiesSagas(entities)),
    fork(formSagas),
    fork(suggestionsSagas)
  ]);
}

sagaMiddleware.run(privateSagas);
