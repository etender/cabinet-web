import { handleActions } from "redux-actions";

import { showRegistrationExpired } from "./RegistrationExpiredActions";

const initialState = {
  enabled: false
};

export default handleActions(
  {
    [showRegistrationExpired]: state => ({
      ...state,
      enabled: true
    })
  },
  initialState
);
