export const REGISTRATION_EXPIRED_PATH = "/expired";

export const API_REGISTRATION_PROLONG_PATH = {
  method: "POST",
  url: "/api/user/registrations/prolong"
};
