import React, { Component } from "react";
import { Button, withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Error from "../../common/Error/Error";

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  button: {
    marginBottom: theme.spacing(2)
  }
});

class RegistrationExpired extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prolongRegistrationDisabled: false
    };

    this.error = { status: 402 };
  }

  handleClick = () => {
    const { prolongRegistration } = this.props;
    prolongRegistration();
    this.setState({ prolongRegistrationDisabled: true });
  };

  render() {
    const { classes } = this.props;
    const { prolongRegistrationDisabled } = this.state;

    return (
      <div className={classes.root}>
        <Error error={this.error}>
          <Button
            variant="contained"
            color="primary"
            disabled={prolongRegistrationDisabled}
            className={classes.button}
            onClick={this.handleClick}
          >
            <FormattedMessage
              id="registration-expired.registration-expired.prolong"
              defaultMessage="Продлить"
            />
          </Button>
        </Error>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(RegistrationExpired));
