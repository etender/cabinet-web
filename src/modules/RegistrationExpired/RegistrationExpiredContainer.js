import React from "react";
import { connect } from "react-redux";

import { prolongRegistration } from "./RegistrationExpiredActions";
import RegistrationExpired from "./RegistrationExpired";

const RegistrationContainer = props => <RegistrationExpired {...props} />;

export default connect(null, {
  prolongRegistration
})(RegistrationContainer);
