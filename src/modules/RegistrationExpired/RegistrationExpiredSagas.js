import { put, call, takeLatest } from "redux-saga/effects";
import { callApi } from "../../services/Api/ApiHelpers";
import { push } from "connected-react-router";
import {
  requestApi,
  successApi,
  failureApi
} from "../../services/Api/ApiActions";
import sagaMiddleware from "../../utils/SagaUtils";
import { showSnackbar } from "../../common/Snackbar/SnackbarActions";
import {
  REGISTRATION_EXPIRED_PATH,
  API_REGISTRATION_PROLONG_PATH
} from "./RegistrationExpiredConstants";
import {
  showRegistrationExpired,
  prolongRegistration
} from "./RegistrationExpiredActions";

import { defineMessages } from "react-intl.macro";

const messages = defineMessages({
  success: {
    id: "registration-expired.registration-expired-sagas-success",
    defaultMessage: "Ваш запрос успешно отправлен!"
  }
});

function* prolongRegistrationSaga(action) {
  yield put(requestApi(prolongRegistration, action.payload, action.meta));

  try {
    yield call(callApi, API_REGISTRATION_PROLONG_PATH);
    yield put(successApi(prolongRegistration, action.payload, action.meta));
    yield put(showSnackbar(messages.success));
  } catch (error) {
    yield put(failureApi(prolongRegistration, error, action.meta));
  }
}

function* showRegistrationExpiredSaga() {
  yield put(push(REGISTRATION_EXPIRED_PATH));
}

export default function* registrationExpiredSagas() {
  yield takeLatest(prolongRegistration, prolongRegistrationSaga);
  yield takeLatest(showRegistrationExpired, showRegistrationExpiredSaga);
}

sagaMiddleware.run(registrationExpiredSagas);
