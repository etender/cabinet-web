import { createAction } from "redux-actions";

export const showRegistrationExpired = createAction(
  "REGISTRATION_EXPIRTED_SHOW"
);

export const prolongRegistration = createAction(
  "REGISTRATION_EXPIRTED_PROLONG"
);
