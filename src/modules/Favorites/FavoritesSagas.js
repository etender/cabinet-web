import {
  all,
  put,
  call,
  fork,
  takeEvery,
  takeLatest
} from "redux-saga/effects";

import { callApi } from "../../services/Api/ApiHelpers";
import sagaMiddleware from "../../utils/SagaUtils";
import {
  requestApi,
  successApi,
  failureApi
} from "../../services/Api/ApiActions";
import { fetchEntitiesSaga } from "../../services/Entities/EntitiesSagas";
import {
  fetchTenders,
  fetchMoreTenders,
  addFavorites,
  removeFavorites
} from "./FavoritesActions";
import {
  API_ADD_FAVORITES_PATH,
  API_REMOVE_FAVORITES_PATH
} from "./FavoritesConstants";

import userChangeSagas from "./UserChange/UserChangeSagas";
import stateChangeSagas from "./StateChange/StateChangeSagas";

function* addTendersSaga(action) {
  yield put(requestApi(addFavorites));

  try {
    const response = yield call(callApi, API_ADD_FAVORITES_PATH, {
      id: action.payload
    });
    yield put(successApi(addFavorites, response));
  } catch (error) {
    yield put(failureApi(addFavorites, error));
  }
}

function* removeTendersSaga(action) {
  yield put(requestApi(removeFavorites));

  try {
    yield call(callApi, API_REMOVE_FAVORITES_PATH, null, {
      id: action.payload
    });
    yield put(successApi(removeFavorites, action.payload, action.meta));
  } catch (error) {
    yield put(failureApi(removeFavorites, error));
  }
}

function* favoritesSagas() {
  yield all([
    fork(userChangeSagas),
    fork(stateChangeSagas),
    takeLatest(fetchTenders, fetchEntitiesSaga),
    takeLatest(fetchMoreTenders, fetchEntitiesSaga),
    takeEvery(addFavorites, addTendersSaga),
    takeEvery(removeFavorites, removeTendersSaga)
  ]);
}

sagaMiddleware.run(favoritesSagas);
