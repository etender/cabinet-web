import { createAction } from "redux-actions";

export {
  fetchTenders,
  fetchMoreTenders
} from "./FavoritesList/FavoritesListActions";
export const addFavorites = createAction("FAVORITES_ADD");
export const removeFavorites = createAction(
  "FAVORITES_REMOVE",
  id => id,
  (id, options) => options
);

export { changeUser, toggleUserChange } from "./UserChange/UserChangeActions";
export {
  changeState,
  toggleStateChange
} from "./StateChange/StateChangeActions";
