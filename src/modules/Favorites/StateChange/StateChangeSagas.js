import { all, takeLatest } from "redux-saga/effects";

import { updateEntitySaga } from "../../../services/Entities/EntitiesSagas";
import { changeState } from "./StateChangeActions";

export default function* stateSagas() {
  yield all([takeLatest(changeState, updateEntitySaga)]);
}
