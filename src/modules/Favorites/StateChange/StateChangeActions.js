import { createAction } from "redux-actions";
import { toggleDialog } from "../../../common/Dialog/DialogActions";
import { STATE_CHANGE_NAME } from "./StateChangeConstants";

export const changeState = createAction(
  "STATE_CHANGE",
  (favoriteId, id) => ({ favoriteId, id }),
  () => ({ entity: "favoritesState" })
);

export const toggleStateChange = tenderId =>
  toggleDialog(STATE_CHANGE_NAME, { tenderId });
