import React from "react";
import { connect } from "react-redux";

import { getOptions } from "../../../common/Dialog/DialogSelectors";
import { changeState, toggleStateChange } from "./StateChangeActions";
import StateChange from "./StateChange";
import { STATE_CHANGE_NAME, STATE_CHANGE_STATES } from "./StateChangeConstants";

const StateChangeContainer = props =>
  props.tenderId ? <StateChange {...props} /> : "";

const mapStateToProps = state => {
  return {
    ...getOptions(state, STATE_CHANGE_NAME),
    states: STATE_CHANGE_STATES
  };
};

export default connect(mapStateToProps, {
  changeState,
  toggleStateChange
})(StateChangeContainer);
