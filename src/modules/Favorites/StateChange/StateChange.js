import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import StateItem from "../StateItem/StateItem";
import DialogContainer from "../../../common/Dialog/DialogContainer";
import { STATE_CHANGE_NAME } from "./StateChangeConstants";

export const messages = defineMessages({
  title: {
    id: "favorites.state-change.title",
    defaultMessage: "Изменить статус"
  },
  valuation: {
    id: "favorites.state-change.valuation",
    defaultMessage: "На рассмотрении"
  },
  accepted: {
    id: "favorites.state-change.accepted",
    defaultMessage: "Подготовка к участию"
  },
  participation: {
    id: "favorites.state-change.participation",
    defaultMessage: "Участие"
  },
  documents: {
    id: "favorites.state-change.documents",
    defaultMessage: "Документы поданы"
  },
  canceled: {
    id: "favorites.state-change.canceled",
    defaultMessage: "Завершён"
  }
});

const styles = theme => ({
  root: {
    minHeight: theme.spacing(10),

    [theme.breakpoints.up("md")]: {
      minWidth: "400px"
    }
  }
});

export class StateChange extends Component {
  static propTypes = {
    tenderId: PropTypes.string.isRequired,
    states: PropTypes.arrayOf(PropTypes.string).isRequired,
    changeState: PropTypes.func.isRequired,
    toggleStateChange: PropTypes.func.isRequired
  };

  handleClick = state => {
    const { changeState, tenderId } = this.props;
    changeState(tenderId, state);

    this.handleClose();
  };

  handleClose = () => {
    const { toggleStateChange } = this.props;
    toggleStateChange();
  };

  render() {
    const { classes, states, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogContainer
        onEnter={this.handleEnter}
        name={STATE_CHANGE_NAME}
        classes={{ paper: classes.root }}
        formDisabled
        title={formatMessage(messages.title)}
      >
        {states.map(state => (
          <StateItem
            key={state}
            state={state}
            name={formatMessage(messages[state])}
            onClick={this.handleClick}
          />
        ))}
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(StateChange));
