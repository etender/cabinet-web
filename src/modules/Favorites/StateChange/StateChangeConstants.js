export const STATE_CHANGE_NAME = "stateChange";

export const STATE_CHANGE_STATES = [
  "valuation",
  "accepted",
  "participation",
  "documents",
  "canceled"
];
