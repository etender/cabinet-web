import React from "react";
import { connect } from "react-redux";

import TendersList from "../../Tenders/TendersList/TendersList";
import { getTenders, getTotalTenders } from "./FavoritesListSelectors";
import FavoritesExportContainer from "../FavoritesExport/FavoritesExportContainer";

const FavoritesListContainer = props => (
  <TendersList {...props} exportContainer={FavoritesExportContainer} />
);

const mapStateToProps = state => {
  return {
    items: getTenders(state),
    total: getTotalTenders(state)
  };
};

export default connect(mapStateToProps, {})(FavoritesListContainer);
