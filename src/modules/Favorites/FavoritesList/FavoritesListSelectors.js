import { createSelector } from "reselect";
import { getFilter } from "../FavoritesFilter/FavoritesFilterSelectors";

const getTendersList = state => state.favorites.list;

export const getTenders = createSelector(getTendersList, list => list.items);

export const getTotalTenders = createSelector(
  getTendersList,
  list => list.total || 0
);

export const getHasMoreTenders = createSelector(
  getTendersList,
  list => list.hasMore
);

export const getOutdated = createSelector(
  getTendersList,
  list => list.outdated
);

export const getFilterQuery = createSelector(getFilter, filter => ({
  sort: filter.date,
  state: filter.state === "none" ? null : filter.state,
  coordinator: filter.coordinator === "none" ? null : filter.coordinator,
  type: filter.type === "none" ? null : filter.type
}));
