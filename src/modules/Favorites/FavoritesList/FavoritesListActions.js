import { createAction } from "redux-actions";

export const fetchTenders = createAction(
  "FAVORITES_TENDERS_FETCH",
  query => query,
  () => ({ entity: "favorites" })
);

export const fetchMoreTenders = createAction(
  "FAVORITES_MORE_TENDERS_FETCH",
  query => query,
  () => ({ entity: "favorites" })
);
