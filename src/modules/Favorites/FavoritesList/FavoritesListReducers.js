import { handleActions } from "redux-actions";

import { successApiType } from "../../../services/Api/ApiHelpers";
import { fetchTender } from "../../Tender/TenderActions";
import { changeState } from "../StateChange/StateChangeActions";
import { changeUser } from "../UserChange/UserChangeActions";
import { fetchTenders, fetchMoreTenders } from "./FavoritesListActions";
import { addFavorites, removeFavorites } from "../FavoritesActions";
const initialState = {
  items: [],
  hasMore: false,
  total: null,
  outdated: false
};

export default handleActions(
  {
    [successApiType(fetchTender)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.id ? { ...item, ...action.payload } : item
      )
    }),

    [successApiType(fetchTenders)]: (state, action) => ({
      ...state,
      items: action.payload.items,
      hasMore: action.payload.items.length === 25,
      total: action.payload.meta.total,
      outdated: false
    }),

    [successApiType(fetchMoreTenders)]: (state, action) => ({
      ...state,
      items: [...state.items, ...action.payload.items],
      hasMore: action.payload.items.length > 0,
      total: action.payload.meta.total
    }),

    [successApiType(addFavorites)]: (state, action) => ({
      ...state,
      items: [action.payload, ...state.items]
    }),

    [successApiType(removeFavorites)]: (state, action) => ({
      ...state,
      items: state.items.filter(item => item.id !== action.payload.id),
      outdated: true
    }),

    [successApiType(changeState)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.favoriteId
          ? { ...item, state: action.payload.id }
          : item
      )
    }),

    [successApiType(changeUser)]: (state, action) => ({
      ...state,
      items: state.items.map(item =>
        item.id === action.payload.favoriteId
          ? { ...item, user: action.payload.user }
          : item
      )
    })
  },
  initialState
);
