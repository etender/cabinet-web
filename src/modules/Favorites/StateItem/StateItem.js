import React, { Component } from "react";
import {
  ListItem,
  ListItemText,
  ListItemIcon,
  withStyles
} from "@material-ui/core";
import PropTypes from "prop-types";
import StateColorDot from "../StateColorDot/StateColorDot";

const styles = theme => ({
  listItemIcon: {
    minWidth: "auto",
    marginRight: "8px"
  }
});

class StateItem extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  handleClick = () => {
    const { onClick, state } = this.props;
    onClick(state);
  };

  render() {
    const { state, name, classes } = this.props;

    return (
      <ListItem button onClick={this.handleClick}>
        <ListItemIcon className={classes.listItemIcon}>
          <StateColorDot state={state} />
        </ListItemIcon>
        <ListItemText primary={name} />
      </ListItem>
    );
  }
}

export default withStyles(styles)(StateItem);
