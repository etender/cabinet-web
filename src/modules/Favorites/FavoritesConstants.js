export const FAVORITES_PATH = "/favorites";
export const TENDER_PATH = "/favorites/tender";

export const API_ADD_FAVORITES_PATH = { method: "POST", url: "/api/favorites" };
export const API_REMOVE_FAVORITES_PATH = {
  method: "DELETE",
  url: "/api/favorites/:id"
};
