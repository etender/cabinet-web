import React from "react";
import { connect } from "react-redux";

import "./FavoritesSagas";
import {
  fetchTenders,
  fetchMoreTenders
} from "./FavoritesList/FavoritesListActions";
import {
  getTenders,
  getHasMoreTenders,
  getFilterQuery,
  getOutdated
} from "./FavoritesList/FavoritesListSelectors";
import Tenders from "../Tenders/Tenders";
import FavoritesFilterContainer from "./FavoritesFilter/FavoritesFilterContainer";
import FavoritesListContainer from "./FavoritesList/FavoritesListContainer";

const FavoritesContainer = props => (
  <Tenders
    {...props}
    listContainer={FavoritesListContainer}
    filterContainer={FavoritesFilterContainer}
  />
);

const mapStateToProps = state => {
  return {
    tenders: getTenders(state),
    hasMore: getHasMoreTenders(state),
    filterQuery: getFilterQuery(state),
    outdated: getOutdated(state)
  };
};

export default connect(mapStateToProps, {
  fetchTenders,
  fetchMoreTenders
})(FavoritesContainer);
