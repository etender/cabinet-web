import { defineMessages } from "react-intl.macro";

export const messages = defineMessages({
  title: {
    id: "favorites.favorite-delete.title",
    defaultMessage: "Удаление закупки"
  },
  message: {
    id: "favorites.favorite-delete.message",
    defaultMessage: 'Закупка будет удалена из раздела "На контроле".'
  }
});
