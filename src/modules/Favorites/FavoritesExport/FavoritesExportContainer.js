import React from "react";
import { connect } from "react-redux";

import { getFilterQuery } from "../FavoritesList/FavoritesListSelectors";
import ExportButtonContainer from "../../../common/ExportButton/ExportButtonContainer";

const FavoritesExportContainer = props => (
  <ExportButtonContainer {...props} name="favoritesExport" />
);

const mapStateToProps = state => {
  return {
    query: {
      search: {},
      ...getFilterQuery(state)
    }
  };
};

export default connect(mapStateToProps)(FavoritesExportContainer);
