import React, { Component } from "react";
import { withStyles } from "@material-ui/core";
import classNames from "classnames";

import red from "@material-ui/core/colors/red";
import orange from "@material-ui/core/colors/orange";
import green from "@material-ui/core/colors/green";
import indigo from "@material-ui/core/colors/indigo";
import grey from "@material-ui/core/colors/grey";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";

const styles = theme => ({
  root: {},
  valuation: {
    color: red["900"]
  },
  accepted: {
    color: orange["800"]
  },
  participation: {
    color: green["800"]
  },
  documents: {
    color: indigo["800"]
  },
  canceled: {
    color: grey["800"]
  },
  stateColorDot: {
    fontSize: "16px"
  }
});

export class StateColorDot extends Component {
  render() {
    const { classes, state, className } = this.props;

    return (
      <FiberManualRecord
        className={classNames(classes[state], classes.stateColorDot, className)}
      />
    );
  }
}

export default withStyles(styles)(StateColorDot);
