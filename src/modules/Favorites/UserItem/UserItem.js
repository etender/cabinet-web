import React, { Component } from "react";
import {
  Avatar,
  ListItem,
  ListItemAvatar,
  ListItemText
} from "@material-ui/core";

export class UserItem extends Component {
  handleClick = () => {
    const { onClick, item } = this.props;
    onClick(item);
  };

  render() {
    const { item } = this.props;

    return (
      <ListItem button onClick={this.handleClick}>
        <ListItemAvatar>
          <Avatar aria-label={item.name} src={item.avatarImageUrl} />
        </ListItemAvatar>
        <ListItemText primary={item.fullName} />
      </ListItem>
    );
  }
}

export default UserItem;
