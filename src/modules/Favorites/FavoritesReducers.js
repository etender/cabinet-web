import { combineReducers } from "redux";

import list from "./FavoritesList/FavoritesListReducers";
import filter from "./FavoritesFilter/FavoritesFilterReducers";

export default combineReducers({
  list,
  filter
});
