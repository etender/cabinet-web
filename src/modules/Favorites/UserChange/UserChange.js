import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { UserItem } from "../UserItem/UserItem";
import DialogContainer from "../../../common/Dialog/DialogContainer";
import { USER_CHANGE_NAME } from "./UserChangeConstants";

const styles = theme => ({
  root: {
    minHeight: theme.spacing(10),

    [theme.breakpoints.up("md")]: {
      minWidth: "400px"
    }
  },

  content: {
    paddingLeft: 0,
    paddingRight: 0
  }
});

export class UserChange extends Component {
  static propTypes = {
    tenderId: PropTypes.string.isRequired,
    users: PropTypes.arrayOf(PropTypes.object).isRequired,
    fetchUsers: PropTypes.func.isRequired,
    changeUser: PropTypes.func.isRequired,
    toggleUserChange: PropTypes.func.isRequired
  };

  handleEnter = () => {
    const { fetchUsers } = this.props;
    fetchUsers();
  };

  handleClick = item => {
    const { changeUser, toggleUserChange, tenderId } = this.props;
    changeUser(tenderId, item);
    toggleUserChange();
  };

  render() {
    const { classes, users } = this.props;

    return (
      <DialogContainer
        onEnter={this.handleEnter}
        name={USER_CHANGE_NAME}
        classes={{ paper: classes.root }}
        formDisabled
        title={
          <FormattedMessage
            id="favorites.user-change.title"
            defaultMessage="Изменить координатора"
          />
        }
      >
        {users.map(item => (
          <UserItem key={item.id} item={item} onClick={this.handleClick} />
        ))}
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(UserChange));
