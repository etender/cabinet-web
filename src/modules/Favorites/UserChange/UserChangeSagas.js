import { all, takeLatest } from "redux-saga/effects";

import { updateEntitySaga } from "../../../services/Entities/EntitiesSagas";
import { changeUser } from "./UserChangeActions";

export default function* suggestionsSagas() {
  yield all([takeLatest(changeUser, updateEntitySaga)]);
}
