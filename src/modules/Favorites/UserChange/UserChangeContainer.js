import React from "react";
import { connect } from "react-redux";

import { getOptions } from "../../../common/Dialog/DialogSelectors";
import { changeUser, toggleUserChange } from "./UserChangeActions";
import { fetchUsers } from "../../../services/Devise/DeviseActions";
import { getUsers } from "../../../services/Devise/DeviseSelectors";
import UserChange from "./UserChange";
import { USER_CHANGE_NAME } from "./UserChangeConstants";

const UserChangeContainer = props =>
  props.tenderId ? <UserChange {...props} /> : "";

const mapStateToProps = state => {
  return {
    users: getUsers(state),
    ...getOptions(state, USER_CHANGE_NAME)
  };
};

export default connect(mapStateToProps, {
  fetchUsers,
  changeUser,
  toggleUserChange
})(UserChangeContainer);
