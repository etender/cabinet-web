import { createAction } from "redux-actions";
import { toggleDialog } from "../../../common/Dialog/DialogActions";
import { USER_CHANGE_NAME } from "./UserChangeConstants";

export const changeUser = createAction(
  "USER_CHANGE",
  (favoriteId, user) => ({ favoriteId, id: user.id, user }),
  () => ({ entity: "favoritesUser" })
);

export const toggleUserChange = tenderId =>
  toggleDialog(USER_CHANGE_NAME, { tenderId });
