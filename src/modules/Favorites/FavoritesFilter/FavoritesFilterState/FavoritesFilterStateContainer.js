import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../FavoritesFilterActions";
import { getFilter } from "../FavoritesFilterSelectors";
import FavoritesFilterState from "./FavoritesFilterState";
import { STATE_CHANGE_STATES } from "../../StateChange/StateChangeConstants";

const FavoritesFilterStateContainer = props => (
  <FavoritesFilterState {...props} />
);

const mapStateToProps = state => {
  return {
    value: getFilter(state).state,
    states: STATE_CHANGE_STATES
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(FavoritesFilterStateContainer);
