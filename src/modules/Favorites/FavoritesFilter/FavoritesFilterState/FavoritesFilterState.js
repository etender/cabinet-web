import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";
import { messages as stateMessages } from "../../StateChange/StateChange";
import StateColorDot from "../../StateColorDot/StateColorDot";

const messages = defineMessages({
  stateField: {
    id: "favorites.favorites-filter-state.state-field",
    defaultMessage: "Любой статус"
  }
});

const styles = theme => ({
  dot: {
    marginRight: "8px"
  }
});

export class FavoritesFilterState extends Component {
  static propTypes = {
    value: PropTypes.string,
    states: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  constructor(props) {
    super(props);

    const { classes, states, intl } = this.props;
    const { formatMessage } = intl;

    this.menu = [
      {
        id: "none",
        name: formatMessage(messages.stateField)
      },
      ...states.map(state => ({
        id: state,
        name: (
          <Fragment>
            <StateColorDot state={state} className={classes.dot} />
            {formatMessage(stateMessages[state])}
          </Fragment>
        )
      }))
    ];
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ state: item.id });
  };

  render() {
    const { intl, value } = this.props;
    const { formatMessage } = intl;

    return (
      <Dropdown
        title={formatMessage(messages.stateField)}
        value={value}
        menu={this.menu}
        onChange={this.handleChange}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(FavoritesFilterState));
