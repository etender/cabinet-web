import { handleActions } from "redux-actions";

import { applyFilter } from "./FavoritesFilterActions";

const initialState = {
  date: "end_at asc",
  state: "none",
  coordinator: "none",
  type: "tender"
};

export default handleActions(
  {
    [applyFilter]: (state, action) => ({ ...state, ...action.payload })
  },
  initialState
);
