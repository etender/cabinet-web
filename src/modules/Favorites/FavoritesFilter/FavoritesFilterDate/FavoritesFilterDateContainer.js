import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../FavoritesFilterActions";
import { getFilter } from "../FavoritesFilterSelectors";
import FavoritesFilterDate from "./FavoritesFilterDate";

const FavoritesFilterDateContainer = props => (
  <FavoritesFilterDate {...props} />
);

const mapStateToProps = state => {
  return {
    value: getFilter(state).date
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(FavoritesFilterDateContainer);
