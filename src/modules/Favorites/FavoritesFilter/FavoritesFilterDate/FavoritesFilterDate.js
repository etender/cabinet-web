import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  ArrowUpward as ArrowUpwardIcon,
  ArrowDownward as ArrowDownwardIcon
} from "@material-ui/icons";
import { ListItemIcon, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  dateField: {
    id: "favorites.favorites-filter-date.date-field",
    defaultMessage: "Дата"
  },
  startAtField: {
    id: "favorites.favorites-filter-date.start-at-field",
    defaultMessage: "Дата проведения"
  },
  endAtField: {
    id: "favorites.favorites-filter-date.end-at-field",
    defaultMessage: "Дата подачи заявок"
  }
});

const styles = theme => ({
  menuItemIcon: {
    minWidth: theme.spacing(3),
    marginRight: 0,
    marginLeft: "auto",
    justifyContent: "flex-end"
  },
  menuItemIconSvg: {
    fontSize: "1em"
  }
});

export class FavoritesFilterDate extends Component {
  static propTypes = {
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    const { intl, classes } = this.props;
    const { formatMessage } = intl;

    this.menu = [
      {
        id: "start_at asc",
        name: (
          <Fragment>
            {formatMessage(messages.startAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowUpwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "start_at desc",
        name: (
          <Fragment>
            {formatMessage(messages.startAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowDownwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "end_at asc",
        name: (
          <Fragment>
            {formatMessage(messages.endAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowUpwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      },
      {
        id: "end_at desc",
        name: (
          <Fragment>
            {formatMessage(messages.endAtField)}
            <ListItemIcon className={classes.menuItemIcon}>
              <ArrowDownwardIcon className={classes.menuItemIconSvg} />
            </ListItemIcon>
          </Fragment>
        )
      }
    ];
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ date: item.id });
  };

  render() {
    const { intl, value } = this.props;
    const { formatMessage } = intl;

    return (
      <Dropdown
        title={formatMessage(messages.dateField)}
        value={value}
        menu={this.menu}
        onChange={this.handleChange}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(FavoritesFilterDate));
