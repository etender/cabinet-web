import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "./FavoritesFilterActions";
import { getFilter } from "./FavoritesFilterSelectors";
import FavoritesFilter from "./FavoritesFilter";
import { getCurrentUser } from "../../../services/Devise/DeviseSelectors";

const FavoritesFilterContainer = props => <FavoritesFilter {...props} />;

const mapStateToProps = state => {
  return {
    ...getFilter(state),
    user: getCurrentUser(state)
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(FavoritesFilterContainer);
