import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import FavoritesFilterTypeContainer from "./FavoritesFilterType/FavoritesFilterTypeContainer";
import FavoritesFilterDateContainer from "./FavoritesFilterDate/FavoritesFilterDateContainer";
import FavoritesFilterStateContainer from "./FavoritesFilterState/FavoritesFilterStateContainer";
import FavoritesFilterCoordinatorContainer from "./FavoritesFilterCoordinator/FavoritesFilterCoordinatorContainer";

const styles = theme => ({
  root: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(1)
  },
  formControl: {
    marginBottom: theme.spacing(3),
    minWidth: 100
  }
});

export class FavoritesFilter extends Component {
  static propTypes = {
    price: PropTypes.string,
    applyFilter: PropTypes.func.isRequired
  };

  render() {
    const { classes, user } = this.props;
    return (
      <div className={classes.root}>
        <FavoritesFilterTypeContainer />
        <FavoritesFilterStateContainer />
        {user.admin && <FavoritesFilterCoordinatorContainer />}
        <FavoritesFilterDateContainer />
      </div>
    );
  }
}

export default withStyles(styles)(FavoritesFilter);
