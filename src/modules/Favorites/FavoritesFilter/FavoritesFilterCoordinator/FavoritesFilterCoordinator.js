import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Avatar, withStyles } from "@material-ui/core";

import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  coordinatorField: {
    id: "favorites.favorites-filter-coordinator.coordinator-field",
    defaultMessage: "Любой координатор"
  }
});

const styles = theme => ({
  avatar: {
    textAlign: "center",
    marginRight: theme.spacing(2),
    width: theme.spacing(3),
    height: theme.spacing(3)
  }
});

export class FavoritesFilterCoordinator extends Component {
  static propTypes = {
    value: PropTypes.string,
    coordinators: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const { fetchUsers } = this.props;
    fetchUsers();
  }

  componentDidUpdate(prevProps) {
    const { classes, coordinators, intl } = this.props;
    const { formatMessage } = intl;

    if (coordinators !== prevProps.coordinators) {
      this.setState({
        menu: [
          {
            id: "none",
            name: formatMessage(messages.coordinatorField)
          },
          ...coordinators.map(user => ({
            id: user.id,
            name: (
              <Fragment>
                {" "}
                <Avatar
                  aria-label={user.fullName}
                  src={user.avatarImageUrl}
                  className={classes.avatar}
                />
                {user.fullName}
              </Fragment>
            )
          }))
        ]
      });
    }
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ coordinator: item.id });
  };

  render() {
    const { intl, value } = this.props;
    const { formatMessage } = intl;
    const { menu } = this.state;

    return (
      <Dropdown
        title={formatMessage(messages.coordinatorField)}
        value={value}
        menu={menu}
        onChange={this.handleChange}
      />
    );
  }
}

export default injectIntl(withStyles(styles)(FavoritesFilterCoordinator));
