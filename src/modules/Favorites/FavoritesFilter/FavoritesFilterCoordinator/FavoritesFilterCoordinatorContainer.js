import React from "react";
import { connect } from "react-redux";

import { getUsers } from "../../../../services/Devise/DeviseSelectors";
import { fetchUsers } from "../../../../services/Devise/DeviseActions";
import { applyFilter } from "../FavoritesFilterActions";
import { getFilter } from "../FavoritesFilterSelectors";
import FavoritesFilterCoordinator from "./FavoritesFilterCoordinator";

const FavoritesFilterCoordinatorContainer = props => (
  <FavoritesFilterCoordinator {...props} />
);

const mapStateToProps = state => {
  return {
    value: getFilter(state).coordinator,
    coordinators: getUsers(state)
  };
};

export default connect(mapStateToProps, {
  applyFilter,
  fetchUsers
})(FavoritesFilterCoordinatorContainer);
