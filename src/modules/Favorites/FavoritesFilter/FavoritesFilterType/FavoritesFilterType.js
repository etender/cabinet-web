import React, { Component } from "react";
import PropTypes from "prop-types";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dropdown from "../../../../common/Dropdown/Dropdown";

const messages = defineMessages({
  tenderField: {
    id: "favorites.favorites-filter-type.tender-field",
    defaultMessage: "Закупки"
  },
  purchaseField: {
    id: "favorites.favorites-filter-date.purchase-field",
    defaultMessage: "План-графики"
  }
});

export class FavoritesFilterType extends Component {
  static propTypes = {
    value: PropTypes.string
  };

  constructor(props) {
    super(props);

    const { intl } = this.props;
    const { formatMessage } = intl;

    this.menu = [
      {
        id: "tender",
        name: formatMessage(messages.tenderField)
      },
      {
        id: "purchase",
        name: formatMessage(messages.purchaseField)
      }
    ];
  }

  handleChange = item => {
    const { applyFilter } = this.props;
    applyFilter({ type: item.id });
  };

  render() {
    const { value } = this.props;

    return (
      <Dropdown value={value} menu={this.menu} onChange={this.handleChange} />
    );
  }
}

export default injectIntl(FavoritesFilterType);
