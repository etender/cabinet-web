import React from "react";
import { connect } from "react-redux";

import { applyFilter } from "../FavoritesFilterActions";
import { getFilter } from "../FavoritesFilterSelectors";
import FavoritesFilterType from "./FavoritesFilterType";

const FavoritesFilterTypeContainer = props => (
  <FavoritesFilterType {...props} />
);

const mapStateToProps = state => {
  return {
    value: getFilter(state).type
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(FavoritesFilterTypeContainer);
