import { takeLatest, put, call } from "redux-saga/effects";
import {
  requestApi,
  successApi,
  failureApi
} from "../../services/Api/ApiActions";
import { callApi } from "../../services/Api/ApiHelpers";
import { DOC_CONSTRUCTOR_PATH } from "./OtherServicesConstants";
import { docConstructor } from "./OtherServicesActions";
import { openLink } from "../../services/Api/ApiActions";

function* docConstructorSaga(action) {
  yield put(requestApi(docConstructor));

  try {
    const response = yield call(callApi, DOC_CONSTRUCTOR_PATH);
    yield put(
      openLink({
        url: response.url,
        target: "_blank"
      })
    );
    yield put(successApi(docConstructor, response));
  } catch (error) {
    yield put(failureApi(docConstructor, error));
  }
}

export default function* otherServicesSagas() {
  yield takeLatest(docConstructor, docConstructorSaga);
}
