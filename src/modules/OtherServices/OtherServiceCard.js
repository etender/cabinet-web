import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  withStyles,
  Card,
  CardContent,
  Button,
  Typography,
  Avatar,
  CardHeader
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import included from "../../images/services/included.png";
import additionally from "../../images/services/additionally.png";

const messages = defineMessages({
  included: {
    id: "other-service.other-service-card.included",
    defaultMessage: "Сервис уже включен в ваш пакет программ"
  },
  additionally: {
    id: "other-service.other-service-card.additionally",
    defaultMessage: "Сервис предоставляется дополнительно"
  },
  buttonDefaut: {
    id: "other-service.other-service-card.button-default",
    defaultMessage: "Перейти"
  },
  buttonGet: {
    id: "other-service.other-service-card.button-get",
    defaultMessage: "Получить"
  }
});

const styles = theme => ({
  mainButton: {
    width: "100%",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  header: {
    fontSize: "1.4em",
    lineHeight: "1.2em"
  },
  avatar: {
    marginRight: "10px"
  },
  avatarImg: {
    height: "48px",
    width: "auto"
  },
  description: {
    fontSize: "13.7px",
    "& p": {
      marginTop: "0px",
      marginBottom: "8px"
    }
  },
  footer: {
    alignSelf: "flex-end",
    marginTop: "auto",
    width: "100%"
  },
  content: {
    padding: theme.spacing(3),
    paddingTop: 0,
    display: "flex",
    flexDirection: "column",
    flex: 1
  },
  cardHeader: {
    padding: theme.spacing(3),
    paddingBottom: theme.spacing(2)
  },
  card: {
    minHeight: "490px",
    display: "flex",
    flexDirection: "column",

    [theme.breakpoints.up("xs")]: {
      minHeight: "420px"
    },
    [theme.breakpoints.up("lg")]: {
      minHeight: "520px"
    },
    [theme.breakpoints.up("lpxl")]: {
      minHeight: "540px"
    },
    [theme.breakpoints.up("xl")]: {
      minHeight: "460px"
    }
  },
  included: {
    display: "flex",
    flexDirection: "row"
  },
  includedAvatar: {
    width: "20px",
    height: "20px",
    marginRight: theme.spacing(1),
    display: "inline-block",
    marginTop: "2px"
  },
  includedTextBlock: {
    display: "inline-block",
    "& p": { fontSize: "12px" }
  },
  info: {
    fontSize: "12px",
    color: "#757575"
  }
});

class OtherServiceCard extends Component {
  static propTypes = {
    value: PropTypes.object.isRequired
  };

  render() {
    const { classes, value, anchorEl, intl } = this.props;
    const { formatMessage } = intl;

    const infoIcon = value.included === false ? additionally : included;
    const infoMessage =
      value.included === false ? messages.additionally : messages.included;
    const buttonMessage =
      messages[value.button ? value.button : "buttonDefaut"];
    const action = this.props[value.action];

    let style = {};
    if (anchorEl) {
      style.width = anchorEl.clientWidth;
    }

    return (
      <Card className={classes.card} style={style}>
        <CardHeader
          avatar={
            <Avatar
              aria-label="recipe"
              src={value.icon}
              classes={{ img: classes.avatarImg, root: classes.avatarImg }}
            ></Avatar>
          }
          title={value.name}
          classes={{
            title: classes.header,
            avatar: classes.avatar
          }}
          className={classes.cardHeader}
        />
        <CardContent className={classes.content}>
          <div className={classes.description}>{value.description}</div>
          <div className={classes.footer}>
            <div className={classes.included}>
              <Avatar
                aria-label="recipe"
                className={classes.includedAvatar}
                src={infoIcon}
                variant="square"
              ></Avatar>
              <div className={classes.includedTextBlock}>
                <Typography
                  dangerouslySetInnerHTML={{
                    __html: formatMessage(infoMessage)
                  }}
                ></Typography>
              </div>
            </div>
            <div>
              {value.url && (
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.mainButton}
                  target="_blank"
                  href={value.url}
                >
                  {formatMessage(buttonMessage)}
                </Button>
              )}
              {action && (
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.mainButton}
                  onClick={action}
                >
                  {formatMessage(buttonMessage)}
                </Button>
              )}
            </div>
            <Typography
              className={classes.info}
              dangerouslySetInnerHTML={{ __html: value.footer }}
            ></Typography>
          </div>
        </CardContent>
      </Card>
    );
  }
}

export default injectIntl(withStyles(styles)(OtherServiceCard));
