import React from "react";
import { connect } from "react-redux";
import { getCurrentUser } from "../../services/Devise/DeviseSelectors";
import OtherServices from "./OtherServices";
import { docConstructor } from "./OtherServicesActions";
import { getServices } from "./OtherServicesHelpers";

const OtherServicesContainer = props => <OtherServices {...props} />;

const mapStateToProps = state => {
  return {
    user: getCurrentUser(state),
    services: getServices()
  };
};

export default connect(mapStateToProps, {
  docConstructor
})(OtherServicesContainer);
