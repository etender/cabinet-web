import React, { Component } from "react";
import SimpleBar from "simplebar-react";
import { withStyles, Grid } from "@material-ui/core";
import OtherServiceCard from "./OtherServiceCard";

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    overflowY: "auto",
    [theme.breakpoints.up("lg")]: {
      width: "calc(100% - 97px)",
      paddingRight: 0
    }
  }
});
class OtherServices extends Component {
  handleDocConstructor = () => {
    const { docConstructorAction } = this.props;
    docConstructorAction();
  };

  render() {
    const { classes, services, docConstructor, user } = this.props;

    return (
      <SimpleBar style={{ height: "100%" }}>
        <Grid container spacing={2} className={classes.root}>
          {services &&
            services.map(
              service =>
                (!service.foreign || !user.foreign) && (
                  <Grid
                    key={service.id}
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    lg={3}
                    xl={2}
                  >
                    <OtherServiceCard
                      value={service}
                      docConstructor={docConstructor}
                    />
                  </Grid>
                )
            )}
        </Grid>
      </SimpleBar>
    );
  }
}

export default withStyles(styles)(OtherServices);
