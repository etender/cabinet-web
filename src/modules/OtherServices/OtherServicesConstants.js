export const DOC_CONSTRUCTOR = "DOC_CONSTRUCTOR";
export const DOC_CONSTRUCTOR_PATH = {
  method: "GET",
  url: "/api/doc_constructor"
};
