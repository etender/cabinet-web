import { createAction } from "redux-actions";
import { DOC_CONSTRUCTOR } from "./OtherServicesConstants";

export const docConstructor = createAction(DOC_CONSTRUCTOR, () => ({
  entity: "docConstructor"
}));
