import React from "react";

import expressCheckLogo from "../../images/services/express_check.png";
import expressDocumentLogo from "../../images/services/express_document.png";
import documentDesignerLogo from "../../images/services/document_designer.png";
import garantLogo from "../../images/services/garant.png";
import electronicTradingLogo from "../../images/services/electronic_trading.png";
import electronicSignatureLogo from "../../images/services/electronic_signature.png";
import expressConnectLogo from "../../images/services/express_connect.png";

const services = [
  {
    id: 1,
    name: "Экспресс Проверка",
    icon: expressCheckLogo,
    url: "https://proverka.gardoc.ru/login",
    description: (
      <>
        <p>Проверка благонадежности контрагентов.</p>
        <p>
          Экономико-математические модели расчетов индекса должной
          осмотрительности и кредитного риска.
        </p>
      </>
    ),
    footer: "Если у вас нет логина и пароля, обратитесь к менеджеру"
  },
  {
    id: 2,
    name: "Экспресс Документ",
    icon: expressDocumentLogo,
    url: "http://www.gardoc.ru/",
    description: (
      <>
        <p>
          Электронный документооборот для доставки документа контрагенту с
          соблюдением требований законодательства.
        </p>
      </>
    ),
    footer: "Для регистрации, используйте эл. подпись «Электронный Экспресс»"
  },
  {
    id: 3,
    name: "Конструктор документов",
    icon: documentDesignerLogo,
    description: (
      <>
        <p>
          Профессиональный сервис по подготовке договоров, доверенностей,
          учредительных документов за пару шагов: просто выберите нужный вариант
          документа и введите ваши данные.
        </p>
      </>
    ),
    footer: "Если у вас нет логина и пароля, обратитесь к менеджеру",
    action: "docConstructor",
    foreign: true
  },
  {
    id: 4,
    name: "Система ГАРАНТ",
    icon: garantLogo,
    url: "https://internet.garant.ru/",
    description: (
      <>
        <p>Профессиональная правовая поддержка ГАРАНТ для участников торгов.</p>
      </>
    ),
    footer: "Если у вас нет логина и пароля, обратитесь к менеджеру"
  },
  {
    id: 5,
    name: "Банковская гарантия",
    icon: electronicTradingLogo,
    url: "https://garant.finaxe.ru/login",
    description: (
      <>
        <p>
          Сервис для подбора банковской гарантии по 44-ФЗ и 223-ФЗ в 24
          банках-партнерах для любого региона на сумму обеспечения до 900 млн.
          руб.
        </p>
      </>
    ),
    footer: "Если у вас нет логина и пароля, обратитесь к менеджеру"
  },
  {
    id: 6,
    name: "Электронная подпись",
    icon: electronicSignatureLogo,
    url: "https://www.garantexpress.ru/zayavka-ep/?ref=etender",
    description: (
      <>
        <p>
          — для основных бизнес-задач
          <br />
          — участие в торгах на 200 + площадках
          <br />
          — работа с государственными информационными системами
          <br />— выпуск от 1 часа
        </p>
      </>
    ),
    included: false,
    footer: "Выпуск электронной подписи по всей России"
  },
  {
    id: 7,
    name: "Экспресс Коннект",
    icon: expressConnectLogo,
    url: "https://econnect.garantexpress.ru/",
    description: (
      <>
        <p>
          Безопасный удалённый доступ сотрудников к корпоративным ресурсам из
          любой точки мира по годовой подписке
        </p>
      </>
    ),
    included: false,
    footer: "Основано на технологии VPN"
  }
];

export const getServices = () => services;
