import { createAction } from "redux-actions";

import { MEMBERS_PATH } from "./MembersConstants";
import { showLink } from "../../services/Api/ApiActions";

export const showMembers = createAction(
  showLink.toString(),
  () => MEMBERS_PATH
);
