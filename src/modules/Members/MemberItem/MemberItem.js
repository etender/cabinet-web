import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import {
  Avatar,
  Typography,
  Button,
  Card,
  CardActions,
  CardContent
} from "@material-ui/core";

import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

const styles = theme => ({
  memberCard: {},
  avatar: {
    textAlign: "center",
    margin: "32px auto",
    width: 60,
    height: 60
  },
  name: {
    textAlign: "center",
    margin: "0 auto",
    fontSize: "1.4em"
  },
  email: {
    textAlign: "center",
    margin: "0 auto"
  },
  invited: {
    textAlign: "center",
    margin: "0 auto"
  },
  cardActions: {
    minHeight: "49px"
  },
  role: {
    textAlign: "center",
    margin: "0 auto",
    fontWeight: 500,
    fontSize: "1em"
  }
});

const messages = defineMessages({
  edit: {
    id: "members.member-item.edit",
    defaultMessage: "Редактировать"
  },
  invited: {
    id: "members.member-item.invited",
    defaultMessage: "Отправленно приглашение"
  },
  remove: {
    id: "members.member-item.remove",
    defaultMessage: "Удалить"
  },
  roleAdmin: {
    id: "members.member-item.role.admin",
    defaultMessage: "Администратор"
  },
  roleCoordinator: {
    id: "members.member-item.role.coordinator",
    defaultMessage: "Координатор"
  }
});

class MembersItems extends Component {
  static propTypes = {
    onDelete: PropTypes.any
  };

  handleDelete = () => {
    const { onDelete, member } = this.props;
    onDelete(member);
  };

  render() {
    const { intl, member, onDelete, classes } = this.props;
    const { formatMessage } = intl;

    return (
      <Card key={member.id} className={classes.memberCard}>
        <CardContent>
          <Avatar
            aria-label={member.name}
            src={member.avatarImageUrl}
            className={classes.avatar}
          />
          <Typography className={classes.name}>
            {member.name} {member.surname}
          </Typography>
          <Typography color="textSecondary" className={classes.email}>
            {member.email}
          </Typography>
          {member.admin && (
            <Typography color="textSecondary" className={classes.role}>
              {formatMessage(messages.roleAdmin)}
            </Typography>
          )}
          {member.coordinator && (
            <Typography color="textSecondary" className={classes.role}>
              {formatMessage(messages.roleCoordinator)}
            </Typography>
          )}
          {member.invited && (
            <Typography className={classes.invited}>
              {formatMessage(messages.invited)}
            </Typography>
          )}
        </CardContent>
        <CardActions className={classes.cardActions}>
          {onDelete && (
            <Button
              edge="end"
              aria-label={formatMessage(messages.remove)}
              onClick={this.handleDelete}
            >
              {formatMessage(messages.remove)}
            </Button>
          )}
        </CardActions>
      </Card>
    );
  }
}

export default injectIntl(withStyles(styles)(MembersItems));
