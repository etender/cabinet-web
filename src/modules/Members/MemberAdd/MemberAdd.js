import React, { Component, Fragment } from "react";
import { Button, TextField, withStyles } from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import { MEMBER_ADD_NAME } from "./MemberAddConstants";
import DialogContainer from "../../../common/Dialog/DialogContainer";

const messages = defineMessages({
  title: {
    id: "members.member-add.title",
    defaultMessage: "Добавить пользователя"
  },
  emailField: {
    id: "members.member-add.email-field",
    defaultMessage: "Email"
  },
  nameField: {
    id: "members.member-add.name-field",
    defaultMessage: "Имя"
  },
  surnameField: {
    id: "members.member-add.surname-field",
    defaultMessage: "Фамилия"
  },
  roleField: {
    id: "members.member-add.role-field",
    defaultMessage: "Роль"
  },
  patronymicField: {
    id: "members.member-add.patronymic-field",
    defaultMessage: "Отчество"
  }
});

const styles = theme => ({
  root: {
    minHeight: theme.spacing(10),

    [theme.breakpoints.up("md")]: {
      minWidth: "400px"
    }
  },
  role: {
    marginTop: theme.spacing(3)
  }
});

export class MemberAdd extends Component {
  handleAction = (value, form) => {
    const { inviteMember } = this.props;
    inviteMember(value, form);
  };

  render() {
    const { classes, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <DialogContainer
        name={MEMBER_ADD_NAME}
        classes={{ paper: classes.root }}
        title={formatMessage(messages.title)}
        onAction={this.handleAction}
        value={{ role: "coordinator" }}
        buttons={
          <Button type="submit" color="primary" autoFocus>
            <FormattedMessage
              id="members.member-add.add"
              defaultMessage="Добавить"
            />
          </Button>
        }
      >
        {({ field, getValue, handleInputChange, error }) => (
          <Fragment>
            <TextField
              name="email"
              inputRef={field}
              value={getValue("email")}
              onChange={handleInputChange}
              required
              fullWidth
              label={formatMessage(messages.emailField)}
              error={!!error("email")}
              helperText={error("email")}
              InputLabelProps={{ required: true }}
            />
            <FormControl component="fieldset" className={classes.role}>
              <FormLabel component="legend">
                {formatMessage(messages.roleField)}
              </FormLabel>
              <RadioGroup
                aria-label="role"
                name="role"
                value={getValue("role")}
                ref={field}
                onChange={handleInputChange}
                row
              >
                <FormControlLabel
                  value="admin"
                  control={<Radio color="primary" />}
                  label="Администратор"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="coordinator"
                  control={<Radio color="primary" />}
                  label="Координатор"
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
          </Fragment>
        )}
      </DialogContainer>
    );
  }
}

export default injectIntl(withStyles(styles)(MemberAdd));
