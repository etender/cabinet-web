import React from "react";
import { connect } from "react-redux";

import { inviteUser } from "../../../services/Devise/DeviseActions";
import MemberAdd from "./MemberAdd";

const MemberAddContainer = props => <MemberAdd {...props} />;

export default connect(null, {
  inviteMember: inviteUser
})(MemberAddContainer);
