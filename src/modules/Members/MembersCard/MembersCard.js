import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { withStyles, Grid, Typography } from "@material-ui/core";
import { FormattedMessage, defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import MemberItem from "../MemberItem/MemberItem";

const messages = defineMessages({
  deleteTitle: {
    id: "members.members-card.delete-title",
    defaultMessage: "Удаление пользователя"
  },
  deleteMessage: {
    id: "members.members-card.delete-message",
    defaultMessage: "Учетная запись пользователя будет удалена."
  }
});

const styles = theme => ({
  membersList: {
    marginBottom: theme.spacing(3)
  },
  header: {
    marginBottom: theme.spacing(3)
  }
});

class MembersCard extends Component {
  static propTypes = {
    canDeleteMember: PropTypes.bool.isRequired,
    fetchMembers: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    confirm: PropTypes.func.isRequired,
    members: PropTypes.arrayOf(PropTypes.object)
  };

  componentDidMount() {
    const { fetchMembers } = this.props;
    fetchMembers();
  }

  hadleDelete = member => {
    const { confirm, deleteUser, intl } = this.props;
    const { formatMessage } = intl;
    confirm(
      formatMessage(messages.deleteTitle),
      formatMessage(messages.deleteMessage),
      () => deleteUser(member.id)
    );
  };

  render() {
    const { classes, members, canDeleteMember } = this.props;

    return (
      <Fragment>
        <Typography variant="h5" className={classes.header}>
          <FormattedMessage
            id="members.members-card.title"
            defaultMessage="Пользователи"
          />
        </Typography>
        <Grid container spacing={2} className={classes.membersList}>
          {members &&
            members.map(member => {
              return (
                <Grid item xs={12} sm={6} md={4} lg={3} key={member.id}>
                  <MemberItem
                    key={member.id}
                    member={member}
                    onDelete={
                      canDeleteMember && !member.readOnly && this.hadleDelete
                    }
                  />
                </Grid>
              );
            })}
        </Grid>
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(MembersCard));
