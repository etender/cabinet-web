import React from "react";
import { connect } from "react-redux";

import { fetchUsers, deleteUser } from "../../../services/Devise/DeviseActions";
import { getMembers, canDeleteMember } from "../MemberSelectors";
import { confirm } from "../../../common/ConfirmationDialog/ConfiramtionActions";
import MembersCard from "./MembersCard";

const MembersCardContainer = props => <MembersCard {...props} />;

const mapStateToProps = state => {
  return {
    canDeleteMember: canDeleteMember(state),
    members: getMembers(state)
  };
};

export default connect(mapStateToProps, {
  fetchMembers: fetchUsers,
  confirm,
  deleteUser
})(MembersCardContainer);
