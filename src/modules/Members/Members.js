import React, { Component, Fragment } from "react";
import SimpleBar from "simplebar-react";
import { Fab, withStyles } from "@material-ui/core";
import { Add as AddIcon } from "@material-ui/icons";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import MemberAddContainer from "./MemberAdd/MemberAddContainer";
import MembersCardContainer from "./MembersCard/MembersCardContainer";
import MembersInvitationsCardContainer from "./MembersInvitationsCard/MembersInvitationsCardContainer";

const messages = defineMessages({
  title: {
    id: "members.title",
    defaultMessage: "Пользователи"
  },
  add: {
    id: "members.add",
    defaultMessage: "Добавить пользователя"
  },
  userLimitTitle: {
    id: "members.limit-title",
    defaultMessage: "Невозможно добавить пользователя"
  },
  userLimitMessage: {
    id: "members.limit-message",
    defaultMessage:
      "Достигнуто максимальное количество пользователей по вашему тарифу."
  }
});

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    overflowY: "auto",
    [theme.breakpoints.up("lg")]: {
      width: "calc(100% - 97px)"
    }
  },
  fab: {
    position: "absolute",
    bottom: "80px",
    right: "50px"
  }
});

class Members extends Component {
  handleAdd = () => {
    const { toggleMemberAdd, confirm, isMembersLimit, intl } = this.props;
    const { formatMessage } = intl;
    if (isMembersLimit) {
      confirm(
        formatMessage(messages.userLimitTitle),
        formatMessage(messages.userLimitMessage)
      );
    } else {
      toggleMemberAdd();
    }
  };

  render() {
    const { classes, intl, canAddMember } = this.props;
    const { formatMessage } = intl;

    return (
      <Fragment>
        <MemberAddContainer />
        <SimpleBar style={{ height: "100%" }}>
          <div className={classes.root}>
            <MembersCardContainer />
            <MembersInvitationsCardContainer />
          </div>
        </SimpleBar>
        {canAddMember && (
          <Fab
            color="primary"
            aria-label={formatMessage(messages.add)}
            className={classes.fab}
            onClick={this.handleAdd}
          >
            <AddIcon />
          </Fab>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(Members));
