import { createSelector } from "reselect";
import {
  getCurrentUser,
  getUsers
} from "../../services/Devise/DeviseSelectors";
import { getInvitations } from "../../services/Devise/Invitation/InvitationSelectors";

export const getMembers = createSelector(
  getCurrentUser,
  getUsers,
  (currentUser, users) =>
    users.map(user => ({ ...user, readOnly: user.id === currentUser.id }))
);

export const getMembersLimit = createSelector(
  getCurrentUser,
  currentUser => currentUser.company.userLimit
);

export const canAddMember = createSelector(
  getCurrentUser,
  user => !user.readOnly && user.admin
);

export const canDeleteMember = createSelector(
  getCurrentUser,
  user => !user.readOnly && user.admin
);

export const isMembersLimit = createSelector(
  getMembers,
  getInvitations,
  getMembersLimit,
  (members, invitations, membersLimit) => {
    return members.length + invitations.length >= membersLimit;
  }
);
