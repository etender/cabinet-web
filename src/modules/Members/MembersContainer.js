import React from "react";
import { connect } from "react-redux";
import { toggleDialog } from "../../common/Dialog/DialogActions";
import { MEMBER_ADD_NAME } from "./MemberAdd/MemberAddConstants";

import { confirm } from "../../common/ConfirmationDialog/ConfiramtionActions";
import { getInvitations } from "../../services/Devise/DeviseSelectors";
import { isMembersLimit, canAddMember } from "./MemberSelectors";
import Members from "./Members";

const MembersContainer = props => <Members {...props} />;

const mapStateToProps = state => {
  return {
    invitations: getInvitations(state),
    isMembersLimit: isMembersLimit(state),
    canAddMember: canAddMember(state)
  };
};

export default connect(mapStateToProps, {
  toggleMemberAdd: () => toggleDialog(MEMBER_ADD_NAME),
  confirm
})(MembersContainer);
