import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Typography, Grid, withStyles } from "@material-ui/core";
import { FormattedMessage, defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import MemberItem from "../MemberItem/MemberItem";

const messages = defineMessages({
  deleteTitle: {
    id: "members.mevers-invitations-card.delete-title",
    defaultMessage: "Удаление приглашения"
  },
  deleteMessage: {
    id: "members.mevers-invitations-card.delete-message",
    defaultMessage: "Приглашение пользотваеля будет удалёно."
  }
});

const styles = theme => ({
  membersList: {
    marginBottom: theme.spacing(3)
  },
  header: {
    marginBottom: theme.spacing(3)
  }
});

class MembersInvitationsCard extends Component {
  static propTypes = {
    canDeleteMember: PropTypes.bool.isRequired,
    confirm: PropTypes.func.isRequired,
    invitations: PropTypes.arrayOf(PropTypes.object)
  };

  componentDidMount() {
    const { fetchInvitations } = this.props;
    fetchInvitations();
  }

  hadleDelete = invitation => {
    const { confirm, deleteInvitation, intl } = this.props;
    const { formatMessage } = intl;
    confirm(
      formatMessage(messages.deleteTitle),
      formatMessage(messages.deleteMessage),
      () => deleteInvitation(invitation.id)
    );
  };

  render() {
    const { invitations, classes, canDeleteMember } = this.props;

    const onDelete = canDeleteMember ? { onDelete: this.hadleDelete } : {};
    return (
      <Fragment>
        {invitations.length > 0 && (
          <Typography variant="h5" className={classes.header}>
            <FormattedMessage
              id="members.mevers-invitations-card.title"
              defaultMessage="Приглашения"
            />
          </Typography>
        )}
        <Grid container spacing={2} className={classes.membersList}>
          {invitations.length > 0 &&
            invitations.map(invitation => {
              return (
                <Grid item xs={12} sm={6} md={4} lg={3} key={invitation.id}>
                  <MemberItem
                    key={invitation.id}
                    member={invitation}
                    {...onDelete}
                  />
                </Grid>
              );
            })}
        </Grid>
      </Fragment>
    );
  }
}

export default injectIntl(withStyles(styles)(MembersInvitationsCard));
