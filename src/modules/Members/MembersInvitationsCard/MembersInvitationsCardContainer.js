import React from "react";
import { connect } from "react-redux";

import { getInvitations } from "../../../services/Devise/DeviseSelectors";
import {
  fetchInvitations,
  deleteInvitation
} from "../../../services/Devise/DeviseActions";
import { canDeleteMember } from "../MemberSelectors";
import { confirm } from "../../../common/ConfirmationDialog/ConfiramtionActions";
import MembersInvitationsCard from "./MembersInvitationsCard";

const MembersInvitationsCardContainer = props => (
  <MembersInvitationsCard {...props} />
);

const mapStateToProps = state => {
  return {
    canDeleteMember: canDeleteMember(state),
    invitations: getInvitations(state)
  };
};

export default connect(mapStateToProps, {
  fetchInvitations,
  confirm,
  deleteInvitation
})(MembersInvitationsCardContainer);
