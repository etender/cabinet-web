import { createSharedPath } from "../Shared/SharedHelpers";

export const REGIONS_PATH = createSharedPath("/analytics/regions");
export const REGIONS_NAME = "regions";
export const REGIONS_DEFAULT_FILTER = { federalLaw: ["fl223", "fl44"] };
