import { clearFilter } from "../Analytics/FilterBuilder/FilterBuilderActions";
import { REGIONS_NAME } from "./AnalyticsRegionsConstants";

export const cleanRegions = () => clearFilter(REGIONS_NAME);
