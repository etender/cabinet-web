import React, { Component } from "react";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import AutocompleteFieldContainer from "../../../common/AutocompleteField/AutocompleteFieldContainer";
import FilterFormContainer from "../../Analytics/FilterForm/FilterFormContainer";
import FilterFormContent from "../../Analytics/FilterForm/FilterFormContent";

const messages = defineMessages({
  regionsField: {
    id: "analytics-regions.filter-form.regions-field",
    defaultMessage: "Регион или город"
  },
  regionsFieldHelpMessage: {
    id: "analytics-regions.filter-form.regions-field-help-message",
    defaultMessage: `<p>Введите наименование региона или города. Вы можете указать одно значений.</p>`
  },
  helpButtonBody: {
    id: "analytics-regions.filter-form.help-button-body",
    defaultMessage: " "
  }
});

class FilterForm extends Component {
  render() {
    const { entity, path, defaultFilter, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <FilterFormContainer
        entity={entity}
        path={path}
        defaultFilter={defaultFilter}
        helpButtonBody={formatMessage(messages.helpButtonBody)}
      >
        {props => (
          <FilterFormContent {...props}>
            {({ field, getValue, handleChange }) => (
              <AutocompleteFieldContainer
                objectValue
                selectOnly
                fullWidth
                label={formatMessage(messages.regionsField)}
                name="regions"
                fieldRef={field}
                value={getValue("regions")}
                onChange={value => handleChange("regions", value)}
                helpButtonBody={formatMessage(messages.regionsFieldHelpMessage)}
              />
            )}
          </FilterFormContent>
        )}
      </FilterFormContainer>
    );
  }
}

export default injectIntl(FilterForm);
