import React, { Component } from "react";
import SimpleBar from "simplebar-react";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core";

import FilterForm from "./FilterForm/FilterForm";
import FilterDescriptionContainer from "../Analytics/FilterDescription/FilterDescriptionContainer";
// import TendersSummaryContainer from "../Analytics/TendersSummary/TendersSummaryContainer";
// import ControlResultsContainer from "../Analytics/ControlResults/ControlResultsContainer";
// import PurchasesSummaryContainer from "../Analytics/PurchasesSummary/PurchasesSummaryContainer";

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    overflowY: "auto",

    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      width: "calc(100% - 97px)"
    }
  }
});

export class AnalyticsRegions extends Component {
  render() {
    const { editing, entity, entityPath, defaultFilter, classes } = this.props;

    return (
      <SimpleBar style={{ height: "100%" }}>
        <div className={classes.root}>
          {editing && (
            <FilterForm
              entity={entity}
              path={entityPath}
              defaultFilter={defaultFilter}
            />
          )}
          {!editing && (
            <>
              <FilterDescriptionContainer
                entity={entity}
                path={entityPath}
                defaultFilter={defaultFilter}
              />
              {/* <TendersSummaryContainer
                filter={filter}
                variant="filled"
                autofetch
              />
              <ControlResultsContainer
                filter={filter}
                variant="filled"
                autofetch
              />
              <PurchasesSummaryContainer
                filter={filter}
                variant="filled"
                autofetch
              /> */}
            </>
          )}
        </div>
      </SimpleBar>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsRegions));
