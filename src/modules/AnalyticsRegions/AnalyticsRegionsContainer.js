import React from "react";
import { connect } from "react-redux";

import {
  getFilter,
  getIsEditing
} from "../Analytics/FilterBuilder/FilterBuilderSelectors";
import {
  REGIONS_NAME,
  REGIONS_PATH,
  REGIONS_DEFAULT_FILTER
} from "./AnalyticsRegionsConstants";
import AnalyticsRegions from "./AnalyticsRegions";

const AnalyticsRegionsContainer = props => <AnalyticsRegions {...props} />;

const mapStateToProps = state => {
  const filter = getFilter(state, REGIONS_NAME);
  return {
    filter,
    editing: getIsEditing(state, REGIONS_NAME),
    entity: REGIONS_NAME,
    entityPath: REGIONS_PATH,
    defaultFilter: REGIONS_DEFAULT_FILTER
  };
};

export default connect(mapStateToProps)(AnalyticsRegionsContainer);
