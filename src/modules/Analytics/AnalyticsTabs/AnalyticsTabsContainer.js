import React from "react";
import { connect } from "react-redux";

import { getSelected } from "./AnalyticsTabsSelectors";
import { applyFilter } from "../FilterBuilder/FilterBuilderActions";
import AnalyticsTabs from "./AnalyticsTabs";

const AnalyticsTabsContainer = props => <AnalyticsTabs {...props} />;

const mapStateToProps = state => {
  return {
    selected: getSelected(state)
  };
};

export default connect(mapStateToProps, {
  applyFilter
})(AnalyticsTabsContainer);
