import { createSelector } from "reselect";
import { getLocationPathname } from "../../../services/Router/RouterSelectors";

import {
  CUSTOMERS_PATH,
  REGIONS_PATH,
  SUPPLIERS_PATH
} from "../AnalyticsConstants";

const pathIndexes = {
  [CUSTOMERS_PATH]: 0,
  [REGIONS_PATH]: 1,
  [SUPPLIERS_PATH]: 2
};

export const getSelected = createSelector(
  getLocationPathname,
  locationPathname =>
    // pathIndexes[locationPathname.replace(/\/filter$/, "")] || 0
    0
);
