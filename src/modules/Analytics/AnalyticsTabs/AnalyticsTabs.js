import React, { Component } from "react";
import { Tabs, Typography, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import {
  // CUSTOMERS_PATH,
  // CUSTOMERS_NAME,
  // REGIONS_PATH,
  // REGIONS_NAME,
  SUPPLIERS_PATH,
  SUPPLIERS_NAME
} from "../AnalyticsConstants";
import AnalyticsTabContainer from "../AnalyticsTab/AnalyticsTabContainer";

const messages = defineMessages({
  customersTitle: {
    id: "analytics.analytics-tabs.customers-title",
    defaultMessage: "по заказчику"
  },
  regionsTitle: {
    id: "analytics.analytics-tabs.regions-title",
    defaultMessage: "по региону"
  },
  suppliersTitle: {
    id: "analytics.analytics-tabs.suppliers-title",
    defaultMessage: "по поставщику"
  }
});

const styles = theme => ({
  root: {
    display: "flex",
    alignItems: "center"
  },
  tabs: {
    borderBottomColor: theme.palette.divider,
    borderBottomWidth: 1,
    borderBottomStyle: "solid",
    width: "100%"
  },
  title: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(3),
    fontSize: "17px"
  }
});

export class AnalyticsTabs extends Component {
  render() {
    const { classes, selected, intl } = this.props;
    const { formatMessage } = intl;

    return (
      <div className={classes.root}>
        <Typography variant="subtitle1" className={classes.title}>
          Аналитика:{" "}
        </Typography>
        <Tabs
          value={selected}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          className={classes.tabs}
        >
          {/* <AnalyticsTabContainer
            entity={CUSTOMERS_NAME}
            path={CUSTOMERS_PATH}
            title={formatMessage(messages.customersTitle)}
          />
          <AnalyticsTabContainer
            entity={REGIONS_NAME}
            path={REGIONS_PATH}
            title={formatMessage(messages.regionsTitle)}
          /> */}
          <AnalyticsTabContainer
            entity={SUPPLIERS_NAME}
            path={SUPPLIERS_PATH}
            title={formatMessage(messages.suppliersTitle)}
          />
        </Tabs>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsTabs));
