import React, { Component } from "react";
import { Button, withStyles } from "@material-ui/core";
import { isEmpty } from "../../../utils/CoreUtils";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Section from "../../../common/Section/Section";
import Form from "../../../common/Form/Form";

const messages = defineMessages({
  title: {
    id: "analytics.filter-form.title",
    defaultMessage: "Данные для построения аналитики"
  }
});

const styles = theme => ({
  searchButton: {
    marginRight: theme.spacing(1)
  }
});

class FilterForm extends Component {
  constructor(props) {
    super(props);

    this.filterRef = React.createRef();
  }

  handleAction = filter => {
    const { applyFilter } = this.props;
    applyFilter(filter);
  };

  handleSearchClick = () => {
    this.filterRef.current.submit();
  };

  handleCleanClick = () => {
    const { clearFilter } = this.props;

    this.filterRef.current.clean();
    clearFilter();
  };

  render() {
    const {
      classes,
      value,
      children,
      buildFilter,
      helpButtonBody,
      validate,
      intl
    } = this.props;
    const { formatMessage } = intl;

    if (isEmpty(value)) {
      return "";
    }

    return (
      <Section
        title={formatMessage(messages.title)}
        variant="filled"
        helpButtonBody={helpButtonBody}
        actions={
          <>
            <Button
              variant="contained"
              color="primary"
              size="medium"
              onClick={this.handleSearchClick}
              className={classes.searchButton}
            >
              <FormattedMessage
                id="analytics.filter-form.analyze"
                defaultMessage="Анализировать"
              />
            </Button>
            <Button
              variant="contained"
              size="medium"
              onClick={this.handleCleanClick}
            >
              <FormattedMessage
                id="analytics.filter-form.clean"
                defaultMessage="Сбросить"
              />
            </Button>
          </>
        }
      >
        <>
          <Form
            action={this.handleAction}
            value={value}
            ref={this.filterRef}
            validate={validate}
          >
            {({
              field,
              arrayField,
              getValue,
              getArrayValue,
              getDateTimeValue,
              error,
              handleChange,
              handleArrayChange
            }) =>
              children &&
              children({
                buildFilter,
                value,
                field,
                arrayField,
                getValue,
                getArrayValue,
                getDateTimeValue,
                error,
                handleChange,
                handleArrayChange
              })
            }
          </Form>
        </>
      </Section>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterForm));
