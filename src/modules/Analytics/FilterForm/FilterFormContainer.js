import React from "react";
import { connect } from "react-redux";

import {
  applyFilter,
  editFilter,
  buildFilter
} from "../FilterBuilder/FilterBuilderActions";
import { getFilter } from "../FilterBuilder/FilterBuilderSelectors";
import FilterForm from "./FilterForm";

const FilterFormContainer = props => <FilterForm {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { entity } = ownProps;
  return {
    value: getFilter(state, entity)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { entity, defaultFilter, path } = ownProps;

  return {
    clearFilter: () => dispatch(editFilter(entity, defaultFilter, path)),
    applyFilter: filter => dispatch(applyFilter(entity, filter, path)),
    buildFilter: filter => dispatch(buildFilter(entity, filter))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterFormContainer);
