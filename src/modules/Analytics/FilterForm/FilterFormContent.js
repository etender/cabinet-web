import React, { Component } from "react";
import {
  FormGroup,
  FormLabel,
  FormControl,
  FormControlLabel,
  Checkbox,
  InputAdornment,
  IconButton,
  TextField,
  withStyles
} from "@material-ui/core";
import NumberFormat from "react-number-format";
import { Event as EventIcon } from "@material-ui/icons";
import { DatePicker } from "@material-ui/pickers";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import AutocompleteFieldContainer from "../../../common/AutocompleteField/AutocompleteFieldContainer";
import FilterForms from "../../Dashboard/FilterForms/FilterForms";
import TreeViewDialogFieldContainer from "../../../common/TreeViewDialogField/TreeViewDialogFieldContainer";
import { isEmpty } from "../../../utils/CoreUtils";
import HelpButton from "../../../common/HelpButton/HelpButton";
import Filters from "../Filters/Filters";

const messages = defineMessages({
  keywordsField: {
    id: "analytics.filter-form.keywords-field",
    defaultMessage: "Cо словами"
  },
  keywordsFieldHelpMessage: {
    id: "analytics.filter-form.keywords-field-help-message",
    defaultMessage: `
    <p>Введите ключевые слова, содержащиеся в извещении процедуры, внутри документации, перечне товаров или лотов. Ключевым словом или словосочетанием может быть название отрасли, товара или услуги.</p>
    <p>Введите слова через запятую, чтобы найти процедуру хотя бы с одним словом. Например: <strong>колеса, шины, резина</strong>. Введите слова, разделяя пробелом, чтобы найти процедуры, содержащие все слова сразу. Например: (ищем) <strong>автомобильные шины</strong> — (получаем) поставка <strong>шин</strong> и <strong>дисков автомобильных</strong>.</p>
    <p>Введите словосочетание в кавычках, чтобы найти процедуры с точным соответствием искомой фразе, сохраняя порядок слов.</p>
    <p>Система применяет правила морфологии: каждое слово автоматически склоняется по падежам и числам. Правила применяются для всех полей.</p>`
  },
  includingField: {
    id: "analytics.filter-form.including-field",
    defaultMessage: "Включая слова"
  },
  includingFieldHelpMessage: {
    id: "analytics.filter-form.including-field-help-message",
    defaultMessage: `<p>Введите любые слова или словосочетания, чтобы уточнить параметры, указанные в поле “Со словами”. Это сузит круг поиска, потому что эти слова обязательно будут присутствовать в описании процедуры, лотов и внутри документации. Например: если вы производите шины для грузовых автомобилей, то для уточнения результатов введите в поле <strong>Камаз, Маз</strong> и т.д.</p>`
  },
  excludingField: {
    id: "analytics.filter-form.excluding-field",
    defaultMessage: "Исключая слова"
  },
  excludingFieldHelpMessage: {
    id: "analytics.filter-form.excluding-field-help-message",
    defaultMessage: `<p>
    Введите слова, которые не должны присутствовать в описании процедуры. Тендеры с этими словами не попадут в поисковую выдачу. Например: если в поле Со словами вы ввели <strong>автозапчасти</strong>, но не хотите, чтобы в результаты попали закупки шин и резины, потому что вы их не производите или не поставляете, то в поле Исключая слова введите <strong>шины, резины</strong>.</p>`
  },
  okpd2Field: {
    id: "analytics.filter-form.okpd2-field",
    defaultMessage: "ОКПД2"
  },
  okpd2FieldHelpMessage: {
    id: "analytics.filter-form.okpd2-field-help-message",
    defaultMessage: `<p>ОКПД2 — Общероссийский классификатор продукции по видам экономической деятельности. Предназначен для поиска и подбора кодов в известной группе без ввода ключевых слов. Любая государственная закупка имеет код ОКПД2. Если же требуется подобрать виды продукции, опираясь только на названия услуг и типов товаров, введите наименование позиции в расположенное в верхней части окна поле поиска.</p>`
  },
  pubDateFromField: {
    id: "analytics.filter-form.pub-date-from-field",
    defaultMessage: "Дата публикации, от"
  },
  pubDateToField: {
    id: "analytics.filter-form.pub-date-to-field",
    defaultMessage: "Дата публикации, до"
  },
  pubDateFieldHelpMessage: {
    id: "analytics.filter-form.pub-date-to-field-help-message",
    defaultMessage: `
    <p>Выберите с помощью календаря дату публикации процедуры.</p>
    <p>Дата публикации процедуры на площадке поможет вам найти процедуры, по конкретной дате.</p>`
  },
  endDateFromField: {
    id: "analytics.filter-form.end-date-from-field",
    defaultMessage: "Дата окончания приема заявок, от"
  },
  endDateToField: {
    id: "analytics.filter-form.end-date-to-field",
    defaultMessage: "Дата окончания приема заявок, до"
  },
  endDateFieldHelpMessage: {
    id: "analytics.filter-form.end-date-to-field-help-message",
    defaultMessage: `
    <p>С помощью календаря выберите дату окончания приема заявок.</p>
    <p>Срок приема заявок важная дата для участника закупок. К этой дате вам нужно внимательно изучить процедуру, зарегистрироваться на площадке и подготовить документы.</p>`
  },
  priceFromField: {
    id: "analytics.filter-form.price-start-field",
    defaultMessage: "Цена, от"
  },
  priceToField: {
    id: "analytics.filter-form.price-end-field",
    defaultMessage: "Цена, до"
  },
  clear: {
    id: "analytics.filter-form.clear",
    defaultMessage: "Очистить"
  },
  cancel: {
    id: "analytics.filter-form.cancel",
    defaultMessage: "Отмена"
  }
});

const styles = theme => ({
  dateFilter: {
    width: "calc((100% / 2) - 22px)",
    marginTop: "0px",
    "&:first-child": {
      marginRight: theme.spacing(3)
    }
  },
  dateHelpBotton: {
    marginTop: theme.spacing(1)
  },
  searchButton: {
    marginRight: theme.spacing(1)
  },
  formField: {
    marginBottom: theme.spacing(2)
  },
  formGroup: {
    marginTop: theme.spacing(1)
  },
  formLabel: {
    paddingTop: theme.spacing(1)
  },
  formFieldPrice: {
    marginBottom: theme.spacing(2),
    marginRight: "20px"
  },
  priceFilter: {
    width: "calc((100% / 2) - 12px)",
    "&:first-child": {
      marginRight: theme.spacing(3)
    }
  }
});

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={value => {
        onChange(value.value);
      }}
      thousandSeparator={" "}
    />
  );
}

class FilterFormContent extends Component {
  constructor(props) {
    super(props);

    this.filterRef = React.createRef();
  }

  handleFilterChanged = filter => {
    const { buildFilter } = this.props;

    buildFilter({ filterId: filter.id });
  };

  render() {
    const {
      classes,
      value,
      field,
      arrayField,
      getArrayValue,
      getDateTimeValue,
      getValue,
      handleChange,
      handleArrayChange,
      children,
      intl
    } = this.props;
    const { formatMessage } = intl;

    if (isEmpty(value)) {
      return "";
    }

    return (
      <>
        <Filters onChange={this.handleFilterChanged} />

        {children &&
          children({
            field,
            arrayField,
            getValue,
            getArrayValue,
            getDateTimeValue,
            handleChange,
            handleArrayChange
          })}

        <AutocompleteFieldContainer
          fullWidth
          label={formatMessage(messages.keywordsField)}
          name="keywords"
          fieldRef={field}
          value={getValue("keywords")}
          onChange={value => handleChange("keywords", value)}
          helpButtonBody={formatMessage(messages.keywordsFieldHelpMessage)}
        />

        <AutocompleteFieldContainer
          fullWidth
          label={formatMessage(messages.includingField)}
          name="including"
          fieldRef={field}
          value={getValue("including")}
          onChange={value => handleChange("including", value)}
          helpButtonBody={formatMessage(messages.includingFieldHelpMessage)}
          className={classes.formFieldAutocomplete}
        />

        <AutocompleteFieldContainer
          fullWidth
          label={formatMessage(messages.excludingField)}
          name="excluding"
          fieldRef={field}
          value={getValue("excluding")}
          onChange={value => handleChange("excluding", value)}
          helpButtonBody={formatMessage(messages.excludingFieldHelpMessage)}
          className={classes.formFieldAutocomplete}
        />

        <div className={classes.formField}>
          <TreeViewDialogFieldContainer
            name="okpds"
            title={formatMessage(messages.okpd2Field)}
            value={getValue("okpds", [])}
            onChange={value => handleChange("okpds", value)}
            helpButtonBody={formatMessage(messages.okpd2FieldHelpMessage)}
          />
        </div>

        <div className={classes.formField}>
          <FilterForms
            value={getValue("forms", [])}
            onChange={value => handleChange("forms", value)}
          />
        </div>

        <div className={classes.formField}>
          <FormControl component="fieldset">
            <FormLabel component="legend" className={classes.formLabel}>
              <FormattedMessage
                id="analytics.filter-form.type"
                defaultMessage="Тип проведения"
              />
            </FormLabel>
            <FormGroup className={classes.formGroup}>
              <FormControlLabel
                control={
                  <Checkbox
                    name="federalLaw"
                    value="fl223"
                    color="primary"
                    inputRef={arrayField}
                    checked={getArrayValue("federalLaw", "fl223")}
                    onChange={handleArrayChange}
                  />
                }
                label={
                  <FormattedMessage
                    id="analytics.filter-form.fl223-field"
                    defaultMessage="ФЗ №223"
                  />
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    name="federalLaw"
                    value="fl44"
                    color="primary"
                    inputRef={arrayField}
                    checked={getArrayValue("federalLaw", "fl44")}
                    onChange={handleArrayChange}
                  />
                }
                label={
                  <FormattedMessage
                    id="analytics.filter-form.fl44-field"
                    defaultMessage="ФЗ №44"
                  />
                }
              />
            </FormGroup>
          </FormControl>
        </div>

        <div className={classes.formField}>
          <DatePicker
            clearable
            autoOk
            disableFuture
            margin="normal"
            format="DD.MM.YYYY"
            label={formatMessage(messages.pubDateFromField)}
            value={getDateTimeValue("pubDateFrom")}
            onChange={value => handleChange("pubDateFrom", value)}
            clearLabel={formatMessage(messages.clear)}
            cancelLabel={formatMessage(messages.cancel)}
            className={classes.dateFilter}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton>
                    <EventIcon />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <DatePicker
            clearable
            autoOk
            disableFuture
            margin="normal"
            format="DD.MM.YYYY"
            label={formatMessage(messages.pubDateToField)}
            value={getDateTimeValue("pubDateTo")}
            onChange={value => handleChange("pubDateTo", value)}
            clearLabel={formatMessage(messages.clear)}
            cancelLabel={formatMessage(messages.cancel)}
            className={classes.dateFilter}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton>
                    <EventIcon />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <HelpButton
            body={formatMessage(messages.pubDateFieldHelpMessage)}
            classes={{ root: classes.dateHelpBotton }}
          />
        </div>

        <div className={classes.formField}>
          <DatePicker
            clearable
            autoOk
            margin="normal"
            format="DD.MM.YYYY"
            label={formatMessage(messages.endDateFromField)}
            value={getDateTimeValue("endDateFrom")}
            onChange={value => handleChange("endDateFrom", value)}
            clearLabel={formatMessage(messages.clear)}
            cancelLabel={formatMessage(messages.cancel)}
            className={classes.dateFilter}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton>
                    <EventIcon />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <DatePicker
            clearable
            autoOk
            margin="normal"
            format="DD.MM.YYYY"
            label={formatMessage(messages.endDateToField)}
            value={getDateTimeValue("endDateTo")}
            onChange={value => handleChange("endDateTo", value)}
            clearLabel={formatMessage(messages.clear)}
            cancelLabel={formatMessage(messages.cancel)}
            className={classes.dateFilter}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton>
                    <EventIcon />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <HelpButton
            body={formatMessage(messages.endDateFieldHelpMessage)}
            classes={{ root: classes.dateHelpBotton }}
          />
        </div>

        <div className={classes.formFieldPrice}>
          <TextField
            name="priceFrom"
            label={formatMessage(messages.priceFromField)}
            className={classes.priceFilter}
            value={getValue("priceFrom")}
            onChange={value => handleChange("priceFrom", value)}
            InputProps={{
              endAdornment: <InputAdornment position="end">₽</InputAdornment>,
              inputComponent: NumberFormatCustom
            }}
          />
          <TextField
            name="priceTo"
            label={formatMessage(messages.priceToField)}
            className={classes.priceFilter}
            value={getValue("priceTo")}
            onChange={value => handleChange("priceTo", value)}
            InputProps={{
              endAdornment: <InputAdornment position="end">₽</InputAdornment>,
              inputComponent: NumberFormatCustom
            }}
          />
        </div>
      </>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterFormContent));
