import React, { lazy } from "react";
import { connect } from "react-redux";

import { ANALYTICS_PATH, ANALYTICS_FILTER_PATH } from "./AnalyticsConstants";
import {
  CUSTOMERS_PATH,
  CUSTOMERS_NAME,
  CUSTOMERS_DEFAULT_FILTER,
  REGIONS_PATH,
  REGIONS_NAME,
  REGIONS_DEFAULT_FILTER,
  SUPPLIERS_PATH,
  SUPPLIERS_NAME,
  SUPPLIERS_DEFAULT_FILTER
} from "./AnalyticsConstants";
import Analytics from "./Analytics";
import { cleanCustomers } from "../AnalyticsCustomers/AnalyticsCustomersActions";
import { cleanRegions } from "../AnalyticsRegions/AnalyticsRegionsActions";
import { cleanSuppliers } from "../AnalyticsSuppliers/AnalyticsSuppliersActions";

const AnalyticsCustomersContainer = lazy(() =>
  import("../AnalyticsCustomers/AnalyticsCustomersContainer")
);
const AnalyticsRegionsContainer = lazy(() =>
  import("../AnalyticsRegions/AnalyticsRegionsContainer")
);
const AnalyticsSuppliersContainer = lazy(() =>
  import("../AnalyticsSuppliers/AnalyticsSuppliersContainer")
);

const routes = {
  [CUSTOMERS_PATH]: { container: AnalyticsCustomersContainer },
  [REGIONS_PATH]: { container: AnalyticsRegionsContainer },
  [SUPPLIERS_PATH]: { container: AnalyticsSuppliersContainer },
  [ANALYTICS_FILTER_PATH]: { container: AnalyticsSuppliersContainer },
  [ANALYTICS_PATH]: { container: AnalyticsSuppliersContainer }
};

const analyticsCustomersFilter = {
  entity: CUSTOMERS_NAME,
  defaultValue: CUSTOMERS_DEFAULT_FILTER
};

const filters = {
  [ANALYTICS_PATH]: {
    entity: SUPPLIERS_NAME,
    defaultValue: SUPPLIERS_DEFAULT_FILTER
  },
  [CUSTOMERS_PATH]: analyticsCustomersFilter,
  [REGIONS_PATH]: {
    entity: REGIONS_NAME,
    defaultValue: REGIONS_DEFAULT_FILTER
  },
  [SUPPLIERS_PATH]: {
    entity: SUPPLIERS_NAME,
    defaultValue: SUPPLIERS_DEFAULT_FILTER
  }
};

const AnalyticsContainer = props => <Analytics {...props} />;

const mapStateToProps = state => {
  return {
    routes,
    filters
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clearFilters: () => {
      dispatch(cleanCustomers());
      dispatch(cleanRegions());
      dispatch(cleanSuppliers());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AnalyticsContainer);
