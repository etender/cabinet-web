import { all, put, takeEvery } from "redux-saga/effects";
import { push } from "connected-react-router";

import { buildPath } from "../../../services/Router/RouterHelpers";
import sagaMiddleware from "../../../utils/SagaUtils";

import { createEntitySaga } from "../../../services/Entities/EntitiesSagas";
import { applyFilter, buildFilter, editFilter } from "./FilterBuilderActions";
import { normalize } from "./FilterBuilderHelpers";

function* applyFilterSaga(action) {
  const {
    payload,
    meta: { path }
  } = action;
  yield put(push(buildPath(path, normalize(payload))));
}

function* editFilterSaga(action) {
  const {
    payload,
    meta: { path }
  } = action;
  yield put(push(buildPath(path + "/filter", normalize(payload))));
}

export default function* filterBuilderSagas() {
  yield all([
    takeEvery(buildFilter, createEntitySaga),
    takeEvery(applyFilter, applyFilterSaga),
    takeEvery(editFilter, editFilterSaga)
  ]);
}

sagaMiddleware.run(filterBuilderSagas);
