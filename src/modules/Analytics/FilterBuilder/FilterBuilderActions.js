import { createAction } from "redux-actions";

export const clearFilter = createAction(
  "ANALYTICS_FILTER_BUILDER_CLEAR",
  () => {},
  entity => ({ entity })
);

export const applyFilter = createAction(
  "ANALYTICS_FILTER_BUILDER_APPLY",
  (_entity, filter) => filter,
  (entity, _filter, path) => ({ entity, path })
);

export const buildFilter = createAction(
  "ANALYTICS_FILTER_BUILDER_BUILD",
  (_entity, filter) => ({ filter }),
  (entity, _filter) => ({ entity, prefix: "analytics/filters/" })
);

export const editFilter = createAction(
  "ANALYTICS_FILTER_BUILDER_EDIT",
  (_entity, filter) => filter,
  (entity, _filter, path) => ({ entity, path })
);
