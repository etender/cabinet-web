import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import "./FilterBuilderSagas";
import {
  getFilterMeta,
  getPathFilter,
  shouldBuildFilter
} from "./FilterBuilderSelectors";
import { buildFilter } from "./FilterBuilderActions";
import FilterBuilder from "./FilterBuilder";

const FilterBuilderContainer = props => <FilterBuilder {...props} />;

FilterBuilderContainer.propTypes = {
  filters: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { filters } = ownProps;
  return {
    filter: getPathFilter(state, filters),
    entity: getFilterMeta(state, filters).entity,
    shouldBuildFilter: shouldBuildFilter(state, filters)
  };
};

export default connect(mapStateToProps, {
  buildFilter
})(FilterBuilderContainer);
