export const normalize = filter => {
  const result = {
    ...filter,
    customers:
      filter.customers &&
      filter.customers.map(customer => customer.id || customer),
    regions: filter.regions && filter.regions.map(region => region.id),
    okpds: filter.okpds && filter.okpds.map(okpd => okpd.id)
  };

  return result;
};
