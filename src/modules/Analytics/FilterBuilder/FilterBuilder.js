import { Component } from "react";

// Component allows to get full filter (with regions and other dictionaries models)
// from server and cache it.
class FilterBuilder extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { filter, entity, buildFilter, shouldBuildFilter } = this.props;

    if (shouldBuildFilter) {
      buildFilter(entity, filter);
    }
  }

  componentDidMount() {
    const { filter, entity, buildFilter, shouldBuildFilter } = this.props;

    if (shouldBuildFilter) {
      buildFilter(entity, filter);
    }
  }

  render() {
    return null;
  }
}

export default FilterBuilder;
