import { handleActions } from "redux-actions";

import { successApiType } from "../../../services/Api/ApiHelpers";
import {
  applyFilter,
  clearFilter,
  buildFilter,
  editFilter
} from "./FilterBuilderActions";
import { isEmpty } from "../../../utils/CoreUtils";

const initialState = {};

export const handleFilterAction = reducer => {
  return (state, action) => {
    const { entity } = action.meta;
    let entityState = state[entity] || [];

    return {
      ...state,
      [entity]: reducer(entityState, action)
    };
  };
};

export default handleActions(
  {
    [clearFilter]: handleFilterAction((state, action) => ({
      value: null
    })),

    [applyFilter]: handleFilterAction((state, action) => ({
      value: action.payload,
      editing: false
    })),

    [buildFilter]: handleFilterAction((state, action) => ({
      ...state,
      editing: action.payload.editing
    })),

    [successApiType(buildFilter)]: handleFilterAction((state, action) => ({
      ...state,
      value: action.payload
    })),

    [editFilter]: handleFilterAction((state, action) => {
      if (isEmpty(action.payload)) {
        return { ...state, editing: true };
      }

      return { ...state, editing: true, value: action.payload };
    })
  },
  initialState
);
