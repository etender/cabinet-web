import { createSelector } from "reselect";
import {
  getLocationHash,
  getLocationPathname,
  isLocationPopOrReplace
} from "../../../services/Router/RouterSelectors";
import { parseHash } from "../../../services/Router/RouterHelpers";
import { isEmpty } from "../../../utils/CoreUtils";
import { normalize } from "./FilterBuilderHelpers";

const getFiltersMeta = (_state, filters) => filters;

export const getFilterMeta = createSelector(
  getLocationPathname,
  getFiltersMeta,
  (path, filters) => {
    path = path.replace(/\/filter$/, "").replace(/\/$/, "");
    return filters[path] || {};
  }
);

export const getPathFilter = createSelector(
  getFilterMeta,
  getLocationHash,
  (fitlerMeta, locationHash) => {
    if (!fitlerMeta.entity) {
      return {};
    }

    const options = parseHash(locationHash);
    const value =
      (locationHash && locationHash !== "" && options) ||
      fitlerMeta.defaultValue ||
      {};
    return value;
  }
);

const getState = state => state.analytics;
const getEntityState = (state, entity) => state.analytics[entity] || {};

export const getCurrentFilter = createSelector(
  getFilterMeta,
  getState,
  (meta, state) => (state[meta.entity] || {}).value || {}
);

export const shouldBuildFilter = createSelector(
  getFilterMeta,
  isLocationPopOrReplace,
  getCurrentFilter,
  (filterMeta, popOrReplace, filter) =>
    filterMeta.entity && (popOrReplace || isEmpty(filter))
);

export const getFilter = (state, entity) =>
  getEntityState(state, entity).value || {};

export const getNormalizeFilter = createSelector(getFilter, filter =>
  normalize(filter)
);

export const getEditing = (state, entity) => {
  const editing = getEntityState(state, entity).editing;
  return editing === undefined ? true : editing;
};

export const getIsEditing = createSelector(getLocationPathname, path =>
  path.includes("/filter")
);
