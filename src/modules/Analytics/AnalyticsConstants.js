import { createSharedPath } from "../Shared/SharedHelpers";

export const ANALYTICS_PATH = createSharedPath("/analytics");
export const ANALYTICS_FILTER_PATH = createSharedPath("/analytics/filter");

export {
  CUSTOMERS_PATH,
  CUSTOMERS_NAME,
  CUSTOMERS_DEFAULT_FILTER
} from "../AnalyticsCustomers/AnalyticsCustomersConstants";
export {
  REGIONS_PATH,
  REGIONS_NAME,
  REGIONS_DEFAULT_FILTER
} from "../AnalyticsRegions/AnalyticsRegionsConstants";
export {
  SUPPLIERS_PATH,
  SUPPLIERS_NAME,
  SUPPLIERS_DEFAULT_FILTER
} from "../AnalyticsSuppliers/AnalyticsSuppliersConstants";
