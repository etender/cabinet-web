import React from "react";
import { connect } from "react-redux";
import {
  applyFilter,
  editFilter
} from "../../Analytics/FilterBuilder/FilterBuilderActions";
import {
  getEditing,
  getFilter
} from "../../Analytics/FilterBuilder/FilterBuilderSelectors";
import AnalyticsTab from "./AnalyticsTab";

const AnalyticsTabContainer = props => <AnalyticsTab {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { entity } = ownProps;

  return {
    editing: getEditing(state, entity),
    filter: getFilter(state, entity)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { entity } = ownProps;
  return {
    applyFilter: (filter, path) => dispatch(applyFilter(entity, filter, path)),
    editFilter: (filter, path) => dispatch(editFilter(entity, filter, path))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalyticsTabContainer);
