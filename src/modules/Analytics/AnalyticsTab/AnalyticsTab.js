import React, { Component } from "react";
import { Tab, Typography, withStyles } from "@material-ui/core";

const styles = theme => ({
  title: {
    textTransform: "none",
    fontSize: "17px"
  }
});

export class AnalyticsTab extends Component {
  handleClick = () => {
    const { applyFilter, editFilter, filter, path, editing } = this.props;

    editing ? editFilter(filter, path) : applyFilter(filter, path);
  };

  render() {
    const { classes, title } = this.props;

    return (
      <Tab
        onClick={this.handleClick}
        label={<Typography className={classes.title}>{title}</Typography>}
      />
    );
  }
}

export default withStyles(styles)(AnalyticsTab);
