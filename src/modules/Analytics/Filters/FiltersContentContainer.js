import React from "react";
import { connect } from "react-redux";

import { getFilters } from "../../Dashboard/Filters/FiltersSelectors";
import FiltersContent from "./FiltersContent";

const FiltersContentContainer = props => <FiltersContent {...props} />;

const mapStateToProps = state => {
  return {
    items: getFilters(state)
  };
};

export default connect(mapStateToProps)(FiltersContentContainer);
