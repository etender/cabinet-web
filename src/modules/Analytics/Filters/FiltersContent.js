import React, { Component } from "react";
import PropTypes from "prop-types";
import { List, withStyles } from "@material-ui/core";
import { injectIntl } from "react-intl";

import { isPresent } from "../../../utils/CoreUtils";
import FilterItem from "../../Dashboard/FilterItem/FilterItem";

const styles = theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  }
});

class FiltersContent extends Component {
  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = this.filtersState();
  }

  filtersState() {
    const { search } = this.props;
    let { items } = this.props;

    if (isPresent(search)) {
      const re = new RegExp(search, "i");
      items = items.filter(item => !search || re.test(item.name));
    }

    return { items: items, enable: items.length > 0 };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { search } = this.props;

    if (prevProps.search !== search) {
      this.setState(this.filtersState());
    }
  }
  render() {
    const { classes, value, onChange } = this.props;
    const { enable, items } = this.state;

    if (!enable) {
      return "";
    }

    return (
      <List className={classes.root}>
        {items.map(item => (
          <FilterItem
            key={item.id}
            onChange={onChange}
            value={item}
            selected={value}
          />
        ))}
      </List>
    );
  }
}

export default injectIntl(withStyles(styles)(FiltersContent));
