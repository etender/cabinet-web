import React, { Component } from "react";
import SimpleBar from "simplebar-react";
import {
  Typography,
  DialogContent,
  Button,
  withStyles
} from "@material-ui/core";
import { defineMessages, FormattedMessage } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dialog from "../../../common/Dialog/Dialog";
import FiltersContentContainer from "./FiltersContentContainer";
import classNames from "classnames";

const messages = defineMessages({
  title: {
    id: "analytics.filters.title",
    defaultMessage: "Фильтр"
  }
});

const styles = theme => ({
  root: {
    display: "inline-flex",
    alignItems: "center",
    marginBottom: theme.spacing(2)
  },
  value: {
    marginLeft: theme.spacing(1)
  },
  openButton: {
    marginLeft: theme.spacing(2)
  },
  dialogPaper: {
    [theme.breakpoints.up("sm")]: {
      minWidth: theme.spacing(90)
    }
  },
  dialogContent: {
    paddingTop: 0,
    [theme.breakpoints.down("sm")]: {
      padding: `0 ${theme.spacing()}px`
    }
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

class Filters extends Component {
  static propTypes = {
    // value: PropTypes.array.isRequired,
    // onChange: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      selected: {},
      value: null
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      // const newValue = this.deserialize(this.props.value);
      // this.setState({ value: newValue });
      // this.selected = this.deserializeSelected(newValue);
    }
  }

  renderContent(props) {
    return <FiltersContentContainer {...props} />;
  }

  handleOpen = event => {
    event.preventDefault();

    this.setState({ open: true });
  };

  handleClose = event => {
    this.setState({ open: false });
  };

  handleChange = value => {
    this.setState({ selected: { [value.id]: true }, selectedValue: value });
  };

  handleApply = () => {
    this.handleClose();

    const { selectedValue } = this.state;
    this.setState({ value: selectedValue });

    const { onChange } = this.props;
    if (onChange) {
      onChange(selectedValue);
    }
  };

  render() {
    const { classes, intl } = this.props;
    const { formatMessage } = intl;
    const { open, selected, value } = this.state;

    return (
      <div className={classes.root}>
        {value && (
          <>
            <Typography>{formatMessage(messages.title)}:</Typography>
            <Typography className={classes.value}>
              {value && value.name}
            </Typography>
          </>
        )}
        <Button
          variant="contained"
          color="primary"
          component="button"
          onClick={this.handleOpen}
          className={classNames(value ? classes.openButton : "")}
        >
          <Typography>
            <FormattedMessage
              id="analytics.filters.select"
              defaultMessage="Выбрать фильтр"
            />
          </Typography>
        </Button>
        <Dialog
          open={open}
          toggleDialog={this.handleClose}
          classes={{ paper: classes.dialogPaper }}
          title={formatMessage(messages.title)}
          formDisabled
          actionsDisabled
          buttons={
            <>
              <Button onClick={this.handleApply} color="primary">
                <FormattedMessage
                  id="analytics.filters.apply"
                  defaultMessage="Применить"
                />
              </Button>
            </>
          }
        >
          <DialogContent className={classes.dialogContent}>
            <SimpleBar style={{ height: "100%" }}>
              {open && (
                <FiltersContentContainer
                  value={selected}
                  onChange={this.handleChange}
                />
              )}
            </SimpleBar>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(Filters));
