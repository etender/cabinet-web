import React, { Component } from "react";
import PropTypes from "prop-types";
import { injectIntl } from "react-intl";
import { withStyles } from "@material-ui/core";

import Router from "../../services/Router/Router";

import FilterBuilderContainer from "./FilterBuilder/FilterBuilderContainer";
import AnalyticsTabsContainer from "./AnalyticsTabs/AnalyticsTabsContainer";

const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    paddingTop: theme.spacing(2),
    width: "100%",
    marginRight: "auto",
    height: "100%"
  },
  content: {
    flexGrow: 1,
    overflow: "hidden"
  }
});

export class Analytics extends Component {
  static propTypes = {
    filters: PropTypes.object.isRequired
  };

  componentWillUnmount() {
    const { clearFilters } = this.props;
    clearFilters();
  }

  render() {
    const { classes, routes, filters } = this.props;

    return (
      <div className={classes.root}>
        <FilterBuilderContainer filters={filters} />
        <AnalyticsTabsContainer />
        <div className={classes.content}>
          <Router routes={routes} />
        </div>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(Analytics));
