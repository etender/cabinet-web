import React, { Component } from "react";
import { injectIntl } from "react-intl";
import {
  Typography,
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableRow,
  withStyles
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";

const styles = theme => ({
  analytics: {
    margin: theme.spacing(3),
    position: "relative"
  },
  header: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1)
  },
  tableHeaderCellFirst: {
    width: "30%"
  },
  message: {
    textAlign: "center",
    position: "absolute",
    top: "13%",
    width: "100%"
  },
  messageText: {
    fontSize: "1.3em"
  }
});

export class AnalyticsDialog extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.analytics}>
        <div className={classes.message}>
          <Typography className={classes.messageText}>
            Приносим свои извинения, раздел находится в разработке. <br />В
            ближайшее время он будет закончен и Вы сможете получить необходимую
            информацию.
          </Typography>
        </div>
        <Skeleton
          variant="text"
          height={30}
          width="40%"
          className={classes.header}
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableHeaderCellFirst}>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <Skeleton
          variant="text"
          height={30}
          width="50%"
          className={classes.header}
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableHeaderCellFirst}>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell className={classes.tableHeaderCellFirst}>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
                <Skeleton variant="text" width="80%" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
                <Skeleton variant="text" width="40px" />
                <Skeleton variant="text" width="40px" />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
                <Skeleton variant="text" width="80%" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
                <Skeleton variant="text" width="40px" />
                <Skeleton variant="text" width="40px" />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
                <Skeleton variant="text" width="80%" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" width="40px" />
                <Skeleton variant="text" width="40px" />
                <Skeleton variant="text" width="40px" />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default injectIntl(withStyles(styles)(AnalyticsDialog));
