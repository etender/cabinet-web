import React, { Component } from "react";
import { Typography, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import { grey } from "@material-ui/core/colors";

import ArrayFieldDescription from "../../Dashboard/FilterDescription/ArrayFieldDescription";
import CollectionFieldDescription from "../../Dashboard/FilterDescription/CollectionFieldDescription";
import DateRangeFieldDescription from "../../Dashboard/FilterDescription/DateRangeFieldDescription";
import FederalLawFieldDescription from "../../Dashboard/FilterDescription/FederalLawFieldDescription";

const messages = defineMessages({
  customers: {
    id: "analytics.filter-description.customers",
    defaultMessage: "заказчик:"
  },
  regions: {
    id: "analytics.filter-description.regions",
    defaultMessage: "по регионам:"
  },
  suppliers: {
    id: "analytics.filter-description.suppliers",
    defaultMessage: "по поставщику:"
  },
  keywords: {
    id: "analytics.filter-description.keywords",
    defaultMessage: "со словами:"
  },
  including: {
    id: "analytics.filter-description.including",
    defaultMessage: "включая слова:"
  },
  excluding: {
    id: "analytics.filter-description.excluding",
    defaultMessage: "исключая слова:"
  },
  pubDate: {
    id: "analytics.filter-description.pub-date",
    defaultMessage: "дата публикации:"
  },
  okpds: {
    id: "analytics.filter-description.okpds",
    defaultMessage: "по ОКПД2:"
  },
  categories: {
    id: "analytics.filter-description.categories",
    defaultMessage: "по категориям:"
  }
});

const styles = theme => ({
  root: {
    color: grey[600]
  }
});

class FilterDescriptionContent extends Component {
  render() {
    const { classes, value, intl } = this.props;
    const { formatMessage } = intl;

    const {
      customers,
      regions,
      suppliers,
      keywords,
      including,
      excluding,
      federalLaw,
      deal,
      categories,
      okpds,
      pubDateFrom,
      pubDateTo
    } = value || {};

    return (
      <Typography className={classes.root}>
        <CollectionFieldDescription
          title={formatMessage(messages.customers)}
          value={customers}
          field="title"
        />
        <CollectionFieldDescription
          title={formatMessage(messages.regions)}
          value={regions}
          field="title"
        />
        <CollectionFieldDescription
          title={formatMessage(messages.suppliers)}
          value={suppliers}
          field="title"
        />
        <ArrayFieldDescription
          title={formatMessage(messages.keywords)}
          value={keywords}
        />
        <ArrayFieldDescription
          title={formatMessage(messages.including)}
          value={including}
        />
        <ArrayFieldDescription
          title={formatMessage(messages.excluding)}
          value={excluding}
        />
        <FederalLawFieldDescription value={federalLaw} deal={deal} />
        <DateRangeFieldDescription
          title={formatMessage(messages.pubDate)}
          from={pubDateFrom}
          to={pubDateTo}
        />
        <CollectionFieldDescription
          title={formatMessage(messages.categories)}
          value={categories}
        />
        <CollectionFieldDescription
          title={formatMessage(messages.okpds)}
          value={okpds}
        />
      </Typography>
    );
  }
}

export default injectIntl(withStyles(styles)(FilterDescriptionContent));
