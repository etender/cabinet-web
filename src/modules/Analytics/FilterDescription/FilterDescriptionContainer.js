import React from "react";
import { connect } from "react-redux";
import { editFilter } from "../../Analytics/FilterBuilder/FilterBuilderActions";
import { getFilter } from "../../Analytics/FilterBuilder/FilterBuilderSelectors";
import FilterDescription from "./FilterDescription";

const FilterDescriptionContainer = props => <FilterDescription {...props} />;

const mapStateToProps = (state, ownProps) => {
  const { entity } = ownProps;
  return {
    value: getFilter(state, entity)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { entity, defaultFilter, path } = ownProps;
  return {
    editFilter: filter => dispatch(editFilter(entity, filter, path)),
    clearFilter: () => dispatch(editFilter(entity, defaultFilter, path))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterDescriptionContainer);
