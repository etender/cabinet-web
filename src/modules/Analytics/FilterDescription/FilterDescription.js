import React, { Component } from "react";
import { Button } from "@material-ui/core";
import { injectIntl } from "react-intl";
import { defineMessages, FormattedMessage } from "react-intl.macro";

import Section from "../../../common/Section/Section";

import FilterDescriptionContent from "./FilterDescriptionContent";
import FilterDescriptionSkeleton from "./FilterDescriptionSkeleton";

const messages = defineMessages({
  title: {
    id: "analytics.filter-description.title",
    defaultMessage: "Данные для построения аналитики"
  }
});
export class FilterDescription extends Component {
  handleEditClick = () => {
    const { editFilter, value } = this.props;

    editFilter(value);
  };

  handleCleanClick = () => {
    const { clearFilter } = this.props;

    clearFilter();
  };

  render() {
    const { value, component, helpButtonBody, intl } = this.props;
    const { formatMessage } = intl;

    const ContentComponent = component || FilterDescriptionContent;
    return (
      <Section
        title={formatMessage(messages.title)}
        variant="filled"
        helpButtonBody={helpButtonBody}
        actions={
          <>
            <Button color="primary" onClick={this.handleEditClick}>
              <FormattedMessage
                id="analytics.filter-description.edit"
                defaultMessage="Изменить"
              />
            </Button>
            <Button color="primary" onClick={this.handleCleanClick}>
              <FormattedMessage
                id="analytics.filter-description.clean"
                defaultMessage="Сбросить"
              />
            </Button>
          </>
        }
      >
        {value && <ContentComponent value={value} />}
        {!value && <FilterDescriptionSkeleton />}
      </Section>
    );
  }
}

export default injectIntl(FilterDescription);
