import React, { Component } from "react";
import { injectIntl } from "react-intl";

import { Skeleton } from "@material-ui/lab";

export class FilterDescriptionSkeleton extends Component {
  render() {
    return (
      <>
        <Skeleton width="100%" height="28px" />
        <Skeleton width="100%" height="28px" />
      </>
    );
  }
}

export default injectIntl(FilterDescriptionSkeleton);
