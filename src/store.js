import { createStore, applyMiddleware, compose } from "redux";
import { routerMiddleware } from "connected-react-router";

import history from "./utils/HistoryUtils";
import sagaMiddleware from "./utils/SagaUtils";
import appReducer from "./app/AppReducers";

const composeEnhancers =
  (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Replace big data to fix Redux DevTool issue:
      //
      //   Application state or actions payloads are too large making Redux DevTools serialization slow and consuming a lot
      //   of memory. See https://git.io/fpcP5 on how to configure it.
      //
      // Just replace big data with short text.
      actionSanitizer: action =>
        action.meta && action.meta.form
          ? { ...action, meta: { ...action.meta, form: "<FORM>" } }
          : action
    })) ||
  compose;

export default createStore(
  appReducer(history),
  composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
);
