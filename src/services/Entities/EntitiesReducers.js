// import { combineReducers } from "redux";
import reduceReducers from "reduce-reducers";

import entity from "./Entity/EntityReducers";
import array from "./ArrayEntities/ArrayEntitiesReducers";
import paginate from "./PaginateEntities/PaginateEntitiesReducers";
import tree from "./TreeEntities/TreeEntitiesReducers";

export default reduceReducers({}, entity, array, tree, paginate);
// export default combineReducers({
//   array,
//   tree
// });
