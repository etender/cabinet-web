import { all, put, call, fork } from "redux-saga/effects";
import { callApi } from "../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../Api/ApiActions";
import { entityUrl, setEntities } from "./EntitiesHelpers";
import entitySagas from "./Entity/EntitySagas";
import arrayEntitiesSagas from "./ArrayEntities/ArrayEntitiesSagas";
import paginateEntitiesSagas from "./PaginateEntities/PaginateEntitiesSagas";
import treeEntitiesSagas from "./TreeEntities/TreeEntitiesSagas";
import fileDownload from "js-file-download";

export function* fetchEntitiesSaga(action) {
  yield entitiesSaga(action, { method: "GET" });
}

export function* createEntitySaga(action) {
  yield entitiesSaga(action, { method: "POST" });
}

export function* updateEntitySaga(action) {
  yield entitiesSaga(action, { method: "PUT", response: action.payload });
}

export function* deleteEntitySaga(action) {
  yield entitiesSaga(action, { method: "DELETE", response: action.payload });
}

export function* entitiesSaga(action, options) {
  yield put(requestApi(action.type, action.payload, action.meta));

  try {
    const payload = action.payload || {};
    const { binnary } = action.meta || {};
    const onlyPayload = !payload.options && !payload.query;
    const response = yield call(
      callApi,
      { method: options.method, url: entityUrl(action), binnary },
      // payload.options,
      // payload.options ? payload.query : payload
      onlyPayload ? payload : payload.options,
      onlyPayload ? payload : payload.query
    );

    if (binnary && response.filename) {
      fileDownload(response.content, response.filename);
    }

    yield put(
      successApi(action.type, options.response || response, action.meta)
    );
  } catch (error) {
    console.log("Entity error %O", error);
    yield put(failureApi(action.type, error, action.meta));
  }
}

export default function entitiesSagas(entities) {
  setEntities(entities);
  return function* sagas() {
    yield all([
      fork(arrayEntitiesSagas),
      fork(paginateEntitiesSagas),
      fork(treeEntitiesSagas),
      fork(entitySagas)
    ]);
  };
}
