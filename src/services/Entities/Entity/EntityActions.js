import { createAction } from "redux-actions";

export const cleanEntity = createAction(
  "ENTITY_CLEAN",
  () => {},
  entity => ({ entity })
);

export const fetchEntity = createAction(
  "ENTITY_FETCH",
  (entity, query) => query,
  (entity, query, options) => ({ entity, ...options })
);

export const createEntity = createAction(
  "ENTITY_CREATE",
  (entity, query) => query,
  (entity, query, options) => ({ entity, ...options })
);
