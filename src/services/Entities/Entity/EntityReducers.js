import { handleActions } from "redux-actions";

import {
  requestApiType,
  successApiType,
  failureApiType
} from "../../Api/ApiHelpers";
import { fetchEntity, createEntity, cleanEntity } from "./EntityActions";
import { handleEntityAction } from "../EntitiesHelpers";

const initialState = {};

export default handleActions(
  {
    [cleanEntity]: handleEntityAction(state => ({
      ...state,
      item: null,
      loading: false
    })),

    [requestApiType(fetchEntity)]: handleEntityAction(state => ({
      ...state,
      loading: true
    })),

    [successApiType(fetchEntity)]: handleEntityAction((state, action) => ({
      ...state,
      item: action.payload,
      loading: false,
      loaded: true
    })),

    [failureApiType(fetchEntity)]: handleEntityAction(state => ({
      ...state,
      loading: false,
      loaded: false
    })),

    [requestApiType(createEntity)]: handleEntityAction(state => ({
      ...state,
      loading: true,
      loaded: false
    })),

    [successApiType(createEntity)]: handleEntityAction((state, action) => ({
      ...state,
      item: action.payload,
      loading: false,
      loaded: true
    })),

    [failureApiType(createEntity)]: handleEntityAction(state => ({
      ...state,
      loading: false,
      loaded: false
    }))
  },
  initialState
);
