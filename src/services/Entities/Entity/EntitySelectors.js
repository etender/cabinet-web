import { getEntityObject } from "../EntitiesHelpers";

export const getEntity = (state, entity) => getEntityObject(state, entity).item;

export const getEntityLoading = (state, entity) =>
  getEntityObject(state, entity).loading;

export const getEntityLoaded = (state, entity) =>
  getEntityObject(state, entity).loaded;
