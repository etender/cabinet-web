import { takeEvery } from "redux-saga/effects";

import { fetchEntity, createEntity } from "./EntityActions";
import { fetchEntitiesSaga, createEntitySaga } from "../EntitiesSagas";

export default function* entitySagas(entities) {
  yield takeEvery(fetchEntity, fetchEntitiesSaga);
  yield takeEvery(createEntity, createEntitySaga);
}
