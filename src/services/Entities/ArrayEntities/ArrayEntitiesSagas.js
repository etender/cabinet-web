import { takeEvery } from "redux-saga/effects";

import { fetchArrayEntities } from "./ArrayEntitiesActions";
import { fetchEntitiesSaga } from "../EntitiesSagas";

export default function* arrayEntitiesSagas(entities) {
  yield takeEvery(fetchArrayEntities, fetchEntitiesSaga);
}
