import { createAction } from "redux-actions";

export const cleanArrayEntities = createAction(
  "ARRAY_ENTITIES_CLEAN",
  () => {},
  entity => ({ entity })
);

export const fetchArrayEntities = createAction(
  "ARRAY_ENTITIES_FETCH",
  (entity, query) => query,
  entity => ({ entity })
);
