import { getEntityObject } from "../EntitiesHelpers";

export const getArrayEntities = (state, entity) =>
  getEntityObject(state, entity).items;

export const getArrayEntitiesLoading = (state, entity) =>
  getEntityObject(state, entity).loading;
