import { handleActions } from "redux-actions";

import {
  requestApiType,
  successApiType,
  failureApiType
} from "../../Api/ApiHelpers";
import { fetchArrayEntities, cleanArrayEntities } from "./ArrayEntitiesActions";
import { handleEntityAction } from "../EntitiesHelpers";

const initialState = {};

export default handleActions(
  {
    [cleanArrayEntities]: handleEntityAction(state => ({
      ...state,
      items: [],
      loading: true
    })),

    [requestApiType(fetchArrayEntities)]: handleEntityAction(state => ({
      ...state,
      loading: false
    })),

    [successApiType(fetchArrayEntities)]: handleEntityAction(
      (state, action) => ({
        ...state,
        items: [...action.payload],
        loading: false,
        loaded: true
      })
    ),

    [failureApiType(fetchArrayEntities)]: handleEntityAction(state => ({
      ...state,
      loading: false,
      loaded: false
    }))
  },
  initialState
);
