import { takeEvery } from "redux-saga/effects";

import { fetchTreeEntities } from "./TreeEntitiesActions";
import { fetchEntitiesSaga } from "../EntitiesSagas";

export default function* treeEntitiesSagas(entities) {
  yield takeEvery(fetchTreeEntities, fetchEntitiesSaga);
}
