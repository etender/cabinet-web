import { createAction } from "redux-actions";

export const fetchTreeEntities = createAction(
  "TREE_ENTITIES_FETCH",
  (entity, query) => query,
  entity => ({ entity })
);
