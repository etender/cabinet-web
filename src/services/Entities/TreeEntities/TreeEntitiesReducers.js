import { handleActions } from "redux-actions";

import { requestApiType, successApiType } from "../../Api/ApiHelpers";
import { fetchTreeEntities } from "./TreeEntitiesActions";

const initialState = {};

export default handleActions(
  {
    [requestApiType(fetchTreeEntities)]: (state, action) => {
      const { entity } = action.meta;
      const parentId = action.payload.parentId || action.payload.id;
      const entityState = state[entity] || {};
      return {
        ...state,
        [entity]: {
          ...entityState,
          [parentId]: { ...entityState[parentId], loading: true }
        }
      };
    },

    [successApiType(fetchTreeEntities)]: (state, action) => {
      const { entity } = action.meta;
      const entityState = state[entity] || {};
      const parentIds = action.payload
        .map(item => item.parentId)
        .filter((item, index, items) => item && items.indexOf(item) === index);
      return {
        ...state,
        [entity]: {
          ...entityState,
          ...parentIds.reduce(
            (result, item) => ({
              ...result,
              [item]: { ...entityState[item], loading: false, loaded: true }
            }),
            {}
          ),
          ...action.payload.reduce(
            (result, item) => ({ ...result, [item.id]: item }),
            {}
          )
        }
      };
    }
  },
  initialState
);
