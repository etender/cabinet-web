export const getTreeEntities = (state, entity, nodeId) =>
  (state.entities[entity] || {})[nodeId];
