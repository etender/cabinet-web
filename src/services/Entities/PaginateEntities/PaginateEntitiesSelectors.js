import { getEntityObject } from "../EntitiesHelpers";

export const getPaginateEntities = (state, entity) =>
  getEntityObject(state, entity);
