import { createAction } from "redux-actions";

export const cleanPaginateEntities = createAction(
  "PAGINATE_ENTITIES_CLEAN",
  () => {},
  entity => ({ entity })
);

export const fetchPaginateEntities = createAction(
  "PAGINATE_ENTITIES_FETCH",
  (entity, query) => query,
  entity => ({ entity })
);
