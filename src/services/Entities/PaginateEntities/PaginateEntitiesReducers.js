import { handleActions } from "redux-actions";

import { successApiType } from "../../Api/ApiHelpers";
import {
  cleanPaginateEntities,
  fetchPaginateEntities
} from "./PaginateEntitiesActions";
import { handleEntityAction } from "../EntitiesHelpers";

const initialState = {};

export default handleActions(
  {
    [cleanPaginateEntities]: handleEntityAction(state => ({
      ...state,
      total: null,
      items: [],
      loading: false,
      perPage: 25
    })),

    [fetchPaginateEntities]: handleEntityAction((state, action) => {
      const { page } = action.payload;
      let newState = {
        ...state,
        total: null,
        loading: true,
        perPage: 25
      };

      if (!page || page === 0) {
        newState.items = [];
      }

      return newState;
    }),

    [successApiType(fetchPaginateEntities)]: handleEntityAction(
      (
        state,
        {
          payload: {
            items,
            meta: { total }
          }
        }
      ) => ({
        ...state,
        items: items,
        total: total,
        loading: false,
        perPage: 25
      })
    )
  },
  initialState
);
