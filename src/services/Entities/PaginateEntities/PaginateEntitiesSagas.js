import { takeEvery } from "redux-saga/effects";

import { fetchPaginateEntities } from "./PaginateEntitiesActions";
import { fetchEntitiesSaga } from "../EntitiesSagas";

export default function* arrayEntitiesSagas(entities) {
  yield takeEvery(fetchPaginateEntities, fetchEntitiesSaga);
}
