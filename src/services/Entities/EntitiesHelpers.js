import decamelize from "decamelize";

let _entities = {};

export const entityUrl = action => {
  const { entity, prefix } = action.meta;
  let url = _entities[action.type] && _entities[action.type][entity];
  if (!url) {
    url = _entities[entity] || "";
  }

  if (!url) {
    url = "/api/";

    if (prefix) {
      url += prefix;
    }

    url += decamelize(entity);
  }

  return url;
};

export const setEntities = entities => {
  _entities = entities;
};

export const handleEntityAction = reducer => {
  return (state, action) => {
    const { entity } = action.meta;
    let entityState = state[entity] || [];

    return {
      ...state,
      [entity]: reducer(entityState, action)
    };
  };
};

export const getEntityObject = (state, entity) => state.entities[entity] || {};
