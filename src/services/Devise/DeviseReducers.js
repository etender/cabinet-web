import { combineReducers } from "redux";

import auth from "./Auth/AuthReducers";
import invitation from "./Invitation/InvitationReducers";
import users from "./Users/UsersReducers";

export default combineReducers({
  auth,
  invitation,
  users
});
