import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Typography,
  Paper,
  LinearProgress,
  withStyles
} from "@material-ui/core";

import Form from "../../../common/Form/Form";

const styles = theme => ({
  main: {
    marginLeft: "auto",
    marginRight: "auto",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%"
  },
  paper: {
    position: "relative",
    overflow: "hidden",
    [theme.breakpoints.down("xs")]: {
      width: `calc(100% - ${theme.spacing(4)}px)`
    }
  },
  progress: {
    width: "100%",
    position: "absolute"
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(8)}px ${theme.spacing(10)}px`,
    [theme.breakpoints.down("sm")]: {
      padding: `${theme.spacing(2)}px ${theme.spacing(2)}px`
    },
    [theme.breakpoints.up("sm")]: {
      minWidth: 500
    }
  },
  logo: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    width: "200px",
    height: "106px"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing()
  },
  buttons: {
    marginTop: theme.spacing(6)
  },
  header: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(3)
  }
});

class Dialog extends Component {
  static propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    buttons: PropTypes.element,
    action: PropTypes.func,
    formDisabled: PropTypes.bool
  };

  renderForm() {
    const {
      classes,
      title,
      children,
      buttons,
      footer,
      action,
      value
    } = this.props;

    return (
      <Form class={classes.form} value={value} action={action}>
        {({ isSubmitting, ...rest }) => (
          <Fragment>
            {isSubmitting && (
              <LinearProgress className={classes.progress} color="secondary" />
            )}
            <div className={classes.content}>
              <img
                src={require("../../../images/logo-color.svg")}
                width={200}
                height={106}
                alt={title}
                className={classes.logo}
              />
              <Typography
                component="h1"
                variant="h5"
                className={classes.header}
              >
                {title}
              </Typography>

              {children({ ...rest })}
              <div className={classes.buttons}>{buttons}</div>
              {footer}
            </div>
          </Fragment>
        )}
      </Form>
    );
  }

  render() {
    const { classes, children, formDisabled } = this.props;

    return (
      <div className={classes.main}>
        <Paper className={classes.paper}>
          {formDisabled ? children : this.renderForm()}
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(Dialog);
