import { DEVISE_PATH } from "../DeviseConstants";

export const CONFIRMATION_PATH = `${DEVISE_PATH}/confirmation`;
export const API_CONFIRM_PATH = {
  method: "GET",
  url: "/api/user/confirmation"
};
