import { put, call, takeLatest } from "redux-saga/effects";
import { push } from "connected-react-router";

import { callApi } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { confirmUser } from "./ConfirmationActions";
import { INDEX_PATH } from "../DeviseConstants";
import { API_CONFIRM_PATH } from "./ConfirmationConstants";

import { defineMessages } from "react-intl.macro";

const messages = defineMessages({
  confirmSuccess: {
    id: "devise.confirmation-sagas.success",
    defaultMessage: "Ваш аккаунт подтвержден!"
  },
  confirmTokenFailure: {
    id: "devise.confirmation-sagas.token-failure",
    defaultMessage:
      "Невреный токен. Пожалуйста, попробуйте подвердить аккаунт заново."
  },
  confirmEmailFailure: {
    id: "devise.confirmation-sagas.email-failure",
    defaultMessage: "Аккаунт уже подвержден."
  }
});

function* confirmSaga(action) {
  yield put(requestApi(confirmUser));

  try {
    const response = yield call(
      callApi,
      API_CONFIRM_PATH,
      {},
      { confirmationToken: action.payload }
    );

    yield put(showSnackbar(messages.confirmSuccess));
    yield put(successApi(confirmUser, response));
    yield put(push(INDEX_PATH));
  } catch (error) {
    yield put(push(INDEX_PATH));

    if (error.message.email) {
      yield put(showSnackbar(messages.confirmEmailFailure));
    } else if (error.message.confirmationToken) {
      yield put(showSnackbar(messages.confirmTokenFailure));
    }
    yield put(failureApi(confirmUser, error));
  }
}

export default function* confirmationSagas() {
  yield takeLatest(confirmUser, confirmSaga);
}
