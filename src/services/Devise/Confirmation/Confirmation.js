import { Component } from "react";
import PropTypes from "prop-types";

class Confirmation extends Component {
  static propTypes = {
    confirm: PropTypes.func
  };

  componentDidMount() {
    const { confirmUser, token } = this.props;
    confirmUser(token);
  }

  render() {
    return "";
  }
}

export default Confirmation;
