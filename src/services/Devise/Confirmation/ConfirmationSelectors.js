import { createSelector } from "reselect";
import { getLocationSearch } from "../../Router/RouterSelectors";

export const getConfirmationToken = createSelector(
  getLocationSearch,
  search => (search.match(/confirmation_token=(.*)/) || [])[1]
);
