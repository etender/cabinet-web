import React from "react";
import { connect } from "react-redux";

import { confirmUser } from "./ConfirmationActions";
import { getConfirmationToken } from "./ConfirmationSelectors";
import Confirmation from "./Confirmation";

const ConfirmationContainer = props => <Confirmation {...props} />;

const mapStateToProps = state => {
  return {
    token: getConfirmationToken(state)
  };
};

export default connect(mapStateToProps, {
  confirmUser
})(ConfirmationContainer);
