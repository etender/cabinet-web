import { createAction } from "redux-actions";

export const confirmUser = createAction("DEVISE_USER_CONFIRM");
