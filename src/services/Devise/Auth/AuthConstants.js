export const API_SIGN_OUT_PATH = {
  method: "DELETE",
  url: "/api/user/sign_out"
};
export const API_VERIFY_PATH = {
  method: "GET",
  url: "/api/user/registrations"
};
