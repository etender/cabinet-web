import { handleActions } from "redux-actions";
import { successApiType, failureApiType } from "../../Api/ApiHelpers";
import { signOutUser, verifyUser } from "./AuthActions";
import { signInUser } from "../SignIn/SignInActions";
import { confirmUser } from "../Confirmation/ConfirmationActions";

const initialState = {
  success: false,
  verified: false,
  current: {},
  error: null,
  users: [],
  invitations: []
};

export default handleActions(
  {
    [successApiType(signInUser)]: (state, action) => ({
      ...state,
      current: action.payload,
      error: null,
      success: true
    }),

    [failureApiType(signInUser)]: (state, action) => ({
      ...state,
      error: action.payload,
      success: false
    }),

    [successApiType(signOutUser)]: state => ({
      ...state,
      current: {},
      success: false
    }),

    [successApiType(verifyUser)]: (state, action) => ({
      ...state,
      current: action.payload,
      success: true,
      verified: true
    }),

    [failureApiType(verifyUser)]: (state, action) => ({
      ...state,
      verified: true,
      error: action.payload
    }),

    [successApiType(confirmUser)]: (state, action) => ({
      ...state,
      current: action.payload
    })
  },
  initialState
);
