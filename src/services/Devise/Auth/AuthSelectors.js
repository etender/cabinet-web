import { createSelector } from "reselect";

export const getAuth = state => state.devise.auth.success;
export const getAuthError = state => state.devise.auth.error;
export const getCurrentUser = state => state.devise.auth.current;
export const getVerified = state => state.devise.auth.verified;

export const getCurrentUserRoles = createSelector(getCurrentUser, user => ({
  admin: user.admin,
  coordinator: user.coordinator
}));

export const getTimeZone = createSelector(getCurrentUser, user =>
  user ? user.timeZone : null
);

export const isTariffLight = createSelector(getCurrentUser, user =>
  user && user.company
    ? ["light_full", "light_base"].includes(user.company.tariff)
    : false
);

export const isReadOnly = createSelector(getCurrentUser, user => user.readOnly);
