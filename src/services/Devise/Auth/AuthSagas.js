import { put, call, fork, cancel, join, take } from "redux-saga/effects";
import { callApi, failureApiType } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { signInUser } from "../SignIn/SignInActions";
import { API_SIGN_OUT_PATH, API_VERIFY_PATH } from "./AuthConstants";
import { signOutUser, verifyUser } from "./AuthActions";
import { showRegistrationExpired } from "../../../modules/RegistrationExpired/RegistrationExpiredActions";

import signInSaga from "../SignIn/SignInSagas";

export default function* authSaga() {
  let verified = yield call(verifySaga);

  let signInTask, signOutTask;

  while (true) {
    if (!verified) {
      const action = yield take(signInUser);

      // Wait until sign out will be completted
      if (signOutTask) {
        yield join(signOutTask);
        signOutTask = null;
      }

      signInTask = yield fork(signInSaga, { user: action.payload });
    }
    const action = yield take([signOutUser, failureApiType(signInUser)]);

    if (action.type === signOutUser.toString()) {
      // Cancel current sign in task
      if (signInTask) {
        yield cancel(signInTask);
        signInTask = null;
      }

      signOutTask = yield fork(signOutSaga);

      verified = false;
    }
  }
}

export function* verifySaga() {
  yield put(requestApi(verifyUser));

  try {
    const response = yield call(callApi, API_VERIFY_PATH);
    yield put(successApi(verifyUser, response));
    return true;
  } catch (error) {
    yield put(failureApi(verifyUser, error));

    if (error.status === 402) {
      yield put(showRegistrationExpired());
    }
    return false;
  }
}

function* signOutSaga() {
  yield put(requestApi(signOutUser));

  try {
    yield call(callApi, API_SIGN_OUT_PATH);
    yield put(successApi(signOutUser));
  } catch (error) {
    yield put(showSnackbar(error.message));
    yield put(failureApi(signOutUser, error));
  }
}
