import { createAction } from "redux-actions";

export const signOutUser = createAction("DEVISE_USER_SIGN_OUT", () => {});
export const verifyUser = createAction("DEVISE_USER_VERIFY");
