import { all, fork } from "redux-saga/effects";

import sagaMiddleware from "../../utils/SagaUtils";

import authSagas from "./Auth/AuthSagas";
import confirmationSagas from "./Confirmation/ConfirmationSagas";
import invitationSagas from "./Invitation/InvitationSagas";
import passwordRecoverySagas from "./PasswordRecovery/PasswordRecoverySagas";
import passwordResetSagas from "./PasswordReset/PasswordResetSagas";
import usersSagas from "./Users/UsersSagas";

function* deviseSagas() {
  yield all([
    fork(authSagas),
    fork(confirmationSagas),
    fork(invitationSagas),
    fork(passwordRecoverySagas),
    fork(passwordResetSagas),
    fork(usersSagas)
  ]);
}

sagaMiddleware.run(deviseSagas);
