import { createSelector } from "reselect";
import { getLocationSearch } from "../../Router/RouterSelectors";

export const getInvitations = state => {
  return state.devise.invitation.invitations;
};
export const getInvitable = state => {
  return state.devise.invitation.invitable;
};

export const getInvitationToken = createSelector(
  getLocationSearch,
  search => (search.match(/invitation_token=(.*)/) || [])[1]
);
