import { DEVISE_PATH } from "../DeviseConstants";

export const INVITATION_PATH = `${DEVISE_PATH}/invitation`;
export const API_INVITATOIN_PATH = {
  method: "GET",
  url: "/api/user/invitation"
};
export const API_INVITE_PATH = { method: "POST", url: "/api/user/invitation" };
export const API_ACCEPT_INVITATOIN_PATH = {
  method: "PUT",
  url: "/api/user/invitation"
};
export const API_DELETE_INVITATOIN_PATH = {
  method: "DELETE",
  url: "/api/user/invitations/:id"
};
