import { all, put, call, takeEvery, takeLatest } from "redux-saga/effects";
import { push } from "connected-react-router";
import { defineMessages } from "react-intl.macro";

import { callApi } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { fetchEntitiesSaga } from "../../Entities/EntitiesSagas";
import { verifySaga } from "../Auth/AuthSagas";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import {
  fetchInvitations,
  fetchInvitation,
  inviteUser,
  acceptInvitation,
  deleteInvitation
} from "./InvitationActions";
import { INDEX_PATH } from "../DeviseConstants";
import {
  API_INVITE_PATH,
  API_INVITATOIN_PATH,
  API_ACCEPT_INVITATOIN_PATH,
  API_DELETE_INVITATOIN_PATH
} from "./InvitationConstants";

const messages = defineMessages({
  acceptInvitationSuccess: {
    id: "devise.invitation.success",
    defaultMessage: "Ваш аккаунт подтвержден!"
  },
  invitationTokenFailure: {
    id: "devise.invitation.token-failure",
    defaultMessage:
      "Невреный токен. Пожалуйста, попробуйте подвердить аккаунт заново."
  },
  invitationEmailFailure: {
    id: "devise.invitation.email-failure",
    defaultMessage: "Не удалось добавить пользователя с таким email."
  }
});

function* inviteSaga(action) {
  yield put(requestApi(inviteUser, action.payload, action.meta));

  try {
    const payload = { ...action.payload };
    payload[payload.role] = true;
    delete payload.role;
    const response = yield call(
      callApi,
      API_INVITE_PATH,
      {},
      { user: payload }
    );
    yield put(successApi(inviteUser, response, action.meta));
  } catch (error) {
    if (error.message.email) {
      yield put(showSnackbar(messages.invitationEmailFailure));
    }
    yield put(failureApi(inviteUser, error, action.meta));
  }
}

function* acceptInvitationSaga(action) {
  yield put(requestApi(acceptInvitation, action.payload, action.meta));

  try {
    const response = yield call(callApi, API_ACCEPT_INVITATOIN_PATH, {
      user: action.payload
    });

    yield put(showSnackbar(messages.acceptInvitationSuccess));
    yield put(successApi(acceptInvitation, response, action.meta));

    // After successful accept invitation, verifySaga user.
    yield call(verifySaga);

    yield put(push(INDEX_PATH));
  } catch (error) {
    if (error.message.invitationToken) {
      yield put(showSnackbar(messages.invitationTokenFailure));
    }

    yield put(failureApi(acceptInvitation, error, action.meta));
  }
}

function* deleteInvitationSaga(action) {
  yield put(requestApi(deleteInvitation, action.payload, action.meta));

  try {
    yield call(callApi, API_DELETE_INVITATOIN_PATH, null, {
      id: action.payload
    });

    yield put(successApi(deleteInvitation, action.payload, action.meta));
  } catch (error) {
    yield put(failureApi(deleteInvitation, error, action.meta));
  }
}

function* fetchInvitationSaga(action) {
  yield put(requestApi(fetchInvitation, action.payload, action.meta));

  try {
    yield call(callApi, API_INVITATOIN_PATH, null, action.payload);
    yield put(successApi(fetchInvitation, action.payload, action.meta));
  } catch (error) {
    yield put(failureApi(fetchInvitation, error, action.meta));
    yield put(push(INDEX_PATH));
  }
}

export default function* invitationSagas() {
  yield all([
    takeEvery(inviteUser, inviteSaga),
    takeEvery(acceptInvitation, acceptInvitationSaga),
    takeLatest(deleteInvitation, deleteInvitationSaga),
    takeLatest(fetchInvitations, fetchEntitiesSaga),
    takeLatest(fetchInvitation, fetchInvitationSaga)
  ]);
}
