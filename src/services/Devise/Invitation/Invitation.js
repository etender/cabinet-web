import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Button, TextField, withStyles } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dialog from "../Dialog/Dialog";

const messages = defineMessages({
  passwordField: {
    id: "devise.invitation.password-field",
    defaultMessage: "Пароль"
  },
  passwordConfirmationField: {
    id: "devise.invitation.password-confirmation",
    defaultMessage: "Подверждение пароля"
  },
  continueButton: {
    id: "devise.invitation.continue-button",
    defaultMessage: "Продолжить"
  },
  nameField: {
    id: "devise.invitation.name-field",
    defaultMessage: "Имя"
  },
  surnameField: {
    id: "devise.invitation.surname-field",
    defaultMessage: "Фамилия"
  },
  patronymicField: {
    id: "devise.invitation.patronymic-field",
    defaultMessage: "Отчество"
  }
});

const styles = theme => ({});

class Invitation extends Component {
  static propTypes = {
    acceptInvitation: PropTypes.func.isRequired,
    fetchInvitation: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { fetchInvitation, token } = this.props;
    fetchInvitation(token);
  }

  handleClose = () => {
    this.props.onClose();
  };

  handleAccept = (value, form) => {
    const { acceptInvitation, token } = this.props;
    acceptInvitation({ ...value, invitationToken: token }, form);
  };

  render() {
    const { intl, invitable } = this.props;
    const { formatMessage } = intl;

    if (!invitable) {
      return "";
    }

    return (
      <Dialog
        action={this.handleAccept}
        buttons={
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
          >
            {formatMessage(messages.continueButton)}
          </Button>
        }
      >
        {({ field, getValue, getBooleanValue, handleInputChange, error }) => (
          <Fragment>
            <TextField
              name="surname"
              inputRef={field}
              value={getValue("surname")}
              onChange={handleInputChange}
              required
              fullWidth
              label={formatMessage(messages.surnameField)}
              InputLabelProps={{ required: true }}
            />
            <TextField
              name="name"
              inputRef={field}
              value={getValue("name")}
              onChange={handleInputChange}
              required
              fullWidth
              label={formatMessage(messages.nameField)}
              InputLabelProps={{ required: true }}
            />
            <TextField
              name="patronymic"
              inputRef={field}
              value={getValue("patronymic")}
              onChange={handleInputChange}
              fullWidth
              label={formatMessage(messages.patronymicField)}
              InputLabelProps={{ required: false }}
            />

            <TextField
              name="password"
              type="password"
              inputRef={field}
              value={getValue("password")}
              onChange={handleInputChange}
              required
              fullWidth
              autoComplete="new-password"
              label={formatMessage(messages.passwordField)}
              error={!!error("password")}
              helperText={error("password")}
              InputLabelProps={{ required: true }}
            />
            <TextField
              name="passwordConfirmation"
              type="password"
              inputRef={field}
              value={getValue("passwordConfirmation")}
              onChange={handleInputChange}
              required
              fullWidth
              autoComplete="new-password"
              label={formatMessage(messages.passwordConfirmationField)}
              error={!!error("passwordConfirmation")}
              helperText={error("passwordConfirmation")}
              InputLabelProps={{ required: true }}
            />
          </Fragment>
        )}
      </Dialog>
    );
  }
}

export default injectIntl(withStyles(styles)(Invitation));
