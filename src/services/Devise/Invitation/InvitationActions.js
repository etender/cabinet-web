import { createAction } from "redux-actions";

export const fetchInvitations = createAction(
  "DEVISE_INVITATIONS_FETCH",
  () => {},
  () => ({ entity: "invitations" })
);

export const fetchInvitation = createAction(
  "DEVISE_INVITATION_FETCH",
  invitationToken => ({ invitationToken }),
  () => ({ entity: "invitation" })
);

export const inviteUser = createAction(
  "DEVISE_USER_INVITE",
  value => value,
  (value, form) => ({ form })
);

export const acceptInvitation = createAction(
  "DEVISE_INVITATION_ACCEPT",
  value => value,
  (value, form) => ({ form })
);

export const deleteInvitation = createAction("DEVISE_INVITATION_DELETE");
