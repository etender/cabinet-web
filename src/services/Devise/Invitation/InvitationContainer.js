import React from "react";
import { connect } from "react-redux";

import { acceptInvitation, fetchInvitation } from "./InvitationActions";
import { getInvitationToken, getInvitable } from "./InvitationSelectors";
import Invitation from "./Invitation";

const InvitationContainer = props => <Invitation {...props} />;

const mapStateToProps = state => {
  return {
    token: getInvitationToken(state),
    invitable: getInvitable(state)
  };
};

export default connect(mapStateToProps, {
  fetchInvitation,
  acceptInvitation
})(InvitationContainer);
