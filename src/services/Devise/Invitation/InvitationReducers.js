import { handleActions } from "redux-actions";
import { successApiType } from "../../Api/ApiHelpers";
import {
  fetchInvitations,
  fetchInvitation,
  inviteUser,
  deleteInvitation
} from "./InvitationActions";

const initialState = {
  invitations: [],
  invitable: false
};

export default handleActions(
  {
    [successApiType(fetchInvitations)]: (state, action) => ({
      ...state,
      invitations: action.payload
    }),

    [successApiType(fetchInvitation)]: (state, action) => ({
      ...state,
      invitable: true
    }),

    [successApiType(inviteUser)]: (state, action) => {
      if (!action.payload.invited) {
        return state;
      }

      return {
        ...state,
        invitations: state.invitations.some(
          user => user.id === action.payload.id
        )
          ? state.invitations
          : [...state.invitations, action.payload]
      };
    },

    [successApiType(deleteInvitation)]: (state, action) => ({
      ...state,
      invitations: state.invitations.filter(user => user.id !== action.payload)
    })
  },
  initialState
);
