import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import Router from "../Router/Router";

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  }
});

class Devise extends Component {
  static propTypes = {
    auth: PropTypes.bool.isRequired,
    routes: PropTypes.shape({}).isRequired
  };

  render() {
    const classes = this.props.classes;
    const { routes, auth, verified } = this.props;

    return (
      <div className={classes.root}>
        <Router routes={routes} auth={auth} verified={verified} />
      </div>
    );
  }
}

export default withStyles(styles)(Devise);
