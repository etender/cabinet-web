import { createAction } from "redux-actions";

export const resetPassword = createAction(
  "DEVISE_PASSWORD_RESET",
  (token, password, passwordConfirmation) => ({
    token,
    password,
    passwordConfirmation
  }),
  (token, password, passwordConfirmation, form) => ({ form })
);
