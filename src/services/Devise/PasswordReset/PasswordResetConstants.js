import { DEVISE_PATH } from "../DeviseConstants";

export const PASSWORD_RESET_PATH = `${DEVISE_PATH}/password/edit`;
export const API_PASSWORD_RESET_PATH = {
  method: "PUT",
  url: "/api/user/password"
};
