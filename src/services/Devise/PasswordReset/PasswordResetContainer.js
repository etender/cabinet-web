import React from "react";
import { connect } from "react-redux";

import { resetPassword } from "./PasswordResetActions";
import { getResetPasswordToken } from "./PasswordResetSelectors";
import PasswordReset from "./PasswordReset";

const PasswordResetContainer = props => <PasswordReset {...props} />;

const mapStateToProps = state => {
  return {
    token: getResetPasswordToken(state)
  };
};

export default connect(mapStateToProps, {
  resetPassword
})(PasswordResetContainer);
