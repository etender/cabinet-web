import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Button, TextField } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dialog from "../Dialog/Dialog";

const messages = defineMessages({
  title: {
    id: "devise.password-reset.title",
    defaultMessage: "Сброс пароля"
  },
  passwordField: {
    id: "devise.password-reset.password-field",
    defaultMessage: "Пароль"
  },
  passwordConfirmationField: {
    id: "devise.password-reset.password-confirmation-field",
    defaultMessage: "Подтвердите пароль"
  },
  recoverButton: {
    id: "devise.password-reset.recover",
    defaultMessage: "Восстановить"
  }
});

class PasswordReset extends Component {
  static propTypes = {
    resetPassword: PropTypes.func.isRequired
  };

  onReset = ({ password, passwordConfirmation }, form) => {
    const { resetPassword, token } = this.props;
    resetPassword(token, password, passwordConfirmation, form);
  };

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <Dialog
        title={formatMessage(messages.title)}
        action={this.onReset}
        buttons={
          <Button type="submit" fullWidth variant="contained" color="primary">
            {formatMessage(messages.recoverButton)}
          </Button>
        }
      >
        {({ field, error, getValue, handleInputChange }) => (
          <Fragment>
            <TextField
              name="password"
              type="password"
              inputRef={field}
              value={getValue("password")}
              onChange={handleInputChange}
              required
              fullWidth
              autoComplete="new-password"
              label={formatMessage(messages.passwordField)}
              error={!!error("password")}
              helperText={error("password")}
              InputLabelProps={{ required: false }}
            />
            <TextField
              name="passwordConfirmation"
              type="password"
              inputRef={field}
              value={getValue("passwordConfirmation")}
              onChange={handleInputChange}
              required
              fullWidth
              autoComplete="new-password"
              label={formatMessage(messages.passwordConfirmationField)}
              error={!!error("passwordConfirmation")}
              helperText={error("passwordConfirmation")}
              InputLabelProps={{ required: false }}
            />
          </Fragment>
        )}
      </Dialog>
    );
  }
}

export default injectIntl(PasswordReset);
