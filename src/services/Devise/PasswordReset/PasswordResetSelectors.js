import { createSelector } from "reselect";
import { getLocationSearch } from "../../Router/RouterSelectors";

export const getResetPasswordToken = createSelector(
  getLocationSearch,
  search => (search.match(/reset_password_token=(.*)/) || [])[1]
);
