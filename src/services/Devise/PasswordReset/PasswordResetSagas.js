import { call, put, takeEvery } from "redux-saga/effects";
import { push } from "connected-react-router";

import { callApi } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { verifySaga } from "../Auth/AuthSagas";
import { INDEX_PATH } from "../DeviseConstants";
import { PASSWORD_RECOVERY_PATH } from "../PasswordRecovery/PasswordRecoveryConstants";
import { API_PASSWORD_RESET_PATH } from "./PasswordResetConstants";
import { resetPassword } from "./PasswordResetActions";

import { defineMessages } from "react-intl.macro";

const messages = defineMessages({
  passwordResetSuccess: {
    id: "devise.password-reset.success",
    defaultMessage: "Пароль изменен успешно."
  },
  passwordResetFailure: {
    id: "devise.password-reset.failure",
    defaultMessage: "Неудалось изменить пароль. Попробуйте снова."
  },
  passwordRecoverSuccess: {
    id: "devise.password-reset.recovery-success",
    defaultMessage: "Инструкции по сбросу пароля отправлены на ваш email!"
  }
});

export function* resetPasswordSaga(action) {
  yield put(requestApi(resetPassword, action.payload, action.meta));

  try {
    const {
      payload: { token, password, passwordConfirmation }
    } = action;
    const response = yield call(callApi, API_PASSWORD_RESET_PATH, {
      user: {
        reset_password_token: token,
        password,
        password_confirmation: passwordConfirmation
      }
    });

    yield put(successApi(resetPassword, response, action.meta));

    // After successful password reset server authenticate user.
    // Verify user, redirect to index and show password reset success message.
    yield call(verifySaga);
    yield put(push(INDEX_PATH));
    yield put(showSnackbar(messages.passwordResetSuccess));
  } catch (error) {
    // If password reset token is invalid
    if (error.message.resetPasswordToken) {
      // redirect to password recovery and show message to try again
      yield put(push(PASSWORD_RECOVERY_PATH));
      yield put(showSnackbar(messages.passwordResetFailure));
    } else {
      // set form errors
      // form.submitted(error.message);
      // yield put(failureApi(resetPassword, error, action.meta));
    }
    yield put(failureApi(resetPassword, error, action.meta));
  }
}

export default function* passwordResetSagas() {
  yield takeEvery(resetPassword, resetPasswordSaga);
}
