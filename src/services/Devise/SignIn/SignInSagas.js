import { put, call } from "redux-saga/effects";
import { replace } from "connected-react-router";

import { callApi } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { INDEX_PATH } from "../DeviseConstants";
import { API_SIGN_IN_PATH } from "./SignInConstants";
import { signInUser } from "./SignInActions";

export default function* signInSaga(options) {
  yield put(requestApi(signInUser));

  // Enable form submitting
  // form.submitting();

  try {
    // const payload = { user: { email, password, remember_me: rememberMe } };
    const response = yield call(callApi, API_SIGN_IN_PATH, options);

    // Disable form submitting
    // form.submitted();

    yield put(successApi(signInUser, response));

    // let path = INDEX_PATH;
    let path = localStorage.getItem("redirectTo");
    if (path) {
      localStorage.removeItem("redirectTo");
    }

    yield put(replace(path || INDEX_PATH));
  } catch (error) {
    // Disable form submitting
    // form.submitted(error.message);

    if (error.status === 401) {
      yield put(showSnackbar(error.message));
    }

    yield put(failureApi(signInUser, error));
  }
}
