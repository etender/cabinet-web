import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import {
  Typography,
  Button,
  TextField,
  FormControlLabel,
  Checkbox,
  withStyles
} from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dialog from "../Dialog/Dialog";
import Error from "../../../common/Error/Error";

const messages = defineMessages({
  title: {
    id: "devise.sign-in.title",
    defaultMessage: "Вход в кабинет"
  },
  emailField: {
    id: "devise.sign-in.email-field",
    defaultMessage: "EMail"
  },
  passwordField: {
    id: "devise.sign-in.password-field",
    defaultMessage: "Пароль"
  },
  passwordRecovery: {
    id: "devise.sign-in.password-recovery",
    defaultMessage: "Забыли пароль?"
  },
  signInButton: {
    id: "devise.sign-in.sign-in-button",
    defaultMessage: "Авторизоваться"
  },
  rememberMe: {
    id: "devise.sign-in.remember-me",
    defaultMessage: "Запомнить меня"
  }
});

const styles = theme => ({
  passwordRecovery: {
    marginTop: theme.spacing(2),
    display: "block",
    textAlign: "right",
    width: "100%",
    color: theme.palette.primary.main,
    textDecoration: "underline",
    cursor: "pointer"
  },

  rememberMe: {
    marginRight: "auto"
  }
});

class SignIn extends Component {
  static propTypes = {
    signInUser: PropTypes.func.isRequired,
    showPasswordRecover: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    guid: PropTypes.string,
    verified: PropTypes.bool
  };

  componentDidMount() {
    this.signIn();
  }

  componentDidUpdate(prevProps) {
    this.signIn();
  }

  signIn() {
    if (this.signedIn) {
      return;
    }

    const { verified, guid, secret, signInUser, signOutUser } = this.props;

    if (!verified) {
      return;
    }

    this.signedIn = true;

    if (guid) {
      signOutUser();
      signInUser({ guid });
    }

    if (secret) {
      signOutUser();
      signInUser({ secret });
    }
  }

  handleAction = ({ email, password, rememberMe }, form) => {
    const { signInUser } = this.props;
    signInUser({ email, password, rememberMe }, form);
  };

  handlePasswordRecovery = () => {
    const { showPasswordRecover } = this.props;
    showPasswordRecover();
  };

  render() {
    const { classes, guid, secret, authError } = this.props;
    const { formatMessage } = this.props.intl;

    if (guid || secret) {
      return authError ? <Error error={authError} /> : "";
    }

    return (
      <Dialog
        title={formatMessage(messages.title)}
        action={this.handleAction}
        buttons={
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
          >
            {formatMessage(messages.signInButton)}
          </Button>
        }
      >
        {({ field, getValue, getBooleanValue, handleInputChange }) => (
          <Fragment>
            <TextField
              name="email"
              inputRef={field}
              value={getValue("email")}
              onChange={handleInputChange}
              required
              fullWidth
              label={formatMessage(messages.emailField)}
              InputLabelProps={{ required: false }}
              autoComplete="email"
            />
            <TextField
              name="password"
              type="password"
              inputRef={field}
              value={getValue("password")}
              onChange={handleInputChange}
              required
              fullWidth
              label={formatMessage(messages.passwordField)}
              InputLabelProps={{ required: false }}
              autoComplete="current-password"
            />
            <Typography
              variant="body1"
              className={classes.passwordRecovery}
              onClick={this.handlePasswordRecovery}
            >
              {formatMessage(messages.passwordRecovery)}
            </Typography>
            <FormControlLabel
              className={classes.rememberMe}
              control={
                <Checkbox
                  name="rememberMe"
                  color="primary"
                  inputRef={field}
                  checked={getBooleanValue("rememberMe", "checkbox")}
                  onChange={handleInputChange}
                />
              }
              label={formatMessage(messages.rememberMe)}
            />
          </Fragment>
        )}
      </Dialog>
    );
  }
}

export default injectIntl(withStyles(styles)(SignIn));
