import { DEVISE_PATH } from "../DeviseConstants";

export const SIGN_IN_PATH = `${DEVISE_PATH}/sign-in`;
export const PRIVATE_SIGN_IN_PATH = `${DEVISE_PATH}/private/sign-in`;
export const FOREIGN_SIGN_IN_PATH = `${DEVISE_PATH}/foreign/sign-in`;
export const API_SIGN_IN_PATH = { method: "POST", url: "/api/user/sign_in" };
