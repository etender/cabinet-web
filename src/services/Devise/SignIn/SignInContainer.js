import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import { getAuthError, getVerified } from "../Auth/AuthSelectors";
import { signInUser } from "./SignInActions";
import { signOutUser } from "../../../services/Devise/DeviseActions";
import { getSignInGuid, getSignInSecret } from "./SignInSelectors";
import { showPasswordRecover } from "../PasswordRecovery/PasswordRecoveryActions";
import SignIn from "./SignIn";

const SignInContainer = props => <SignIn {...props} />;

const mapStateToProps = state => {
  return {
    guid: getSignInGuid(state),
    secret: getSignInSecret(state),
    authError: getAuthError(state),
    verified: getVerified(state)
  };
};

export default connect(mapStateToProps, {
  signInUser,
  signOutUser,
  showPasswordRecover,
  push
})(SignInContainer);
