import { createSelector } from "reselect";
import { getLocationSearch } from "../../Router/RouterSelectors";

export const getSignInGuid = createSelector(
  getLocationSearch,
  search => (search.match(/data=(.*)/) || [])[1]
);

export const getSignInSecret = createSelector(
  getLocationSearch,
  search => (search.match(/secret=(.*)/) || [])[1]
);
