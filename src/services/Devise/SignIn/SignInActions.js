import { createAction } from "redux-actions";

export const signInUser = createAction(
  "DEVISE_USER_SIGN_IN",
  options => options,
  (options, form) => ({ form })
);
