import { call, put, takeEvery } from "redux-saga/effects";
import { push } from "connected-react-router";
import { defineMessages } from "react-intl.macro";

import { callApi } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { recoverPassword } from "./PasswordRecoveryActions";
import { SIGN_IN_PATH } from "../SignIn/SignInConstants";
import { API_PASSWORD_RECOVERY_PATH } from "./PasswordRecoveryConstants";

const messages = defineMessages({
  passwordResetSuccess: {
    id: "devise.password-recovery.success",
    defaultMessage: "Пароль изменен успешно."
  },
  passwordResetFailure: {
    id: "devise.password-recovery.failure",
    defaultMessage: "Неудалось изменить пароль. Попробуйте снова."
  },
  passwordRecoverSuccess: {
    id: "devise.password-recovery.recovery-success",
    defaultMessage: "Инструкции по сбросу пароля отправлены на ваш email!"
  }
});

export function* recoverPasswordSaga(action) {
  const {
    meta: { form }
  } = action;

  form.submitting();

  yield put(requestApi(recoverPassword));

  try {
    const {
      payload: { email }
    } = action;
    const response = yield call(callApi, API_PASSWORD_RECOVERY_PATH, {
      user: { email }
    });

    form.submitted();

    yield put(successApi(recoverPassword, response));

    // Redirect to sing in and show password recovery success message
    yield put(push(SIGN_IN_PATH));
    yield put(showSnackbar(messages.passwordRecoverSuccess));
  } catch (error) {
    // Disable form submitting and set errors
    form.submitted(error.message);

    yield put(showSnackbar(messages.passwordResetFailure));
    yield put(failureApi(recoverPassword, error));
  }
}

export default function* passwordRecoverySagas() {
  yield takeEvery(recoverPassword, recoverPasswordSaga);
}
