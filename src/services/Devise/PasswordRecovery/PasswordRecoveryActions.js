import { createAction } from "redux-actions";
import { showLink } from "../../Api/ApiActions";
import { PASSWORD_RECOVERY_PATH } from "./PasswordRecoveryConstants";

export const showPasswordRecover = createAction(
  showLink.toString(),
  () => PASSWORD_RECOVERY_PATH
);
export const recoverPassword = createAction(
  "DEVISE_PASSWORD_RECOVER",
  email => ({ email }),
  (email, form) => ({ form })
);
