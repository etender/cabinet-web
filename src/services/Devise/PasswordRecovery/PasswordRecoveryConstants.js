import { DEVISE_PATH } from "../DeviseConstants";

export const PASSWORD_RECOVERY_PATH = `${DEVISE_PATH}/password`;
export const API_PASSWORD_RECOVERY_PATH = {
  method: "POST",
  url: "/api/user/password"
};
