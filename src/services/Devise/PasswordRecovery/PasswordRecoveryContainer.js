import React from "react";
import { connect } from "react-redux";

import { recoverPassword } from "./PasswordRecoveryActions";
import PasswordRecovery from "./PasswordRecovery";

const PasswordRecoveryContainer = props => <PasswordRecovery {...props} />;

const mapStateToProps = () => {
  return {};
};

export default connect(mapStateToProps, {
  recoverPassword
})(PasswordRecoveryContainer);
