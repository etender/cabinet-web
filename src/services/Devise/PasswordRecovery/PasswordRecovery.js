import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, TextField } from "@material-ui/core";
import { defineMessages } from "react-intl.macro";
import { injectIntl } from "react-intl";

import Dialog from "../Dialog/Dialog";

const messages = defineMessages({
  title: {
    id: "devise.password-recovery.title",
    defaultMessage: "Восстановление пароля"
  },
  emailField: {
    id: "devise.password-recovery.email-field",
    defaultMessage: "EMail"
  },
  recoverButton: {
    id: "devise.password-recovery.recover",
    defaultMessage: "Восстановить"
  }
});

class PasswordRecovery extends Component {
  static propTypes = {
    recoverPassword: PropTypes.func.isRequired
  };

  onRecover = ({ email }, form) => {
    const { recoverPassword } = this.props;

    recoverPassword(email, form);
  };

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <Dialog
        title={formatMessage(messages.title)}
        action={this.onRecover}
        buttons={
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
          >
            {formatMessage(messages.recoverButton)}
          </Button>
        }
      >
        {({ field, error, getValue, handleInputChange }) => (
          <TextField
            name="email"
            required
            inputRef={field}
            value={getValue("email")}
            onChange={handleInputChange}
            fullWidth
            label={formatMessage(messages.emailField)}
            autoComplete="email"
            error={!!error("email")}
            helperText={error("email")}
            InputLabelProps={{ required: false }}
          />
        )}
      </Dialog>
    );
  }
}

export default injectIntl(PasswordRecovery);
