import { handleActions } from "redux-actions";
import { successApiType } from "../../Api/ApiHelpers";
import { updateUser, deleteUser, fetchUsers } from "./UsersActions";
import { inviteUser } from "../Invitation/InvitationActions";

const initialState = [];

export default handleActions(
  {
    [successApiType(fetchUsers)]: (state, action) => action.payload,

    [successApiType(updateUser)]: (state, action) =>
      state.map(user =>
        user.id === action.payload.id ? { ...user, ...action.payload } : user
      ),

    [successApiType(deleteUser)]: (state, action) =>
      state.filter(user => user.id !== action.payload),

    [successApiType(inviteUser)]: (state, action) => {
      if (action.payload.invited) {
        return state;
      }

      return state.some(user => user.id === action.payload.id)
        ? state
        : [...state, action.payload];
    }
  },
  initialState
);
