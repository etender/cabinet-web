import { createAction } from "redux-actions";

export const updateUser = createAction(
  "DEVISE_USER_UPDATE",
  value => value,
  (value, form) => ({ form })
);

export const cancelUpdateUser = createAction("DEVISE_UPDATE_USER_CANCEL");

export const fetchUsers = createAction(
  "DEVISE_USERS_FETCH",
  () => {},
  () => ({ entity: "users" })
);

export const deleteUser = createAction("DEVISE_USER_DELETE");
