export const API_UPDATE_PATH = {
  method: "PUT",
  url: "/api/user/registrations"
};

export const API_CANCEL_UPDATE_PATH = {
  method: "DELETE",
  url: "/api/user/confirmation"
};

export const API_DELETE_PATH = {
  method: "DELETE",
  url: "/api/users/:user_id"
};
