import { all, put, call, takeLatest, takeEvery } from "redux-saga/effects";
import { callApi } from "../../Api/ApiHelpers";
import { requestApi, successApi, failureApi } from "../../Api/ApiActions";
import { fetchEntitiesSaga } from "../../Entities/EntitiesSagas";
import { showSnackbar } from "../../../common/Snackbar/SnackbarActions";
import { verifySaga } from "../Auth/AuthSagas";
import {
  API_UPDATE_PATH,
  API_CANCEL_UPDATE_PATH,
  API_DELETE_PATH
} from "./UsersConstants";
import {
  fetchUsers,
  updateUser,
  cancelUpdateUser,
  deleteUser
} from "./UsersActions";

function* updateUserSaga(action) {
  yield put(requestApi(updateUser, action.payload, action.meta));

  try {
    yield call(callApi, API_UPDATE_PATH, { user: action.payload });
    yield call(verifySaga);
    yield put(successApi(updateUser, action.payload, action.meta));
  } catch (error) {
    yield put(failureApi(updateUser, error, action.meta));
  }
}

function* cancelUpdateUserSaga(action) {
  yield put(requestApi(cancelUpdateUser, action.payload, action.meta));

  try {
    yield call(callApi, API_CANCEL_UPDATE_PATH, { user: action.payload });
    yield call(verifySaga);
    yield put(successApi(cancelUpdateUser, action.payload, action.meta));
  } catch (error) {
    yield put(failureApi(cancelUpdateUser, error, action.meta));
  }
}

function* deleteUserSaga(action) {
  yield put(requestApi(deleteUser, action.payload, action.meta));

  try {
    yield call(callApi, API_DELETE_PATH, { user_id: action.payload }, {});
    yield put(successApi(deleteUser, action.payload, action.meta));
  } catch (error) {
    if (error.message.base) {
      yield put(showSnackbar(error.message.base[0]));
    }
    yield put(failureApi(deleteUser, error, action.meta));
  }
}

export default function* usersSagas() {
  yield all([
    takeLatest(fetchUsers, fetchEntitiesSaga),
    takeEvery(updateUser, updateUserSaga),
    takeEvery(cancelUpdateUser, cancelUpdateUserSaga),
    takeEvery(deleteUser, deleteUserSaga)
  ]);
}
