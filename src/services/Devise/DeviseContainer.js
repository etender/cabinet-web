import React, { lazy } from "react";
import { connect } from "react-redux";

import "./DeviseSagas";
import Devise from "./Devise";
import { CONFIRMATION_PATH } from "./Confirmation/ConfirmationConstants";
import { INVITATION_PATH } from "./Invitation/InvitationConstants";
import { PASSWORD_RECOVERY_PATH } from "./PasswordRecovery/PasswordRecoveryConstants";
import { PASSWORD_RESET_PATH } from "./PasswordReset/PasswordResetConstants";
import {
  SIGN_IN_PATH,
  PRIVATE_SIGN_IN_PATH,
  FOREIGN_SIGN_IN_PATH
} from "./SignIn/SignInConstants";

import { getVerified, getAuth, getSignInGuid } from "./DeviseSelectors";

const SignInContainer = lazy(() => import("./SignIn/SignInContainer"));
const PasswordRecoveryContainer = lazy(() =>
  import("./PasswordRecovery/PasswordRecoveryContainer")
);
const PasswordResetContainer = lazy(() =>
  import("./PasswordReset/PasswordResetContainer")
);
const ConfirmationContainer = lazy(() =>
  import("./Confirmation/ConfirmationContainer")
);
const InvitationContainer = lazy(() =>
  import("./Invitation/InvitationContainer")
);

const routes = {
  [PRIVATE_SIGN_IN_PATH]: { container: SignInContainer, secure: undefined },
  [FOREIGN_SIGN_IN_PATH]: { container: SignInContainer, secure: undefined },
  [SIGN_IN_PATH]: { container: SignInContainer, secure: false },
  [PASSWORD_RESET_PATH]: { container: PasswordResetContainer },
  [PASSWORD_RECOVERY_PATH]: {
    container: PasswordRecoveryContainer,
    secure: false
  },
  [CONFIRMATION_PATH]: { container: ConfirmationContainer },
  [INVITATION_PATH]: { container: InvitationContainer }
};

const DeviseContainer = props => <Devise {...props} />;

const mapStateToProps = state => {
  return {
    verified: getVerified(state),
    auth: getAuth(state) && !getSignInGuid(state),
    routes
  };
};

export default connect(mapStateToProps)(DeviseContainer);
