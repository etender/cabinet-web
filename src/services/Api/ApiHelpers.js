import camelize from "camelize";
import decamelize from "decamelize";
import decamelizeKeysDeep from "decamelize-keys-deep";
import { compact, normalize, isPresent } from "../../utils/CoreUtils";

export const callApi = (path, options, query) => {
  const config = {
    method: path.method,
    // Default value for new Browser is same-origin. Overwrite default value to support Спутник browser
    credentials: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest"
    }
  };
  let { url } = path;

  const params = options || query;
  const appliedParams = {};
  if (params) {
    if (path.method === "GET" || query) {
      url = Object.keys(params).reduce((result, key) => {
        const re = new RegExp(`/:${key}`);
        if (!re.test(result)) {
          return result;
        }

        appliedParams[key] = null;
        return result.replace(re, `/${params[key]}`);
      }, url);
    }

    if (path.method === "POST" || path.method === "PUT") {
      config.body = JSON.stringify(
        decamelizeKeysDeep(normalize(query || options))
      );
    }
  }

  if (path.method === "GET") {
    url = buildUrl(url, { ...query, ...appliedParams });
  }

  if (window.shared) {
    config.headers["X-Referer"] = document.referrer;
  }

  return fetch(url, config)
    .then(
      response => {
        if (response.ok) {
          if (path.binnary && response.headers.get("content-disposition")) {
            return fetchBinnary(response);
          }

          return [202, 204].includes(response.status) ? {} : response.json();
        }

        return fetchError(response);
      },
      error => {
        // console.log('Fetch error %O', error);

        return Promise.reject(error);
      }
    )
    .then(response =>
      path.binnary && response.filename ? response : camelize(response)
    );
};

const fetchBinnary = response => {
  const filename = response.headers
    .get("content-disposition")
    .match(/filename="([^"]+)"/)[1];

  return response
    .blob()
    .then(content => Promise.resolve({ filename, content }));
};

const parseError = (error, status) => {
  if ([401, 422].includes(status)) {
    const payload = camelize(JSON.parse(error));
    return { message: payload.errors || payload.error || payload, status };
  }

  return { message: error, status };
};

const fetchError = response => {
  return response
    .text()
    .then(error => Promise.reject(parseError(error, response.status)));
};

const buildUrl = (url, query) => {
  if (!query) {
    return url;
  }

  if (typeof query === "string") {
    return `${url}/${query}`;
  }

  if (typeof query === "object") {
    const queryString = serializeQuery(compact(query));
    return isPresent(queryString) ? `${url}?${queryString}` : url;
  }

  throw new Error(
    "[createUrl] Unsupported query type. Only string or object query types possible."
  );
};

const serializeQuery = (params, prefix) => {
  const query = Object.keys(params).map(key => {
    const value = params[key];

    if (params.constructor === Array) key = `${prefix}[]`;
    else if (params.constructor === Object)
      key = prefix ? `${prefix}[${key}]` : key;

    // For moment JS object
    if (value && value.format) {
      return `${decamelize(key)}=${encodeURIComponent(value.format())}`;
    }

    if (typeof value === "object") return serializeQuery(value, key);
    else return `${decamelize(key)}=${encodeURIComponent(value)}`;
  });

  return [].concat.apply([], query).join("&");
};

export const requestApiType = type => `${type}_REQUEST`;
export const successApiType = type => `${type}_SUCCESS`;
export const failureApiType = type => `${type}_FAILURE`;
