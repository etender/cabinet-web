import { handleActions } from "redux-actions";

import { startLoading, stopLoading } from "./ApiActions";

const initialState = {
  loading: false
};

export default handleActions(
  {
    [startLoading]: state => ({
      ...state,
      loading: true
    }),
    [stopLoading]: state => ({
      ...state,
      loading: false
    })
  },
  initialState
);
