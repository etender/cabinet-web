import { createAction } from "redux-actions";

export const requestApi = (type, payload, meta) => ({
  type: type + "_REQUEST",
  payload: payload,
  meta: meta
});

export const successApi = (type, response, meta) => ({
  type: type + "_SUCCESS",
  payload: response,
  meta
});

export const failureApi = (type, error, meta) => ({
  type: type + "_FAILURE",
  payload: error,
  error: true,
  meta
});

export const startFetching = createAction(
  "FETCHING_START",
  action => action.payload,
  action => action.meta
);

export const stopFetchingCreator = action => {
  return {
    ...createAction(
      "FETCHING_STOP",
      action => action.payload,
      action => action.meta
    )(action),
    error: action.error
  };
};
stopFetchingCreator.toString = () => "FETCHING_STOP";
export const stopFetching = stopFetchingCreator;

export const startLoading = createAction("LOADING_START");
export const stopLoading = createAction("LOADING_STOP");

export const openLink = createAction("LINK_OPEN");
export const showLink = createAction("LINK_SHOW");
