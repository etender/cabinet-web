import {
  all,
  take,
  put,
  call,
  race,
  takeEvery,
  fork,
  delay
} from "redux-saga/effects";
import { push } from "connected-react-router";

import sagaMiddleware from "../../utils/SagaUtils";

import { successApiType, failureApiType } from "./ApiHelpers";
import { signOutUser, verifyUser } from "../Devise/DeviseActions";
import { showRegistrationExpired } from "../../modules/RegistrationExpired/RegistrationExpiredActions";
import {
  openLink,
  showLink,
  startFetching,
  stopFetching,
  startLoading,
  stopLoading
} from "./ApiActions";
import { fetchSuggestions } from "../../common/Suggestions/SuggestionsActions";
import { prolongRegistration } from "../../modules/RegistrationExpired/RegistrationExpiredActions";

const ignoreActionTypes = [verifyUser, fetchSuggestions, prolongRegistration];

function monitorableAction(action) {
  return (
    action.type.includes("REQUEST") &&
    ignoreActionTypes.every(fragment => !action.type.includes(fragment))
  );
}

function identifyAction(action) {
  return action.type
    .split("_")
    .slice(0, -1)
    .join("_");
}

function getSuccessType(action) {
  return successApiType(identifyAction(action));
}

function getFailType(action) {
  return failureApiType(identifyAction(action));
}

function _openLink(options) {
  window.open(options.url || options, options.target || "_blank");
}

function* openLinkSaga(action) {
  yield call(_openLink, action.payload);
}

function* showLinkSaga(action) {
  yield put(push(action.payload));
}

let fetchingCounter = 0;

export function* monitor(action) {
  yield put(startFetching(action));

  if (fetchingCounter === 0) {
    yield put(startLoading());
  }
  fetchingCounter++;

  const { success, failure } = yield race({
    success: take(getSuccessType(action)),
    failure: take(getFailType(action))
  });

  yield put(stopFetching(success || failure));
  yield fork(stopLoadingSaga);

  if (!window.shared && failure && failure.error) {
    if (failure.payload.status === 401) {
      yield put(signOutUser());
    }

    if (failure.payload.status === 402) {
      yield put(showRegistrationExpired());
    }
  }
}

function* stopLoadingSaga() {
  yield delay(500);

  fetchingCounter--;
  if (fetchingCounter === 0) {
    yield put(stopLoading());
  }
}

function* apiSagas() {
  yield all([
    // fork(fetchingSaga),
    takeEvery(monitorableAction, monitor),
    takeEvery(openLink, openLinkSaga),
    takeEvery(showLink, showLinkSaga)
  ]);
}

sagaMiddleware.run(apiSagas);
