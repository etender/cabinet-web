import React, { Component, Suspense } from "react";
import PropTypes from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import PrivateRoute from "./PrivateRoute/PrivateRoute";

class Router extends Component {
  static propTypes = {
    routes: PropTypes.object.isRequired,
    fallback: PropTypes.node
  };

  render() {
    const { fallback } = this.props;

    return (
      <Suspense fallback={fallback || ""} ms={500}>
        <Switch>{this.routes()}</Switch>
      </Suspense>
    );
  }

  redirectToRoot(props, rootPath) {
    return (
      <Route
        {...props}
        render={() => <Redirect to={{ pathname: rootPath }} />}
      />
    );
  }

  routes() {
    const { rootPath, routes, auth, verified, active } = this.props;

    return Object.keys(routes)
      .filter(path => {
        const { secure } = routes[path];
        return !secure || verified;
      })
      .map(path => {
        const { container: Container, secure, expired } = routes[path];

        let props = { key: path, path };
        let fullProps = { ...props, component: Container, auth };
        if (expired) {
          return active ? (
            this.redirectToRoot(props, rootPath)
          ) : (
            <Route {...fullProps} />
          );
        }

        // redirect non secure path when user authenticated
        if (auth && (secure === false || (expired && active))) {
          return this.redirectToRoot(props, rootPath);
        }

        const RouteComponent = secure ? PrivateRoute : Route;
        return <RouteComponent {...fullProps} />;
      });
  }
}

export default Router;
