import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import PropTypes from "prop-types";

import { SIGN_IN_PATH } from "../../Devise/SignIn/SignInConstants";

class PrivateRoute extends Component {
  static defaultProps = {
    auth: false
  };

  static propTypes = {
    auth: PropTypes.bool.isRequired
  };

  render() {
    const { component: Component, auth, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props => {
          if (!auth) {
            const {
              location: { pathname, hash }
            } = props;
            localStorage.setItem("redirectTo", pathname + hash);
          }

          return auth ? (
            <Component {...rest} />
          ) : (
            <Redirect
              to={{
                pathname: SIGN_IN_PATH,
                state: { from: props.location }
              }}
            />
          );
        }}
      />
    );
  }
}

export default PrivateRoute;
