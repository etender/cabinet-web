export const getLocationPathname = state =>
  state.router.location.pathname || "";
export const getLocationSearch = state => state.router.location.search;
export const getLocationKey = state => state.router.location.key;
export const getLocationHash = state => state.router.location.hash;
export const isLocationPopOrReplace = state =>
  ["POP", "REPLACE"].includes(state.router.action);
