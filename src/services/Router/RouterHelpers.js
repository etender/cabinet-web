import { compact, isEmpty } from "../../utils/CoreUtils";

export const buildPath = (path, options) => {
  options = compact(options);
  if (isEmpty(options)) {
    return path;
  }

  return `${path}#${JSON.stringify(options)}`;
};

export const parseHash = hash => {
  try {
    return { ...JSON.parse(decodeURIComponent(hash.slice(1))) };
  } catch (exception) {
    return {};
  }
};
