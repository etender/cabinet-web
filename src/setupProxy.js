// Setup development proxy.
// https://github.com/facebook/create-react-app/blob/2a7fd5a1eac3f78162f28fa49ab69586d70d2e61/docusaurus/docs/proxying-api-requests-in-development.md
const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(
    proxy("/api", {
      target: "http://localhost:3001/",
      pathRewrite: { "^/api": "" }
    })
  );
};
