import ru from "./ru.json";

export const formats = {
  number: {
    rub: {
      style: "currency",
      currency: "RUB"
    },
    usd: {
      style: "currency",
      currency: "USD"
    }
  }
};

export default { ru };
