import moment from "moment";

export const isObject = value => !isNull(value) && value.constructor === Object;
export const isEmptyObject = value =>
  isObject(value) && Object.keys(value).length === 0;

export const isArray = value => !isNull(value) && value.constructor === Array;
export const isEmptyArray = value => isArray(value) && value.length === 0;

export const isString = value => !isNull(value) && value.constructor === String;
export const isEmptyString = value => isString(value) && value.trim() === "";

export const isNull = value => value === undefined || value === null;
export const isEmpty = value =>
  isNull(value) ||
  isEmptyArray(value) ||
  isEmptyString(value) ||
  isEmptyObject(value);
export const isPresent = value => !isEmpty(value);

export const isEqual = (value1, value2) => {
  if (isObject(value1) && isObject(value2)) {
    return JSON.stringify(value1) === JSON.stringify(value2);
  }

  return value1 === value2;
};

export const compact = object => {
  if (isEmpty(object)) {
    return object;
  }

  let result = {};
  let value = null;
  Object.keys(object).forEach(key => {
    value = object[key];
    if (isObject(value)) {
      value = compact(value);
    }

    if (isPresent(value)) {
      result[key] = value;
    }
  });

  return result;
};

export const normalize = object => {
  if (isEmpty(object)) {
    return object;
  }

  let result = {};
  let value = null;
  Object.keys(object).forEach(key => {
    value = object[key];
    if (moment.isMoment(value)) {
      value = value.toString();
    } else if (isObject(value)) {
      value = normalize(value);
    }

    result[key] = value;
  });

  return result;
};
