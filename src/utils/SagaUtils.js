import createSagaMiddleware from "redux-saga";

// export the middleware
export default createSagaMiddleware();
