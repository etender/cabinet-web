// translationRunner.js
const manageTranslations = require("react-intl-translations-manager").default;

// es2015 import
// import manageTranslations from 'react-intl-translations-manager';

manageTranslations({
  messagesDirectory: "src/translations/messages",
  translationsDirectory: "src/translations/locales/",
  languages: ["ru"] // any language you need
});
