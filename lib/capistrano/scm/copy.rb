require 'capistrano/scm/plugin'

class Capistrano::SCM::Copy < Capistrano::SCM::Plugin
  def define_tasks
    eval_rakefile File.expand_path("../tasks/copy.rake", __FILE__)
  end

  def register_hooks
    after 'deploy:new_release_path', 'copy:create_release'
    before 'deploy:check', 'copy:check'
    before 'deploy:set_current_revision', 'copy:set_current_revision'
  end
end
