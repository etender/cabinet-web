
namespace :copy do
  archive_name = "archive.tar.gz"

  desc "Deploy #{archive_name} to release_path"
  task :create_release do
    sh "cd #{fetch(:build_to)} && tar -czf ../#{archive_name} ."

    on roles(:web) do
      # Make sure the release directory exists
      execute :mkdir, "-p", release_path

      # Create a temporary file on the server
      tmp_file = capture("mktemp")

      # Upload the archive, extract it and finally remove the tmp_file
      upload!(archive_name, tmp_file)
      execute :tar, "-xzf", tmp_file, "-C", release_path
      execute :rm, tmp_file
    end
  end

  task :clean do |t|
    # Delete the local archive
    File.delete archive_name if File.exists? archive_name
  end

  after 'deploy:finished', 'copy:clean'

  task :check

  desc 'Read revision from REVISION file if exists'
  task :set_current_revision do
    set :current_revision, `git rev-list --max-count=1 #{fetch(:branch)}`.strip
  end
end
