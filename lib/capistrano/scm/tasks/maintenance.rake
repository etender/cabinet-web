namespace :maintenance do
  desc "Turn on maintenance mode"
  task :enable do
    on fetch(:maintenance_roles) do
      execute "touch #{fetch(:maintenance_path)}"
    end
  end

  desc "Turn off maintenance mode"
  task :disable do
    on fetch(:maintenance_roles) do
      execute "rm -f #{fetch(:maintenance_path)}"
    end
  end
end

namespace :load do
  task :defaults do
    set_if_empty :maintenance_roles, -> { roles(:web) }
    set_if_empty :maintenance_path, -> { "#{shared_path}/maintenance.html" }
  end
end
